<?php
namespace App\Helpers;

use App\Models\TranslateTool;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

use LaravelLocalization;

class TranslateKey {

    public static function key(string $key)
    {
        //$locale = strtolower(Session::get('locale'));
        $locale = LaravelLocalization::getCurrentLocale() ?: 'en';

        if(Cache::has('keys')){
            $keys = Cache::get('keys');
        } else {
            $keys = TranslateTool::getAll($locale)->pluck('text_'.$locale,'label')->toArray();
            Cache::put('keys',$keys, Carbon::now()->addSecond(7)); //7 seconds is enough to load one page
        }

        if(array_key_exists($key,$keys)){
            return $keys[$key];
        } else {
            return 'error';
        }
    }
}
