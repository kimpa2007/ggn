<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SaveAccountRequest;

use App\Jobs\UpdateUserInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Session;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('admin.account')->with(compact('user'));
    }

    public function save(SaveAccountRequest $request)
    {
    	$user = $request->get('id') ? $this->repository->find($request->get('id')) : new User();

		//check for unique email
		if(!$request->get('id') || $request->get('email') != $user->email){

			if ($this->repository->findWhere(['email'=>$request->get('email')])->count() > 0) {
				Session::flash('notify_error', "This email has been taken");

		        return $request->get("id")
		            ? redirect()->action('Admin\Expert\AccountController@show', ['id' => $request->get("id")])->withInput()
		            : redirect()->action('Admin\Expert\AccountController@create')->withInput();
			}
		}

    	$user->firstname = $request->get("firstname");
		$user->lastname = $request->get("lastname");
		$user->email = $request->get("email");
		$user->password = trim($request->get('password')) ? trim(Hash::make($request->get('password'))) : '';
		$user->image = $request->get("image");

		if($user->save()) {

            Session::flash('notify_success', "Utilisateur enregistré avec succès");
            return redirect()->action('Admin\Expert\AccountController@show', ['id' => $user->id]);
        }

        Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");

        return $request->get("id")
            ? redirect()->action('Admin\Expert\AccountController@show', ['id' => $request->get("id")])->withInput()
            : redirect()->action('Admin\Expert\AccountController@create')->withInput();
    }
}
