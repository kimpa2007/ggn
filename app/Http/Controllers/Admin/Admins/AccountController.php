<?php

namespace App\Http\Controllers\Admin\Admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveAdminRequest;

use App\Models\Content;
use App\Models\Country;
use App\Models\Role;
use App\Models\User;
use App\Models\Zone;
use App\Models\UserZone;
use DB;

use Illuminate\Support\Facades\Response;
use App\Repositories\UserRepository;

use Datatables;
use Auth;
use Hash;
use Session;


/**
 * Tests :
 * - email exists
 * - no password
 * - password not confirmed
 * -
 */
class AccountController extends Controller
{

	public function __construct(UserRepository $repository)
	{
	    $this->repository = $repository;
		$this->middleware('auth');
	}

    public function index(Request $request)
    {
        return view('admin.admins.index');
    }

	/*
    *   Return datatables JSON data
    */
    public function getData(Request $request)
    {
    	$experts = $this->repository->getDatatableData()->whereHas('roles', function($query){
    		$query->where('name','admin');
    	})->get();

		return Datatables::of($experts)
            ->filterColumn('full_name', function($query, $keyword) {
                $sql = "CONCAT(users.firstname,' ',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
			->filterColumn('zone', function($query, $keyword) {
                $sql = "zones.name LIKE ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('zone', function ($item) {

				if($item->expertZones->count() > 0)
					return $item->expertZones->first()->name;
				else
					return null;

            	//return $item->expertZones->first()->name;
            })
            ->addColumn('date', function ($item) {
                return $item->created_at->format('d/m/Y');
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    action('Admin\Admins\AccountController@show', ['id' => $item->id])).' &nbsp; '.
                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
            })
        ->make(true);
    }

	public function show($id, Request $request)
    {
    	$user = $this->repository->find($id);
    	//dd($user);
        return view('admin.admins.edit', [
            'user' => $user
        ]);
    }

    public function create( Request $request)
    {
        return view('admin.admins.edit');
    }

	public function save(SaveAdminRequest $request)
    {
    	$user = $request->get('id') ? $this->repository->find($request->get('id')) : new User();

		//check for unique email
		if(!$request->get('id') || $request->get('email') != $user->email){

			if ($this->repository->findWhere(['email'=>$request->get('email')])->count() > 0) {
				Session::flash('notify_error', "This email has been taken");

		        return $request->get("id")
		            ? redirect()->action('Admin\Admins\AccountController@show', ['id' => $request->get("id")])->withInput()
		            : redirect()->action('Admin\Admins\AccountController@create')->withInput();
			}
		}

    $user->firstname = $request->get("firstname");
		$user->lastname = $request->get("lastname");
		$user->email = $request->get("email");
		if($request->has('password') && $request->get('password') != ''){
			$user->password = trim($request->get('password')) ? trim(Hash::make($request->get('password'))) : '';
		}

		$user->image = $request->get("image");

		$zone = Zone::find($request->get("zone_id"));
		$adminRole = Role::where('name','admin')->first();

		if($user->save()) {

			if(!$request->get("id")){
				//if no exists add role expert
				$user->roles()->save($adminRole);
			}
			else {
				//remove
				UserZone::where('user_id',$user->id)->delete();
			}

			//save zone
			//$user->expertZones()->save($zone,['type' => UserZone::TYPE_EXPERT]);

            Session::flash('notify_success', "Utilisateur enregistré avec succès");
            return redirect()->action('Admin\Admins\AccountController@show', ['id' => $user->id]);
        }

        Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");

        return $request->get("id")
            ? redirect()->action('Admin\Admins\AccountController@show', ['id' => $request->get("id")])->withInput()
            : redirect()->action('Admin\Admins\AccountController@create')->withInput();
    }



	public function delete(Request $request)
	{
		$user = $this->repository->find($request->get('id'));

		if($user){
			if($user->delete()){
				return Response::json([
					'error' => false,
					'code' => 200
				],200);
			}
		}

		return Response::json([
			'error' => true,
			'code' => 400
		],400);
	}

}
