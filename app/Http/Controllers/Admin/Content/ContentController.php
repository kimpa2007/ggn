<?php

namespace App\Http\Controllers\Admin\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Http\Requests\Expert\SaveContentRequest;

use App\Models\Content;
use App\Models\LatestnewsOrder;
use App\Models\Country;
use App\Models\Zone;
use App\Services\FileService;

use App\Jobs\Content\CreateContent;

use App\Repositories\CountryRepository;
use App\Repositories\ZoneRepository;
use App\Repositories\UserRepository;

use Datatables;
use Auth;
use Session;

class ContentController extends Controller
{
    public function __construct(FileService $fileService, CountryRepository $countryRepository, ZoneRepository $zoneRepository, UserRepository $repositoryUser)
    {
        $this->fileService = $fileService;
        $this->countries = $countryRepository;
        $this->zones = $zoneRepository;
        $this->user = $repositoryUser;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('admin.contents.index');
    }

    public function create(Request $request)
    {
        $experts = $this->user->getDatatableData()->whereHas('roles', function ($query) {
            $query->where('name', 'expert');
        })->get();

        return view('admin.contents.form', [
            "countriesTree" => $this->countries->getPublished()->get(),
            "experts" => $experts
        ]);
    }

    public function show($id, Request $request)
    {
        $content = Content::find($id);

        $experts = $this->user->getDatatableData()->whereHas('roles', function ($query) {
            $query->where('name', 'expert');
        })->get();

        return view('admin.contents.form', [
            "experts" => $experts,
            "countriesTree" => $this->countries->getPublished()->get(),
            "content" => $content
        ]);
    }


    public function save(SaveContentRequest $request)
    {
        try {
            $content = $this->dispatchNow(CreateContent::fromRequest($request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
            return $request->get("content_id")
             ? redirect()->route('admin.content.show',  $request->get("content_id"))->withInput()
             : redirect()->route('admin.content.create')->withInput();
        }
        return redirect()->route('admin.content.show', $content->id);
    }

    public function getData(Request $request)
    {
        //$contents = Content::with('countrie');


        $contents = Content::leftJoin('content_translations', 'contents.id', '=', 'content_translations.content_id')
            ->leftJoin('country_translations', 'contents.country_id', '=', 'country_translations.country_id')
            ->leftJoin('contents_themes', 'contents_themes.content_id', '=', 'contents.id')
            ->leftJoin('themes', 'contents_themes.theme_id', '=', 'themes.id')
            ->leftJoin('theme_translations', 'themes.id', '=', 'theme_translations.theme_id')
            ->select(
                'contents.*',
                'content_translations.title',
                'country_translations.name as country_name'
            )
            ->where('content_translations.locale', 'fr')
            ->groupBy('contents.id');


        return Datatables::eloquent($contents)
            ->filterColumn('country_name', function ($query, $keyword) {
                $query->whereRaw("country_translations.name LIKE ?", ["%$keyword%"]);
            })
            ->filterColumn('title', function ($query, $keyword) {
                $query->whereRaw("content_translations.title LIKE ?", ["%$keyword%"]);
            })
            ->filterColumn('themes', function ($query, $keyword) {
                $query->whereRaw("themes.id LIKE ?", ["%$keyword%"]);
            })
            ->editColumn('themes', function ($item) {
                return $item->themes->pluck('name')->toArray();
            })
            ->editColumn('status', function ($item) {
                $status = Content::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->addColumn('latest_news', function ($item) {
              return  isset($item->latest_news) && ($item->latest_news ==  1 )? 'Latest News' : null;
            })
            ->filterColumn('latest_news', function ($query, $keyword) {
                $query->whereRaw("latest_news LIKE ?", ["%$keyword%"]);
            })
            ->addColumn('movie_on_top', function ($item) {
              return  isset($item->movie_on_top) && ($item->movie_on_top ==  1 )? 'Free home' : null;
            })
            ->filterColumn('movie_on_top', function ($query, $keyword) {
               $query->whereRaw('movie_on_top LIKE ?', ["%{$keyword}%"]);
            })
            ->addColumn('action', function ($item) {
                return '<a href="' . action('Admin\Content\ContentController@show', ['id' => $item->id]) . '" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>'.' &nbsp; '.
                '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
            })
            ->make(true);
    }

        public function indexLatestnews(Request $request)
        {
            $contents = Content::where('latest_news','=', '1')
            ->join('latestnews_order','latestnews_order.content_id','=','contents.id')
            ->orderBy('latestnews_order.order_news')
            ->get()
            ->toArray();

            return view('admin.contents.latestnews', [
                "contents" => $contents
            ]);

        }

      public function updateOrderLatestnews(Request $request)
      {
        if($request->exists('order')){
    			$order = $request->get('order');
                LatestnewsOrder::query()->truncate();
                $newOrder = 1;

                foreach ($order as $value) {
                  $insertOrder = new LatestnewsOrder();
                  $insertOrder->order_news = $newOrder;
                  $insertOrder->content_id = $value;
                  $insertOrder->save();
                  $newOrder++;
                }

    			return Response::json([
    	            'error' => false,
    	            'code'  => 200
    	        ], 200);
    		}
      }


    public function changeStatus(Request $request)
    {
        $itemId = $request->get('itemId');
        $status = $request->get('status');

        $item = Content::find($itemId);

        if ($item->status != $status) {
            $item->status = $status;

            //changed successfully
            if ($item->save()) {
                return Response::json([
                    'error' => false,
                    'code'  => 200
                ], 200);
            } else {
                return Response::json([
                    'error' => true,
                    'code' => 400
                ], 400);
            }
        }

        //nothing to change
        return Response::json([
            'error' => false,
            'code'  => 204
        ], 200);
    }


    public function updateOrder(Request $request)
    {
        if ($request->exists('order')) {
            $order = $request->get('order');

            foreach ($order as $row) {
                Content::where('id', $row["id"])->update(['order' => $row["newOrder"]]);
            }

            return Response::json([
                'error' => false,
                'code'  => 200
            ], 200);
        }

        return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);
    }

    public function delete(Request $request)
    {
        $item = Content::find($request->get('id'));

        if ($item) {
            if ($item->delete()) {
                return Response::json([
                    'error' => false,
                    'code' => 200
                ], 200);
            }
        }

        return Response::json([
            'error' => true,
            'code' => 400
        ], 400);
    }
}
