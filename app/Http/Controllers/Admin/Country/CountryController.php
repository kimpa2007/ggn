<?php

namespace App\Http\Controllers\Admin\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveCountryRequest;

use App\Models\Country;
use App\Models\CountryZone;
use App\Models\Content;
use App\Jobs\Country\CreateCountry;

use Illuminate\Support\Facades\Response;

use Datatables;
use Auth;
use Hash;
use Session;

use App\Services\ImageService;
use App\Repositories\CountryRepository;


class CountryController extends Controller
{

	public function __construct(CountryRepository $repository)
	{
	    $this->repository = $repository;
			$this->middleware('auth');
	}

    public function index(Request $request)
    {
        return view('admin.countries.index');
    }

		/*
    *   Return datatables JSON data
    */
    public function getData(Request $request)
    {
			$countries = $this->repository->getDatatableData()->get();

			return Datatables::of($countries)
	            ->addColumn('action', function ($item) {
	                return sprintf(
	                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
	                    action('Admin\Country\CountryController@show', ['id' => $item->id])).' &nbsp; '.
	                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
	            })
				->rawColumns(['order', 'action'])
        ->make(true);
    }

	public function show($id, Request $request)
    {
    	 $countrie = $this->repository->find($id);
       return view('admin.countries.edit', [
	            'countrie' => $countrie
	        ]);
    }

    public function create( Request $request)
    {
        return view('admin.countries.edit');
    }

	public function save(SaveCountryRequest $request)
    {

    	try {
            $country = $this->dispatchNow(CreateCountry::fromRequest($request));
           	Session::flash('notify_success', "Pays enregistré avec succès");
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
            return  $request->get("id")? redirect()->action('Admin\Country\CountryController@show', ['id' => $request->get("id")])->withInput() : redirect()->action('Admin\Country\CountryController@create')->withInput();
        }
        return redirect()->action('Admin\Country\CountryController@show', ['id' => $country->id]);

    }

	public function updateOrder(Request $request)
	{

		if($request->exists('order')){
			$order = $request->get('order');

			foreach($order as $row){
				Country::where('id', $row["id"])->update(['order' => $row["newOrder"]]);
		   	}

			return Response::json([
	            'error' => false,
	            'code'  => 200
	        ], 200);
		}

		return Response::json([
	            'error' => true,
	            'code'  => 400
	        ], 400);

	}

	public function delete(Request $request)
	{

		$countrie = $this->repository->find($request->get('id'));

		if($countrie){
			if($countrie->delete()){
				return Response::json([
					'error' => false,
					'code' => 200
				],200);
			}
		}

		return Response::json([
			'error' => true,
			'code' => 400
		],400);
	}

	public function contents($id, Request $request)
	{
		$country = $this->repository->find($id);

		return view('admin.countries.contents', [
			'countrie' => $country
		]);
	}


	public function getContentsData($id, Request $request)
	{
		$contents = Content::where('country_id', $id)->get();
        $contents->load('countrie');

        return Datatables::of($contents)
			->editColumn('status', function ($item) {
					$status = Content::getStatus();
					return isset($status[$item->status]) ? $status[$item->status] : null;
			})
			->addColumn('action', function ($item) {
                return '<a href="' . action('Admin\Content\ContentController@show', ['id' => $item->id]) . '" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>'.' &nbsp; '.
                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
				;
            })
            ->make(true);
	}

}
