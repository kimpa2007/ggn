<?php

namespace App\Http\Controllers\Admin\CustomPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Str;
use App\Jobs\CustomPage\CreateCustomPage;

use App\Models\CustomPage;

use App\Http\Requests\Admin\SaveCustomPageRequest;

use Datatables;
use Session;

class CustomPagesController extends Controller
{
    /**
     * Display a listing of the Custom Pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.custompage.index');
    }


     /**
     * Datatables for Custom Pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        return Datatables::of(CustomPage::all())
            ->addColumn('actions', function ($item) {
                return sprintf(
                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    route('admin.custompage.show', ['id' => $item->id])).' &nbsp; ';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }



    /**
     * Show the form for creating a new Custom Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.custompage.form');
    }


    /**
     * Store a newly created Custom Page in storage.
     *
     * @param  App\Http\Requests\Admin\SaveCustomPageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveCustomPageRequest $request)
    {
        try {
            $custom = $this->dispatchNow(CreateCustomPage::fromRequest($request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
            return  $redirect()->route('admin.custompage.show', $custom->id)->withInput();
        }
        return  redirect()->route('admin.custompage.show', $custom->id);
    }



    /**
     * Show the form for editing the specified Custom Page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.custompage.form', [
            'page' => CustomPage::findOrFail($id)
        ]);
    }

    /**
     * Update the specified Custom Page in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveCustomPageRequest $request, $id)
    {
        try {
            $custom = $this->dispatchNow(CreateCustomPage::fromRequest($request, $id));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
            return redirect()->route('admin.custompage.show', $id)->withInput();
        }

        return redirect()->route('admin.custompage.show', $id);
    }

    /**
     * Remove the specified Custom Page from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = CustomPage::findOrFail($id);
        $message = sprintf('La page %s a été correctement supprimée.', $page->title_es);

        CustomPage::destroy($id);

        return redirect()
            ->route('admincustom.index')
            ->with('flash_message', $message);
    }
}
