<?php

namespace App\Http\Controllers\Admin\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveExpertRequest;

use App\Models\Content;
use App\Models\Country;
use App\Models\Role;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\Zone;
use App\Models\UserZone;
use DB;

use Illuminate\Support\Facades\Response;
use App\Repositories\UserRepository;

use Datatables;
use Auth;
use Hash;
use Session;

/**
 * Tests :
 * - email exists
 * - no password
 * - password not confirmed
 * -
 */
class AccountController extends Controller
{
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('admin.experts.index');
    }

    /*
    *   Return datatables JSON data
    */
    public function getData(Request $request)
    {
        $experts = $this->repository->getDatatableData()->whereHas('roles', function ($query) {
            $query->where('name', 'expert');
        })->get();
        return Datatables::of($experts)
            ->addColumn('id', function ($item) {
                  return isset($item->id) ? $item->id : 0;
            })
            ->filterColumn('full_name', function ($query, $keyword) {
                $sql = "CONCAT(users.firstname,' ',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('zone', function ($query, $keyword) {
                            $sql = "zones.name LIKE ? ";
                            $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('zone', function ($item) {
                if ($item->expertZones->count() > 0) {
                    return $item->expertZones->first()->name;
                } else {
                    return null;
                }
                //return $item->expertZones->first()->name;
            })
            ->addColumn('date', function ($item) {
                return $item->created_at->format('d/m/Y');
            })
            ->addColumn('order', function ($item) {
                return isset($item->profile->order) ? $item->profile->order : 0;
            })
            ->addColumn('action', function ($item) {
                $buttons = "";
                $buttons .= sprintf(
                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    action('Admin\Expert\AccountController@show', ['id' => $item->id])).' &nbsp; '.
                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';

                if (Auth::user()->hasRole('super-admin')) {
                    $buttons.= sprintf(
                      '<a href="%s" class="btn btn-table"><i class="fa fa-sign-in"></i> &nbsp; Login as</a>',
                      route('admin.users.login', ['id' => $item->id])
                  );
                }

                return $buttons;
            })
                ->rawColumns(['order', 'action'])
        ->make(true);
    }

    public function updateOrder(Request $request)
    {
        if ($request->exists('order')) {
            $order = $request->get('order');

            foreach ($order as $row) {
                $profile = UserProfile::where('user_id', $row["id"])->first();
                if ($profile) {
                    $profile->order = $row["newOrder"];
                    $profile->save();
                }
            }

            return Response::json([
                    'error' => false,
                    'code'  => 200
                ], 200);
        }

        return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);
    }


    public function show($id, Request $request)
    {
        return view('admin.experts.edit', [
            'user' => $this->repository->find($id)
        ]);
    }

    public function create(Request $request)
    {
        return view('admin.experts.edit');
    }

    public function save(SaveExpertRequest $request)
    {
        $user = $request->get('id') ? $this->repository->find($request->get('id')) : new User();

        //check for unique email
        if (!$request->get('id') || $request->get('email') != $user->email) {
            if ($this->repository->findWhere(['email'=>$request->get('email')])->count() > 0) {
                Session::flash('notify_error', "This email has been taken");

                return $request->get("id")
                    ? redirect()->action('Admin\Expert\AccountController@show', ['id' => $request->get("id")])->withInput()
                    : redirect()->action('Admin\Expert\AccountController@create')->withInput();
            }
        }

        $user->firstname = $request->get("firstname");
        $user->lastname = $request->get("lastname");
        $user->email = $request->get("email");
		$user->image = $request->get("image");

        if ($request->has('password') && $request->get('password') != '') {
            $user->password = trim($request->get('password')) ? trim(Hash::make($request->get('password'))) : '';
        }

        $zone = Zone::find($request->get("zone_id"));
        $expertRole = Role::where('name', 'expert')->first();

        if ($user->save()) {

            if (!$request->get("id")) {
                //if no exists add role expert
                $user->roles()->save($expertRole);

				UserProfile::create([
					'user_id' => $user->id,
					'content' => '',
					'image' => '',
					'order' => UserProfile::max('order') + 1
				]);

            } else {
                //remove
                UserZone::where('user_id', $user->id)->delete();
            }

            //save zone
            $user->expertZones()->save($zone, [
				'type' => UserZone::TYPE_EXPERT
			]);

            Session::flash('notify_success', "Utilisateur enregistré avec succès");
            return redirect()->action('Admin\Expert\AccountController@show', ['id' => $user->id]);
        }

        Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");

        return $request->get("id")
            ? redirect()->action('Admin\Expert\AccountController@show', ['id' => $request->get("id")])->withInput()
            : redirect()->action('Admin\Expert\AccountController@create')->withInput();
    }



    public function delete(Request $request)
    {
        $user = $this->repository->find($request->get('id'));

        if ($user) {
            if ($user->delete()) {
                return Response::json([
                    'error' => false,
                    'code' => 200
                ], 200);
            }
        }

        return Response::json([
            'error' => true,
            'code' => 400
        ], 400);
    }
}
