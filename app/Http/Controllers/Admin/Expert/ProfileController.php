<?php

namespace App\Http\Controllers\Admin\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Expert\SaveProfileRequest;

use App\Repositories\UserRepository;

use App\Models\UserProfile;
use App\Models\Language;

use App\Jobs\Expert\UpdateProfile;

use Datatables;
use Auth;
use Session;

use App\Services\ImageService;

class ProfileController extends Controller
{

	public function __construct(ImageService $imageService, UserRepository $repository)
	{
		  $this->imageService = $imageService;
			$this->repository = $repository;
			$this->middleware('auth');
	}

    public function index($id,Request $request)
    {
        $user = $this->repository->find($id);

        return view('admin.experts.profile', [
            'user' => $user
        ]);
    }


    public function save(SaveProfileRequest $request)
    {
        try {
            $user = $this->dispatchNow(UpdateProfile::fromRequest($request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
        }
        return redirect()->action('Admin\Expert\ProfileController@index', ['id' => $user->id])
            ->withInput();
    }
}
