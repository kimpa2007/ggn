<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Content;
use App\Models\Zone;
use App\Models\Partner;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();
        $experts = User::whereHas('roles', function($query){
        		$query->where('name','expert');
        	});
        $partners = Partner::all();
        $contents = Content::orderBy('created_at','DESC')->limit(10);
        $zones = Zone::where('status',Zone::STATUS_PUBLISHED);

        return view('admin.home',compact('users','contents',
          'zones','experts','partners'));
    }
}
