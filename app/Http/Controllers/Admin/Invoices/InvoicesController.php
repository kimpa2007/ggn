<?php

namespace App\Http\Controllers\Admin\Invoices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Jobs\Partner\DeleteThemeRequested;
use App\Jobs\Partner\AcceptThemeRequested;

use Session;
use Datatables;
use Storage;
use Carbon\Carbon;
use App\Models\PartnerOrder;


class InvoicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.invoices.index');
    }

    public function getData(Request $request)
    {
        $items = PartnerOrder::all();

        return Datatables::collection($items)

            ->editColumn('partner', function ($order) {
                return $order->partner->name;
            })
            ->editColumn('date', function ($order) {
                return Carbon::parse($order->created_at)->format('d/m/Y');
            })
            ->editColumn('status', function ($order) {
                return $order->getStatusName();
            })
            ->editColumn('amount', function ($order) {
                return $order->amount.' €';
            })
            ->editColumn('type', function ($order) {
                return $order->getTypeName();
            })
            ->addColumn('action', function ($order) {

                $actions = "";

                if(isset($order->invoice)){
                  $actions .= '<a href="'.Storage::url($order->invoice).'" class="" target="_blank"><i class="fa fa-file-o"></i> &nbsp; Facture</a> |';
                }

                if($order->type == PartnerOrder::TYPE_CREDIT){
                  $actions .= sprintf(' <a href="%s" class="" /><i class="fa fa-eye"></i> &nbsp; Commande</a>',
                      route('admin.orders.credit.show', $order));
                }
                else if($order->type == PartnerOrder::TYPE_PLAN){
                  $actions .= sprintf(' <a href="%s" class="" /><i class="fa fa-eye"></i> &nbsp; Commande</a>',
                      route('admin.orders.plan.show', $order));
                }

                return $actions;
            })
            ->rawColumns(['invoice', 'action'])
            ->make(true);
    }
}
