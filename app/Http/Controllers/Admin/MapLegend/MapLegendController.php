<?php

namespace App\Http\Controllers\Admin\MapLegend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Http\Requests\Admin\MapLegend\CreateMapLegendRequest;
use App\Jobs\MapLegend\CreateMapLegend;

use App\Http\Requests\Admin\MapLegend\UpdateMapLegendRequest;
use App\Jobs\MapLegend\UpdateMapLegend;

use App\Http\Requests\Admin\MapLegend\DeleteMapLegendRequest;
use App\Jobs\MapLegend\DeleteMapLegend;

use App\Models\MapLegend;
use App\Services\FileService;

use Datatables;
use Auth;
use Session;

class MapLegendController extends Controller
{
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('admin.maplegends.index');
    }

    public function create(Request $request)
    {
        return view('admin.maplegends.form');
    }

    public function store(CreateMapLegendRequest $request)
    {
        try {
            $mapLegend = $this->dispatchNow(CreateMapLegend::fromRequest($request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');

            return redirect(action('Admin\MapLegend\MapLegendController@show', $mapLegend));
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
        }

        return redirect(action('Admin\MapLegend\MapLegendController@create'))->withInput();
    }

    public function update(MapLegend $mapLegend, UpdateMapLegendRequest $request)
    {
        try {
            $this->dispatchNow(UpdateMapLegend::fromRequest($mapLegend, $request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');

            return redirect(action('Admin\MapLegend\MapLegendController@show', $mapLegend));
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
        }

        return redirect(action('Admin\MapLegend\MapLegendController@create'))->withInput();
    }

    public function show(MapLegend $mapLegend, Request $request)
    {
        return view('admin.maplegends.form', [
            'mapLegend' => $mapLegend
        ]);
    }

    public function getData()
    {
        return Datatables::of(MapLegend::all())
            ->addColumn('icon', function ($item) {
              return '<img src="/storage/' . $item->icon . '" />';
            })
            ->addColumn('name', function ($item) {
              return '<a href="' . action('Admin\MapLegend\MapLegendController@show', $item) . '">'.$item->name.'</a>';
            })
            ->addColumn('action', function ($item) {
                return '
                    <a href="' . action('Admin\MapLegend\MapLegendController@show', $item) . '" class="btn btn-table"><i class="fa fa-eye"></i> Voir</a>
                    <a href="' . action('Admin\MapLegend\MapLegendController@delete', $item) . '" class="btn btn-table toogle-delete" data-id="'.$item->id.'" data-url="'.action('Admin\MapLegend\MapLegendController@delete', $item).'"><i class="fa fa-trash"></i> Supprimer</a>
                ';
            })
            ->rawColumns(['icon', 'name', 'action'])
        ->make(true);
    }


    public function delete(MapLegend $mapLegend, DeleteMapLegendRequest $request)
    {
        $error = false;
        $message = null;

        try {
            if (!$this->dispatchNow(DeleteMapLegend::fromRequest($mapLegend, $request))) {
                $error = true;
            }
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
        }

        return Response::json([
            'error' => $error,
            'message' => $message,
            'code' => $error ? 400 : 200
        ], $error ? 400 : 200);
    }

}
