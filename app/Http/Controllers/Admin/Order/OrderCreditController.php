<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use Session;
use Datatables;
use Carbon\Carbon;
use App\Models\PartnerOrder;
use App\Jobs\DeletePartnerOrder;
use App\Jobs\UpdatePartnerOrder;
use App\Http\Requests\PartnerOrderRequest;
use App\Repositories\PartnerOrderRepository;
use App\Services\FileService;

use App\Jobs\Email\SendTemplatedEmail;

use App\Models\User;
class OrderCreditController extends Controller
{

    public function __construct(PartnerOrderRepository $orders,FileService $fileService)
    {
        $this->fileService = $fileService;
        $this->orders = $orders;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.orders.credits.index');
    }

    public function show(PartnerOrder $order)
    {
        return view('admin.orders.credits.edit', [
            'order' => $order
        ]);
    }

    public function save(PartnerOrder $order, PartnerOrderRequest $request)
    {

        $uploadedFilePath = $request->file('file') ? $this->fileService->upload($request->file('file')) : null;

        // Convert date format FR to US before update
        $request["start_at"] = $request["start_at"] ? Carbon::createFromFormat('d/m/Y', $request["start_at"])->format('Y-m-d') : null;
        $request["end_at"] = $request["end_at"] ? Carbon::createFromFormat('d/m/Y', $request["end_at"])->format('Y-m-d') : null;
        $request["paid_at"] = $request["paid_at"] ? Carbon::createFromFormat('d/m/Y', $request["paid_at"])->format('Y-m-d') : null;


        // Set attachment
        if($uploadedFilePath) {
            $filePath = "/public/invoices/" . basename($uploadedFilePath);
            if($this->fileService->move($uploadedFilePath, $filePath)) {

                if($order->invoice) {
                    $order->deleteAttachment();
                }

                $request["invoice"] = "invoices/" . basename($uploadedFilePath);
            }
        }


        $this->dispatchNow(UpdatePartnerOrder::fromRequest($order, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.orders.credit.show', $order);
    }


    public function delete(PartnerOrder $order, Request $request)
	{
        try {
            $this->dispatchNow(new DeletePartnerOrder($order));
            Session::flash('notify_success', "Suppression effectuée avec succès");
        } catch (Exception $ex) {
            Session::flash('notify_error', "Une erreur s'est produite lors de la suppression");
        }

        return redirect()->route('admin.orders.credit.index');
	}

    public function getData(Request $request)
    {
        $orders = $this->orders
            ->with(["partner"])
            ->all()
            ->where('type', PartnerOrder::TYPE_CREDIT);

        return Datatables::collection($orders)
            ->editColumn('paid_at', function ($order) {
                return Carbon::parse($order->paid_at)->format('d/m/Y');
            })
            ->editColumn('created_at', function ($order) {
                return Carbon::parse($order->created_at)->format('d/m/Y');
            })
            ->editColumn('status', function ($order) {
                return $order->getStatusName();
            })
            ->addColumn('action', function ($order) {
                return sprintf(
                    '<a href="%s">Voir</a> | <a href="%s" class="toggle-delete" />Supprimer</a>',
                    route('admin.orders.credit.show', $order),
                    route('admin.orders.credit.delete', $order)
                );
            })
        ->make(true);
    }
}
