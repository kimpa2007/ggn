<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Jobs\Partner\DeleteThemeRequested;
use App\Jobs\Partner\AcceptThemeRequested;

use Session;
use Datatables;
use Carbon\Carbon;
use App\Models\ZoneRequested;


class OrderThemeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.orders.zones.index');
    }

    public function getData(Request $request)
    {
        $items = ZoneRequested::all();

        return Datatables::collection($items)

            ->editColumn('partner', function ($order) {
                return $order->partner->name;
            })
            ->editColumn('zone', function ($order) {
                return $order->zone->name;
            })
            ->editColumn('requested_at', function ($order) {
                return Carbon::parse($order->requested_at)->format('d/m/Y');
            })
            ->editColumn('status', function ($order) {
                return $order->getStatusName();
            })
            ->addColumn('action', function ($order) {

                $actions = "";

                if($order->status == ZoneRequested::STATUS_PENDING){
                  $actions .= sprintf('<a href="%s" class="btn btn-table"><i class="fa fa-check"></i> &nbsp; Valider</a>',
                    route('admin.orders.zones.accept', $order));
                }

                $actions .= sprintf(' <a href="%s" class="btn btn-table toggle-delete" /><i class="fa fa-trash"></i> &nbsp; Supprimer</a>',
                    route('admin.orders.zones.delete', $order));

                return $actions;
            })
            ->make(true);
    }

    public function accept(ZoneRequested $order)
    {
        try {
            $this->dispatchNow(new AcceptThemeRequested($order));
            Session::flash('notify_success', "Zone validated avec succès");
        } catch (Exception $ex) {
            Session::flash('notify_error', "Une erreur s'est produite");
        }

        return redirect()->route('admin.orders.zones');
    }

    public function delete(ZoneRequested $order, Request $request)
  	{
          try {
              $this->dispatchNow(new DeleteThemeRequested($order));
              Session::flash('notify_success', "Suppression effectuée avec succès");
          } catch (Exception $ex) {
              Session::flash('notify_error', "Une erreur s'est produite lors de la suppression");
          }

          return redirect()->route('admin.orders.zones');
  	}
}
