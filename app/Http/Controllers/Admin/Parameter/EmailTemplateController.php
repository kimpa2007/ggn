<?php

namespace App\Http\Controllers\Admin\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Repositories\EmailTemplateRepository;
use App\Models\EmailTemplate;
use App\Http\Requests\EmailTemplateRequest;

use App\Jobs\CreateEmailTemplate;
use App\Jobs\UpdateEmailTemplate;

use Datatables;
use Session;

class EmailTemplateController extends Controller
{

    public function __construct(EmailTemplateRepository $emails)
    {
        $this->emails = $emails;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('admin.parameters.emails.index');
    }

    public function show(EmailTemplate $template)
    {
        return view('admin.parameters.emails.form', [
            'template' => $template
        ]);
    }

    public function newEmailTemplate()
    {
        return view('admin.parameters.emails.form');
    }

    public function create(EmailTemplateRequest $request)
    {
        $template = $this->dispatchNow(CreateEmailTemplate::fromRequest($request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.parameters.emails.show', $template);
    }


    public function update(EmailTemplate $template, EmailTemplateRequest $request)
    {
        $this->dispatchNow(UpdateEmailTemplate::fromRequest($template, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.parameters.emails.show', $template);
    }

    public function getData(Request $request)
    {
		return Datatables::of($this->emails->all())
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    route('admin.parameters.emails.show', $item)
                );
            })
			->rawColumns(['action'])
        ->make(true);
    }

}
