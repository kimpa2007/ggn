<?php

namespace App\Http\Controllers\Admin\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Services\ImageService;
use App\Repositories\PartnerRepository;
use App\Repositories\CreditOperationRepository;

use App\Models\CreditOperation;

use App\Jobs\Partner\SaveCredit;
use App\Http\Requests\Admin\Partner\SaveCreditRequest;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;

class CreditController extends Controller
{

    public function __construct(
        ImageService $imageService,
        PartnerRepository $partners,
        CreditOperationRepository $operations
    ) {
        $this->imageService = $imageService;
        $this->partners = $partners;
        $this->operations = $operations;
        $this->middleware('auth');
    }

    public function show($id)
    {
        return view('admin.partners.credits', [
            "partner" => $this->partners->find($id)
        ]);
    }


    public function save($id, SaveCreditRequest $request)
    {
        try {
            $this->dispatchNow(SaveCredit::fromRequest($this->partners->find($id), $request));
            Session::flash('notify_success', "Enregistrement effectué avec succès");
        } catch (Exception $ex) {
            Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");
        }

        return redirect()->action('Admin\Partner\CreditController@show', ['id' => $id]);
    }

    /*
    *   Datatable list of partners
    */
    public function getCreditOperationData($id, Request $request)
    {
        return Datatables::collection($this->operations->all()->where('partner_id', $id))
            ->editColumn('done_at', function ($item) {
                return Carbon::parse($item->done_at)->format('d/m/Y H:i:s');
            })
            ->editColumn('type', function ($item) {
                return $item->getTypeName();
            })
        ->make(true);
    }
}
