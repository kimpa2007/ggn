<?php

namespace App\Http\Controllers\Admin\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;
use App\Models\Notification;
use App\Jobs\CreateNotification;
use App\Jobs\UpdateNotification;
use App\Repositories\PartnerRepository;
use App\Repositories\NotificationsRepository;
use App\Http\Requests\NotificationRequest;


class NotificationController extends Controller
{

    public function __construct(PartnerRepository $partners, NotificationsRepository $notifications)
    {
        $this->partners = $partners;
        $this->notifications = $notifications;
        $this->middleware('auth');
    }

    public function index($id)
    {
        return view('admin.partners.notifications', [
            "partner" => $this->partners->find($id)
        ]);
    }

    public function show($id, Notification $notification)
    {
        return view('admin.partners.notification', [
            "partner" => $this->partners->find($id),
            "notification" => $notification
        ]);
    }

    public function update($id, Notification $notification, NotificationRequest $request)
    {
        $this->dispatchNow(UpdateNotification::fromRequest($notification, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.partner.notifications.show', [
            'id' => $notification->partner_id,
            'notification' => $notification
        ]);
    }

    public function create($id)
    {
        return view('admin.partners.notification', [
            "partner" => $this->partners->find($id)
        ]);
    }

    public function save($id, NotificationRequest $request)
    {
        $notification = $this->dispatchNow(CreateNotification::fromRequest($request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.partner.notifications.show', [
            'id' => $notification->partner_id,
            'notification' => $notification
        ]);
    }

    /*
    *   Datatable list of partners
    */
    public function getData($id, Request $request)
    {
        $partner = $this->partners->find($id);

        return Datatables::collection($partner->notifications)
            ->editColumn('status', function ($item) {
                return $item->getStatusName();
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    route('admin.partner.notifications.show', [
                        'id' => $item->partner_id,
                        'notification' => $item
                    ])
                );
            })
        ->make(true);
    }

}
