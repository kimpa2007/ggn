<?php

namespace App\Http\Controllers\Admin\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Services\ImageService;

use App\Repositories\PartnerRepository;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;

class OptionController extends Controller
{

    public function __construct(ImageService $imageService, PartnerRepository $partnerRepository)
    {
        $this->imageService = $imageService;
        $this->repository = $partnerRepository;
        $this->middleware('auth');
    }

    public function show($id)
    {
        return view('admin.partners.options', [
            "partner" => $this->repository->find($id)
        ]);
    }

    public function save($id, Request $request)
    {
        $partner = $this->repository->update($request->all(), $id);

        if($partner) {
            Session::flash('notify_success', "Enregistrement effectué avec succès");
        } else {
            Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");
        }

        return redirect()->action('Admin\Partner\OptionController@show', ['id' => $id]);
    }
}
