<?php

namespace App\Http\Controllers\Admin\Partner;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PartnerRepository;
use Illuminate\Support\Facades\Response;


class OrderController extends Controller
{

    public function __construct(PartnerRepository $partnerRepository) {
        $this->partnerRepository = $partnerRepository;
        $this->middleware('auth');
    }

    public function show($id)
    {
        return view('admin.partners.orders', [
            "partner" => $this->partnerRepository->find($id)
        ]);
    }


    /*
    *   Datatable list of partners
    */
    public function getPlansData($id, Request $request)
    {
        return Datatables::collection($this->partnerRepository->getPlansOrders($id))
            ->editColumn('status', function ($item) {
                return $item->getStatusName();
            })
            ->editColumn('type_payment', function ($item) {
                return $item->getTypePaymentName();
            })
            ->editColumn('paid_at', function ($item) {
                return Carbon::parse($item->paid_at)->format('d/m/Y');
            })
            ->editColumn('start_at', function ($item) {
                return Carbon::parse($item->start_at)->format('d/m/Y');
            })
            ->editColumn('end_at', function ($item) {
                return Carbon::parse($item->end_at)->format('d/m/Y');
            })
            ->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d/m/Y');
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s">Voir</a>',
                    route('admin.orders.plan.show', $item)
                );
            })
        ->make(true);
    }


    /*
    *   Datatable list of partners
    */
    public function getCreditsData($id, Request $request)
    {
        return Datatables::collection($this->partnerRepository->getCreditsOrders($id))
            ->editColumn('status', function ($item) {
                return $item->getStatusName();
            })
            ->editColumn('type_payment', function ($item) {
                return $item->getTypePaymentName();
            })
            ->editColumn('paid_at', function ($item) {
                return Carbon::parse($item->paid_at)->format('d/m/Y');
            })
            ->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d/m/Y');
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s">Voir</a>',
                    route('admin.orders.credit.show', $item)
                );
            })
        ->make(true);
    }

}
