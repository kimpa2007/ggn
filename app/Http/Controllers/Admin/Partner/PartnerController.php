<?php

namespace App\Http\Controllers\Admin\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Services\ImageService;
use App\Repositories\PartnerRepository;
use App\Repositories\UserRepository;
use App\Http\Requests\Admin\Partner\SavePartnerRequest;
use App\Jobs\CreateUser;
use App\Jobs\UpdateUser;

use App\Models\User;
use App\Models\Role;
use App\Models\Partner;
use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;


class PartnerController extends Controller
{
    public function __construct(PartnerRepository $partners, UserRepository $users)
    {
        $this->partners = $partners;
        $this->users = $users;
        $this->middleware('auth');
    }

    /*
    *   Liste all partners
    */
    public function index()
    {
        return view('admin.partners.index');
    }

    /*
    *   Edit partner data
    */
    public function show($id)
    {
        $managers = $this->users->with(array('roles' => function($query) {
             $query->where('roles.name', 'partner');
        }))->all();

        return view('admin.partners.account', [
            "partner" => $this->partners->find($id),
            "managers" => $managers
        ]);
    }

    public function newPartner()
    {
        $managers = $this->users->with(array('roles' => function($query) {
             $query->where('roles.name', 'partner');
        }))->all();

        return view('admin.partners.account', [
            "managers" => $managers
        ]);
    }

    /*
    *   Save partner
    */
    public function update($id, Request $request)
    {
        return $this->save($request, $id);
    }


    public function create(SavePartnerRequest $request)
    {
        return $this->save($request);
    }

    public function save($request, $id = null)
    {
        // FIXME : Put this code into 2 jobs : UpdatePartner and CreatePartner
        //try {
            if($request->get('id') == '') {

                $userId = $this->dispatchNow(new CreateUser(
                    $request->get('firstname'),
                    $request->get('lastname'),
                    $request->get('email'),
                    Role::where('name', 'partner')->first()->id,
                    $request->get('password'),
                    User::STATUS_ACTIVE
                ));

                $data = $request->all();
                $data["manager_id"] = $userId;
                $partner = $this->partners->create($data);

                //update Parent id
                User::where('id',$userId)->update(['partner_id' => $partner->id]);

            } else {
                $partner = $this->partners->update($request->all(), $id);

                $data = $request->all();
                $data["role_id"] = Role::where('name', 'partner')->first()->id;

                $this->dispatchNow(new UpdateUser($partner->manager, $data));
            }

            Session::flash('notify_success', "Enregistrement effectué avec succès");

        // } catch (\Exception $ex) {
        //     echo $ex->getMessage();
        //     exit();
        //     Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");
        // }

        return redirect()->action('Admin\Partner\PartnerController@show', ['id' => $partner->id]);
    }


    public function delete($id)
    {
        $partner = $this->partners->find($id);

        try {

            Schema::disableForeignKeyConstraints();
            $partner->creditsOperations()->delete();
            $partner->orders()->delete();
            $partner->plansPurposed()->delete();

            if(!empty($partner->manager)){
                $partner->manager->contentViews()->delete();
                $partner->manager->delete();
            }
            $partner->delete();
            Schema::enableForeignKeyConstraints();

            Session::flash('notify_success', "Suppression effectuée avec succès");
            return redirect()->action('Admin\Partner\PartnerController@index');

        } catch (\Exception $e) {
            Session::flash('notify_error', "Une erreur s'est produite pendant la suppression");
            return redirect()->action('Admin\Partner\PartnerController@show', ['id' => $id]);
        }
    }

    /*
    *   Datatable list of partners
    */
    public function getData(Request $request)
    {
        return Datatables::collection($this->partners->with("manager")->all())
            ->filterColumn('full_name', function($query, $keyword) {
                $sql = "CONCAT(manager.firstname,' ',manager.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('action', function ($item) {
                $buttons = "";
                $buttons.= sprintf(
                    '<a href="%s" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    action('Admin\Partner\PartnerController@show', ['id' => $item->id])
                );
                if(Auth::user()->hasRole('super-admin')){


                    if(!empty($item->manager)){
                      $buttons.= sprintf(
                          '<a href="%s" class="btn btn-table"><i class="fa fa-sign-in"></i> &nbsp; Login as</a>',
                          route('admin.users.login', ['id' =>  $item->manager->id])
                      );
                    }
                }

                return $buttons;

            })
            ->addColumn('manager', function ($item) {


                if(!empty($item->manager)){
                    return $item->manager->full_name;
                }
                else 
                    return "";
            })
            ->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d/m/Y');
            })
        ->make(true);
    }
}
