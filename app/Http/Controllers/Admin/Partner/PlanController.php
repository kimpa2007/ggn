<?php

namespace App\Http\Controllers\Admin\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Repositories\PartnerRepository;

use Session;
use Datatables;
use Carbon\Carbon;
use App\Models\PartnerOrder;
use App\Models\Partner;

use App\Http\Requests\Admin\Partner\CreatePartnerPlanRequest;

use App\Jobs\Partner\CreatePartnerPlanOrder;

class PlanController extends Controller
{
    public function __construct(PartnerRepository $partners) {
        $this->partners = $partners;
        $this->middleware('auth');
    }

    public function show($id)
    {

        return view('admin.partners.plans', [
            "partner" => $this->partners->find($id)
        ]);
    }

    /*
    public function add(PartnerOrder $partner, PartnerPlanRequest $request)
    {
        //$plan = $this->dispatchNow(UpdatePartnerPlan::fromRequest($plan, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.partners.plan', $partner->id);
    }
    */

    public function getData($id, Request $request)
    {
        $orders = $this->partners
            ->with(["orders", "orders.plan"])
            ->find($id)
            ->orders
            ->where('type', PartnerOrder::TYPE_PLAN);

        return Datatables::collection($orders)
            ->editColumn('start_at', function ($item) {
                return Carbon::parse($item->start_at)->format('d/m/Y');
            })
            ->editColumn('end_at', function ($item) {
                return Carbon::parse($item->end_at)->format('d/m/Y');
            })
            ->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d/m/Y');
            })
            ->editColumn('status', function ($item) {
                return $item->getStatusName();
            })
            ->addColumn('zones', function ($item) {

                if(isset($item->plan_id)){

                  return $item->plan->zones->map(function($zone){
                      return $zone->name;
                  })->toArray();
                }
                else {
                  return '';
                }
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s">Voir</a>',
                    route('admin.orders.plan.show', $item)
                );
            })
        ->make(true);
    }


    public function create($id,CreatePartnerPlanRequest $request)
    {

      try {

        $partner = $this->partners->find($id);

        $orderId = $this->dispatchNow(CreatePartnerPlanOrder::fromRequest($partner,$request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.orders.plan.show',["id" => $orderId]);

      }
      catch(\Exception $e ) {
        Session::flash('notify_error', $e->getMessage());
      }

      return view('admin.partners.plans', [
          "partner" => $partner
      ]);

    }

}
