<?php

namespace App\Http\Controllers\Admin\Partner;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;
use App\Jobs\SetPartnerRules;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PartnerRepository;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\PartnerRulesRequest;

class RuleController extends Controller
{

    public function __construct(PartnerRepository $partnerRepository) {
        $this->partnerRepository = $partnerRepository;
        $this->middleware('auth');
    }

    public function show($id)
    {
        return view('admin.partners.rules', [
            "partner" => $this->partnerRepository->find($id)
        ]);
    }

    public function save($id, PartnerRulesRequest $request)
    {
        $this->dispatchNow(SetPartnerRules::fromRequest($id, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.partner.rules', $id);
    }

}
