<?php

namespace App\Http\Controllers\Admin\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Admin\SaveUserRequest;
use \Prettus\Validator\Exceptions\ValidatorException;

use App\Services\ImageService;
use App\Repositories\UserRepository;
use App\Repositories\PartnerRepository;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;
use App\Models\User;
class UserController extends Controller
{

    public function __construct(UserRepository $userRepository, PartnerRepository $partnerRepository)
    {
        $this->userRepository = $userRepository;
        $this->partnerRepository = $partnerRepository;
        $this->middleware('auth');
    }

    public function index($id)
    {
        return view('admin.partners.users', [
            "partner" => $this->partnerRepository->find($id)
        ]);
    }

    /*
    *   Return datatables JSON data
    */
    public function getData($id, Request $request)
    {
        return Datatables::collection($this->userRepository->findByField("partner_id", $id))
            ->filterColumn('full_name', function($query, $keyword) {
                $sql = "CONCAT(users.firstname,' ',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('full_name', function ($item) {
                return $item->full_name;
            })
            ->addColumn('action', function ($item) {
                 return '
                     <a href="' . action('Admin\User\AccountController@show', ['id' => $item->id]) . '" class="btn btn-table">
                         <i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Voir
                     </a>

                     ';
             })
                        ->editColumn('status', function ($item) {
                $status = User::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d/m/Y');
            })
        ->make(true);
    }
}
