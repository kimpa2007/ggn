<?php

namespace App\Http\Controllers\Admin\Plan;

use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Carbon\Carbon;
use App\Jobs\CreatePartnerPlan;
use App\Jobs\UpdatePartnerPlan;
use App\Jobs\DeletePartnerPlan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Repositories\PartnerPlanRepository;
use App\Http\Requests\PartnerPlanRequest;

use App\Models\PartnerPlan;

class PartnerPlanController extends Controller
{

    public function __construct(PartnerPlanRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.plans.index');
    }

	public function getData(Request $request)
    {

		$plans = $this->repository->all();

        return Datatables::of($plans)

            ->addColumn('id', function ($item) {
                return $item->id;
            })

            ->addColumn('zone', function ($item) {
                if(!empty($item->zones())){
            	   return $item->getZonesNames();
                }
            })
      		
            ->addColumn('description', function ($item) {

      				$text = html_entity_decode(strip_tags($item->description));

                return substr($text,0,30).(strlen($text) > 15 ? '...':'');
            })

            ->addColumn('months', function ($item) {
                return $item->months;
            })

            ->editColumn('status', function ($item) {
                $status = PartnerPlan::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
    		
            ->addColumn('price', function ($item) {

      			$times = PartnerPlan::getTimes();
                if(!empty($times) and !empty($item->time)){
                    return $item->price.'€ /'.$times[$item->time];
                }
            	return $item->price.'€ '/*/ '.$times[$item->time]*/;
            })




            ->addColumn('action', function ($item) {
                return '<a href="' . route('admin.plan', ['id' => $item->id]) . '" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>'.' &nbsp; '.
                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
            })
            ->make(true);
    }

	  public function create()
    {
        return view('admin.plans.form');
    }

    public function show(PartnerPlan $plan)
    {
        return view('admin.plans.form', [
            "plan" => $plan
        ]);
    }

	  public function save(PartnerPlanRequest $request)
    {
        $plan = $this->dispatchNow(CreatePartnerPlan::fromRequest($request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.plan', $plan->id);
    }

    public function update(PartnerPlan $plan, PartnerPlanRequest $request)
    {
        $plan = $this->dispatchNow(UpdatePartnerPlan::fromRequest($plan, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.plan', $plan->id);
    }

    public function delete(Request $request)
    {
    	$plan = $this->repository->find($request->get('id'));

        if($this->dispatchNow(new DeletePartnerPlan($plan))) {

            Session::flash('notify_success', "Suppression effectuée avec succès");
            return redirect()->action('Admin\Plan\PartnerPlanController@index');
        }

        Session::flash('notify_error', "Une erreur s'est produite pendant la suppression");

        return redirect()->route('admin.plan', $plan);
    }
}
