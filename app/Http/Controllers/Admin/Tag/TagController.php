<?php

namespace App\Http\Controllers\Admin\Tag;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Jobs\Tags\DeleteTags;
use App\Http\Requests\Admin\Tags\DeleteTagsRequest;

use App\Jobs\Tags\CreateTag;
use App\Http\Requests\Admin\Tags\CreateTagRequest;

use App\Jobs\Tags\UpdateTags;
use App\Http\Requests\Admin\Tags\UpdateTagsRequest;

use App\Models\Tag;
use Datatables;
use Auth;
use Session;

class TagController extends Controller
{
    public function __construct()
    {}

    public function index(Request $request)
    {
        return view('admin.tags.index');
    }

    public function delete(DeleteTagsRequest $request)
    {
        try {
            if ($this->dispatchNow(DeleteTags::fromRequest($request))) {
                Session::flash('notify_success', 'Enregistrement effectué avec succès');
            } else {
                Session::flash('notify_error', "Une erreur s'est produite lors de la suppression");
            }
        } catch (\Exception $ex) {
            Session::flash('notify_error', $ex->getMessage());
        }

        return redirect()->route('admin.tags.index');
    }

    public function create(CreateTagRequest $request)
    {
        try {
            if ($this->dispatchNow(CreateTag::fromRequest($request))) {
                Session::flash('notify_success', 'Enregistrement effectué avec succès');
            } else {
                Session::flash('notify_error', "Une erreur s'est produite lors de l'enregistrement");
            }
        } catch (\Exception $ex) {
            Session::flash('notify_error', $ex->getMessage());
        }

        return redirect()->route('admin.tags.index');
    }


    public function update(UpdateTagsRequest $request)
    {
        try {
            if ($this->dispatchNow(UpdateTags::fromRequest($request))) {
                Session::flash('notify_success', 'Enregistrement effectué avec succès');
            } else {
                Session::flash('notify_error', "Une erreur s'est produite lors de l'enregistrement");
            }
        } catch (\Exception $ex) {
            Session::flash('notify_error', $ex->getMessage());
        }

        return redirect()->route('admin.tags.index');
    }
}
