<?php

namespace App\Http\Controllers\Admin\Theme;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveThemeRequest;

use App\Models\Theme;
use App\Jobs\Theme\CreateTheme;

use Illuminate\Support\Facades\Response;

use Datatables;
use Auth;
use Hash;
use Session;

use App\Repositories\ThemeRepository;


class ThemeController extends Controller
{

    public function index(Request $request)
    {
        return view('admin.themes.index');
    }

	/*
    *   Return datatables JSON data
    */
    public function getData(Request $request)
    {
			$themes = Theme::orderBy('position')->get();
//			$themes = Theme::all()->sortBy('position');

			return Datatables::of($themes)
				->addColumn('id', function ($item) {
				    return $item->id;
				})
				->addColumn('position', function ($item) {
				    return $item->position;
				})
				->addColumn('name', function ($item) {
				    return $item->translate('fr')->name;
				})
	            ->addColumn('action', function ($item) {
	                return sprintf(
	                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
	                    action('Admin\Theme\ThemeController@show', ['id' => $item->id])).' &nbsp; '.
	                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
	            })
				->rawColumns(['name', 'action'])
        ->make(true);
    }

    public function create( Request $request)
    {
        return view('admin.themes.edit');
    }

    public function delete(Request $request)
	{

		$theme = Theme::find($request->get('id'));

		if($theme){
			if($theme->delete()){
				return Response::json([
					'error' => false,
					'code' => 200
				],200);
			}
		}

		return Response::json([
			'error' => true,
			'code' => 400
		],400);
	}

	public function show($id, Request $request)
    {
    	$theme = Theme::find($id);

		return view('admin.themes.edit', [
            'theme' => $theme
        ]);
    }

	public function save(SaveThemeRequest $request)
    {
    	
    	try {
            $theme = $this->dispatchNow(CreateTheme::fromRequest($request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
            return  $request->get("id")?redirect()->route('admin.theme.show', $theme->id)->withInput():redirect()->route('admin.theme.create')->withInput();
        }
        return  redirect()->route('admin.theme.show', $theme->id);
    }

    public function updatePosition(Request $request)
    {
    	$params = $request->all();
    	$id = $params['idTheme'];
    	$position = $params['newPosition'];


    	$theme = Theme::find($id);
    	$theme->position = $position;
    	$theme->save();
		
		return Response::json([
			'error' => false,
			'code' => 200
		],200);
    }
}
