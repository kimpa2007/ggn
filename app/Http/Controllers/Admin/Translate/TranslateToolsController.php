<?php

namespace App\Http\Controllers\Admin\Translate;


use App\Http\Requests\Admin\SaveTranslateToolRequest;
use App\Models\TranslateTool;
use App\Http\Controllers\Controller;

use Datatables;
use Illuminate\Http\Request;
use Session;

class TranslateToolsController extends Controller
{
    /**
     * Display a listing of the Custom Pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.translate.index');
    }

    public function create()
    {
        return view('admin.translate.create');
    }

    public function edit($id){
        $translate = TranslateTool::findOrFail($id);
        return view('admin.translate.create',['translate'=>$translate]);
    }

    public function delete($id) {
        $translate = TranslateTool::findOrFail($id);
        try {
            $translate->delete();
            Session::flash('notify_success', "Suppression effectuée avec succès");

        } catch (\Exception $e) {
            Session::flash('notify_error', "Une erreur s'est produite pendant la suppression");
        }
        return redirect(route('admin.translate.index'));
    }

    public function save($id = null,SaveTranslateToolRequest $request){
        //si on edit
        if(!empty($id)) {
            $translate = TranslateTool::findOrFail($id);
            try {
                $translate->update($request->all());
                Session::flash('notify_success', 'Enregistrement mise à jour avec succès');

            } catch (\Exception $e) {
                Session::flash('notify_error', "Une erreur s'est produite pendant l'enregistrement");
            }
        } else {
            //si c'est un nouvel enregistrement
            try {
                TranslateTool::create($request->all());
                Session::flash('notify_success', 'Enregistrement effectué avec succès');
            } catch (\Exception $e) {
                Session::flash('notify_error', "Une erreur s'est produite pendant l'enregistrement");
            }
        }

        return redirect(route('admin.translate.index'));
    }

    public function getData()
    {
        $translates = TranslateTool::getAll();

        return Datatables::of($translates)
            ->addColumn('actions', function ($item) {
                return sprintf(
                        '<a href="%s" class="btn btn-table view-item"><i class="fa fa-pencil-square-o"></i> &nbsp; Editer</a>',
                        route('admin.translate.edit', ['id' => $item->id])).' &nbsp; '.

                        '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>'
                    ;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
