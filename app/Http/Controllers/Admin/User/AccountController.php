<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\CreateUserRequest;

use App\Jobs\CreateUser;
use App\Jobs\UpdateUser;

use App\Repositories\UserRepository;

use App\Models\User;
use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use DB;

class AccountController extends Controller
{
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
        $this->middleware('auth');
    }

    public function index($type = null)
    {
        return view('admin.users.index', [
            'type' => $type
        ]);
    }

    public function show($id)
    {
        return view('admin.users.account', [
            'user' => $this->users->find($id)
        ]);
    }

    public function create()
    {
        return view('admin.users.account');
    }

    /*
    *   Return datatables JSON data
    */
    public function getData($type = null, Request $request)
    {
        $users = $this->users->getDatatableData()
            ->leftJoin('roles_users', 'users.id', '=', 'roles_users.user_id')
            ->leftJoin('roles', 'roles_users.role_id', '=', 'roles.id');

        if ($type) {
            $users->where('roles.name', strtolower($type));
        }

        return Datatables::of($users)
            ->filterColumn('full_name', function ($query, $keyword) {
                $query->whereRaw("CONCAT(users.firstname,'-',users.lastname)  like ?", ["%{$keyword}%"]);
            })

            ->addColumn('role', function ($item) {
                return $item->getRoleName();
            })
            ->filterColumn('role', function ($query, $keyword) {
                $query->whereRaw('roles.display_name LIKE ?', [substr($keyword, 1, -1)]);
            })

            ->editColumn('status', function ($item) {
                $status = User::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->filterColumn('status', function ($query, $keyword) {
                $query->whereRaw('status LIKE ?', [array_search(substr($keyword, 1, -1), User::getStatus())]);
            })

            ->addColumn('action', function ($item) {
                $buttons = "";
                $buttons.= sprintf(
                    '<a href="%s" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    action('Admin\User\AccountController@show', ['id' => $item->id])
                );
                if (Auth::user()->hasRole('super-admin') && $item->status == User::STATUS_ACTIVE) {
                    $buttons.= sprintf(
                      '<a href="%s" class="btn btn-table"><i class="fa fa-sign-in"></i> &nbsp; Login as</a>',
                      route('admin.users.login', ['id' => $item->id])
                  );
                }

                return $buttons;
            })
        ->make(true);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $this->dispatchNow(UpdateUser::fromRequest($user, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.users.account', $user->id);
    }


    public function save(CreateUserRequest $request)
    {
        $userId = $this->dispatchNow(CreateUser::fromRequest($request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.users.account', $userId);
    }

    public function loginAs($id, Request $request)
    {
        $user = User::findOrFail($id);
       //ore use your own way to get the user

      Auth::login($user);

        return redirect()->route('home');
    }

    public function delete(Request $request, $type=null)
    {
        $user = $this->users->find($request->get('id'));

        if ($user) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');

            if ($user->delete()) {
                return view('admin.users.index', [
                    'type' => $type
                ]);
            }
        }

        Session::flash('notify_error', "Erreur lors de la suppression du compte.");

        return redirect()->route('admin.users.account', $request->get('id'));
    }
}
