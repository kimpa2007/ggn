<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Http\Requests\UserAddCreditsRequest;

use App\Repositories\UserRepository;
use App\Repositories\CreditOperationRepository;

use App\Models\User;
use App\Jobs\AddCreditUser;

use Auth;
use Datatables;
use Session;
use Carbon\Carbon;
use Exception;

class CreditController extends Controller
{

    public function __construct(UserRepository $users, CreditOperationRepository $operations)
    {
        $this->users = $users;
        $this->operations = $operations;
        $this->middleware('auth');
    }


    public function show($id)
    {
        return view('admin.users.credit', [
            'user' => $this->users->find($id)
        ]);
    }

    public function save(User $user, UserAddCreditsRequest $request)
    {
        try {
            $this->dispatchNow(AddCreditUser::fromRequest($user, $request));
            Session::flash('notify_success', "Enregistrement effectué avec succès");
        } catch (Exception $ex) {
            Session::flash('notify_error', $ex->getMessage());
        }

        return redirect()->route('admin.users.credits', $user);
    }


    public function getCreditOperationData(User $user, Request $request)
    {
        $operations = $this->operations
            ->all()
            ->where('user_id', $user->id)
            ->where('partner_id', null);

        return Datatables::collection($operations)
            ->editColumn('done_at', function ($item) {
                return Carbon::parse($item->done_at)->format('d/m/Y');
            })
            ->editColumn('type', function ($item) {
                return $item->getTypeName();
            })
        ->make(true);
    }
}
