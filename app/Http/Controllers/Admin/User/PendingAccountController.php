<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Admin\SaveUserRequest;
use \Prettus\Validator\Exceptions\ValidatorException;

use App\Services\ImageService;

use App\Repositories\UserRepository;
use App\Repositories\PartnerRepository;

use App\Models\User;
use Auth;
use Hash;
use Session;
use Storage;
use Datatables;
use Form;

class PendingAccountController extends Controller
{

    public function __construct(ImageService $imageService, UserRepository $users, PartnerRepository $partners)
    {
        $this->users = $users;
        $this->partners = $partners;
        $this->imageService = $imageService;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.users.pending');
    }

    public function setActive(Request $request)
    {
        $userId = $request->get("id");
        $attr = [
            "status" => User::STATUS_ACTIVE
        ];

        if($this->users->update($attr, $userId)) {
            return Response::json([
                'success' => true
            ], 200);
        }

        return Response::json([
            'success' => false
        ], 400);
    }


    public function setRefused(Request $request)
    {
        $userId = $request->get("id");
        $attr = [
            "status" => User::STATUS_REFUSED
        ];

        if($this->users->update($attr, $userId)) {
            return Response::json([
                'success' => true
            ], 200);
        }

        return Response::json([
            'success' => false
        ], 400);
    }


    public function setPartner(Request $request)
    {
        $userId = $request->get("id");

        $attr = [
            "partner_id" => $request->get('partner_id')
        ];

        if($this->users->update($attr, $userId)) {
            return Response::json([
                'success' => true
            ], 200);
        }

        return Response::json([
            'success' => false
        ], 400);
    }


    /*
    *   Return datatables JSON data
    */
    public function getData(Request $request)
    {
        $users = $this->users
            ->getDatatableData()
            ->where('status', User::STATUS_PENDING);

        $options = $this->partners->all()->pluck('name', 'id')->prepend('Aucune', '');

        return Datatables::of($users)
            ->filterColumn('full_name', function($query, $keyword) {
                $sql = "CONCAT(users.firstname,' ',users.lastname) like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('role', function ($item) {
                return $item->getRoleName();
            })
            ->addColumn('created_at', function ($item) {
                return $item->created_at->format('d/m/Y');
            })
            ->addColumn('partner', function ($item) use ($options) {
                return Form::select('partner_id', $options, $item->partner_id, [
                    'class' => 'form-control trigger-partner',
                    'data-id' => $item->id
                ]);
            })
            ->editColumn('status', function ($item) {
                $status = User::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->addColumn('action', function ($item) {

                switch($item->status) {
                    case User::STATUS_ACTIVE:
                        return sprintf(
                            '<a href="#" data-id="%d" class="btn btn-table trigger-refuse">Refuser</a>
                            <a href="%s" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                            $item->id,
                            action('Admin\User\AccountController@show', ['id' => $item->id])
                        );
                    break;

                    case User::STATUS_REFUSED:
                    case User::STATUS_PENDING:
                        return sprintf(
                            '<a href="#" data-id="%d" class="btn btn-table trigger-active">Activer</a>
                            <a href="#" data-id="%d" class="btn btn-table trigger-refuse">Refuser</a>
                            <a href="%s" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                            $item->id,
                            $item->id,
                            action('Admin\User\AccountController@show', ['id' => $item->id])
                        );
                    break;

                }
            })
        ->make(true);
    }


}
