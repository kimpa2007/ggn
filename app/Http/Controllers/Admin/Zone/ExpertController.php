<?php

namespace App\Http\Controllers\Admin\Zone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveZoneRequest;

use App\Models\Zone;

use Illuminate\Support\Facades\Response;
use App\Repositories\UserRepository;

use Datatables;
use Auth;
use Hash;
use Session;


class ExpertController extends Controller
{

	public function __construct(  UserRepository $repository)
	{
	    $this->repository = $repository;
		$this->middleware('auth');
	}

    public function index($id,Request $request)
    {
        return view('admin.zones.experts.index', [
            'zone' => Zone::find($id)
        ]);
    }
	
	/*
    *   Return datatables JSON data
    */
    public function getData($id,Request $request)
    {
    	
		$experts = $this->repository->getDatatableData()
			->whereHas('expertZones', function($query) use ($id){
	    		$query->where('zone_id',$id);
	    	})
			->whereHas('roles', function($query){
	    		$query->where('name','expert');
	    	})->get();
		
		return Datatables::of($experts)
            ->filterColumn('full_name', function($query, $keyword) {
                $sql = "CONCAT(users.firstname,' ',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
			->filterColumn('zone', function($query, $keyword) {
                $sql = "zones.name LIKE ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('zone', function ($item) {
            	
				if($item->expertZones->count() > 0)
					return $item->expertZones->first()->name;
				else 
					return null;
				
            	//return $item->expertZones->first()->name;
            })
            ->addColumn('date', function ($item) {
                return $item->created_at->format('d/m/Y');
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
                    action('Admin\Expert\AccountController@show', ['id' => $item->id]));
            })
        ->make(true);
    }

}
