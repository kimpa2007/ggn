<?php

namespace App\Http\Controllers\Admin\Zone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveZoneRequest;

use App\Models\Zone;

use Illuminate\Support\Facades\Response;

use Datatables;
use Auth;
use Hash;
use Session;


class StatsController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth');
	}

    public function index($id, Request $request)
    {
    	
		return view('admin.zones.stats.index', [
            'zone' => Zone::find($id)
        ]);
    }

}
