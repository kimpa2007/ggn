<?php

namespace App\Http\Controllers\Admin\Zone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\SaveZoneRequest;

use App\Models\Zone;
use App\Models\CountryZone;
use App\Models\UserZone;
use App\Jobs\Zone\CreateZone;

use Illuminate\Support\Facades\Response;

use Datatables;
use Auth;
use Hash;
use Session;


use App\Services\ImageService;
use App\Repositories\ZoneRepository;


class ZoneController extends Controller
{

		public function __construct(ZoneRepository $repository)
		{
		    $this->repository = $repository;
				$this->middleware('auth');
		}

    public function index(Request $request)
    {
        return view('admin.zones.index');
    }

		/*
    *   Return datatables JSON data
    */
    public function getData(Request $request)
    {
			$zones = $this->repository->getDatatableData()->get();

			return Datatables::of($zones)
				->addColumn('countries', function ($item) {
					$countries =  $item->countries->map(function ($country) {
						    return $country->name;
						})->toArray();
					return implode(',', $countries);
				})
	            ->addColumn('action', function ($item) {
	                return sprintf(
	                    '<a href="%s" class="btn btn-table view-item"><i class="fa fa-eye"></i> &nbsp; Voir</a>',
	                    action('Admin\Zone\ZoneController@show', ['id' => $item->id])).' &nbsp; '.
	                    '<a href="#" class="btn btn-table remove-item" data-id="'.$item->id.'"><i class="fa fa-trash"></i> &nbsp; Supprimer</a>';
	            })
				->rawColumns(['order', 'action', 'countries'])
        ->make(true);
    }

	public function show($id, Request $request)
    {
    	$zone = $this->repository->find($id);

		return view('admin.zones.edit', [
            'zone' => $zone
        ]);
    }

    public function create( Request $request)
    {
        return view('admin.zones.edit');
    }

	public function save(SaveZoneRequest $request)
    {
    	try {
            $zone = $this->dispatchNow(CreateZone::fromRequest($request));
            Session::flash('notify_success', 'Enregistrement effectué avec succès');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
            return  $request->get("id")?redirect()->route('admin.zone.show', $zone->id)->withInput():redirect()->route('admin.zone.create')->withInput();
        }
        return  redirect()->route('admin.zone.show', $zone->id);
    }

	public function updateOrder(Request $request)
	{

		if($request->exists('order')){
			$order = $request->get('order');

			foreach($order as $row){
				Zone::where('id', $row["id"])->update(['order' => $row["newOrder"]]);
		   	}

			return Response::json([
	            'error' => false,
	            'code'  => 200
	        ], 200);
		}

		return Response::json([
	            'error' => true,
	            'code'  => 400
	        ], 400);

	}

	public function delete(Request $request)
	{

		$zone = $this->repository->find($request->get('id'));

		if($zone){
			if($zone->delete()){
				return Response::json([
					'error' => false,
					'code' => 200
				],200);
			}
		}

		return Response::json([
			'error' => true,
			'code' => 400
		],400);
	}

}
