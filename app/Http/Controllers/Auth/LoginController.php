<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Request;
use App\Models\Partner;
use Redirect;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	protected function redirectTo()
	{
		if(Auth::user()->hasRole('user')){
			//return route('user.index');
      return '/';
		}

		if(Auth::user()->hasRole('expert')){
            return action('Expert\AccountController@index');
		}

    if(Auth::user()->hasRole('partner')){
			  return '/';
		}

        if(Auth::user()->hasRole('admin')){
			return action('Admin\HomeController@index');
		}

        if(Auth::user()->hasRole('super-admin')){
			return '/admin/';
		}

        return '/';
	}
}
