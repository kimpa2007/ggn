<?php

namespace App\Http\Controllers\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Expert\SaveAccountRequest;

use Illuminate\Support\Facades\Response;

use Auth;
use Hash;
use Session;
use Storage;

class AccountController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();
		$user->load('expertZones');

        return view('expert.account', [
            'user' => $user
        ]);
    }

    public function save(SaveAccountRequest $request)
    {
        $user = Auth::user();

        $user->firstname = $request->get("firstname");
        $user->lastname = $request->get("lastname");
        $user->email = $request->get("email");
		$user->image = $request->get("image");

        if(trim($request->get("password"))) {
            if($request->get("password") != $request->get("confirm_password")) {
                Session::flash('notify_error', "Votre mot de passe n'est pas correctement confirmé");
                return redirect()->action('Expert\AccountController@index');
            }
            $user->password = trim(Hash::make($request->get('password')));
        }

        if($user->save()) {
            Session::flash('notify_success', "Votre compte vient d'être mis à jours");
        } else {
            Session::flash('notify_error', "Une erreur s'est produite lors de l\'enregistrement");
        }

        return redirect()->action('Expert\AccountController@index')
            ->withInput();
    }

}
