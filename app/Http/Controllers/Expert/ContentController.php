<?php

namespace App\Http\Controllers\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Expert\SaveContentRequest;

use App\Models\Content;
use App\Models\Country;
use App\Services\FileService;

use App\Repositories\CountryRepository;

use Illuminate\Support\Facades\Response;

use Datatables;
use Auth;
use Session;

class ContentController extends Controller
{

		public function __construct(FileService $fileService,CountryRepository $countryRepository)
		{
			$this->fileService = $fileService;
			$this->countries = $countryRepository;
			$this->middleware('auth');
		}

    public function index(Request $request)
    {
        return view('expert.contents.index');
    }


		public function create(Request $request)
    {
				$user = Auth::user();
				$userZones = $user->getExpertZonesIds()->toArray();

				if($userZones == null){
					//no zone assigned
					abort(403,'Unauthorized action.');
				}

        return view('expert.contents.form', [
					"countriesTree" => $this->countries->getCountryTree($user->expertZones),
				]);
    }

    public function show($id, Request $request)
    {
        $user = Auth::user();
        $content = Content::find($id);

				$userZones = $user->getExpertZonesIds()->toArray();

				//ACL for contents made with same countries
				/*if($userZones == null || isset($content) && !in_array($content->zone_id,$userZones)){
					abort(403,'Unauthorized action.');
				}*/

        return view('expert.contents.form', [
            "countriesTree" => $this->countries->getCountryTree($user->expertZones),
            "content" => $content
        ]);
    }


    public function save(SaveContentRequest $request)
    {

			$user = Auth::user();
	        $userZones = $user->getExpertZonesIds()->toArray();

			//ACL for contents made with same countries
			if($userZones == null || isset($content) && !in_array($content->zone_id,$userZones)){
				//abort(403,'Unauthorized action.');
				Session::flash('notify_error', "Unauthorized action");
			}

				$content = $request->get('content_id') ? Content::find($request->get('content_id')) : new Content();
        $content->country_id = $request->get('country_id');
        $content->created_by = Auth::user()->id;
        $content->title = $request->get('title');
				$content->theme = $request->get('theme');
        $content->slug = str_slug($content->title, '-');
        $content->description = $request->get('description');
        $content->image = $request->get('image');
				$content->image_2 = $request->get('image_2');

				$content->legend = $request->get('legend');
				$content->legend_2 = $request->get('legend_2');

				$content->copyright = $request->get('copyright');
				$content->copyright_2 = $request->get('copyright_2');

			if(!$request->get('content_id')){
					$content->order = (Content::where("country_id", $content->country_id)->max('order')) + 1;
			}

			$content->type = $request->get('type');
			$content->content = '';

			$uploadedFilePath = false;

			$content->content = isset($request->get('content')[$request->get('type')]) ?
			$request->get('content')[$request->get('type')] : '' ;



        if ($content->save()) {
            Session::flash('notify_success', "Enregistrement effectué avec succès");
        } else {
            Session::flash('notify_error', "Une erreur s'est produite lors de l'enregistrement");
        }

        return redirect()->action('Expert\ContentController@show', ['id' => $content->id])
            ->withInput();
    }

    public function getData(Request $request)
    {

				$contents = Content::where('created_by', Auth::user()->id)->get();
        $contents->load('countrie');

        return Datatables::of($contents)
						->editColumn('status', function ($item) {
								$status = Content::getStatus();
								return isset($status[$item->status]) ? $status[$item->status] : null;
						})
            ->addColumn('action', function ($item) {
                return '<a href="' . action('Expert\ContentController@show', ['id' => $item->id]) . '" class="btn btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>';
            })
            ->make(true);
    }

	public function changeStatus(Request $request)
	{
		$itemId = $request->get('itemId');
		$status = $request->get('status');

		$item = Content::find($itemId);

		if($item->status != $status){
			$item->status = $status;

			//changed successfully
			if($item->save()){
				return Response::json([
		            'error' => false,
		            'code'  => 200
		        ], 200);

			}
			else {
				return Response::json([
	                'error' => true,
	                'code' => 400
	            ], 400);
			}
		}

		//nothing to change
		return Response::json([
            'error' => false,
            'code'  => 204
        ], 200);

	}
/*
	public function updateOrder(Request $request)
	{

		if($request->exists('order')){
			$order = $request->get('order');

			foreach($order as $row){
				Content::where('id', $row["id"])->update(['order' => $row["newOrder"]]);
		   	}

			return Response::json([
	            'error' => false,
	            'code'  => 200
	        ], 200);
		}

		return Response::json([
	            'error' => true,
	            'code'  => 400
	        ], 400);

	}
*/
}
