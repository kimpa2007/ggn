<?php

namespace App\Http\Controllers\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Content;
use App\Models\Product;
use App\Models\Zone;
use App\Models\Partner;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $user = Auth::user();


      $contents = Content::where('created_by', $user->id)->limit(10);


      return view('expert.home',compact('users','contents',
        'zones','experts','partners'));
    }
}
