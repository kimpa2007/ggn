<?php

namespace App\Http\Controllers\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Expert\SaveProfileRequest;

use App\Models\UserProfile;

use Datatables;
use Auth;
use Session;

class ProfileController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
	
	
	public function index(Request $request)
    {
        $user = Auth::user();

        return view('expert.profile', [
            'user' => $user
        ]);
    }


    public function save(SaveProfileRequest $request)
    {
        $userProfile = Auth::user()->profile ? Auth::user()->profile : null;

        if(!$userProfile) {
            $userProfile = new UserProfile();
            $userProfile->user_id = Auth::user()->id;
        }

        $userProfile->content = $request->get('content');
		$userProfile->image = $request->get('image');

        if ($userProfile->save()) {
            Session::flash('notify_success', "Enregistrement effectué avec succès");
        } else {
            Session::flash('notify_error', "Une erreur s'est produite lors de l'enregistrement");
        }

        return redirect()->action('Expert\ProfileController@index')
            ->withInput();
    }
	
}
