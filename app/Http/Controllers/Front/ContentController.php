<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Content;

use App\Jobs\Content\AddContentView;

use App\Http\Requests\ContactRequest;

use App\Repositories\ContentRepository;
use App\Repositories\UserRepository;

use Auth;
use Session;
use Redis;
use JWTAuth;

class ContentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        ContentRepository $contentRepository,
        UserRepository $userRepository
    ) {
        $this->users = $userRepository;
        $this->contents = $contentRepository;
        $this->itemsPerPage = 20;
        $this->page = 1;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $content = Content::where('id', $id)->where('status', Content::STATUS_PUBLISHED)->first();
        if (!isset($content)) {
            abort(404);
        }

        $crawler = false;
        
        // ip de qwant (scrawler de contenus) rang  ip 194.187.171.1-250
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        
        $high_ip_long = ip2long("194.187.171.250");
        $low_ip_long = ip2long("194.187.171.1");
        $myip_long = ip2long($ip);
        $crawler = false;

        $version_mini = false;

        if (!empty($ip) and $myip_long <= $high_ip_long && $low_ip_long <= $myip_long) {
          $crawler = true;
        }elseif(!Auth::user()){
         // abort(403);
          $version_mini = true;
        }

     /*   if(!Auth::user()){
          abort(403);
        }
*/
        if (!$version_mini and !$crawler and !Auth::user()->canAccessContent($content)) {
            abort(403);
        }

        if (!$crawler and Auth::user()) {
            $this->dispatchNow(new AddContentView(Auth::user(), $content));
        }

          $videos = $this->contents->getPublished()->where('movie_on_top', '1')->get();
          $lastsVideosIds = $videos->sortByDesc(function ($video, $key) {
              return $video->published_at != NULL ? strtotime($video->published_at) : 0;
          })
            ->take(4)
            ->pluck('id')
            ->toArray();
          if(in_array($id, $lastsVideosIds)) {
            $isfree = route('content-free',$content->id);
          } else {
            $isfree = NULL;
          }

        if($version_mini){
          return view('front.contents.page_mini', [
              "files" => $content->type == Content::TYPE_MOVIE ? str_replace("/files/1/", "", $content->content):'',
              "content" => $content,
              "expert" => $this->users->find($content->created_by),
              "isfree" => $isfree,
              "link" => $content->type == Content::TYPE_MOVIE
                  ? '//media.globalgeonews.com/movies/' . str_replace("/files/1/", "", $content->translate('fr')->content ? $content->translate('fr')->content : $content->translate('en')->content)
                  : null
          ]);

        }else{
          return view('front.contents.page', [
              "files" => $content->type == Content::TYPE_MOVIE ? str_replace("/files/1/", "", $content->content):'',
              "content" => $content,
              "expert" => $this->users->find($content->created_by),
              "isfree" => $isfree,
              "link" => $content->type == Content::TYPE_MOVIE
                  ? '//media.globalgeonews.com/movies/' . str_replace("/files/1/", "", $content->translate('fr')->content ? $content->translate('fr')->content : $content->translate('en')->content)
                  : null
          ]);
        }
    }

    public function showFree($id)
    {

      $videos = $this->contents->getPublished()->where('movie_on_top', '1')->get();

      $lastsVideosIds = $videos->sortByDesc(function ($video, $key) {
          return $video->published_at != NULL ? strtotime($video->published_at) : 0;
      })
        //->slice(0, 2)
        ->take(4)
        ->pluck('id')
        ->toArray();

      if(!in_array($id, $lastsVideosIds)) {
        abort(403);
      }

      $content = $this->contents->find($id);
      $expert = $this->users->find($content->created_by);
      // keep check you never know

      return view('front.contents.page', [
        "files" => $content->type == Content::TYPE_MOVIE ? '//media.globalgeonews.com/movies/' .  str_replace("/files/1/", "", $content->content):'',
        "content" => $content,
        "expert" => $expert,
        "isfree" => route('content-free',$content->id),
        "link" => $content->type == Content::TYPE_MOVIE
            ? '//media.globalgeonews.com/movies/' . str_replace("/files/1/", "", $content->translate('fr')->content ? $content->translate('fr')->content : $content->translate('en')->content)
            : null
      ]);
    }

    public function showPreview($id)
    {
      if(Auth::user()->hasRole('super-admin')){
        $content = Content::where('id', $id)->first();

        return view('front.contents.page', [
            "files" => $content->type == Content::TYPE_MOVIE ? '//media.globalgeonews.com/movies/' .  str_replace("/files/1/", "", $content->content):'',
            "content" => $content,
            "expert" => $this->users->find($content->created_by),
            "link" => $content->type == Content::TYPE_MOVIE
                ? '//media.globalgeonews.com/movies/' . str_replace("/files/1/", "", $content->translate('fr')->content ? $content->translate('fr')->content : $content->translate('en')->content)
                : null
        ]);
      } else {
          abort(404);
      }
    }

    public function forMoreContent()
    {
        return view('front.contents.more.page');
    }

     public function rss()
    {
        $contents = Content::where( [['status', '=', Content::STATUS_PUBLISHED], ['type','LIKE', 'RICHTEXT'] ])->get();
        return response()->view('front.contents.rss', compact('contents'))->header('Content-Type', 'text/xml'); 
    }
}
