<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\UserProfile;
use App\Models\Content;
use App\Models\Country;
use App\Models\Zone;
use App\Models\UserZone;

use App\Repositories\UserRepository;
use App\Repositories\ZoneRepository;

use Auth;
use Session;

class ContributorController extends Controller
{
    public function __construct(UserRepository $users, ZoneRepository $zones)
    {
        $this->users = $users;
        $this->zones = $zones;
    }

    public function index(Request $request)
    {
        $contributors = $this->users->with('profile')->whereHas('roles', function ($query) {
            $query->where('name', 'expert');
        })->get();

        $contributors = $contributors->sortBy(function ($contributor, $key) {
            return isset($contributor->profile->order) ? $contributor->profile->order : null;
        });

        return view('frontend/contributors/liste', [
            'users' => $contributors
        ]);
    }

    public function show($id, Request $request, $zoneId = null, $countryId = null)
    {
        $user = $this->users->with('profile')->whereHas('roles', function ($query) use($id) {
            $query->where('name', 'expert');
        })->find($id);

        return view('frontend/contributors/page', [
            'user' => $user
        ]);
    }

    public function zone($id)
    {
        $zone = $this->zones->find($id);
        $contributors = $zone->experts()->with('profile')->get();

        $contributors = $contributors->sortBy(function ($contributor, $key) {
            return isset($contributor->profile->order) ? $contributor->profile->order : null;
        });

        return view('frontend/contributors/liste', [
            'users' => $contributors
        ]);
    }
}
