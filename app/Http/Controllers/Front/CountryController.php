<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Country;
use App\Models\Zone;
use App\Models\Theme;
use App\Repositories\ContentRepository;
use Auth;
use Session;
use App\Models\User;

class CountryController extends Controller
{

    public function __construct(ContentRepository $contents)
    {
        $this->contents = $contents;
        $this->itemsPerPage = 20;
    }

    public function contributors($id, Request $request)
    {
        $country = Country::find($id);

        $ids = Content::where('country_id', $country->id)->where('status', Content::STATUS_PUBLISHED)->get()->pluck('created_by')->toArray();

        $contributors = User::whereIn('id', $ids)->whereHas('roles', function ($query) use($id) {
            $query->where('name', 'expert');
        })->get();

        return view('front.countries.contributors', [
            "country" => $country,
            "contributors" => $contributors
        ]);
    }

    public function index($id, $theme_slug = null, Request $request)
    {
        $contents =$this->contents->searchByCountryTheme($id, $theme_slug? Theme::findBySlug( $theme_slug )->id : null );
        return view('front.countries.page', [
            "country" => Country::find($id),
            "contents" => $contents,
            "count" => count($contents),
            "itemsPerPage" => $this->itemsPerPage,
            "themeSlug" => $theme_slug,
            "page" => $request->get('page') ? $request->get('page') : 0
        ]);
    }
}
