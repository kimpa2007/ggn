<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelLocalization;
use App\Models\CustomPage;

class CustomPageController extends Controller
{
    public function show($slug)
    {
    	$page = CustomPage::findBySlug($slug);

        if(!$page->translate(LaravelLocalization::getCurrentLocale())->slug == $slug){
            return redirect()->route('custompage', $page->translate(LaravelLocalization::getCurrentLocale())->slug);
        }

        return view('front.custompages.page',[
            'page' => $page
        ]);
    }
}
