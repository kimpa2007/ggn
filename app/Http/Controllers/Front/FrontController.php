<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Zone;
use App\Models\Country;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\Partner;
use App\Models\Content;

use App\Jobs\SendContact;
use App\Jobs\Content\AddContentView;

use App\Http\Requests\ContactRequest;

use App\Repositories\ZoneRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ContentRepository;
use App\Repositories\UserRepository;

use Auth;
use Session;
use Redis;
use JWTAuth;

class FrontController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        ZoneRepository $zoneRepository,
        CountryRepository $countryRepository,
        ContentRepository $contentRepository,
        UserRepository $userRepository
    ) {
        $this->users = $userRepository;
        $this->zones = $zoneRepository;
        $this->country = $countryRepository;
        $this->contents = $contentRepository;
        $this->itemsPerPage = 20;
        $this->page = 1;
        $this->pageAnalysis = 1;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->get('page')) {
            $this->page = $request->get('page');
        }

        $contentsLatestNews = Content::where([['latest_news','=', '1'] ,['status' ,'=' ,'PUBLISHED']])
        ->join('latestnews_order','latestnews_order.content_id','=','contents.id')
        ->orderBy('latestnews_order.order_news')
        ->get();


    /*    $contentsLatestNewsQuery = $this->contents->getPublished()
            ->where('latest_news', '=', '1')
            ->orderBy('published_at', 'desc');
            $contentsLatestNews = $contentsLatestNewsQuery
            ->offset(($this->page-1) * $this->itemsPerPage)
            ->limit($this->itemsPerPage)
            ->get();*/

        if ($request->get('pageAnalysis')) {
            $this->pageAnalysis = $request->get('pageAnalysis');
        }

        $contentsAnalysisQuery = $this->contents->searchByTheme(2, $this->page, $this->itemsPerPage);
        $content_search = $this->contents;

        return view('home.home', [
            "contentsLatestNews" => $contentsLatestNews,
            "contentsAnalysis" => $contentsAnalysisQuery,
            "contentsAnalysis_count" => $contentsAnalysisQuery->count(),
            "users_count" => null,
            "itemsPerPage" => $this->itemsPerPage,
            "country_count" => null,
            'content_search' => $content_search
        ]);
    }


    public function content($id)
    {
        $content = Content::where('id', $id)
            ->where('status', Content::STATUS_PUBLISHED)
            ->first();

        if (!isset($content)) {
            //return 400 page not found
            abort(404);
        }

        if (!Auth::user()->canAccessContent($content)) {
            abort(403);
        }

        if (Auth::user()) {
            $this->dispatchNow(new AddContentView(Auth::user(), $content));
        }
        if($content->type == Content::TYPE_MOVIE) {
            $file = str_replace("/files/1/", "", $content->content);
            $link ='//media.globalgeonews.com/movies/'.$file;

        } else {
                $link ='';
        }
		     $expert = $this->users->find($content->created_by);
        return view('content-free', [
          "link" => $link,
          "content" => $content,
          "expert" => $expert
        ]);
    }

      public function showPreview($id)
      {
        if(Auth::user()->hasRole('super-admin')){
          $content = Content::where('id', $id)->first();
          if($content->type == Content::TYPE_MOVIE) {
              $file = str_replace("/files/1/", "", $content->content);
              $link ='//media.globalgeonews.com/movies/'.$file;
          } else {
                  $link ='';
          }
		        $expert = $this->users->find($content->created_by);
          return view('content-free', [
            "link" => $link,
            "content" => $content,
            "expert" => $expert
          ]);
        } else {
            abort(404);
        }
      }

    public function contact()
    {
        return view('contact', [
         'user' => Auth::user()
       ]);
    }
    public function forMoreContent()
    {
        return view('for-more-content');
    }
    /**
    */
    public function sendContact(ContactRequest $request)
    {
        if ($this->dispatchNow(SendContact::fromRequest($request))) {
            $notification = [
          'message' => 'Message envoyé avec succès !',
          'alert-type' => 'success'
        ];
        } else {
            $notification = [
              'message' => 'Une erreur s\'est produite',
              'alert-type' => 'error'
            ];
        }

        return redirect()->route('contact')->with($notification);
    }


    public function autologin(Request $request)
    {

        //JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();

        if ($user) {
            Auth::loginUsingId($user->id);

            return redirect('/');
        }
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    { 
      return view('front.login.login', []);
    }
      
       public function maintenance()
    {
        return view('maintenance');
    }
}
