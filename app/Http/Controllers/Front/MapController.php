<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Zone;
use App\Models\Country;
use App\Models\MapLegend;
use App\Models\Content;

use App\Repositories\ZoneRepository;
use App\Repositories\ContentRepository;
use Auth;

use LaravelLocalization;
use Carbon\Carbon;

class MapController extends Controller
{
    public function __construct(ContentRepository $contents, ZoneRepository $zones)
    {
        $this->contents = $contents;
        $this->zones = $zones;
    }

    public function show()
    {
        return view('map.show');
    }

    public function getData()
    {
        $zonesQuery = $this->zones->getPublished();

        if(Auth::user()){
            $zonesQuery->whereIn('id', Auth::user()->getAllowedZones());
        }
        $zones = $zonesQuery->get();

        $countriesIds = [];
        foreach($zones as $zone) {
            foreach($zone->countries as $country) {
                $countriesIds[] = $country->id;
            }
        }


        $contents = Content::whereIn('country_id', $countriesIds)
            ->where([
                'has_map' => 1,
                'status' => Content::STATUS_PUBLISHED
            ])
            ->whereHas('map', function ($query) {
                $query->whereDate('start_at', '<=', Carbon::now());
                $query->whereDate('end_at', '>=', Carbon::now());
            })
            ->get();


        $data = [
            'defaultStyle' => config('homemap.defaultStyle'),
            'selectedStyle' => config('homemap.selectedStyle'),
            'legends' => [],
            'countries' => [],
            'markers' => []
        ];

        foreach(MapLegend::where('status', MapLegend::STATUS_ENABLE)->get() as $legend) {
            $data['legends'][] = [
                'id' => $legend->id,
                'name' => $legend->translate(LaravelLocalization::getCurrentLocale())->name,
                'icon' => 'storage' . $legend->icon,
                'color' => $legend->color
            ];
        }

        foreach(Country::all() as $country) {
            $data['countries'][] = [
                'iso' => $country->iso3,
                'url' => route('country', $country->id)
            ];
        }

        if($contents) {
            foreach($contents as $content) {
                if($content->has_map) {
                    $latlng = json_decode($content->map->latlng);

                    if(!empty($latlng)){
                        $data["markers"][] = [
                            'latlng' => [$latlng->lat, $latlng->lng],
                            'legend_id' => $content->map->legend_id,
                            'url' => route('user.content.show',$content->id)
                        ];
                    }
                }

            }
        }

        return response()->json($data);
    }
}
