<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\ContentRepository;

class SearchController extends Controller
{
    public function __construct(ContentRepository $contentRepository)
    {
        $this->contents = $contentRepository;
        $this->itemsPerPage = 20;
    }

    public function index(Request $request)
    {
        $contents = $this->contents->searchByTag($request->get('q'));

        return view('search', [
            'contents' => $contents ? $contents->paginate($this->itemsPerPage) : null
        ]);
    }
}
