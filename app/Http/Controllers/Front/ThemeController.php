<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Zone;
use App\Models\Country;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\Partner;
use App\Models\Content;
use App\Models\Theme;

use App\Jobs\SendContact;
use App\Jobs\Content\AddContentView;

use App\Http\Requests\ContactRequest;

use App\Repositories\ZoneRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ContentRepository;
use App\Repositories\UserRepository;

use Auth;
use Session;
use Redis;
use JWTAuth;

class ThemeController extends Controller
{
    public function __construct(
        ZoneRepository $zoneRepository,
        CountryRepository $countryRepository,
        ContentRepository $contentRepository,
        UserRepository $userRepository
    ) {
        $this->users = $userRepository;
        $this->zones = $zoneRepository;
        $this->country = $countryRepository;
        $this->contents = $contentRepository;
        $this->itemsPerPage = 20;
        $this->page = 1;
        $this->pageAnalysis = 1;
    }

    public function show($slug, Request $request)
    {
        $theme = Theme::findBySlug( $slug );
        $page = $request->get('page') ? $request->get('page') : 0;
        $contents = $this->contents->searchByTheme($theme->id, $page, $this->itemsPerPage);


        return view('front.themes.page', [
            'count' => $this->contents->searchByTheme($theme->id)->count(),
            'contents' => $contents,
            'theme' => $theme,
            "itemsPerPage" => $this->itemsPerPage,
        ]);
    }
}
