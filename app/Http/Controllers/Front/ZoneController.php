<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Zone;
use App\Models\Content;
use App\Models\Country;

use Illuminate\Support\Facades\Response;

use App\Repositories\ZoneRepository;
use App\Repositories\UserRepository;

use Auth;
use Session;

class ZoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ZoneRepository $repository,UserRepository $userRepository)
    {
        //$this->middleware('auth');
        $this->zoneRepository = $repository;
        $this->userRepository = $userRepository;

        //contents counter
        $this->itemsPerPage = 6;
    		$this->page = 1;

        //zones countes
        $this->zonesPerPage = 8;
    		$this->zonesPage = 1;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $zonesQuery = $this->zoneRepository->getPublished();

      if(Auth::user()){
        $zonesQuery->whereIn('id',Auth::user()->getAllowedZones());
      }

      //proces the pages
      if($request->get('page')){
        $this->zonesPage = $request->get('page');
      }

  		$zones = $zonesQuery
        ->offset(($this->zonesPage-1) * $this->zonesPerPage)
        ->limit($this->zonesPerPage)
        ->get();

  		return view('zones',[
  			"zones" => $zones,
        "zones_count" => $zonesQuery->count(),
        "itemsPerPage" => $this->zonesPerPage,
        "page" => $this->zonesPage
  		]);
    }

    public function show(Request $request,$zoneId,$countryId=null)
    {
      if(!Auth::user()){
        abort(403);
      }

      //if this zone is not avaiblae for this user
      $userHasPartner = false;
      if(Auth::user() && Auth::user()->partner){
        $userHasPartner = true;

        $availableZones = Auth::user()->partner->first()->getZonesIds();
        if(!in_array($zoneId,$availableZones)){
          abort(403);
        }
      }

  		$zone = Zone::find($zoneId);

  		if(!isset($zone)){
  			//zone exists
  			abort(404);
  		}

      if($countryId != null){
        //countrie dons not exists
        if(!Country::where('id',$countryId)->exists()){
          abort(404);
        }

        //this countrie is not of this zone
        $countrie = Country::find($countryId);
        if($countrie->zone->id != $zoneId){
          abort(404);
        }
      }

  		$countries = $zone->countries()->whereNull('parent_id')->get();

      $contentQuery = Content::whereIn('country_id', $countries->pluck('id')->toArray())->where('status',Content::STATUS_PUBLISHED);


      //perform the filters
      if(!$userHasPartner){
        //if no parent, return void contents
        $contentQuery = $contentQuery->where('country_id',0);
      }
      else if($countryId != null){
        //came from a direct link
        $countriesIds = [];
        $countriesIds[] = $countryId;
        $countriesIds = array_merge($countriesIds,Country::getTreeIds($countryId));


        $contentQuery = $contentQuery->whereIn('country_id',$countriesIds);
      }
      else if($request->get('countries')){
        //came from ajax directly
        $countriesIds = $request->get('countries');

        //retrieve and array with selected
        foreach($request->get('countries') as $countryId){

            $countriesIds = array_merge($countriesIds,Country::getTreeIds($countryId));
        }

        $contentQuery = $contentQuery->whereIn('country_id',$countriesIds);
      }

      if($request->get('filters')){
        $contentQuery = $contentQuery->whereIn('type',$request->get('filters'));
      }

      //count the result
      $contentsCount = $contentQuery->count();

      //proces the pages
      if($request->get('page')){
        $this->page = $request->get('page');
      }

  		$contents = $contentQuery->offset(($this->page-1) * $this->itemsPerPage)
  			->limit($this->itemsPerPage)
        ->get();


      $expertsCount = $zone->experts->count();

      $expert = null;

      if($expertsCount == 1){
        $expert = $zone->experts->first();
        $recipient_id = $expert->id;
      }
      else {
        //if more than one expert or no expert
        $firstAdmin = $this->userRepository->admins()->first();
        $recipient_id = $firstAdmin->id;
      }

  		$data = [
  			"zone" => $zone,
        "expert" => $expert,
        "recipient_id" => $recipient_id,
  			"countries" => $countries,
  			"contents" => $contents,
        "contentsCount" => $contentsCount,
        "itemsPerPage" => $this->itemsPerPage
  		];

      if($countryId != null)
        $data["countryId"] = $countryId;

  		return view('zone',$data);

    }
}
