<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ContentRepository;
use Illuminate\Support\Facades\Response;
use Storage;
use App\Models\Content;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ContentRepository $repository)
    {
     	$this->repository = $repository;
    }

    public function importTheme(){
        $file = fopen(Storage::path("../import/contenu-theme.csv"),'r');
        $i = 0;

        while ( ($data = fgetcsv($file, 1000, ",")) !==FALSE )
        {
            if($i > 0){
                $themes = [];

                $id = $data[0];
                $is_actif = $data[1];
                $politique_internationale = $data[2];
                $economie = $data[3];
                $analyse = $data[4];
                $societe = $data[5];
                $environnement = $data[6];

                if($politique_internationale == 1){
                    $themes[] =  1;
                }

                if($economie == 1){
                    $themes[] =  3;
                }

                if($analyse == 1){
                    $themes[] =  2;
                }

                if($societe == 1){
                    $themes[] =  4;
                }

                 if($environnement == 1){
                    $themes[] =  5;
                }

                if($is_actif == 1){
                    $status = "PUBLISHED";
                }else{
                    $status = "DRAFT";
                }
                $content = Content::find($id);

                if(empty($content)){
                    //echo "PB: ".$id;
                }else{
                    $content->status = $status;
                    $content->themes()->sync($themes);
                    $content->save();
                }
            }

            $i++;

        }

        fclose($file);
    }
}
