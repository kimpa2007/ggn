<?php

namespace App\Http\Controllers\Offers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Zone;
use App\Models\Country;
use App\Models\MapLegend;
use App\Models\Content;
use App\Models\PartnerPlan;
use App\Models\PartnerOrder;

use App\Models\CicPayment;

use App\Repositories\ZoneRepository;
use App\Repositories\ContentRepository;
use Illuminate\Support\Facades\Response;

use Auth;

use LaravelLocalization;
use Carbon\Carbon;

class OfferController extends Controller
{

    public function show()
    {
        $abonnements = PartnerPlan::where('type', 'PUBLIC')->where('status', 'ACTIVE')->get();
        return view('offer.show', ['abonnements' => $abonnements ]);
    }

    public function createCommand(Request $request)
    {


        $params = $request->all();
        $current_user = \Auth::user();

       // dump($current_user->partner());

        $plan = PartnerPlan::where('id',  $params['plan_id'])->first();

        $order = new PartnerOrder;
        $order->status= PartnerOrder::STATUS_PENDING;
        $order->plan_id = /*$plan*/ $params['plan_id'];
        $order->created_at = date('Y-m-d H:i:s');
        $order->type_payment = PartnerOrder::PAYMENT_CB;
        $order->partner_id = $current_user->partner_id;
        $order->amount = $plan->price;
        $order->type = PartnerOrder::TYPE_PLAN;
        
        if(array_key_exists('cursus', $params)){
            $order->cursus = $params['cursus'];
        }

        if(array_key_exists('ecole', $params)){
            $order->ecole = $params['ecole'];
        }

         if(array_key_exists('promo', $params)){
            $order->promo_code = $params['promo'];
        }

        if($order->save()){
            $cic_payment = new CicPayment();
            $cic_payment->initialize($order);

            return Response::json([
                     'error' => false,
                     'id'  =>  $order->id,
                     'form' => $cic_payment->generateForm()
            ]);
        }

        return Response::json([
                     'error' => true,
        ]);

    }

    public function getData()
    {
    }


      public function check(Request $request)
    {
        $params = $request->all();
        file_put_contents('test.txt', serialize($params).PHP_EOL, FILE_APPEND);
        $cic = new CicPayment();
        $cic->check($params);
    }

    public function ok(Request $request)
    {
        return view('offer.confirmation_ok');
    }

    public function ko(Request $request)
    {
        return view('offer.confirmation_ko');
    }
}
