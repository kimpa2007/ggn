<?php

namespace App\Http\Controllers\Partner;

use App\Http\Requests\Partner\UpdateInfoRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Partner;

use Auth;

class AccountController extends Controller
{
    /**
     * Main partners account dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $partner = $user->partner;

        return view('partner.info')->with(compact('user','partner'));
    }

    /**
     * Update partner info.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInfoRequest $request)
    {
        $partner = Partner::findOrFail(Auth::user()->partner_id);

        if($partner->manager_id == Auth::user()->id){
            $partner->fill($request->all());
            $partner->type = $partner->type;
            $partner->save();
        }
        return redirect()->route('partner.account.index', $partner->id)->with('flash_message', "Enregistrement effectué avec succès");

    }

}
