<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Models\Partner;

use App\Jobs\SendContact;

use Auth;

class ContactController extends Controller
{
    /**
     * Main partners contact page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$user = Auth::user();
    	$partner = Partner::findOrFail( $user->partner_id );
        return view('partner.contact',compact('user','partner'));
    }

    public function submit(ContactRequest $request)
    {

        if($this->dispatchNow(SendContact::fromRequest($request))){
          $notification = [
            'message' => 'Message envoyé avec succès !',
            'alert-type' => 'success'
          ];
        }
        else {
          $notification = [
            'message' => 'Une erreur s\'est produite',
            'alert-type' => 'error'
          ];
        }

        return redirect()->route('partner.contact')->with($notification);
    }
}
