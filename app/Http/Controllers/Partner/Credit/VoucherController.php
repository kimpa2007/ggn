<?php

namespace App\Http\Controllers\Partner\Credit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Partner\CreateVoucherRequest;
use App\Jobs\Partner\CreateVoucher;

use App\Models\Partner;
use Auth;
use Session;

class VoucherController extends Controller
{
    public function index()
    {
        return view('partner.credits.voucher.index');
    }

    public function process(CreateVoucherRequest $request)
    {
        try {
            $this->dispatchNow(CreateVoucher::fromRequest(Auth::user()->partner, $request));
            return redirect()->route('partner.credits.voucher.success');
        } catch (\Exception $e) {
            Session::flash('notify_error', $e->getMessage());
        }

        return redirect()->route('partner.credits.voucher');
    }

    public function success()
    {
        return view('partner.credits.voucher.success');
    }

}
