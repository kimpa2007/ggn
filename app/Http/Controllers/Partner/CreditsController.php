<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Partner\OrderCreditRequest;
use App\Http\Requests\Partner\CreditsDistributionRequest;
use App\Http\Requests\Admin\Partner\SaveCreditRequest;

use App\Jobs\Partner\OrderCredit;
use App\Jobs\Partner\DistributeCreditsUsers;
use App\Jobs\Partner\DivideCreditsUsers;

use App\Repositories\UserRepository;
use App\Repositories\CreditOperationRepository;

use App\Models\Partner;
use App\Models\PartnerOrder;
use App\Models\CreditOperation;

use Illuminate\Support\Facades\Response;

use Exception;
use Datatables;
use Auth;
use Session;

class CreditsController extends Controller
{
    public function __construct(UserRepository $userRepository, CreditOperationRepository $creditOpsRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
        $this->creditOpsRepository = $creditOpsRepository;
    }

    /**
     * Main partners credits page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partner.credits.history');
    }

    public function getSubventionsHistoryData()
    {
        $partner_id = Auth::user()->partner_id;

        if ($partner_id == null) {
            abort(500);
        }

        $creditOperations = $this->creditOpsRepository->getPartnerSubventionPetitions($partner_id);

        return Datatables::of($creditOperations)
            ->addColumn('type', function ($item) {
                return CreditOperation::getTypes()[$item->type];
            })
            ->addColumn('status', function ($item) {
                $status = CreditOperation::getStatusOptions();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->addColumn('done_at', function ($item) {
                return \Carbon\Carbon::parse($item->done_at)->format('d-m-Y');
            })
            ->make(true);
    }


    public function distribution()
    {
        return view('partner.credits.distribution');
    }

    public function getDistributionData()
    {

      //retrieve all users of this partner
      $users = $this->userRepository->activeUsersForPartner(Auth::user()->partner_id)->orderBy('id', 'ASC');

        return Datatables::of($users)
          ->addColumn('name', function ($item) {
              return $item->getFullNameAttribute();
          })
          ->addColumn('solde', function ($item) {
              return $item->credits;
          })
          ->addColumn('action', function ($item) {
              return '<a href="#" class="btn btn-secondary-2 btn-table toggle-minus" data-id="'.$item->id.'"> - </a>'.
              '<input type="text" class="credits-value" data-id="'.$item->id.'" name="credits" value="0">'.
              '<a href="#" class="btn btn-secondary-2 btn-table toggle-plus" data-id="'.$item->id.'"> + </a>';
          })

          ->addColumn('futur_solde', function ($item) {
              return $item->credits;
          })

          ->make(true);
    }

    public function saveDistribution(CreditsDistributionRequest $request)
    {
        try {
            $partner = Auth::user()->partner;

            if ($partner == null) {
                throw new Exception("Partner is not defined");
            }
            $users = json_decode($request->get('users'), true);

            $this->dispatch(new DistributeCreditsUsers($partner, $users, CreditOperation::TYPE_PARTNER_GIFT));

            $notification = [
              'message' => 'Enregistrement effectué avec succès.',
              'alert-type' => 'success'
            ];
        } catch (\Exception $e) {
            $notification = [
              'message' => $e->getMessage(),
              'alert-type' => 'error'
            ];
        }

        return redirect()
            ->route('partner.credits.distribution')
            ->with($notification);
    }

    public function saveSameCredits(SaveCreditRequest $request)
    {
        try {
            $partner = Auth::user()->partner;

            if ($partner == null) {
                throw new Exception("Partner is not defined");
            }

            $this->dispatch(DivideCreditsUsers::fromRequest($partner, $request));

            $notif = [
                'message' => 'Enregistrement effectué avec succès.',
                'alert-type' => 'success'
            ];
        } catch (\Exception $e) {
            $notif = [
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            ];
        }

        return redirect()
            ->route('partner.credits.distribution')
            ->with($notif);
    }


    // --------------------------------------------- //
    // Buy credit process
    // --------------------------------------------- //
    public function order()
    {
        return view('partner.credits.order.index');
    }

    public function processOrder(OrderCreditRequest $request)
    {
        try {
            $order = $this->dispatchNow(OrderCredit::fromRequest(Auth::user()->partner, $request));

            $notification = [
              'message' => 'Achat effectué avec succès.',
              'alert-type' => 'success'
            ];

            return redirect()->route('partner.credits.order.success', [
                'order' => $order
            ])->with($notification);

        } catch (\Exception $e) {
            //Session::flash('notify_error', $e->getMessage());

            $notification = [
              'message' => $e->getMessage(),
              'alert-type' => 'error'
            ];
        }

        return redirect()->route('partner.credits.order')->with($notification);
    }

    public function orderSuccess(PartnerOrder $order)
    {
        return view('partner.credits.order.success', [
            'order' => $order
        ]);
    }
    // --------------------------------------------- //
}
