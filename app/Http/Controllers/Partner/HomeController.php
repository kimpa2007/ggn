<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\ZoneRepository;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ZoneRepository $repository)
    {
        $this->middleware('auth');
        $this->zoneRepository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $zonesQuery = $this->zoneRepository->getPublished();
        $zones = $zonesQuery->get();
        $allowedZones = Auth::user()->getAllowedZones();
        $user = Auth::user();

        $contents = [];
        return view('partner.home',compact('user',
          'contents','zones','allowedZones'));
    }
}
