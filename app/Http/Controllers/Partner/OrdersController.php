<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\PartnerOrderRepository;

use App\Models\PartnerOrder;
use App\Models\Content;

use Auth;
use Datatables;

class OrdersController extends Controller
{

    public function __construct(PartnerOrderRepository $orders)
    {
        $this->orders = $orders;
    }

    public function index()
    {
        return view('partner.orders.index');
    }

    public function data()
    {
        $orders = $this->orders->all()->where('partner_id', Auth::user()->partner_id);

        return Datatables::of($orders)

            ->editColumn('plan', function ($item) {
                return $item->getPlanName();
            })

            ->editColumn('status', function ($item) {
                return $item->getStatusName();
            })
            
            ->editColumn('created_at', function ($item) {
                if(empty($item->created_at)) return "";
                return $item->created_at->format('d/m/Y');
            })
            
            ->editColumn('start_at', function ($item) {
                if(empty($item->start_at)) return "";
                return $item->start_at->format('d/m/Y');
            })

            ->editColumn('end_at', function ($item) {
                if(empty($item->end_at)) return "";                
                return $item->end_at->format('d/m/Y');
            })

            ->editColumn('amount', function ($item) {
                return $item->amount . ' &euro;';
            })           
            
            ->addColumn('action', function ($item) {
                switch($item->status) {
                    case PartnerOrder::STATUS_PENDING:
                        return '<a href="' . route('partner.orders.pay', $item->id) . '">Payer cette commande</a> | <a href="' . route('partner.orders.cancel', $item->id) . '" class="toggle-cancel">Annuler</a>';
                    break;
                }
            })
            
            ->make(true);
    }

    public function cancel($id, Request $request)
    {
        $partnerOrder = PartnerOrder::find($id);
        $notif = [
            'message' => 'Une erreur s\'est produite',
            'alert-type' => 'error'
        ];

        if($partnerOrder) {
            $partnerOrder->status = PartnerOrder::STATUS_CANCELED;
            if($partnerOrder->save()) {
                $notif = [
                    'message' => 'Commande annulée avec succès.',
                    'alert-type' => 'success'
                ];
            }
        }

        return redirect()
            ->route('partner.orders')
            ->with($notif);
    }

    public function pay($id, Request $request)
    {
        return view('partner.orders.pay', [
            'order' => PartnerOrder::find($id)
        ]);
    }
}
