<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    /**
     * Main partners forfaits dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$partner = Partner::with(['orders,plans'])

        return view('partner.plans',compact('partner'));
    }
}
