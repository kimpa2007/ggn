<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PartnerPlanRepository;
use App\Repositories\PartnerOrderRepository;
use App\Repositories\PlanPurposedRepository;
use App\Models\Partner;
use App\Models\PartnerOrder;
use App\Models\PartnerPlan;
use App\Models\PlanPurposed;
use Datatables;
use App;
use Auth;


use App\Jobs\Partner\OrderPlan;

class PlansController extends Controller
{

    public function __construct(PartnerPlanRepository $plans, PartnerOrderRepository $orders,  PlanPurposedRepository $purposed)
    {
        $this->plans = $plans;
        $this->orders = $orders;
        $this->purposed = $purposed;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('partner.plans.index', [
            'partner' => Partner::findOrFail(Auth::user()->partner_id)
        ]);
    }

    public function getData()
    {
        if (!Auth::user()->partner_id) {
            App::abort(403, 'Unauthorized action.');
        }

        $orders = Auth::user()->partner->orders->where('type', PartnerOrder::TYPE_PLAN);

        $planOrders = collect();
        if($orders) {
            foreach($orders as $order) {
                $arr = [
                    'id' => $order->id,
                    'name' => $order->plan->name,
                    'zones' => $order->plan->zones->map(function ($PlanZone) {
                        return $PlanZone->name;
                    })->toArray(),
                    'status' => $order->status,
                    'status_name' => $order->getStatusName(),
                    'time' => $order->plan->time,
                    'start_at' => $order->start_at,
                    'end_at' => $order->end_at,
                    'type' => 'ORDER'
                ];

                $planOrders->push((object) $arr);
            }
        }


        $plansPurposed = Auth::user()->partner->plansPurposed;
        if($plansPurposed) {
            foreach($plansPurposed as $purpose) {
                $arr = [
                    'id' => $purpose->id,
                    'name' => $purpose->plan->name,
                    'zones' => $purpose->plan->zones->map(function ($PlanZone) {
                        return $PlanZone->name;
                    })->toArray(),
                    'status' => $purpose->status,
                    'status_name' => $purpose->getStatusName(),
                    'time' => $order->plan->time,
                    'type' => 'PURPOSE'
                ];

                $planOrders->push((object) $arr);
            }
        }

        return Datatables::of($planOrders)
            ->addColumn('duration', function ($item) {
                return $item->time . ' mois';
            })
            ->editColumn('status', function ($item) {
                return $item->status_name;
            })
            ->addColumn('actions', function ($item) {
                if($item->type == "ORDER") {
                    if ($item->status == PartnerOrder::STATUS_PENDING) {
                        return '<a href="' . route('partner.plans.order.confirm', $item->id) . '" class="btn btn-secondary-2 btn-table ">Payer</a> &nbsp; '
                        . '<a href="' . route('partner.plans.order.cancel', $item->id) . '" data-id="'.$item->id.'" class="btn btn-secondary-2 btn-table color-danger toggle-delete">Annuler</a>';
                    }
                } else {
                    if ($item->status == PlanPurposed::STATUS_PURPOSED) {
                        return '<a href="' . route('partner.plans.purpose.confirm', $item->id) . '" class="btn btn-secondary-2 btn-table ">Acheter</a> &nbsp; '
                        . '<a href="' . route('partner.plans.purpose.refuse', $item->id) . '" data-id="'.$item->id.'" class="btn btn-secondary-2 btn-table color-danger toggle-delete">Refuser</a>';
                    }
                }

            })->rawColumns(['actions'])
        ->make(true);
    }


    public function orderConfirm($id)
    {
        return view('partner.plans.order', [
            'order' => $this->orders->find($id)
        ]);
    }


    public function orderSave($id, Request $request)
    {
        $order = $this->orders->find($id);

        if($order) {
            $order->type_payment = $request->get('payment_type');
            $order->save();
        }

        return view('partner.plans.success', [
            'order' => $order
        ]);
    }

    public function orderCancel($id)
    {
        try {
            $order = $this->orders->find($id);
            $order->status = PartnerOrder::STATUS_CANCELED;

            if($order->save()) {
                $notif = [
                    'message' => 'Forfait annulé',
                    'alert-type' => 'success'
                ];
            }
        } catch (\Exception $ex) {
            $notif = [
                'message' => $ex->getMessage(),
                'alert-type' => 'error'
            ];
        }

        return redirect()
            ->route('partner.plans')
            ->with($notif);
    }


    public function purposeConfirm($id)
    {
        return view('partner.plans.purpose', [
            'purpose' => $this->purposed->find($id)
        ]);
    }

    public function purposeSave($id, Request $request)
    {
        $purpose = $this->purposed->find($id);

        $order = $this->dispatchNow(new OrderPlan(Auth::user()->partner, $purpose->plan, [
            'payment_type' => $request->get('payment_type')
        ]));

        if($order) {
            //$purpose->status = PlanPurposed::STATUS_ACCEPTED;
            //$purpose->save();
            $purpose->delete();
        }

        return view('partner.plans.success', [
            'order' => $order
        ]);
    }

    public function purposeRefuse($id)
    {
        try {
            $purpose = $this->purposed->find($id);
            $purpose->status = PlanPurposed::STATUS_REFUSED;

            if($purpose->save()) {
                $notif = [
                    'message' => 'Proposition refusée',
                    'alert-type' => 'success'
                ];
            }
        } catch (\Exception $ex) {
            $notif = [
                'message' => $ex->getMessage(),
                'alert-type' => 'error'
            ];
        }

        return redirect()
            ->route('partner.plans')
            ->with($notif);
    }
}
