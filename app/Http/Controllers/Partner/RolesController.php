<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Requests\Partner\UpdatePartnerUserRolesRequest;

use Auth;

use App\Models\Role;

class RolesController extends Controller
{
      
     /**
     * See roles from user.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles($id)
    {
    	$user = User::findOrFail($id);

        return view('partner.users.roles',compact('user'));
    }

    /**
     * Update user roles from partner account
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRoles(UpdatePartnerUserRolesRequest $request,$id)
    {
    	$user = User::findOrFail($id);
    	//security
    	if($user->partner_id !== $request['partner_id'] && $user->partner_id !== Auth::user()->partner_id){
    		abort(403, 'Unauthorized action');
    	}

    	switch ($request['permission']) {
    		case 'manage-users':
    			$role = Role::where('name','social-manager')->first();
    			break;
    		case 'manage-credits':
    			$role = Role::where('name','credit-manager')->first();
    			break;
    		case 'buy-content':
    			$role = Role::where('name','plan-manager')->first();
    			break;
    	}

    	//
    	if($user->can($request['permission'])){
    		$user->roles()->detach($role);
    		$action = "supprimé de";
    	}else{
    		$user->attachRole($role);
    		$action = "ajouté à";
    	}

    	$user->save();

        return redirect()->route('partner.users.roles',$id)->with(compact('user'))->with('flash_message','Le pouvoir a été '.$action.' '.$user->firstname.' avec succes.');
    }
}
