<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\ZoneRepository;

use App\Jobs\Partner\CreateThemePetition;

use App\Http\Requests\Partner\ThemePetitionRequest;

use Illuminate\Support\Facades\Response;

use Auth;

class ZonesController extends Controller
{
    public function __construct(ZoneRepository $repository)
    {
        $this->middleware('auth');
        $this->zoneRepository = $repository;

    }

    /**
     * Show the partners available zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zonesQuery = $this->zoneRepository->getPublished();

        $zones = $zonesQuery->get();

        return view('partner.zones',[
          "zones" => $zones,
          "allowedZones" => Auth::user()->getAllowedZones()
        ]);

    }

    public function sendThemePetition(ThemePetitionRequest $request)
    {
      try {

        $partner = Auth::user()->partner;

        if($partner == null ){
          throw new Exception("Partner is not defined");
        }

        $this->dispatchNow(CreateThemePetition::fromRequest($partner,$request));

        return Response::json([
                'error' => false,
                'code'  => 200
            ], 200);

      }
      catch(\Exception $e ) {
        return Response::json([
              'error' => true,
              'code' => 400,
              'message' => $e->getMessage()
          ], 400);
      }

    }
}
