<?php

namespace App\Http\Controllers\Partner;

use App\Http\Requests\Partner\UserValidationRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\UserRepository;
use App\Repositories\CreditOperationRepository;

use App\Models\User;
use App\Models\CreditOperation;

use Illuminate\Support\Facades\Response;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;

use App\Jobs\CreateUser;
use App\Jobs\UpdateUser;


use Datatables;
use Auth;
use Hash;
use Session;

use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $users, CreditOperationRepository $coRepository)
    {
        $this->users = $users;
        $this->coRepository = $coRepository;
        $this->middleware('auth');
    }

    /**
     * Show the partner's users list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partner.users.index');
    }

    public function create()
    {
        return view('partner.users.form');
    }

    public function show($id, Request $request)
      {
          return view('partner.users.form', [
              'user' => $this->users->find($id)
          ]);
      }

    public function save(CreateUserRequest $request)
    {
        $requestarray = $request;
        $requestarray['status'] = User::STATUS_ACTIVE;
        $requestarray['partner_id'] = Auth::user()->partner->id;

        $userId = $this->dispatchNow(CreateUser::fromRequest($requestarray));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('partner.users.index');
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $requestarray = $request;
        $requestarray['partner_id'] = Auth::user()->partner->id;
        $requestarray['role_id'] = '1';

        $this->dispatchNow(UpdateUser::fromRequest($user, $requestarray));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('partner.users.show', $user->id);
    }

     /**
     * Show the partner's users validation area.
     *
     * @return \Illuminate\Http\Response
     */

    public function getData()
    {
        $users = $this->users->activeUsersForPartner(Auth::user()->partner_id);

        return Datatables::of($users)
            ->addColumn('full_name', function ($item) {
                return $item->full_name;
            })
            ->filterColumn('full_name', function ($query, $keyword) {
                $sql = "CONCAT(users.firstname,'-',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($item) {
                $status = User::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->editColumn('created_at', function ($item) {
                return $item->created_at->format('d/m/Y');
            })
            ->addColumn('action', function ($item) {
                return '
                <a href="' . action('Partner\UserController@show', ['id' => $item->id]) . '" data-id="%d" class="btn btn-table trigger-active">Editer</a>';
            })
            ->make(true);
    }

/*
    public function validationUserData()
    {
        $users = $this->users->pendingUsersForPartner(Auth::user()->partner_id);

        return Datatables::of($users)
            ->addColumn('full_name', function ($item) {
                return $item->full_name;
            })
            ->filterColumn('full_name', function ($query, $keyword) {
                $sql = "CONCAT(users.firstname,'-',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($item) {
                $status = User::getStatus();
                return isset($status[$item->status]) ? $status[$item->status] : null;
            })
            ->editColumn('created_at', function ($item) {
                return $item->created_at->format('d/m/Y');
            })
            ->addColumn('action', function ($item) {
                return sprintf(
                    '<a href="#" data-id="%d" class="btn btn-secondary-2 btn-table trigger-active">Valider</a>
                    <a href="#" data-id="%d" class="btn btn-secondary-2 btn-table color-danger trigger-refuse">Refuser</a>',
                    $item->id,
                    $item->id
                );
            })
            ->make(true);
    }
*/
}
