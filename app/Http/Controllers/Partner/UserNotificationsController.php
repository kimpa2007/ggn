<?php

namespace App\Http\Controllers\Partner;

use App\Http\Requests\NotificationRequest;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\NotificationsRepository;

use App\Models\User;
use App\Models\UserNotification;
use App\Models\Notification;

use App\Jobs\CreateNotification;
use App\Jobs\UpdateNotification;

use Datatables;
use Auth;
use Carbon\Carbon;

class UserNotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NotificationsRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('auth');
    }



    public function getData()
    {
        $notes = $this->repository->notificationsByPartner(Auth::user()->partner_id);

        return Datatables::of($notes)
          ->editColumn('status', function ($item) {
              $status = Notification::getStatus();
              return isset($status[$item->status]) ? $status[$item->status] : null;
          })
          ->make(true);
    }

    public function getNotificationReadsData($id)
    {
        $users = $this->repository->usersPerNotification($id);
        //dd($users->first());
        return Datatables::of($users)
            ->addColumn('full_name', function ($item) {
                return $item->full_name;
            })
            ->filterColumn('full_name', function($query, $keyword) {
                $sql = "CONCAT(users.firstname,'-',users.lastname)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($item) {
                return $item->pivot->read_at?'Lu':'Pas lu';
            })
            ->addColumn('read', function ($item) {
                return $item->pivot->read_at? $item->pivot->read_at:'-';
            })
            ->make(true);
    }



    public function saveNotification(NotificationRequest $request){

        $notif = $this->dispatchNow(CreateNotification::fromRequest($request));
        if($request['status'] == 'SENT'){
            $users = User::atPartner(Auth::user()->partner_id)->get();
            $sync_data = [];
            foreach ($users as $key => $user) {
                $sync_data[$user->id] = ['status' => UserNotification::STATUS_NEW,'received_at'=>Carbon::now()];
                # code...
            }
            //dd($sync_data);
            $notif->users()->attach($sync_data);
            return redirect()->route('partner.users.notify.index')->with('flash_message', "Notification envoyée avec succès");
        }else{
            return redirect()->route('partner.users.notify.index')->with('flash_message', "Brouillon sauvgardé avec succès");
        }



    }






    /**
     * Send notifications interface.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partner.users.notify');
    }

    /**
     * Read notification.
     *
     * @return \Illuminate\Http\Response
     */
    public function readNotification($id)
    {
        $this->repository->readNotification($id);
        return redirect()->route('user.home');
    }

    /**
     * edit draft.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDraft($id)
    {
        $notification = $this->repository->getDraftNotification($id);
        return view('partner.users.notify-draft')->with(compact('notification'));
    }

    public function updateDraft($id,NotificationRequest $request){

        $notification = Notification::findOrFail($id);
        $notif = $this->dispatchNow(UpdateNotification::fromRequest($notification,$request));
        if($request['status'] == 'SENT'){
            $users = User::atPartner(Auth::user()->partner_id)->get();
            $sync_data = [];
            foreach ($users as $key => $user) {
                $sync_data[$user->id] = ['status' => UserNotification::STATUS_NEW,'received_at'=>Carbon::now()];
                # code...
            }
            //dd($sync_data);
            $notif->users()->attach($sync_data);
            return redirect()->route('partner.users.notify.index')->with('flash_message', "Notification envoyé avec succès");
        }else{
            return redirect()->route('partner.users.notify.index')->with('flash_message', "Draft actualisé avec succès");
        }



    }





}
