<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\UpdateInfoRequest;

use App\Jobs\UpdateUserInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Repositories\PartnerRepository;

use App\Repositories\ZoneRepository;
use App\Jobs\CreateUser;

use App\Models\User;
use App\Models\Role;
use App\Models\Partner;

use Auth;
use Session;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ZoneRepository $repository, PartnerRepository $partners)
    {
        $this->middleware('auth');
        $this->zoneRepository = $repository;
        $this->partners = $partners;

    }   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $zonesQuery = $this->zoneRepository->getPublished();
      $zones = $zonesQuery->get();
      $allowedZones = Auth::user()->getAllowedZones();
      $user = Auth::user();

    	$contents = $user->contentViews()->orderBy('viewed_at','DESC')->limit(5)->get();
    	$contents->load('content');

      $notifications = $user->newNotifications();

      return view('user.home',[
        		'contents'=>$contents
        	])->with(compact('user','notifications','zones','allowedZones'));
    }


    /**
     * Show the user infos form.
     *
     * @return \Illuminate\Http\Response
     */
    public function info()
    {
        $user = Auth::user();

        return view('user.info')->with(compact('user'));
    }


    /**
     * Update the user table.
     *
     * @method PUT
     * @param \App\Http\Requests\User\UpdateInfoRequest
     * @return \Illuminate\Http\Response
     */
    public function updateSelfInfo(UpdateInfoRequest $request)
    {
        $user = Auth::user();
        $this->dispatchNow(UpdateUserInfo::fromRequest($user, $request));

        return redirect()->route('user.info', $user->id)->with('flash_message', "Enregistrement effectué avec succès");

    }


    /**
     * Show the user's documents.
     *
     * @return \Illuminate\Http\Response
     */
    public function documents()
    {
        return view('user.documents');
    }

}
