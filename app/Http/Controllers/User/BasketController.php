<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Country;
use App\Models\Order;

use App\Jobs\User\ProcessOrder;

use Illuminate\Support\Facades\Response;

use Auth;
use Session;
use Validator;
use \Exception;

class BasketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $orderId = null)
    {
        $order = null;

        if ($orderId != null) {
            if (!Order::where('id', $orderId)->exists()) {
                abort(404);
            }

            $order = Order::find($orderId);

            //add order to session
            Session::put('basketOrder', $order);
        }

        //retrieve order from session
        $order = Session::get('basketOrder');

        $data = [
            "order" => $order,
            "credits" => Auth::user()->credits
        ];

        return view('user.basket-step-1', $data);
    }

    public function removeOrderFromBasket(Request $request)
    {
        //clean sesssion
        Session::forget('basketOrder');

        return redirect(route('user.panier'));
    }

    public function payment(Request $request)
    {
        $order = Session::get('basketOrder', null);
		$credits = Auth::user()->credits;

        if ($order == null) {
            return redirect(route('user.panier'));
        }

        return view('user.basket-step-2', [
            "order" => $order,
            "credits" => $credits,
            "firstOptionDisabled" => $order->product->price > $credits ? true : false
        ]);
    }


    public function successCredits(Request $request)
    {
		$validator = Validator::make($request->all(), [
		    'payment-type' => 'required'
		], [
			'payment-type.required' => 'Vous devez indiquez une option de paiement'
		]);

        if ($validator->fails()) {
            return redirect()->route('user.panier.payment')->withErrors($validator);
        }

        $order = Session::get('basketOrder'); // FIXME : send error message if not order

		if(!$order) {
			echo 'Order not exist';
			exit();
		}

		$order->payment_type = $request->get('payment-type');

        if($order->save()) {
			// try {
			// 	dispatch(new ProcessOrder(Auth::user(), $order));
			// } catch (Exception $ex) {
			// 	// Make redirect here -)
			// 	echo $ex->getMessage();
			// 	exit();
			// }

			dispatch(new ProcessOrder(Auth::user(), $order));
		}

		// clean sesssion
		Session::forget('basketOrder');

		switch($order->payment_type) {
			case Order::PAYMENT_TYPE_2: //credit + cheque
				return view('user.basket-step-3-2');
			break;

			case Order::PAYMENT_TYPE_3: //cheque
				return view('user.basket-step-3-2');
			break;

			default: //credit

				return view('user.basket-step-3-1');
			break;
		}
    }
}
