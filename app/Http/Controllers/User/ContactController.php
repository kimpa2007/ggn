<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Jobs\SendContact;

use App\Http\Requests\ContactRequest;

use Auth;
use Session;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.contact',[
          'user' => Auth::user()
        ]);
    }

    /**
    */
    public function sendContact(ContactRequest $request)
    {

      if($this->dispatchNow(SendContact::fromRequest($request))){
        $notification = [
          'message' => 'Message envoyé avec succès !',
          'alert-type' => 'success'
        ];
      }
      else {
        $notification = [
          'message' => 'Une erreur s\'est produite',
  				'alert-type' => 'error'
        ];
      }

      return redirect()->route('user.contact')->with($notification);
    }

}
