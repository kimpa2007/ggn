<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Jobs\Content\AddContentView;

use App\Repositories\UserRepository;

use App\Models\Content;
use App\Models\Country;

use Auth;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('auth');

    }

	  public function show($contentId)
    {
      /* Realy used */
    	$content = Content::where('id',$contentId)
        ->where('status',Content::STATUS_PUBLISHED)->first();

  		if(!isset($content)){
  			//return 400 page not found
  			abort(404);
  		}

      $countrie = Country::where('id',$content->country_id)->firstOrFail();
      $zonesIds = $countrie->zones->pluck('zone_id')->toArray();
      if(!Auth::user()->isZoneAllowed($zonesIds)){
        //zone not allowed
        abort(403);
      }

  		if(Auth::user()){
      		$this->dispatchNow(new AddContentView(Auth::user(),$content));
    	}

  		$data = [
  			"content" => $content,
        "expert" => $this->repository->find($content->created_by)
  		];


       return view('content',$data);
    }

}
