<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Repositories\ContentViewedRepository;

use Auth;
use Datatables;
use Session;
use Carbon\Carbon;

class ContentViewedController extends Controller
{

    public function __construct(ContentViewedRepository $contentViewed)
    {
        $this->contentViewed = $contentViewed;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user.content-viewed');
    }

    public function getData( Request $request)
    {
        $user = Auth::user();
        return Datatables::collection($this->contentViewed->all()->where('user_id', $user->id))
            ->editColumn('date', function ($item) {
                return Carbon::parse($item->viewed_at)->format('d/m/Y');
            })
			->addColumn('title', function ($item) {
                return $item->content->title;
            })
			->addColumn('description', function ($item) {

				$text = strip_tags($item->content->description);

                return substr($text,0,100).(strlen($text) > 100 ? '...':'');
            })

			->addColumn('countrie', function ($item) {
                return $item->content->countrie->name;
            })

			->addColumn('action', function ($item) {
            	return '<a target="_blank" href="'.route('user.content.show',['id' => $item->content->id]).'" class="btn btn-secondary-2 btn-table"><i class="fa fa-eye"></i> &nbsp; Voir</a>';

            })

        ->make(true);
    }
}
