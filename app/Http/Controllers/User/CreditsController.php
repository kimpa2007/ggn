<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Http\Requests\UserAddCreditsRequest;

use App\Repositories\UserRepository;
use App\Repositories\CreditOperationRepository;

use App\Models\User;
use App\Jobs\AddCreditUser;

use Auth;
use Datatables;
use Session;
use Carbon\Carbon;

class CreditsController extends Controller
{

    public function __construct(UserRepository $users, CreditOperationRepository $operations)
    {
        $this->users = $users;
        $this->operations = $operations;
        $this->middleware('auth');
    }


    public function show()
    {
        return view('user.credits', [
            'user' => $this->users->find( Auth::user()->id )
        ]);
    }

    public function save(User $user, UserAddCreditsRequest $request)
    {
        $this->dispatchNow(AddCreditUser::fromRequest($user, $request));

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        return redirect()->route('admin.users.credits', $user);
    }


    public function getCreditOperationData( Request $request)
    {
        $user = Auth::user();
        return Datatables::collection($this->operations->all()->where('user_id', $user->id))
            ->editColumn('done_at', function ($item) {
                return Carbon::parse($item->done_at)->format('d/m/Y');
            })
            ->editColumn('type', function ($item) {
                return $item->getTypeName();
            })
        ->make(true);
    }
}
