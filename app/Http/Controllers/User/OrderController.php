<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Country;
use App\Models\Order;
use App\Models\CreditOperation;

use App\Jobs\DeleteOrder;

use Illuminate\Support\Facades\Response;

use Datatables;
use Auth;
use Session;

class OrderController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('user.orders');
    }

    public function getData( Request $request)
    {
        $user = Auth::user();

				$orders = Order::where('user_id', $user->id)->get();
				$orders->load('product', 'expert');

				$statusOptions = Order::getStatus();

        return Datatables::of($orders)
            ->addColumn('date', function ($item) {
                return $item->ordered_at->format('d/m/Y');
            })
			->addColumn('product', function ($item) {
                return $item->product->name;
            })
			->addColumn('description', function ($item) {
				$text = strip_tags($item->product->description);
                return substr($text,0,100).(strlen($text) > 100 ? '...':'');
            })

			->addColumn('expert', function ($item) {
				if($item->expert != null)
                	return $item->expert->firstname.' '. $item->expert->lastname;
				else
					return "";
            })

			->addColumn('zone', function ($item) {
                return $item->product->zone->name;
            })

			->addColumn('price', function ($item) {
                return $item->product->price." €";
            })

			->addColumn('status', function ($item) use ($statusOptions) {
                return $statusOptions[$item->status];
            })

			->addColumn('action', function ($item) {

				if(!$item->is_payed)
					return '<a href="'.route('user.panier',['order' => $item]).'" class="btn btn-secondary-2 btn-table"><i class="fa fa-shopping-cart"></i> &nbsp; Payer</a>&nbsp;'.
						'<a href="'.route('user.orders.delete', ['order' => $item]).'" data-id="'.$item->id.'" class="btn  btn-secondary-2 '.
						'btn-table color-danger toggle-delete"><i class="fa fa-trash"></i> &nbsp; Annuler</a>';
				else
					return '';

            })

            ->make(true);
    }

	public function delete(Order $order, Request $request)
	{
        if($this->dispatchNow(new DeleteOrder($order))) {
            $notification = [
            	'message' => 'Suppression effectuée avec succès',
							'alert-type' => 'success'
            ];

        } else {
			$notification = [
            	'message' => 'Une erreur s\'est produite lors de la suppression',
							'alert-type' => 'error'
            ];
        }

        return redirect()->route('user.orders')->with($notification);
	}
}
