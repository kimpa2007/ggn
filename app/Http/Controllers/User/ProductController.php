<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Order;
use App\Models\Rule;

use App\Jobs\User\CreateSubventionPetition;

use Illuminate\Support\Facades\Response;

use Auth;
use Session;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

  	public function order(Request $request)
  	{

  		if(Auth::user() == null){
  			return Response::json([
                  'error' => true,
                  'code' => 400,
                  'message' => 'User is not defined'
              ], 400);
  		}

  		$productId = $request->get('productId');

  		if(!Product::find($productId)->exists()){
  			return Response::json([
                  'error' => true,
                  'code' => 400,
                  'message' => 'Product does not exists'
              ], 400);
  		}

  		$order = new Order();
  		$order->product_id = $productId;
  		$order->user_id = Auth::user()->id;

  		if($order->save()){
  			return Response::json([
  	            'error' => false,
                  'order' => $order,
  	            'code'  => 200
  	        ], 200);
  		}

  		//error
  		return Response::json([
              'error' => true,
              'code' => 400,
              'message' => 'Error saving the order'
          ], 400);
  	}

    public function subventionPetition(Request $request)
  	{

  		if(Auth::user() == null){
  			return Response::json([
                  'error' => true,
                  'code' => 400,
                  'message' => 'User is not defined'
              ], 400);
  		}

  		$productId = $request->get('productId');

  		if(!Product::find($productId)->exists()){
  			return Response::json([
                  'error' => true,
                  'code' => 400,
                  'message' => 'Product does not exists'
              ], 400);
  		}

      if(Auth::user()->partner &&
        !Auth::user()->partner->hasRule(Rule::RULE_PARTNER_CREDITS_GRANT_USERS)){
          return Response::json([
                    'error' => true,
                    'code' => 400,
                    'message' => 'Method not allowed'
                ], 400);
      }

  		if($this->dispatchNow(new CreateSubventionPetition(Auth::user(),$productId))){

  			return Response::json([
  	            'error' => false,
  	            'code'  => 200
  	        ], 200);
  		}

  		//error
  		return Response::json([
              'error' => true,
              'code' => 400,
              'message' => 'Error saving the order'
          ], 400);
  	}

}
