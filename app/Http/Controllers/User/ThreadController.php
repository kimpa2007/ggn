<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Jobs\Thread\CreateNewThread;

use Illuminate\Support\Facades\Response;

use App\Http\Requests\Thread\CreateThreadRequest;

use App\Http\Requests\Expert\ReplyThread;

use App\Services\FileService;

use App\Models\Thread;
use App\Models\User;
use Auth;
use Session;
use Validator;

class ThreadController extends Controller
{

    public function __construct(FileService $fileService)
  	{
      $this->fileService = $fileService;
  		$this->itemsPerPage = 5;
  		$this->page = 1;
  		$this->middleware('auth');
  	}

    public function index(Request $request)
    {
        $user = Auth::user();

        //proces the pages
        if($request->get('page')){
          $this->page = $request->get('page');
        }

        $count = $user->sentQuestions()->count();

		    $threads = $user->sentQuestions()
					->orderBy('sent_at', 'desc')
          ->offset(($this->page-1) * $this->itemsPerPage)
          ->limit($this->itemsPerPage)
					->get();

        return view('user.threads.index', [
            'threads' => $threads,
            'count' => $count,
            'itemsPerPage' => $this->itemsPerPage
        ]);
    }

    public function show($id, Request $request)
    {
        $thread = Thread::find($id);
        $count = Thread::where('first_id',$id)->count();

    		if($request->get('page')){
    			$this->page = $request->get('page');
    		}

    		$children = Thread::where('first_id',$id)
    			->orderBy('sent_at','desc')
    			->offset(($this->page-1) * $this->itemsPerPage)
    			->limit($this->itemsPerPage)
    			->get();

          $last = $thread->childrens->isNotEmpty() ? $thread->childrens->last() : $thread;

          if($last->sender_id != Auth::user()->id) {
              $thread->setReaded();
          }

          return view('user.threads.show', [
              "thread" => $thread,
              "children" => $children,
              "count" => $count,
              "itemsPerPage" => $this->itemsPerPage
          ]);
    }

    public function save(ReplyThread $request)
    {
        $user = Auth::user();
        $thread = Thread::find($request->get('thread_id'));
        $uploadedFilePath = $request->file('file') ? $this->fileService->upload($request->file('file')) : null;

        switch($request->get('action')) {
            // Edit
            case "edit":
                $thread->message = $request->get('message');
                $thread->save();
                Session::flash('notify_success', "Votre message vient d'être modifié.");
            break;

            // Reply
            default:
                $recipient = isset($thread->first->sender_id) ? User::find($thread->first->sender_id) : User::find($thread->sender_id);
                $thread = $thread->reply($request->get('message'), $user, $recipient);
                Session::flash('notify_success', "Votre message vient d'être envoyé");
            break;
        }

        // Set attachment
        if($uploadedFilePath) {
            $filePath = "/public/threads/" . basename($uploadedFilePath);
            if($this->fileService->move($uploadedFilePath, $filePath)) {

                if($thread->attachment) {
                    $thread->deleteAttachment();
                }

                $thread->attachment = "/storage/threads/" . basename($uploadedFilePath);
                $thread->save();
            }
        }

        return redirect()->route('user.threads.show', isset($thread->first->id) ? $thread->first->id : $thread->id);
    }

    public function delete(Request $request)
    {

        if(!$request->get('thread_id') || !Thread::where('id',$request->get('thread_id'))->exists()){
            return Response::json([
                    'error' => true,
                    'code' => 400,
                    'message' => 'Thread id does not exists'
                ], 400);
        }

        $thread = Thread::find($request->get('thread_id'));

        if($thread) {
            if($thread->delete()) {
                return Response::json([
                      'error' => false,
                      'code'  => 200
                  ], 200);
            }
        }

        return Response::json([
                  'error' => true,
                  'code' => 400,
                  'message' => 'Error deleting the thread'
              ], 400);
    }


    public function sendQuestion(CreateThreadRequest $request)
    {

      if(Auth::user() == null){
  			return Response::json([
                  'error' => true,
                  'code' => 400,
                  'message' => 'User is not defined'
              ], 400);
  		}

      if($this->dispatchNow(CreateNewThread::fromRequest(Auth::user(),$request))){

          return Response::json([
                'error' => false,
                'code'  => 200
            ], 200);
      }

      //error
  		return Response::json([
                'error' => true,
                'code' => 400,
                'message' => 'Error creating the question'
            ], 400);

    }

}
