<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\UpdateInfoRequest;

use App\Jobs\UpdateUserInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Repositories\PartnerRepository;

use App\Repositories\ZoneRepository;
use App\Jobs\CreateUser;

use App\Models\User;
use App\Models\Role;
use App\Models\Partner;

use Auth;
use Session;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ZoneRepository $repository, PartnerRepository $partners)
    {
        $this->zoneRepository = $repository;
        $this->partners = $partners;

    }

    public function create(CreateUserRequest $request)
    {

        return $this->save($request);
    }

    public function save($request, $id = null)
    {
        if($request->get('id') == '') {

            $userId = $this->dispatchNow(new CreateUser(
                $request->get('firstname'),
                $request->get('lastname'),
                $request->get('email'),
                Role::where('name', 'partner')->first()->id,
                $request->get('password'),
                User::STATUS_ACTIVE
            ));

            $data = $request->all();
            $data["manager_id"] = $userId;
            $data["name"] = $request->get('firstname')." ".$request->get('lastname');
            $partner = $this->partners->create($data);

            //update Parent id
            User::where('id',$userId)->update(['partner_id' => $partner->id]);

            Session::flash('notify_success', "Enregistrement effectué avec succès");

            $user = User::find($userId);
            Auth::login($user);

            return redirect()->route('offer.show');


        } else {
            $partner = $this->partners->update($request->all(), $id);

            $data = $request->all();
            $data["role_id"] = Role::where('name', 'partner')->first()->id;

            $this->dispatchNow(new UpdateUser($partner->manager, $data));
        }

        Session::flash('notify_success', "Enregistrement effectué avec succès");

        $user = User::find($userId);
        Auth::login($user);

        return redirect()->route('home');
    }
}
