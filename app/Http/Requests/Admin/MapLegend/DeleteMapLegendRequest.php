<?php

namespace App\Http\Requests\Admin\MapLegend;

use Illuminate\Foundation\Http\FormRequest;

class DeleteMapLegendRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
