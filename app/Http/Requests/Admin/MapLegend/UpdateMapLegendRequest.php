<?php

namespace App\Http\Requests\Admin\MapLegend;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class UpdateMapLegendRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'icon' => 'required',
            'status' => 'required',
            'key' => 'required',
        ];

        foreach (Language::all() as $lang) {
            $rules['name.' . $lang->locale] = 'required';
        }

        return $rules;
    }
}
