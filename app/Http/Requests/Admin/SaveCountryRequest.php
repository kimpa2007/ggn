<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class SaveCountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $validation_cond = ['status' => 'required'];
        foreach (Language::all() as $lang)
        {
            $validation_cond['name.'.$lang->locale] = 'required';
            //$validation_cond['description.'.$lang->locale] = 'required';
            $validation_cond['slug.'.$lang->locale] = 'required';
        }

        return $validation_cond ;

    }
}
