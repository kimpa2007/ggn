<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class SaveCustomPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'identifier' => 'required'
        ];

        foreach (Language::all() as $lang) {
            $rules['title.' . $lang->locale] = 'required';
            $rules['content.' . $lang->locale] = 'required';
            $rules['slug.' . $lang->locale] = 'required';
        }

        return $rules;
    }
}
