<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class SaveThemeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $langues  = Language::where('id', 1)->get();
        $validation_cond = [];
        foreach ($langues  as $lang)
        {
            $validation_cond['name.'.$lang->locale] = 'required';
            $validation_cond['meta_description.'.$lang->locale] = 'required';
            $validation_cond['meta_title.'.$lang->locale] = 'required';
        }

        return $validation_cond ;

    }
}
