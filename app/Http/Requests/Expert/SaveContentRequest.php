<?php

namespace App\Http\Requests\Expert;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class SaveContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation_cond = [
            'type' => 'required',
            'country_id' => 'required',
            'created_by' => 'required',
        ];


        ///modifier on ne veux plus que fr
        //$langues = Language::all();
        $langues = Language::where('id', 1)->get();
        
        foreach ($langues as $lang)
        {
            $validation_cond['title.'.$lang->locale] = 'required';
            $validation_cond['description.'.$lang->locale] = 'required';
        }

        if(request('has_map') == 1) {
            $validation_cond["legend_id"] = 'required';
            $validation_cond["latlng"] = 'required';
        }

        return $validation_cond ;
    }
}
