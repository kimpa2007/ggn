<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class PartnerPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $validation_cond = [
          //  'time' => 'required',
            'months' => 'required',
            'status' => 'required',
            'price' => 'required|integer',
            'type' => 'required',
           // 'zones' => 'required'
        ];

        $langues = Language::where('id', 1)->get();
        
        foreach ($langues as $lang)
        {
            $validation_cond['name.'.$lang->locale] = 'required';
        }

        return $validation_cond ;

    }
}
