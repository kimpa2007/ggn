<?php
namespace App\Jobs;

use App\Models\Order;
use App\Models\CreditOperation;
use Carbon\Carbon;


class AcceptCreditSubvention
{
    private $operation;

    public function __construct(CreditOperation $operation)
    {
        $this->operation = $operation;
    }

    public function handle()
    {
        $attr = [
            "done_at" => Carbon::now(),
            "status" => CreditOperation::STATUS_DONE
        ];

        if(!$this->operation->update($attr)) {
            return false;
        }

        return false;
    }
}
