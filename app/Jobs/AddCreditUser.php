<?php
namespace App\Jobs;

use App\Models\User;
use App\Models\CreditOperation;
use App\Jobs\Partner\SaveCredit as SavePartnerCredit;
use App\Http\Requests\UserAddCreditsRequest;
use Carbon\Carbon;
use Exception;

class AddCreditUser
{
    private $user;

    private $quantity;

    private $type;

    public function __construct(User $user, $quantity, $type = null)
    {
        $this->user = $user;
        $this->quantity = $quantity;
        $this->type = $type;
    }

    public static function fromRequest(User $user, UserAddCreditsRequest $request)
    {
        return new AddCreditUser(
            $user,
            $request->get('quantity'),
            $request->get('type')
        );
    }

    public function handle()
    {
        if($this->user->partner) {

            if($this->quantity > $this->user->partner->credits) {
                throw new Exception(__('exceptions.insufficient_partner_credit', ['credits' => $this->user->partner->credits]));
                return false;
            }
        }

        $attr = [
            "credits" => $this->user->credits + $this->quantity
        ];

        if(!$this->user->update($attr)) {
            return false;
        }

        $operation = CreditOperation::create([
            "user_id" => $this->user->id,
            "type" => $this->type ? $this->type : CreditOperation::TYPE_ADMIN_CREDIT,
            "balance" => $this->user->credits,
            "quantity" => $this->quantity,
            "done_at" => Carbon::now()
        ]);

        if($this->user->partner) {
            dispatch(new SavePartnerCredit($this->user->partner, -($this->quantity), $operation->type, $this->user));
        }

        return true;
    }
}
