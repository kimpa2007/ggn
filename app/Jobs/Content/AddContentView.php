<?php
namespace App\Jobs\Content;

use App\Models\User;
use App\Models\Content;
use App\Models\ContentViewed;

use Carbon\Carbon;

class AddContentView
{

    private $user;
    private $content;


    public function __construct(User $user, Content $content)
    {
        $this->user = $user;
        $this->content = $content;
    }

    public function handle()
    {
    	
		$contentViewed = ContentViewed::where('user_id',$this->user->id)
        	->where('content_id',$this->content->id);
		
        if($contentViewed->exists()){
        
			//update the date
			$contentViewedFirst = $contentViewed->first();
			
			$contentViewedFirst->viewed_at = Carbon::now();
			$contentViewedFirst->save();
        }
		else {
			ContentViewed::create([
				'user_id' => $this->user->id,
				'content_id' => $this->content->id
			]);
		}

        return true;
    }
}
