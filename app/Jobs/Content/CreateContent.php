<?php
namespace App\Jobs\Content;

use App\Models\User;
use App\Models\Content;
use App\Models\ContentMap;
use App\Models\Language;
use App\Models\LatestnewsOrder;
use Auth;

use App\Http\Requests\Expert\SaveContentRequest;

class CreateContent
{
    private $attributes;

    public function __construct(array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'title',
            'description',
            'content',
            'type',
            'movie_on_top',
            'contact',
            'published_at',
            'type',
            'latest_news',
            'created_by',
            'country_id',
            'search_form',
            'content_id',
            'order',
            'image',
            'uploading',
            'legend',
            'copyright',
            'image_2',
            'legend_2',
            'copyright_2',
            'theme',
            'tags',
            'latest_news',
            'has_map',
            'map_display_from',
            'map_display_to',
            'legend_id',
            'latlng'
        ]);
    }

    public static function fromRequest(SaveContentRequest $request)
    {
        return new self($request->all());
    }


    public function handle()
    {
        $isUpdate = $this->attributes['content_id']  ? true : false;
        $content = $this->attributes['content_id'] ? Content::find($this->attributes['content_id']) : new Content();
        $wasLatestNews = $content->latest_news;

        if (!$this->attributes['content_id']) {
            $content->order = (Content::where("country_id", $content->country_id)->max('order')) + 1;
        }
        $content->created_by = isset($this->attributes['created_by'])?$this->attributes['created_by']:'';
        $content->country_id = isset($this->attributes['country_id'])?$this->attributes['country_id']:'';
        $content->movie_on_top = isset($this->attributes['movie_on_top'])?$this->attributes['movie_on_top']:'';
        $content->latest_news = isset($this->attributes['latest_news'])?$this->attributes['latest_news']:'';
        $content->image = isset($this->attributes['image'])?$this->attributes['image']:'';
        $content->image_2 = isset($this->attributes['image_2'])?$this->attributes['image_2']:'';
        $content->type = isset($this->attributes['type'])?$this->attributes['type']:'';
        $content->published_at = $this->attributes['published_at'];
        $content->has_map = isset($this->attributes['has_map'])?$this->attributes['has_map']:'';

        //$langues = Language::all();-
        $langues = Language::where('id', 1)->get();

        foreach ($langues as $lang) {

            $content->translateOrNew($lang->locale)->legend = isset($this->attributes['legend'][$lang->locale])? $this->attributes['legend'][$lang->locale]: '';
            $content->translateOrNew($lang->locale)->legend_2 = isset($this->attributes['legend_2'][$lang->locale])?$this->attributes['legend_2'][$lang->locale]:'';
            $content->translateOrNew($lang->locale)->copyright = isset($this->attributes['copyright'][$lang->locale])?$this->attributes['copyright'][$lang->locale]:'';
            $content->translateOrNew($lang->locale)->copyright_2 = isset($this->attributes['copyright_2'][$lang->locale])?$this->attributes['copyright_2'][$lang->locale]:'';
            $content->translateOrNew($lang->locale)->title = $this->attributes['title'][$lang->locale];
            $content->translateOrNew($lang->locale)->slug = str_slug($content->translate($lang->locale)->title, '-');
            $content->translateOrNew($lang->locale)->description = $this->attributes['description'][$lang->locale];
            $content->translateOrNew($lang->locale)->contact = isset($this->attributes['contact'][$lang->locale])?$this->attributes['contact'][$lang->locale]:'';
            $content->translateOrNew($lang->locale)->content = '';
            if ($content->type == Content::TYPE_RICHTEXT) {
                $content->translateOrNew($lang->locale)->content = isset($this->attributes['content'][$this->attributes['type']][$lang->locale]) ? $this->attributes['content'][$this->attributes['type']][$lang->locale] : '';
            } else {
                $content->content = isset($this->attributes['content'][$this->attributes['type']]) ? $this->attributes['content'][$this->attributes['type']] : '' ;
            }
        }



        if($content->save()) {

            $tags = isset($this->attributes['tags']) ? $this->attributes['tags'] : [];
            $content->tags()->sync($tags);

            if (!empty($this->attributes['theme'])) {
                $contentId = $content->id;
                $content = Content::find($contentId);
                $content->themes()->sync($this->attributes['theme']);
                $content->save();
            }

            $content->map()->delete();

            if($content->has_map) {
                ContentMap::create([
                    'content_id' => $content->id,
                    'start_at' => isset($this->attributes['map_display_from']) ? trim($this->attributes['map_display_from']) : null,
                    'end_at' => isset($this->attributes['map_display_to']) ? trim($this->attributes['map_display_to']) : null,
                    'legend_id' => isset($this->attributes['legend_id']) ? trim($this->attributes['legend_id']) : null,
                    'latlng' => isset($this->attributes['latlng']) ? trim($this->attributes['latlng']) : null,
                ]);
            }

            if ($this->attributes['latest_news'] == 1) {
                if(!$isUpdate) {
                    $this->rebuildOrder($content);
                } else {
                    if(!$wasLatestNews) {
                        $this->rebuildOrder($content);
                    }
                }
            } else {
                $this->rebuildOrder();
            }

            return $content;
        }

        return false;
    }

    private function addLastOrder($content)
    {
        $delOrder = LatestnewsOrder::where("content_id", $content->id);
        $delOrder->delete();

        $insertOrder = new LatestnewsOrder();
        $insertOrder->order_news = (LatestnewsOrder::max('order_news')) + 1;
        $insertOrder->content_id = $content->id;
        $insertOrder->save();
    }

    private function rebuildOrder($firstContent = null)
    {
        $orders = LatestnewsOrder::all();
        LatestnewsOrder::query()->truncate();
        $order = 1;

        if($firstContent) {
            LatestnewsOrder::create([
                'content_id' => $firstContent->id,
                'order_news' => $order,
            ]);
            $order++;
        }

        foreach ($orders as $value) {
            LatestnewsOrder::create([
                'content_id' => $value['content_id'],
                'order_news' => $order,
            ]);
            $order++;
        }
    }
}
