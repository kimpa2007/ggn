<?php
namespace App\Jobs\Country;

use App\Models\User;
use App\Models\Country;
use App\Models\CountryCountry;
use App\Models\UserCountry;
use App\Models\Language;
use Auth;

use App\Http\Requests\Admin\SaveCountryRequest;


class CreateCountry
{
    private $attributes;

    public function __construct(
        array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'id',
            'name',
            'description',
            'slug',
            'status',
            'image',
            'image_align',
            'legend',
            'copyright'
        ]);
    }

    public static function fromRequest(SaveCountryRequest $request)
    {
        return new self($request->all());
    }


    public function handle()
    {   

        $country = $this->attributes['id'] ? Country::find($this->attributes['id']) : new Country();

        foreach (Language::all() as $lang)
        {
            $country->translateOrNew($lang->locale)->name = $this->attributes["name"][$lang->locale];
            $country->translateOrNew($lang->locale)->slug = $this->attributes["slug"][$lang->locale];
            $country->translateOrNew($lang->locale)->description = $this->attributes["description"][$lang->locale];
            $country->translateOrNew($lang->locale)->legend = isset($this->attributes["legend"][$lang->locale])?$this->attributes["legend"][$lang->locale]:'';
            $country->translateOrNew($lang->locale)->copyright = isset($this->attributes["copyright"][$lang->locale])?$this->attributes["copyright"][$lang->locale]:'';
        }

        $country->image = $this->attributes["image"];
        $country->image_align = $this->attributes["image_align"];
        $country->status = $this->attributes["status"];

        if(!$this->attributes['id']){
            $country->order = Country::max('order') + 1;
        }
             
        $country->save();

        return $country;      
    }
}
