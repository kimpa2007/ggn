<?php
namespace App\Jobs;

use App\Http\Requests\EmailTemplateRequest;

use App\Models\EmailTemplate;

class CreateEmailTemplate
{
    public function __construct(
        string $name,
        string $template,
        string $key
    ) {
        $this->name = $name;
        $this->template = $template;
        $this->key = $key;
    }

    public static function fromRequest(EmailTemplateRequest $request)
    {
        return new CreateEmailTemplate(
            $request->get('name'),
            $request->get('template'),
            $request->get('key')
        );
    }

    public function handle()
    {
        $template = new EmailTemplate([
            'name' => $this->name,
            'template' => $this->template,
            'key' => $this->key
        ]);
        $template->save();

        return $template;
    }
}
