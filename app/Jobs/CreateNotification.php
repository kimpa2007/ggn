<?php
namespace App\Jobs;

use App\Http\Requests\NotificationRequest;

use App\Models\Notification;


class CreateNotification
{
    public function __construct(
         $partner_id,
        string $title,
        string $message,
        string $status
    ) {
        $this->partner_id = $partner_id;
        $this->title = $title;
        $this->message = $message;
        $this->status = $status;
    }

    public static function fromRequest(NotificationRequest $request)
    {
        return new CreateNotification(
            $request->get('partner_id'),
            $request->get('title'),
            $request->get('message'),
            $request->get('status')
        );
    }

    public function handle()
    {
        $notification = new Notification([
            'partner_id' => $this->partner_id,
            'title' => $this->title,
            'message' => $this->message,
            'status' => $this->status
        ]);
        $notification->save();

        return $notification;
    }
}
