<?php
namespace App\Jobs;

use App\Models\PartnerPlan;
use App\Models\PlanZone;
use App\Models\PlanPurposed;
use App\Http\Requests\PartnerPlanRequest;
use App\Models\User;
use App\Models\Language;

class CreatePartnerPlan
{
    public function __construct(
        $name,
        $time,
        $status,
        $price,
        $type,
        array $zonesId,
        User $author,
        $id = null,
        $description = null,
        $partners = []
    ) {
        $this->name = $name;
        $this->time = $time;
        $this->status = $status;
        $this->price = $price;
        $this->type = $type;
        $this->zonesId = $zonesId;
        $this->author = $author;
        $this->id = $id;
        $this->description = $description;
        $this->partners = $partners;
    }

    public static function fromRequest(PartnerPlanRequest $request)
    {
        return new CreatePartnerPlan(
            $request->get('name'),
         //   $request->get('time'),
            $request->get('status'),
            $request->get('price'),
            $request->get('type'),
            $request->get('zones'),
            $request->user(),
            $request->get('id'),
            $request->get('description'),
            $request->get('partners_purposed'),
            $request->get('months')
        );
    }

    public function handle()
    {
        $plan = new PartnerPlan();
   //     $plan->time = 0;
        $plan->status = $this->status;
        $plan->type = $this->type;
        $plan->price = $this->price;
        $plan->created_by = $this->author->id;
        $plan->months = $this->months;

        //$langues = Language:all();
        $langues = Language::where('id', 1)->get();

        foreach ($langues as $lang)
        {
            $plan->translateOrNew($lang->locale)->name = $this->name[$lang->locale];
            $plan->translateOrNew($lang->locale)->description = $this->description[$lang->locale];
            $plan->translateOrNew($lang->locale)->slug = str_slug($this->name[$lang->locale], '-') . '-'. $lang->locale. '-';
        }


        $plan->save();
        $plan->zones()->sync($this->zonesId);

        // Adding partners
        $data = [];
        if(!empty($this->partners)) {
    			foreach($this->partners as $p) {
    				$data[$p] = [
    					"status" => PlanPurposed::STATUS_PURPOSED
    				];
    			}
        }
        $plan->partnersPurposed()->sync($data);

        return $plan;
    }
}
