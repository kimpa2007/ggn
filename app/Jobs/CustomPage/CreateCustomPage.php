<?php
namespace App\Jobs\CustomPage;

use App\Models\User;
use App\Models\CustomPage;
use App\Models\Language;
use Auth;

use App\Http\Requests\Admin\SaveCustomPageRequest;

class CreateCustomPage
{
    private $attributes;
    private $id;

    public function __construct(array $attributes = [], $id = null)
    {
        $this->id = $id;
        $this->attributes = array_only($attributes, [
            'title',
            'content',
            'slug',
            'identifier'
        ]);
    }

    public static function fromRequest(SaveCustomPageRequest $request, $id = null)
    {
        return new self($request->all(), $id);
    }

    public function handle()
    {
        $custompage = $this->id
            ? CustomPage::find($this->id)
            : new CustomPage();

        foreach (Language::all() as $lang) {
            $custompage->translateOrNew($lang->locale)->title = $this->attributes["title"][$lang->locale];
            $custompage->translateOrNew($lang->locale)->slug = $this->attributes["slug"][$lang->locale];
            $custompage->translateOrNew($lang->locale)->content = $this->attributes["content"][$lang->locale];
        }

        $custompage->identifier = $this->attributes["identifier"];


        return $custompage->save() ? $custompage : false;
    }
}
