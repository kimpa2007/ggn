<?php
namespace App\Jobs;

use App\Models\Order;

class DeleteOrder
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function handle()
    {
        if($this->order->delete()) {
            return true;
        }

        return false;
    }
}
