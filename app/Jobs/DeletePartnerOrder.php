<?php
namespace App\Jobs;

use App\Models\PartnerOrder as Order;

class DeletePartnerOrder
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function handle()
    {
        if($this->order->delete()) {
            return true;
        }

        return false;
    }
}
