<?php
namespace App\Jobs;

use App\Models\PartnerPlan;

class DeletePartnerPlan
{
    private $plan;

    public function __construct(PartnerPlan $plan)
    {
        $this->plan = $plan;
    }

    public function handle()
    {
        if($this->plan->delete()) {
            return true;
        }

        return false;
    }
}
