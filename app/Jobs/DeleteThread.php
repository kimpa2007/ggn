<?php
namespace App\Jobs;

use App\Models\Thread;

class DeleteThread
{
    private $thread;

    public function __construct(Thread $thread)
    {
        $this->thread = $thread;
    }

    public function handle()
    {
        if($this->thread->delete()) {
            return true;
        }

        return false;
    }
}
