<?php
namespace App\Jobs;

use App\Models\User;

class DeleteUser
{
    private $thread;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        if($this->user->delete()) {
            return true;
        }

        return false;
    }
}
