<?php
namespace App\Jobs\Email;

use App\Models\EmailTemplate;
use Mail;
use Blade;

class SendTemplatedEmail
{
    private $key;

    private $data;

    private $email;

    public function __construct($key, $email, $data = [])
    {
        $this->key = $key;
        $this->data = $data;
        $this->email = $email;
    }

    public static function render($string, $data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($data, EXTR_SKIP);

        try {
            eval('?' . '>' . Blade::compileString($string));
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }

        return ob_get_clean();
    }

    public function handle()
    {
        $template = EmailTemplate::where('key', $this->key)->first();
        $email = $this->email;

        if(!$template) {
            throw new \Exception('Template ' . $this->key . ' not exist');
        }

        Mail::raw($this->render($template->template, $this->data), function ($m) use ($template, $email) {
            $m->from(env('MAIL_COMPANY_EMAIL'), env('MAIL_COMPANY_NAME'));
            $m->to($email)->subject($template->subject);
        });

        return true;
    }
}
