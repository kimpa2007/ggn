<?php
namespace App\Jobs\Expert;

use App\Models\User;
use App\Models\UserProfile;
use App\Models\Language;
use Auth;

use App\Http\Requests\Expert\SaveProfileRequest;


class UpdateProfile
{
    private $attributes;

    public function __construct(
        array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'id',
            'image',
            'content'
        ]);
    }

    public static function fromRequest(SaveProfileRequest $request)
    {
        return new self($request->all());
    }


    public function handle()
    {   
        $user = User::find($this->attributes['id']);

        $userProfile = $user->profile ? $user->profile : null;

        if(!$userProfile) {
            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
        }

        foreach (Language::all() as $lang)
        {
            $userProfile->translateOrNew($lang->locale)->content = $this->attributes["content"][$lang->locale];
        }
        $userProfile->image = $this->attributes["image"];

        $userProfile->save();

        return $user;      
    }
}
