<?php
namespace App\Jobs\MapLegend;

use App\Models\User;
use App\Models\MapLegend;
use App\Models\Language;
use Auth;

use App\Http\Requests\Admin\MapLegend\CreateMapLegendRequest;
use App\Services\FileService;
use Storage;

class CreateMapLegend
{
    private $attributes;

    public function __construct(array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'icon',
            'name',
            'color',
            'status',
            'key'
        ]);
    }

    public static function fromRequest(CreateMapLegendRequest $request)
    {
        return new self($request->all());
    }

    public function handle()
    {
        if(isset($this->attributes["icon"])) {
            $filePath = sprintf(
                "/maplegends/%s.%s",
                uniqid(rand(), false),
                pathinfo($this->attributes["icon"], PATHINFO_EXTENSION)
            );
            $filePath = strtolower($filePath);
            $tmpPath = '/public/' .  $this->attributes["icon"];

            if(Storage::move($tmpPath, '/public' . $filePath)) {
                $this->attributes['icon'] = $filePath;
            }
        }

        $mapLegend = new MapLegend([
            'icon' => $this->attributes['icon'],
            'color' => $this->attributes['color'],
            'status' => $this->attributes['status'],
            'key' => $this->attributes['key'],
        ]);

        foreach (Language::all() as $lang) {
            $mapLegend->translateOrNew($lang->locale)->name = isset($this->attributes['name'][$lang->locale])? $this->attributes['name'][$lang->locale]: null;
        }

        return $mapLegend->save() ? $mapLegend : null;
    }
}
