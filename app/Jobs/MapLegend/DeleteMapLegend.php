<?php
namespace App\Jobs\MapLegend;

use App\Models\MapLegend;
use App\Http\Requests\Admin\MapLegend\DeleteMapLegendRequest;
use Storage;

class DeleteMapLegend
{
    private $attributes;

    public function __construct(MapLegend $mapLegend, array $attributes = [])
    {
        $this->mapLegend = $mapLegend;
    }

    public static function fromRequest(MapLegend $mapLegend, DeleteMapLegendRequest $request)
    {
        return new self($mapLegend, $request->all());
    }

    public function handle()
    {
        Storage::delete('/public' . $this->mapLegend->icon);

        return $this->mapLegend->delete();
    }
}
