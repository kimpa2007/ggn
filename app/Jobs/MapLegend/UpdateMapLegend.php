<?php
namespace App\Jobs\MapLegend;

use App\Http\Requests\Admin\MapLegend\UpdateMapLegendRequest;
use App\Models\MapLegend;
use App\Models\Language;
use Storage;

class UpdateMapLegend
{
    private $attributes;

    public function __construct(MapLegend $mapLegend, array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'icon',
            'name',
            'color',
            'status',
            'key'
        ]);
        $this->mapLegend = $mapLegend;
    }

    public static function fromRequest(MapLegend $mapLegend, UpdateMapLegendRequest $request)
    {
        return new self($mapLegend, $request->all());
    }

    public function handle()
    {
        if(isset($this->attributes["icon"]) && $this->mapLegend->icon != $this->attributes["icon"]) {
            Storage::delete('/public' . $this->mapLegend->icon);

            $filePath = sprintf(
                "/maplegends/%s.%s",
                uniqid(rand(), false),
                pathinfo($this->attributes["icon"], PATHINFO_EXTENSION)
            );
            $filePath = strtolower($filePath);
            $tmpPath = '/public/' .  $this->attributes["icon"];

            if(Storage::move($tmpPath, '/public' . $filePath)) {
                $this->attributes['icon'] = $filePath;
            }
        }

        foreach (Language::all() as $lang) {
            $this->mapLegend->translateOrNew($lang->locale)->name = isset($this->attributes['name'][$lang->locale])? $this->attributes['name'][$lang->locale]: null;
        }

        return $this->mapLegend->update([
            'icon' => $this->attributes['icon'],
            'status' => $this->attributes['status'],
            'color' => $this->attributes['color'],
            'key' => $this->attributes['key'],
        ]);
    }
}
