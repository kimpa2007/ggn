<?php
namespace App\Jobs\Partner;

use App\Models\ZoneRequested;

class AcceptThemeRequested
{
    private $zone;

    public function __construct(ZoneRequested $zone)
    {
        $this->zone = $zone;
    }

    public function handle()
    {
        $this->zone->status = ZoneRequested::STATUS_DONE;
        if($this->zone->save()) {

            // TODO Send admin mails here :)
            // Send partner emails here

            return true;
        }

        return false;
    }
}
