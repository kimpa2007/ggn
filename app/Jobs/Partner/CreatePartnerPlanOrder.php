<?php
namespace App\Jobs\Partner;

use App\Http\Requests\Admin\Partner\CreatePartnerPlanRequest;

use App\Models\Partner;
use App\Models\PartnerOrder;
use App\Models\PartnerPlan;
use Carbon\Carbon;

class CreatePartnerPlanOrder
{

    /**
    * @var array
    */
    private $attributes;

    public function __construct(Partner $partner,array $attributes = []) {
        $this->partner = $partner;
        $this->attributes = array_only($attributes, ['plan_id', 'start_at']);
    }

    public static function fromRequest(Partner $partner,CreatePartnerPlanRequest $request)
    {
        return new CreatePartnerPlanOrder($partner,$request->all());
    }

    public function handle()
    {

        $data = $this->attributes;

        $plan = PartnerPlan::findOrFail($this->attributes['plan_id']);

        $start_date = Carbon::createFromFormat('d/m/Y', $this->attributes['start_at']);
        $end_date = Carbon::createFromFormat('d/m/Y', $this->attributes['start_at'])->addMonths($plan->months);


        $start_at = $start_date->format('Y-m-d');
        $end_at = $end_date->format('Y-m-d');

        $order = new PartnerOrder();
        $order->partner_id = $this->partner->id;
        $order->plan_id = $plan->id;
        $order->start_at = $start_at;
        $order->end_at = $end_date;
        $order->amount = $plan->price;
        $order->status = PartnerOrder::STATUS_PENDING;
        $order->type_payment = PartnerOrder::PAYMENT_CHEQUE;
        $order->type = PartnerOrder::TYPE_PLAN;

        $order->save();

        return $order->id;
    }
}
