<?php
namespace App\Jobs\Partner;

use App\Http\Requests\Partner\ThemePetitionRequest;

use App\Models\Partner;
use App\Models\ZoneRequested;

use Exception;

class CreateThemePetition
{
    private $partner;
    private $zone_id;

    public function __construct(Partner $partner, $zone_id)
    {
        $this->partner = $partner;
        $this->zone_id = $zone_id;
    }

    public static function fromRequest(Partner $partner, ThemePetitionRequest $request)
    {
        return new CreateThemePetition(
            $partner,
            $request->get('zone_id')
        );
    }

    public function handle()
    {
        //create zone order
        $themeRequest = new ZoneRequested();
        $themeRequest->partner_id = $this->partner->id;
        $themeRequest->zone_id = $this->zone_id;
        $themeRequest->status = ZoneRequested::STATUS_PENDING;

        if($themeRequest->save()){

          //TODO send emails?

          return true;
        }

        return false;
    }
}
