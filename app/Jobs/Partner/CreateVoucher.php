<?php
namespace App\Jobs\Partner;

use App\Http\Requests\Partner\CreateVoucherRequest;

use App\Models\Partner;
use App\Models\PartnerVoucher;
use App\Models\CreditOperation;
use App\Jobs\Email\SendTemplatedEmail;
use Carbon\Carbon;

class CreateVoucher
{
    private $partner;

    private $amount;

    private $quantity;

    public function __construct(Partner $partner, $amount, $quantity)
    {
        $this->partner = $partner;
        $this->amount = $amount;
        $this->quantity = $quantity;
    }

    public static function fromRequest(Partner $partner, CreateVoucherRequest $request)
    {
        return new CreateVoucher(
            $partner,
            $request->get('amount'),
            $request->get('quantity')
        );
    }

    public function handle()
    {
        PartnerVoucher::create([
            'partner_id' => $this->partner->id,
            'amount' => $this->amount,
            'total_amount' => ($this->amount * $this->quantity),
            'remain' => $this->quantity,
            'quantity' => $this->quantity,
            'status' => PartnerVoucher::STATUS_PENDING,
        ]);

        CreditOperation::create([
            "partner_id" => $this->partner->id,
            "type" => CreditOperation::TYPE_PARTNER_VOUCHER,
            "status" => CreditOperation::STATUS_DONE,
            "quantity" => ($this->amount * $this->quantity),
            "balance" => $this->partner->credits,
            "done_at" => Carbon::now(),
        ]);

        return true;
    }
}
