<?php
namespace App\Jobs\Partner;

use App\Models\ZoneRequested;

class DeleteThemeRequested
{
    private $zone;

    public function __construct(ZoneRequested $zone)
    {
        $this->zone = $zone;
    }

    public function handle()
    {
        if($this->zone->delete()) {

            // TODO Send admin mails here :)
            // Send partner emails here

            return true;
        }

        return false;
    }
}
