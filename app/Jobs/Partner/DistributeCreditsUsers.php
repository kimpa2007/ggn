<?php
namespace App\Jobs\Partner;

use App\Models\Partner;
use App\Models\CreditOperation;
use App\Models\User;

use App\Http\Requests\Admin\Partner\SaveCreditRequest;
use Carbon\Carbon;
use Exception;
use App\Jobs\AddCreditUser;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DistributeCreditsUsers implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $partner;

    private $users;

    private $type;

    public function __construct(Partner $partner, $users,$type = null)
    {
        $this->partner = $partner;
        $this->users = $users;
        $this->type = $type;
    }

    public function handle()
    {
        if(empty($this->users)) {
            return true;
        }

        foreach($this->users as $userValues) {
            $user = User::find($userValues['id']);

            if($user != null && $userValues['add_value'] != 0) {
                dispatch(new AddCreditUser($user, $userValues['add_value'], $this->type));
            }
        }

        return true;
    }
}
