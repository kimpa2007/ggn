<?php
namespace App\Jobs\Partner;

use App\Models\Partner;
use App\Models\CreditOperation;
use App\Models\User;

use App\Http\Requests\Admin\Partner\SaveCreditRequest;
use Carbon\Carbon;
use Exception;
use App\Jobs\AddCreditUser;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DivideCreditsUsers
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $partner;

    private $quantity;

    private $type;

    //private $user;

    public function __construct(Partner $partner, $quantity,  $type = null)
    {
        $this->partner = $partner;
        $this->quantity = $quantity;
        $this->type = $type;

        $this->users = $partner->users()->where('status',User::STATUS_ACTIVE)->get();
    }

    public static function fromRequest(Partner $partner, SaveCreditRequest $request)
    {
        return new DivideCreditsUsers(
            $partner,
            $request->get('quantity'),
            $request->get('type')
        );
    }

    public function handle()
    {
        if(empty($this->users)) {
            throw new Exception('Users is empty');
        }

        if($this->quantity < 0){
            throw new Exception('Quantity must be a positive number');
        }

        foreach($this->users as $user) {
            dispatch(new AddCreditUser($user, $this->quantity, $this->type));
        }

        return true;
    }
}
