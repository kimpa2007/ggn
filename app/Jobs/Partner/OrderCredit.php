<?php
namespace App\Jobs\Partner;

use App\Http\Requests\Partner\OrderCreditRequest;

use App\Models\Partner;
use App\Models\User;
use App\Models\PartnerOrder;
use App\Jobs\Email\SendTemplatedEmail;

class OrderCredit
{
    private $partner;

    private $amount;

    private $payment_type;

    public function __construct(Partner $partner, $amount, $payment_type)
    {
        $this->partner = $partner;
        $this->amount = $amount;
        $this->payment_type = $payment_type;
    }

    public static function fromRequest(Partner $partner, OrderCreditRequest $request)
    {
        return new OrderCredit(
            $partner,
            $request->get('amount'),
            $request->get('payment_type')
        );
    }

    public function handle()
    {
        $order = PartnerOrder::create([
            'partner_id' => $this->partner->id,
            'status' => PartnerOrder::STATUS_PENDING,
            'type' => PartnerOrder::TYPE_CREDIT,
            'type_payment' => $this->payment_type,
            'amount' => $this->amount
        ]);

        // Send e-mail to admin users
        $admins = User::whereHas('roles', function($query){
            $query->where('name','admin');
        })->get()->map(function ($u) {
            return $u->email;
        })->toArray();

        dispatch(new SendTemplatedEmail('admin.order.credit.created', $admins, [
            'order' => $order
        ]));

        // Send e-mail to partner manager
        dispatch(new SendTemplatedEmail('partner.order.credit.created', $order->partner->manager->email, [
            'order' => $order
        ]));

        return $order;
    }
}
