<?php
namespace App\Jobs\Partner;

//use App\Http\Requests\Admin\Partner\OrderPlanRequest;

use App\Models\Partner;
use App\Models\PartnerOrder;
use App\Models\PartnerPlan;
use Carbon\Carbon;

class OrderPlan
{

    /**
    * @var array
    */
    private $attributes;
    private $partner;
    private $plan;

    public function __construct(Partner $partner, PartnerPlan $plan, array $attributes = []) {
        $this->partner = $partner;
        $this->plan = $plan;
        $this->attributes = array_only($attributes, ['payment_type']);
    }

    // public static function fromRequest(Partner $partner,OrderPlanRequest $request)
    // {
    //     return new OrderPlan($partner,$request->all());
    // }

    public function handle()
    {
        return PartnerOrder::create([
            'partner_id' => $this->partner->id,
            'plan_id' => $this->plan->id,
            'amount' => $this->plan->price,
            'status' => PartnerOrder::STATUS_PENDING,
            'type_payment' => $this->attributes['payment_type'],
            'type' => PartnerOrder::TYPE_PLAN,
        ]);
    }
}
