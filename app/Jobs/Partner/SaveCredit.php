<?php
namespace App\Jobs\Partner;

use App\Models\Partner;
use App\Models\CreditOperation;
use App\Models\User;

use App\Http\Requests\Admin\Partner\SaveCreditRequest;
use Carbon\Carbon;

class SaveCredit
{
    private $partner;

    private $quantity;

    private $type;

    private $user;

    public function __construct(Partner $partner, $quantity, $type = null, User $user = null)
    {
        $this->partner = $partner;
        $this->quantity = $quantity;
        $this->type = $type;
        $this->user = $user;
    }

    public static function fromRequest(Partner $partner, SaveCreditRequest $request)
    {
        return new SaveCredit(
            $partner,
            $request->get('quantity'),
            $request->get('type'),
            $request->get('user_id') ? User::find($request->get('user_id')) : null
        );
    }

    public function handle()
    {
        $attr = [
            "credits" => $this->partner->credits + $this->quantity
        ];

        if(!$this->partner->update($attr)) {
            return false;
        }

        CreditOperation::create([
            "partner_id" => $this->partner->id,
            "user_id" => $this->user ? $this->user->id : null,
            "type" => $this->type ? $this->type : CreditOperation::TYPE_ADMIN_CREDIT,
            "status" => CreditOperation::STATUS_DONE,
            "quantity" => $this->quantity,
            "balance" => $this->partner->credits,
            "done_at" => Carbon::now(),
            "status" => CreditOperation::STATUS_DONE
        ]);

        return true;
    }
}
