<?php
namespace App\Jobs\Partner;

use App\Http\Requests\Partner\SubventionAcceptRequest;

use App\Models\CreditOperation;
use App\Models\Partner;
use App\Models\User;

use App\Jobs\Partner\SaveCredit;

use Exception;

class SubventionAccept
{
    private $cO;
    private $amount;

    public function __construct(CreditOperation $cO, $amount)
    {
        $this->cO = $cO;
        $this->amount = $amount;
    }

    public static function fromRequest(CreditOperation $cO, SubventionAcceptRequest $request)
    {
        return new SubventionAccept(
            $cO,
            $request->get('amount')
        );
    }

    public function handle()
    {

        //check partner has credits
        if($this->cO->partner->credits < $this->amount){
          throw new Exception('insuficient credits. Current credits : '.$this->cO->partner->credits);
        }

        //add credits to user
        $user = User::find($this->cO->user_id);
        $user->credits = $user->credits + $this->amount;
        $user->save();

        //save credit operation user
        $this->cO->quantity = $this->amount;
        $this->cO->status = CreditOperation::STATUS_DONE;
        $this->cO->balance = $user->credits;
        $this->cO->save();

        dispatch(new SaveCredit($this->cO->partner,-$this->amount,CreditOperation::TYPE_PARTNER_GIFT,$user));

        // Send admin mails here :)
        // Send partner emails here

        return true;
    }
}
