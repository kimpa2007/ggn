<?php
namespace App\Jobs\Partner;

use App\Models\CreditOperation;
use App\Models\Partner;
use App\Models\User;

class SubventionDenied
{
    private $cO;

    public function __construct(CreditOperation $cO)
    {
        $this->cO = $cO;
    }

    public function handle()
    {
        $this->cO->status = CreditOperation::STATUS_REFUSED;
        $this->cO->save();

        // Send admin mails here :)
        // Send partner emails here

        return true;
    }
}
