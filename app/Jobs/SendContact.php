<?php
namespace App\Jobs;

use App\Http\Requests\ContactRequest;

use Mail;

class SendContact
{


    /**
    * @var array
    */
    private $attributes;

    public function __construct(array $attributes = []) {
        $this->attributes = array_only($attributes, ['name', 'email', 'subject','comment']);
    }

    public static function fromRequest(ContactRequest $request)
    {
        return new SendContact($request->all());
    }

    public function handle()
    {
        //send email

        $data = $this->attributes;

        Mail::send('emails.contact', $data, function ($message) use ($data) {

          $message->from($data['email'], $data['name']);

          $message->to(env('MAIL_COMPANY_EMAIL'))
            ->cc($data['email'])
            ->subject($data['subject']);

        });

        return true;
    }
}
