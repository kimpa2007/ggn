<?php
namespace App\Jobs;

use App\Models\Notification;
use App\Models\UserNotification;
use App\Http\Requests\NotificationRequest;

use Carbon\Carbon;

class SendNotification
{
    /**
    * @var \App\Models\Notification
    */
    private $notification;

    /**
    * @var array
    */
    private $attributes;

    public function __construct(Notification $notification) {
        $this->notification = $notification;
    }

    public function handle(): Notification
    {
        $this->notification->update([
            'status' => Notification::STATUS_SENT,
            'sent_at' => Carbon::now()
        ]);

        $usersId = $this->notification->partner->users->pluck('id');
        if($usersId->isNotEmpty()) {
            $users = [];
            foreach($usersId->toArray() as $id) {
                $users[$id] = [
                    'status' => UserNotification::STATUS_NEW,
                    'received_at' => Carbon::now()
                ];
            }
            $this->notification->users()->sync($users);
        }

        return $this->notification;
    }
}
