<?php
namespace App\Jobs;

use App\Http\Requests\SubscriptionRequest;

use Mail;

class SendSignup
{


    /**
    * @var array
    */
    private $attributes;

    public function __construct(array $attributes = []) {
        $this->attributes = array_only($attributes, ['firstname', 'lastname', 'email_signup','comment_on_signup','autopass']);
    }

    public static function fromRequest(SubscriptionRequest $request)
    {
        return new SendSignup($request->all());
    }

    public function handle()
    {
        //send email

        $data = $this->attributes;

        Mail::send('emails.signup', $data, function ($message) use ($data) {

            $message->from($data['email_signup'], $data['firstname']);

            $message->to(config('ca.mail_company_email'))
            //->cc()
            ->subject('new subscription at ClubApprendre: '. $data['firstname'].' '. $data['lastname']);

        });

        Mail::send('emails.signuptouser', $data, function ($message) use ($data) {

            $message->from(config('ca.mail_company_email'), 'ClubApprendre subscription');

            $message->to($data['email_signup'])
                //->cc()
                ->subject('Inscrit dans ClubApprendre: '. $data['firstname'].' '. $data['lastname']);

        });

        return true;
    }
}
