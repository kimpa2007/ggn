<?php
namespace App\Jobs;

use App\Models\Partner;
use App\Http\Requests\PartnerRulesRequest;

class SetPartnerRules
{
    public function __construct($partnerId, $rulesId) {
        $this->rulesId = $rulesId;
        $this->partnerId = $partnerId;
    }

    public static function fromRequest($id, PartnerRulesRequest $request)
    {
        return new SetPartnerRules($id, $request->get('rules'));
    }

    public function handle()
    {
        $rulesId = !empty($this->rulesId) ? $this->rulesId : [];
        $partner = Partner::find($this->partnerId);
        $partner->rules()->sync($rulesId);

        return $partner;
    }
}
