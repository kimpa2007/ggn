<?php
namespace App\Jobs\Tags;

use App\Http\Requests\Admin\Tags\CreateTagRequest;
use App\Models\Tag;

class CreateTag
{
    private $tags;

    public function __construct(array $attributes = [])
    {
        $fields = (new Tag())->getFillable();

        $this->attributes = array_only($attributes, $fields);
    }

    public static function fromRequest(CreateTagRequest $request)
    {
        return new self($request->all());
    }

    public function handle()
    {
        return Tag::create($this->attributes);
    }
}
