<?php
namespace App\Jobs\Tags;

use App\Http\Requests\Admin\Tags\DeleteTagsRequest;
use App\Models\Tag;

class DeleteTags
{
    private $tags;

    public function __construct(array $tags = [])
    {
        $this->tags = $tags;
    }

    public static function fromRequest(DeleteTagsRequest $request)
    {
        return new self($request->get('to_remove'));
    }

    public function handle()
    {
        return Tag::whereIn('id', $this->tags)->delete();
    }
}
