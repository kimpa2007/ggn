<?php
namespace App\Jobs\Tags;

use App\Http\Requests\Admin\Tags\UpdateTagsRequest;
use App\Models\Tag;

class UpdateTags
{
    private $tags;

    public function __construct(array $tags = [])
    {
        $this->tags = $tags;
    }

    public static function fromRequest(UpdateTagsRequest $request)
    {
        return new self($request->get('tags'));
    }

    public function handle()
    {
        foreach($this->tags as $id => $name) {
            Tag::find($id)->update([
                'name' => $name
            ]);
        }

        return true;
    }
}
