<?php
namespace App\Jobs\Theme;

use App\Models\Theme;
use App\Models\Language;
use App\Http\Requests\Admin\SaveThemeRequest;

class CreateTheme
{
    private $attributes;

    public function __construct(
        array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'id',
            'name',
            'meta_description',
            'meta_title',
            'position'
        ]);
    }

    public static function fromRequest(SaveThemeRequest $request)
    {
        return new self($request->all());
    }


    public function handle()
    {   
        $theme = $this->attributes['id'] ? Theme::find($this->attributes['id']) : new Theme();
        $langues = Language::where('id', 1)->get();

        foreach ($langues as $lang)
        {
            $theme->translateOrNew($lang->locale)->name = $this->attributes["name"][$lang->locale];
            $theme->translateOrNew($lang->locale)->meta_title = $this->attributes["meta_title"][$lang->locale];
            $theme->translateOrNew($lang->locale)->meta_description = $this->attributes["meta_description"][$lang->locale];  
            $theme->translateOrNew($lang->locale)->slug = str_slug( $this->attributes['name'][$lang->locale], '-');           
        }

        $theme->position = $this->attributes['position'];
             
        $theme->save();

        return $theme;      
    }
}
