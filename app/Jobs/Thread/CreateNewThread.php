<?php
namespace App\Jobs\Thread;

use App\Models\User;
use App\Models\Thread;

use Carbon\Carbon;

use App\Http\Requests\Thread\CreateThreadRequest;

class CreateNewThread
{

    private $user;
    private $expert_id;
    private $zone_id;
    private $recipient_id;
    private $message;

    public function __construct(User $user, $recipient_id, $expert_id, $zone_id, $message)
    {
        $this->user = $user;
        $this->expert_id = $expert_id;
        $this->recipient_id = $recipient_id;
        $this->zone_id = $zone_id;
        $this->message = $message;
    }

    public static function fromRequest(User $user, CreateThreadRequest $request)
    {
      return new CreateNewThread(
        $user,
        $request->get('recipient_id'),
        $request->get('expert_id'),
        $request->get('zone_id'),
        $request->get('message')
      );
    }

    public function handle()
    {
        //create threads
        $thread = new Thread();

        $thread->message = $this->message;
        $thread->zone_id = $this->zone_id;
        $thread->sender_id = $this->user->id;
        $thread->recipient_id = $this->recipient_id;
        $thread->expert_id = $this->expert_id;
        $thread->status = Thread::STATUS_OPEN;


        //TODO send notification?

        //TODO send emails?

        return $thread->save();
    }
}
