<?php
namespace App\Jobs;

use App\Http\Requests\EmailTemplateRequest;

use App\Models\EmailTemplate;

class UpdateEmailTemplate
{
    /**
    * @var \App\Models\EmailTemplate
    */
    private $template;

    /**
    * @var array
    */
    private $attributes;

    public function __construct(EmailTemplate $template, array $attributes = []) {
        $this->template = $template;
        $this->attributes = array_only($attributes, ['name', 'template', 'key', 'subject']);
    }

    public static function fromRequest(EmailTemplate $template, EmailTemplateRequest $request)
    {
        return new UpdateEmailTemplate($template, $request->all());
    }

    public function handle()
    {
        $this->template->update($this->attributes);

        return $this->template;
    }
}
