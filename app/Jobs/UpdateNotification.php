<?php
namespace App\Jobs;

use App\Models\Notification;
use App\Http\Requests\NotificationRequest;
use App\Jobs\SendNotification;

class UpdateNotification
{

    /**
    * @var \App\Models\Notification
    */
    private $notification;

    /**
    * @var array
    */
    private $attributes;

    public function __construct(Notification $notification, array $attributes = []) {
        $this->notification = $notification;
        $this->attributes = array_only($attributes, ['partner_id', 'title', 'message', 'status']);
    }

    public static function fromRequest(Notification $notification, NotificationRequest $request)
    {
        return new UpdateNotification($notification, $request->all());
    }

    public function handle()
    {
        $this->notification->update($this->attributes);

        if($this->notification->status == Notification::STATUS_PENDING) {
            (new SendNotification($this->notification))->handle();
        }

        return $this->notification;
    }
}
