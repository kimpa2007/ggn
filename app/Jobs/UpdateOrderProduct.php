<?php
namespace App\Jobs;

use App\Models\Order;
use App\Http\Requests\OrderRequest;

class UpdateOrderProduct
{

    /**
    * @var \App\Models\Notification
    */
    private $order;

    /**
    * @var array
    */
    private $attributes;

    public function __construct(Order $order, array $attributes = []) {
        $this->order = $order;
        $this->attributes = array_only($attributes, [
            'status',
            'is_payed',
            'done_at',
            'traited_by',
            'comment'
        ]);
    }

    public static function fromRequest(Order $order, OrderRequest $request)
    {

        return new UpdateOrderProduct($order, $request->all());
    }

    public function handle()
    {
        $this->order->update($this->attributes);

        return $this->order;
    }
}
