<?php
namespace App\Jobs;

use App\Http\Requests\PartnerOrderRequest;

use App\Models\CreditOperation;
use App\Models\PartnerOrder;
use App\Jobs\Email\SendTemplatedEmail;
use App\Jobs\Partner\SaveCredit;
use Carbon\Carbon;

class UpdatePartnerOrder
{

    /**
    * @var \App\Models\Notification
    */
    private $order;

    /**
    * @var array
    */
    private $attributes;

    public function __construct(PartnerOrder $order, array $attributes = []) {
        $this->order = $order;
        $this->attributes = array_only($attributes, [
            'partner_id',
            'plan_id',
            'start_at',
            'end_at',
            'status',
            'paid_at',
            'type_payment',
            'invoice'
        ]);
    }

    public static function fromRequest(PartnerOrder $order, PartnerOrderRequest $request)
    {
        return new UpdatePartnerOrder($order, $request->all());
    }

    public function handle()
    {
        $this->order->update($this->attributes);

        if($this->order->status == PartnerOrder::STATUS_PAID) {
            dispatch(new SaveCredit($this->order->partner, $this->order->amount, CreditOperation::TYPE_PARTNER_ORDER));
            dispatch(new SendTemplatedEmail('partner.credits.validate', $this->order->partner->manager->email, [
                'order' => $this->order
            ]));
        }

        return $this->order;
    }
}
