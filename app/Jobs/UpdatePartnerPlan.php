<?php
namespace App\Jobs;

use App\Models\PartnerPlan;
use App\Models\PlanZone;
use App\Models\PlanPurposed;
use App\Http\Requests\PartnerPlanRequest;
use App\Models\User;
use App\Models\Language;

class UpdatePartnerPlan
{

	/**
    * @var \App\Models\PartnerPlan
    */
    private $plan;

    /**
    * @var array
    */
    private $attributes;


    public function __construct(PartnerPlan $plan, array $attributes = []) {

		$this->plan = $plan;

		$this->attributes = array_only($attributes, [
            'name',
        //    'time',
            'status',
            'price',
            'type',
            'zones',
            'description',
            'partners_purposed',
            'months'
        ]);
	}

    public static function fromRequest($plan, PartnerPlanRequest $request)
    {
    	return new UpdatePartnerPlan( $plan, $request->all() );
    }

    public function handle()
    {


        $this->plan->months =  $this->attributes['months'];
        $this->plan->status = $this->attributes['status'];
        $this->plan->type = $this->attributes['type'];
        $this->plan->price = $this->attributes['price'];
        $langues = Language::where('id', 1)->get();

        foreach ($langues as $lang)
        {
            $this->plan->translateOrNew($lang->locale)->name = $this->attributes['name'][$lang->locale];
            $this->plan->translateOrNew($lang->locale)->description = $this->attributes['description'][$lang->locale];
            $this->plan->translateOrNew($lang->locale)->slug = str_slug( $this->attributes['name'][$lang->locale], '-') . '-'. $lang->locale. '-' .$this->plan->id;
        }


        $this->plan->save();

        if(array_key_exists('zones', $this->attributes)){
            $this->plan->zones()->sync($this->attributes['zones']);
        }

		$data = [];

        if(!empty($this->attributes['partners_purposed'])) {
			foreach($this->attributes['partners_purposed'] as $p) {
				$data[$p] = [
					"status" => PlanPurposed::STATUS_PURPOSED
				];
			}
        }

        $this->plan->partnersPurposed()->sync($data);

        return $this->plan;
    }
}
