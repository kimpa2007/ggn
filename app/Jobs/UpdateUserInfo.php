<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Requests\User\UpdateInfoRequest;
use App\Models\User;
use Hash;

class UpdateUserInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
    * @var \App\Models\User
    */
    private $user;

    /**
    * @var array
    */
    private $attributes;


    public function __construct(User $user, array $attributes = [])
    {
        $this->user = $user;

        $this->attributes = array_only($attributes, [
            'firstname',
            'lastname',
            'email',
            'telephone',
            'city',
            'postal_code',
            'address'
        ]);
    }

    public static function fromRequest(User $user, UpdateInfoRequest $request): self
    {
        return new UpdateUserInfo($user, $request->all());
    }

    public function handle()
    {
        $this->user->update(array_only($this->attributes, [
            'firstname',
            'lastname',
            'email',
            'telephone',
            'city',
            'postal_code',
            'address'
        ]));

        return $this->user;
    }
}
