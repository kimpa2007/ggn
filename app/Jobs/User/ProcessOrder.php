<?php
namespace App\Jobs\User;

use App\Models\User;
use App\Models\Order;

use App\Models\CreditOperation;
use Carbon\Carbon;

use App\Models\PartnerVoucher;
use App\Models\UserVoucher;

class ProcessOrder
{
    private $order;

    private $user;

    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    private function setPaidOrder()
    {
        if(!$this->order->update([
            "is_payed" => 1
        ])) {
            return false;
        }

        return true;
    }


    public function handle()
    {
        switch($this->order->payment_type) {

    			case Order::PAYMENT_TYPE_2: //credit + cheque
                    CreditOperation::create([
                        "user_id" => $this->user ? $this->user->id : null,
                        "type" => CreditOperation::TYPE_PRODUCT_PURCHASE,
                        "quantity" => -($this->user->credits),
                        "balance" => 0,
                        "done_at" => Carbon::now()
                    ]);

                    if(!$this->user->update([
                        "credits" => 0
                    ])) {
                        return false;
                    }
    			break;

    			case Order::PAYMENT_TYPE_3: //cheque
    				return view('user.basket-step-3-2');
    			break;

                /*
                *   FIXME : put this on another JOB :D
                */
                case Order::PAYMENT_TYPE_4: // voucher
                    if(!$this->user->partner) {
                        return false;
                    }

                    $voucher = $this->user->partner->vouchers
                        ->where('status', PartnerVoucher::STATUS_PENDING)
                        ->first();

                    if($voucher) {
                        $userVoucher = UserVoucher::create([
                            'user_id' => $this->user->id,
                            'voucher_id' => $voucher->id,
                            'order_id' => $this->order->id,
                            'spent_at' => Carbon::now()
                        ]);

                        if($userVoucher) {

                            $voucher->remain = $voucher->remain - 1;
                            $voucher->save();

                            // Increase user credits
                            $this->user->credits += $voucher->amount;

                            if($this->user->save()) {

                                CreditOperation::create([
                                    "user_id" => $this->user->id,
                                    "type" => CreditOperation::TYPE_USER_VOUCHER,
                                    "quantity" => $voucher->amount,
                                    "balance" => $this->user->credits,
                                    "done_at" => Carbon::now()
                                ]);

                                // Decrease user credits with product price
                                $this->user->credits -= $this->order->product->price;

                                if(($this->setPaidOrder()) && $this->user->save()) {
                                    CreditOperation::create([
                                        "user_id" => $this->user->id,
                                        "type" => CreditOperation::TYPE_PRODUCT_PURCHASE,
                                        "quantity" => -($this->order->product->price),
                                        "balance" => $this->user->credits,
                                        "done_at" => Carbon::now()
                                    ]);

                                    return true;
                                }

                            }
                        }
                    }
    			break;

    			default: //credit
                    $this->user->credits -= $this->order->product->price;

                    if($this->user->save()) {

                        CreditOperation::create([
                            "user_id" => $this->user ? $this->user->id : null,
                            "type" => CreditOperation::TYPE_PRODUCT_PURCHASE,
                            "quantity" => -($this->order->product->price),
                            "balance" => $this->user->credits,
                            "done_at" => Carbon::now()
                        ]);

                        $this->setPaidOrder();

                        return true;
                    }
    			break;
    		}

        return false;
    }
}
