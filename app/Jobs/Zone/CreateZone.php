<?php
namespace App\Jobs\Zone;

use App\Models\User;
use App\Models\Zone;
use App\Models\CountryZone;
use App\Models\UserZone;
use App\Models\Language;
use Auth;

use App\Http\Requests\Admin\SaveZoneRequest;


class CreateZone
{
    private $attributes;

    public function __construct(
        array $attributes = [])
    {
        $this->attributes = array_only($attributes, [
            'id',
            'name',
            'description',
            'slug',
            'status',
            'image',
            'experts',            
            'countries',
            'legend',
            'copyright',
            'meta_title',
            'meta_description',
        ]);
    }

    public static function fromRequest(SaveZoneRequest $request)
    {
        return new self($request->all());
    }


    public function handle()
    {   
        $zone = $this->attributes['id'] ? Zone::find($this->attributes['id']) : new Zone();

        foreach (Language::all() as $lang)
        {
            $zone->translateOrNew($lang->locale)->name = $this->attributes["name"][$lang->locale];
            $zone->translateOrNew($lang->locale)->slug = $this->attributes["slug"][$lang->locale];
            $zone->translateOrNew($lang->locale)->description = $this->attributes["description"][$lang->locale];
            $zone->translateOrNew($lang->locale)->legend = isset($this->attributes["legend"][$lang->locale])?$this->attributes["legend"][$lang->locale]:'';
            $zone->translateOrNew($lang->locale)->copyright = isset($this->attributes["copyright"][$lang->locale])?$this->attributes["copyright"][$lang->locale]:'';
            $zone->translateOrNew($lang->locale)->meta_title = isset($this->attributes["meta_title"][$lang->locale])?$this->attributes["meta_title"][$lang->locale]:'';
            $zone->translateOrNew($lang->locale)->meta_description = isset($this->attributes["meta_description"][$lang->locale])?$this->attributes["meta_description"][$lang->locale]:'';
        }

        $zone->image = $this->attributes["image"];
        $zone->status = $this->attributes["status"];

        if(!$this->attributes['id']){
            $zone->order = Zone::max('order') + 1;
        }

        $result = [];
        if(isset($this->attributes["experts"])){
            $experts = $this->attributes["experts"];
            foreach($experts as $key => $value){
                $result[$value] = ['type' => UserZone::TYPE_EXPERT];
            }
        }
        $zone->experts()->sync($result);

        $resultCountry = [];
        if(isset($this->attributes["countries"])){
            $countries = $this->attributes["countries"];
            foreach($countries as $keyCountry => $valueCountry){
                $resultCountry[$keyCountry] = $valueCountry;
            }
        }
        $zone->countries()->sync($resultCountry);        
        $zone->save();

        return $zone;      
    }
}
