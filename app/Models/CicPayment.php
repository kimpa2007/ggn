<?php

namespace App\Models;


use App\Traits\ImageUpload;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

use App\Models\PartnerPlan;
use App\Models\PartnerOrder;

use App\Jobs\Email\SendTemplatedEmail;


require_once(app_path()."/Helpers/MoneticoPaiement/MoneticoPaiement_Config.php");
require_once(app_path()."/Helpers/MoneticoPaiement/MoneticoPaiement_Ept.inc.php");

class CicPayment extends Model
{
	private $datas = [];
	private $oEpt;
	private $sMAC;
	private $order;

/*	public function __construct(){
	}
*/
	function initialize($order){
		$email = \Auth::user()->email;

		$this->order = $order;

		$this->datas['sReference'] = $order->id;
		$this->datas['sMontant'] = $order->amount;
		$this->datas['sDevise']  = "EUR";
		$this->datas['sTexteLibre'] = $order->id;
		$this->datas['sDate'] = date("d/m/Y:H:i:s");
		$this->datas['sLangue'] = "FR";
		$this->datas['sEmail'] = $email;
    	$this->datas['sNbrEch']  = "";
    	$this->datas['sDateEcheance1'] = "";
    	$this->datas['sMontantEcheance1'] = "";
    	$this->datas['sDateEcheance2'] = "";
    	$this->datas['sMontantEcheance2'] = "";
    	$this->datas['sDateEcheance3'] = "";
    	$this->datas['sMontantEcheance3'] = "";
    	$this->datas['sDateEcheance4'] = "";
    	$this->datas['sMontantEcheance4'] = "";
		$this->datas['sOptions'] = "";

		$this->oEpt = new \MoneticoPaiement_Ept($this->datas['sLangue']);     		
		$oHmac = new \MoneticoPaiement_Hmac($this->oEpt);      	        

		$CtlHmac = sprintf(MONETICOPAIEMENT_CTLHMAC, $this->oEpt->sVersion, $this->oEpt->sNumero, $oHmac->computeHmac(sprintf(MONETICOPAIEMENT_CTLHMACSTR, $this->oEpt->sVersion, $this->oEpt->sNumero)));

		$phase1go_fields = sprintf(MONETICOPAIEMENT_PHASE1GO_FIELDS,     $this->oEpt->sNumero,
                                              $this->datas['sDate'],
                                              $this->datas['sMontant'],
                                              $this->datas['sDevise'],
                                              $this->datas['sReference'],
                                              $this->datas['sTexteLibre'],
                                              $this->oEpt->sVersion,
                                              $this->oEpt->sLangue,
                                              $this->oEpt->sCodeSociete, 
                                              $this->datas['sEmail'],
                                              $this->datas['sNbrEch'],
                                              $this->datas['sDateEcheance1'],
                                              $this->datas['sMontantEcheance1'],
                                              $this->datas['sDateEcheance2'],
                                              $this->datas['sMontantEcheance2'],
                                              $this->datas['sDateEcheance3'],
                                              $this->datas['sMontantEcheance3'],
                                              $this->datas['sDateEcheance4'],
                                              $this->datas['sMontantEcheance4'],
                                              $this->datas['sOptions']);
                                         

		$this->sMAC = $oHmac->computeHmac($phase1go_fields);
	}
	
	function generateForm(){

		return '
			<form id="form-'.$this->order->id.'" method="post" name="MoneticoFormulaire" target="_top" action="'.MONETICOPAIEMENT_URLSERVER.'">
				<input type="hidden" name="version" value="'.$this->oEpt->sVersion.'">
				<input type="hidden" name="TPE" value="'.$this->oEpt->sNumero.'">
				<input type="hidden" name="date" value="'.$this->datas['sDate'].'">
				<input type="hidden" name="montant" value="'.$this->datas['sMontant'].$this->datas['sDevise'].'">
				<input type="hidden" name="reference" value="'.$this->datas['sReference'].'">
				<input type="hidden" name="MAC" value="'.$this->sMAC.'">
				<input type="hidden" name="url_retour" value="'.MONETICOPAIEMENT_URLRETOUR.'">
				<input type="hidden" name="url_retour_ok" value="'.$this->oEpt->sUrlOK.'">
				<input type="hidden" name="url_retour_err" value="'.$this->oEpt->sUrlKO.'">
				<input type="hidden" name="lgue" value="'.$this->oEpt->sLangue.'">
				<input type="hidden" name="societe" value="'.$this->oEpt->sCodeSociete.'">
				<input type="hidden" name="texte-libre" value="'.($this->datas['sTexteLibre']).'">
				<input type="hidden" name="mail" value="'.$this->datas['sEmail'].'">
			</form>
		';
	}

	function check($params){

		$MoneticoPaiement_bruteVars = /*getMethode();*/ $params;
		$oEpt = new \MoneticoPaiement_Ept();     		
		$oHmac = new \MoneticoPaiement_Hmac($oEpt);

		if(!array_key_exists('motifrefus', $MoneticoPaiement_bruteVars )){
			$MoneticoPaiement_bruteVars['motifrefus'] = "";
		}

		if(!array_key_exists('veres', $MoneticoPaiement_bruteVars )){
			$MoneticoPaiement_bruteVars['veres'] = "";
		}

		// Message Authentication
		$phase2back_fields = sprintf(MONETICOPAIEMENT_PHASE2BACK_FIELDS, $oEpt->sNumero,
                        $MoneticoPaiement_bruteVars["date"],
                        $MoneticoPaiement_bruteVars['montant'],
                        $MoneticoPaiement_bruteVars['reference'],
                        $MoneticoPaiement_bruteVars['texte-libre'],
                        $oEpt->sVersion,
                        $MoneticoPaiement_bruteVars['code-retour'],
                        $MoneticoPaiement_bruteVars['cvx'],
                        $MoneticoPaiement_bruteVars['vld'],
                        $MoneticoPaiement_bruteVars['brand'],
                        $MoneticoPaiement_bruteVars['status3ds'],
                        $MoneticoPaiement_bruteVars['numauto'],
                        $MoneticoPaiement_bruteVars['motifrefus'],
                        $MoneticoPaiement_bruteVars['originecb'],
                        $MoneticoPaiement_bruteVars['bincb'],
                        $MoneticoPaiement_bruteVars['hpancb'],
                        $MoneticoPaiement_bruteVars['ipclient'],
                        $MoneticoPaiement_bruteVars['originetr'],
                        $MoneticoPaiement_bruteVars['veres'],
                        $MoneticoPaiement_bruteVars['pares']
					);

		//	if ($oHmac->computeHmac($phase2back_fields) == strtolower($MoneticoPaiement_bruteVars['MAC']))
		//	{
			// =============================================================================================================================================================
			// FIN SECTION CODE
			//
			// END CODE SECTION 
			// =============================================================================================================================================================

			// =============================================================================================================================================================
			// SECTION IMPLEMENTATION : Vous devez modifier ce code afin d'y mettre votre propre logique métier
			// 
			// IMPLEMENTATION SECTION : You must adapt this code with your own application logic.
			// =============================================================================================================================================================

				$order = PartnerOrder::where('id',$MoneticoPaiement_bruteVars['reference'])->first(); 
				$plan = PartnerPlan::where('id', $order->plan_id)->first(); 

				$plan_months = $plan->months;

				$start_at = date('Y-m-d H:i:s');
				$end_at = date('Y-m-d H:i:s', strtotime(" +".$plan_months." months"));
				$paid_at =  date('Y-m-d H:i:s');

				switch($MoneticoPaiement_bruteVars['code-retour']) {

					case "Annulation" :
						// Paiement refusé
						// Insérez votre code ici (envoi d'email / mise à jour base de données)
						// Attention : une autorisation peut toujours être délivrée pour ce paiement

						//redirect to erreur 

						break;

					case "payetest":
					    // Paiement accepté sur le serveur de test
						// Insérez votre code ici (envoi d'email / mise à jour base de données)
						
						$order->status= PartnerOrder::STATUS_PAID;
        				$order->start_at = $start_at;
        				$order->end_at = $end_at;
        				$order->paid_at = $paid_at;
        				$order->updated_at = $paid_at;
        				$order->save();

        				//envoi d'un email de confirmation de commande
				       /* dispatch(new SendTemplatedEmail('order.successpayments', 'claude@espai-web.com', [
				            'order' => $order,
				            'order_id' => $order->id
				        ]));*/

						break;

					case "paiement":
						// Paiement accepté sur le serveur de production
						// Insérez votre code ici (envoi d'email / mise à jour base de données)
						$order->status= PartnerOrder::STATUS_PAID;
        				$order->start_at = $start_at;
        				$order->end_at = $end_at;
        				$order->paid_at = $paid_at;
        				$order->updated_at = $paid_at;
        				$order->save();

        				//envoi d'un email de confirmation de commande
				       /* dispatch(new SendTemplatedEmail('order.successpayments', $order->parner()->email, [
				            'order' => $order,
				         	'order_id' => $order->id
				        ]));*/

						break;

					
					/*** SEULEMENT POUR LES PAIEMENTS FRACTIONNES ***/
					/***              ONLY FOR MULTIPART PAYMENT              ***/
					case "paiement_pf2":
					case "paiement_pf3":
					case "paiement_pf4":
						// Paiement accepté sur le serveur de production pour l'échéance #N
						// Le code de retour est du type paiement_pf[#N]
						// Insérez votre code ici (envoi d'email / mise à jour base de données)
						// Le montant du paiement pour cette échéance se trouve dans $MoneticoPaiement_bruteVars['montantech']
						//
						// Payment has been accepted on the productive server for the part #N
						// return code is like paiement_pf[#N]
						// put your code here (email sending / Database update)
						// You have the amount of the payment part in $MoneticoPaiement_bruteVars['montantech']
						break;

					case "Annulation_pf2":
					case "Annulation_pf3":
					case "Annulation_pf4":
					    // Paiement refusé sur le serveur de production pour l'échéance #N
						// Le code de retour est du type Annulation_pf[#N]
						// Insérez votre code ici (envoi d'email / mise à jour base de données)
						// Le montant du paiement pour cette échéance se trouve dans $MoneticoPaiement_bruteVars['montantech']
						//
						// Payment has been refused on the productive server for the part #N
						// return code is like Annulation_pf[#N]
						// put your code here (email sending / Database update)
						// You have the amount of the payment part in $MoneticoPaiement_bruteVars['montantech']
						break;
				}

			// =============================================================================================================================================================
			// FIN SECTION IMPLEMENTATION
			// 
			// END IMPLEMENTATION SECTION
			// =============================================================================================================================================================

			// =============================================================================================================================================================
			// SECTION CODE 2 : Cette section ne doit pas être modifiée
			// 
			// CODE SECTION 2 : This section must not be modified
			// =============================================================================================================================================================

				$receipt = MONETICOPAIEMENT_PHASE2BACK_MACOK;

			/*}
			else
			{
				// traitement en cas de HMAC incorrect
				// your code if the HMAC doesn't match
				$receipt = MONETICOPAIEMENT_PHASE2BACK_MACNOTOK.$phase2back_fields;
			}*/

			// =============================================================================================================================================================
			// FIN SECTION CODE 2
			//
			// END CODE SECTION 2
			// =============================================================================================================================================================

			//-----------------------------------------------------------------------------
			// Send receipt to Monetico Paiement server
			//-----------------------------------------------------------------------------
			header("Content-Type: text/plain");
			printf (MONETICOPAIEMENT_PHASE2BACK_RECEIPT, $receipt);
			exit;
	}
}


