<?php

namespace App\Models;


use App\Traits\ImageUpload;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Content extends Model
{
    use Translatable,ImageUpload{
        ImageUpload::getAttribute  as getAttributeImageUpload;
        Translatable::getAttribute insteadof ImageUpload;

        ImageUpload::setAttribute  as setAttributeImageUpload;
        Translatable::setAttribute insteadof ImageUpload;
    }

    use ImageUpload;
	use Translatable;

    public $translationModel = 'App\Models\ContentTranslation';

    protected $imagesUpload = ['image'];
    protected $imagesUpload_2 = ['image_2'];

    const STATUS_PUBLISHED = 'PUBLISHED';
    const STATUS_DRAFT = 'DRAFT';

    const TYPE_RICHTEXT = 'RICHTEXT';
    const TYPE_MOVIE = 'MOVIE';

    const THEME_GEOPOLITICS = 'GEOPOLITICS';
    const THEME_ANALYSIS = 'ANALYSIS';
    const THEME_ECONOMY = 'ECONOMY';
    const THEME_SOCIETY = 'SOCIETY';

    protected $table = 'contents';

    public $translatedAttributes = [
		'title',
		'slug',
		'description',
        'content',
		'contact',
		'legend',
		'legend_2',
		'copyright',
		'copyright_2'
		];

    protected $fillable = [
        'id',
        'country_id',
        'type',
		'movie_on_top',
		'latest_news',
        'status',
        'order',
        'views',
        'created_by',
        'published_at'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'published_at'
    ];

    public function type()
    {
        return $this->hasOne('App\Models\ContentType', 'id', 'type_id');
    }

    public function countrie()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function owner()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\ContentMedia', 'content_id', 'id');
    }

    public function contentviewed()
    {
        return $this->hasOne('App\Models\ContentViewed', 'content_id', 'id');
    }

	public function tags()
    {
    	return $this->belongsToMany('App\Models\Tag', 'contents_tags', 'content_id', 'tag_id');
    }

    // QuestionType model
    public function questions()
    {
      return $this->hasMany('App\Models\PartnerOrder', 'content_id');
    }

    public static function getStatus()
    {
        return [
            self::STATUS_PUBLISHED => 'Publié',
            self::STATUS_DRAFT => 'Brouillon'
        ];
    }

	    public static function getTypes()
	    {
	        return [
	            self::TYPE_RICHTEXT => 'Texte',
	            self::TYPE_MOVIE => 'Vidéo',
	        ];
	    }

		public static function getThemes()
		{
			return [
				self::THEME_GEOPOLITICS => 'Geopolitics',
				self::THEME_ANALYSIS => 'Analysis',
				self::THEME_ECONOMY => 'Economy',
				self::THEME_SOCIETY => 'Society'
			];
		}

		public static function getThemesFromString($str)
		{
			switch($str) {
				case 'Geopolitics':
						return 'GEOPOLITICS';
				break;

				case 'Analysis':
						return 'ANALYSIS';
				break;

				case 'Economy':
						return 'ECONOMY';
				break;

				case 'Society':
						return 'SOCIETY';
				break;
			}
		}

	public function themes()
    {
    	return $this->belongsToMany('App\Models\Theme', 'contents_themes');
    }

    public function map()
    {
        return $this->belongsTo('App\Models\ContentMap', 'id', 'content_id');
    }

    public function title()
    {
        return trans(self::title);
    }
}
