<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentMap extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contents_maps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content_id',
        'start_at',
        'end_at',
        'legend_id',
        'latlng'
    ];

    public $timestamps = false;

    public function content()
    {
        return $this->belongsTo('App\Models\Content');
    }

    public function legend()
    {
        return $this->belongsTo('App\Models\MapLegend');
    }
}
