<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentMedia extends Model
{
    protected $table = 'contents_medias';

    protected $fillable = [
        'id',
        'content_id',
        'name',
        'value',
        'type'
    ];

    public function content()
    {
    	return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }

}
