<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;
use Dimsav\Translatable\Translatable;

class ContentTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
    	'title',
		'slug',
		'description',
        'content',
		'contact',
		'legend',
		'legend_2',
		'copyright',
		'copyright_2'
    ];
}
