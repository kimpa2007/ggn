<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentViewed extends Model
{
    protected $table = 'contents_viewed';

    protected $fillable = [
        'id',
        'user_id',
        'content_id',
        'viewed_at'
    ];

    public $timestamps = false;

    public function user()
    {
    	return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function content()
    {
    	return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }

}
