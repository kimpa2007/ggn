<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Dimsav\Translatable\Translatable;

class Country extends Model
{
    use NodeTrait;
    use Translatable;

    protected $table = 'countries';

    public $translationModel = 'App\Models\CountryTranslation';


    const STATUS_PUBLISHED = 'PUBLISHED';
    const STATUS_DRAFT = 'DRAFT';

    protected $fillable = [
        'id',
        'name',
        'slug',
        'image_align',
        'description',
        'order',
        'legend',
        'copyright'
    ];

    public $translatedAttributes = [
      'name',
      'slug',
      'description',
      'legend',
      'copyright',
      ];


    public function zones()
    {
    	return $this->hasMany('App\Models\CountryZone', 'country_id', 'id');
    }

    public function contentsCountry()
    {
    	return $this->hasMany('App\Models\Content', 'country_id', 'id');
    }
    public function contents()
    {
    	return $this->hasMany('App\Models\Content', 'country_id', 'id');
    }
    public function countryZones()
    {
        return $this->belongsToMany('App\Models\Zone', 'countries_zones');
    }

    public static function getTree($id)
    {
    	return Country::descendantsOf($id)->sortBy('order')->toTree($id);
    }
    public static function getTreeIds($id)
    {
    	$ids = [];
      $countries = Country::descendantsOf($id);

      foreach($countries as $country){
        $ids[] = $country->id;
      }

      return $ids;

    }
    public static function getPublished()
  	{
  		return Country::where('status',self::STATUS_PUBLISHED);
  	}

	  public static function getStatus()
    {
        return [
        	self::STATUS_DRAFT => 'Brouillon',
          self::STATUS_PUBLISHED => 'Publié'
        ];
    }




}
