<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;
use Dimsav\Translatable\Translatable;

class CountryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
    	'name',
		'slug',
		'description',
		'legend',
		'copyright'
    ];
}
