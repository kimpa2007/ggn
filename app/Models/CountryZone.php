<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryZone extends Model
{
    protected $table = 'countries_zones';

    protected $fillable = [
        'id',
        'country_id',
        'zone_id'
    ];

    public $timestamps = false;

    public function country()
    {
    	return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function zone()
    {
    	return $this->hasOne('App\Models\Zone', 'id', 'zone_id');
    }
}
