<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditOperation extends Model
{
    protected $table = 'credits_operations';

    const TYPE_PARTNER_GIFT = 'PARTNER_GIFT';
    const TYPE_PARTNER_VOUCHER = 'PARTNER_VOUCHER';
    const TYPE_PARTNER_ORDER = 'PARTNER_ORDER';
    const TYPE_PARTNER_SUBVENTION = 'PARTNER_SUBVENTION';
    const TYPE_ADMIN_CREDIT = 'ADMIN_CREDIT';
    const TYPE_PRODUCT_PURCHASE = 'USER_PURCHASE';
    const TYPE_USER_VOUCHER = 'USER_VOUCHER';

    const STATUS_DONE = 'DONE';
    const STATUS_PENDING = 'PENDING';
    const STATUS_REFUSED = 'REFUSED';


    /*
    *   Return translated status
    */
    public static function getTypes()
    {
        return [
            self::TYPE_PARTNER_GIFT => 'Dons association',
            self::TYPE_PARTNER_ORDER => 'Commande de crédit',
            self::TYPE_PARTNER_SUBVENTION => 'Subvention utilisateur',
            self::TYPE_ADMIN_CREDIT => 'Opération admin',
            self::TYPE_PRODUCT_PURCHASE => 'Achat de produit',
            self::TYPE_PARTNER_VOUCHER => 'Chèque-cadeau',
            self::TYPE_USER_VOUCHER => 'Chèque-cadeau'
        ];
    }

    public function getTypeName()
    {
        return isset($this->getTypes()[$this->type]) ? $this->getTypes()[$this->type] : null;
    }

    public static function getStatusOptions()
    {
        return [
            self::STATUS_DONE => 'Terminé',
            self::STATUS_PENDING => 'En attente',
            self::STATUS_REFUSED => 'Refusé'
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatusOptions()[$this->status]) ? $this->getStatusOptions()[$this->status] : null;
    }

    protected $fillable = [
        'id',
        'partner_id',
        'user_id',
        'partner_order_id',
        'quantity',
        'balance',
        'type',
        'status',
        'done_at'
    ];

    public function user()
    {
    	return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function partner()
    {
    	return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }

    public function partnerOrder()
    {
    	return $this->hasOne('App\Models\PartnerOrder', 'id', 'partner_order_id');
    }

    public function order()
    {
      return $this->belongsTo('App\Models\Order', 'id', 'credit_id');
    }

}
