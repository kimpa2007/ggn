<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Models\CustompageTranslation;

class CustomPage extends Model
{
	use Translatable;

    protected $table = 'custompages';

    public $translationModel = 'App\Models\CustompageTranslation';

	public $translatedAttributes = [
		'title',
		'slug',
		'content',
	];

    protected $fillable = [
		'identifier'
    ];

    public function scopeFindBySlug($query, $slug)
    {
        $translation = CustompageTranslation::where('slug', $slug)->first();
        return $query->find($translation->custom_page_id);
    }
}
