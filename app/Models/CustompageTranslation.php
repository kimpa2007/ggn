<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;
use Dimsav\Translatable\Translatable;

class CustompageTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = [
    	'title',
		'slug',
		'content'
    ];
}
