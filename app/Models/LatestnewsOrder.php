<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Content;

class LatestnewsOrder extends Model
{
    protected $table = 'latestnews_order';

    protected $fillable = [
        'content_id',
        'order_news'
    ];

    public $timestamps = false;

    public function content()
    {
    	return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }
}
