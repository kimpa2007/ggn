<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;

class LinkMovie extends Model
{

    protected $table = 'link_movie';

    protected $fillable = [
      'id',
      'ip',
      'user_agent',
      'file_id',
      'file_patch',
      'created_at',
    ];

}
