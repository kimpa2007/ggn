<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Dimsav\Translatable\Translatable;

class MapLegend extends Model
{
    use Translatable;

    const STATUS_ENABLE = 'ENABLE';
    const STATUS_DISABLE = 'DISABLE';

    protected $table = 'maps_legends';

    protected $fillable = [
        'icon',
        'status',
        'color',
        'key'
    ];

    public $translatedAttributes = [
        'name'
    ];

    public $translationModel = 'App\Models\MapLegendTranslate';

    public $timestamps = false;

    public static function getStatus()
    {
        return [
            self::STATUS_ENABLE => 'Actif',
            self::STATUS_DISABLE => 'Inactif',
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status]) ? $this->getStatus()[$this->status] : null;
    }

}
