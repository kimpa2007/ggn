<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;
use Dimsav\Translatable\Translatable;

class MapLegendTranslate extends Model
{
    public $timestamps = false;

    protected $table = 'map_legend_translations';

    protected $fillable = [
    	'name'
    ];
}
