<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'id',
        'partner_id',
        'title',
        'message',
        'sent_at',
        'status',
    ];

    const STATUS_SENT = 'SENT';
    const STATUS_SENDING = 'SENDING';
    const STATUS_PENDING = 'PENDING';
    const STATUS_DRAFT = 'DRAFT';

    /*
    *   Return translated status
    */
    public static function getStatus()
    {
        return [
            self::STATUS_SENT => 'Envoyé',
            self::STATUS_SENDING => 'En cours d\'envoi',
            self::STATUS_PENDING => 'En attente',
            self::STATUS_DRAFT => 'Brouillon'
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status]) ? $this->getStatus()[$this->status] : null;
    }

    public function partner()
    {
    	return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }

    public function users()
    {
    	return $this->belongsToMany('App\Models\User', 'users_notifications', 'notification_id', 'user_id');
    }

    public function scopeByPartner($query,$partner_id)
    {
        return $query->where('partner_id', $partner_id)->withCount(['users'])->orderBy('created_at','desc');
    }

}
