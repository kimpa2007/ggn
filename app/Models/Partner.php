<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Storage;
use App\Models\CreditOperation;
use App\Models\PartnerOrder;
use Carbon\Carbon;

use App\Traits\ImageUpload;

class Partner extends Model
{
    use ImageUpload;

    const TYPE_NORMAL = 'NORMAL';
    const TYPE_OTHER = 'OTHER';

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_DISABLED = 'DISABLED';

    protected $table = 'partners';

    protected $fillable = [
        'id',
        'name',
        'type',
        'status',
        'color1',
        'color2',
        'color3',
        'title',
        'logo',
        'credits',
        'manager_id',
        'address',
        'referent_data'
    ];

    protected $imagesUpload = [
        'logo'
    ];

    public function rules()
    {
        return $this->belongsToMany('App\Models\Rule', 'partners_rules', 'partner_id', 'rule_id');
    }

    public function hasRule($rule)
    {
        return $this->rules()->where('type',$rule)->exists();
    }

    public function users()
    {
        return $this->hasMany('App\Models\User', 'partner_id', 'id');
    }

    public function purposedPlans()
    {
        return $this->belongsToMany('App\Models\PartnerPlan', 'plans_purposed', 'partner_id', 'plan_id');
    }

    public function plansPurposed()
    {
        return $this->hasMany('App\Models\PlanPurposed', 'partner_id', 'id');
    }

    public function plans()
    {
        return $this->belongsToMany('App\Models\PartnerPlan', 'partners_orders', 'partner_id', 'plan_id')
            ->where('partners_orders.type', PartnerOrder::TYPE_PLAN);
    }

    public function manager()
    {
        return $this->hasOne('App\Models\User', 'id', 'manager_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\PartnerOrder', 'partner_id', 'id');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\PartnerVoucher', 'partner_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification', 'partner_id', 'id');
    }

    public function creditsOperations()
    {
        return $this->hasMany('App\Models\CreditOperation', 'partner_id', 'id');
    }

    public function getZonesIds()
    {
        //return all payed orders, with its plan, and with its zones

        $ordersWithZones = $this->orders()
          ->where('status', PartnerOrder::STATUS_PAID)
          ->where('start_at', '<=', date("Y-m-d"))
          ->where('end_at', '>=', date("Y-m-d"))
          ->get();

        $zones = [];
        foreach ($ordersWithZones as $order) {

            $PlanZoneItems = $order->plan->zones()->get();

            foreach ($PlanZoneItems as $PlanZone) {
                $zones[] = $PlanZone->id;
            }
        }

        return $zones;
    }

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /*
    *   Return translated status
    */
    public static function getStatus()
    {
        return [
            self::STATUS_ACTIVE => 'Activé',
            self::STATUS_DISABLED => 'Désactivé'
        ];
    }

    /*
    *   Return translated status
    */
    public static function getTypes()
    {
        return [
            self::TYPE_NORMAL => 'Grand compte',
            self::TYPE_OTHER => 'Autre'
        ];
    }


    // FIX : trait do this !
    public function delete()
    {
        $this->deleteLogo();

        return parent::delete();
    }


    public function deleteLogo()
    {
        Storage::has($this->logo) ? Storage::delete($this->logo) : false;
        $this->logo = null;
        $this->save();
    }

    // FIXME : put into job :) Jobs/PartnerAddCredit
    // public function addCredit($quantity, $type)
    // {
    //     if($quantity && $type) {
    //         $this->credits += $quantity;
    //
    //         if($this->save()) {
    //             CreditOperation::create([
    //                 "partner_id" => $this->id,
    //                 "type" => $type,
    //                 "quantity" => $quantity,
    //                 "done_at" => Carbon::now()
    //             ]);
    //
    //             return true;
    //         }
    //     }
    //
    //     return false;
    // }
}
