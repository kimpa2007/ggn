<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Storage;

class PartnerOrder extends Model
{
    const STATUS_PENDING = 'PENDING';
    const STATUS_PAID = 'PAID';
    const STATUS_CANCELED = 'CANCELED';

    const TYPE_CREDIT = 'CREDIT';
    const TYPE_PLAN = 'PLAN';

    const PAYMENT_CB = 'CB';
    const PAYMENT_TRANSFERT = 'TRANSFERT';
    const PAYMENT_CHEQUE = 'CHEQUE';

    protected $table = 'partners_orders';

    protected $fillable = [
        'id',
        'partner_id',
        'status',
        'type_payment',
        'paid_at',
        'type',
        'amount',
        'plan_id',
        'start_at',
        'end_at',
        'invoice'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'paid_at',
        'start_at',
        'end_at'
    ];

    // public function setStartAtAttribute($value)
    // {
    //     $this->attributes['start_at'] = Carbon::createFromFormat('d-m-Y', $value);
    // }
    //
    // public function setEndAtAttribute($value)
    // {
    //     $this->attributes['end_at'] = Carbon::createFromFormat('d-m-Y', $value);
    // }
    //
    // public function setPaidAtAttribute($value)
    // {
    //     $this->attributes['paid_at'] = Carbon::createFromFormat('d-m-Y', $value);
    // }

    public function partner()
    {
    	return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }

    public function plan()
    {
    	return $this->hasOne('App\Models\PartnerPlan', 'id', 'plan_id');
    }

    public function planZones()
    {
        return $this->hasManyThrough(
            'App\Models\PlanZone', 'App\Models\PartnerPlan',
            'id', //Foreign key of PartnerOrder model on PartnerPlan
            'zone_id',
            'id'
        );
    }

    public static function getStatus()
    {
        return [
            self::STATUS_PENDING => 'En attente',
            self::STATUS_PAID => 'Payé',
            self::STATUS_CANCELED => 'Annulé'
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status])
            ? $this->getStatus()[$this->status]
            : null;
    }

    public static function getType()
    {
        return [
            self::TYPE_PLAN => 'Plan',
            self::TYPE_CREDIT => 'Crédit'
        ];
    }

     public function getPlanName()
    {
        $plan = $this->plan()->get();
        
        if(!empty($plan)){
            return $plan->first()->name;
        }
        return "";
    }


    public function getTypeName()
    {
        return isset($this->getType()[$this->type])
            ? $this->getType()[$this->type]
            : null;
    }

    public static function getTypePayment()
    {
        return [
            self::PAYMENT_CB => 'Carte bleu',
            self::PAYMENT_TRANSFERT => 'Virement',
            self::PAYMENT_CHEQUE => 'Chèque bancaire'
        ];
    }

    public function getTypePaymentName()
    {
        return isset($this->getTypePayment()[$this->type_payment])
            ? $this->getTypePayment()[$this->type_payment]
            : null;
    }

    public function scopeByPartner($query,$id)
    {
        return $query->where('partner_id',$id);
    }

    public function delete()
    {
        $this->deleteAttachment();

        return parent::delete();
    }

    public function deleteAttachment()
    {
        Storage::has($this->invoice) ? Storage::delete($this->invoice) : false;
        $this->invoice = null;
        $this->save();
    }

}
