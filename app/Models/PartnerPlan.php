<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Models\PlanZone;

use DB;

class PartnerPlan extends Model
{
    const TYPE_PUBLIC = 'PUBLIC';
    const TYPE_PRIVATE = 'PRIVATE';

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_UNACTIVE = 'UNACTIVE';

    protected $table = 'partners_plans';

    const VISU_3 = 10;
    const VISU_1_MOIS = 11;

 use Translatable;

    public $translationModel = 'App\Models\PartnerPlanTranslation';

    public $translatedAttributes = [
        'name',
        'description'
      ];

    protected $fillable = [
        'id',
        'created_by',
        'name',
        'description',
        'price',
        'status',
       // 'time',
        'type',
        'months'
    ];

    public function content()
    {
      return $this->belongsTo('App\Models\Content', 'content_id');
    }

    public function zones()
    {
    	return $this->belongsToMany('App\Models\Zone', 'plans_zones', 'plan_id', 'zone_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\PartnerOrder', 'plan_id', 'id');
    }

    public function partnersPurposed()
    {
    	return $this->belongsToMany('App\Models\Partner', 'plans_purposed', 'plan_id', 'partner_id');
    }

    public static function getTypes()
    {
        return [
            self::TYPE_PUBLIC => 'Public',
            self::TYPE_PRIVATE => 'Privé'
        ];
    }

	public static function getTimes()
	{
		return [
            0.03 => "3 jours",
			1 => "1 mois",
            3 => "3 mois",
            6 => "6 mois",
            12 => "1 an",
            24 => "2 ans"
		];
	}

    public function getTypeName()
    {
        return isset($this->getStatus()[$this->status])
            ? $this->getStatus()[$this->status]
            : null;
    }


    public static function getStatus()
    {
        return [
            self::STATUS_ACTIVE => 'Activé',
            self::STATUS_UNACTIVE => 'Désactivé'
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status])
            ? $this->getStatus()[$this->status]
            : null;
    }

    public function addTheme(ReplyAble $replyAble)
    {
        $this->replyAbleRelation()->associate($replyAble);
    }


    public static function getFullName()
    {
        return PartnerPlan::where('status',self::STATUS_ACTIVE)->select([
            'partners_plans.*',
            DB::raw("CONCAT(partners_plans.name,' - ',partners_plans.time,' moins') as full_name"),
        ]);
    }

	/*
	 * Return the expert zones names
	 */
	public function getZonesNames()
	{
		$zones = "";

		if($this->zones->isNotEmpty()) {
			foreach($this->zones as $zone){
				$zones .= $zone->name.' ';
            }
        }

		return $zones;
	}

}
