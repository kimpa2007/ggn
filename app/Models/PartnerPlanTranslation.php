<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class PartnerPlanTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = [
		'name',
		'slug',
		'description'
    ];


}
