<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerVoucher extends Model
{
    const STATUS_PENDING = 'PENDING';
    const STATUS_SPENT = 'SPENT';

    protected $table = 'partners_vouchers';

    protected $fillable = [
        'id',
        'partner_id',
        'amount',
        'total_amount',
        'quantity',
        'remain',
        'status',
        'created_at',
    ];

    protected $dates = [
        'created_at'
    ];


    public function partner()
    {
        return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }


    public static function getStatus()
    {
        return [
            self::STATUS_PENDING => 'En attente',
            self::STATUS_SPENT => 'Dépensé'
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status])
            ? $this->getStatus()[$this->status]
            : null;
    }
}
