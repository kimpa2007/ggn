<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanPurposed extends Model
{
    protected $table = 'plans_purposed';

    const STATUS_PURPOSED = 'PURPOSED';
    const STATUS_REFUSED = 'REFUSED';
    const STATUS_ACCEPTED = 'ACCEPTED';

    protected $fillable = [
        'id',
        'plan_id',
        'partner_id',
        'status'
    ];

    public $timestamps = false;

    public function plan()
    {
    	return $this->hasOne('App\Models\PartnerPlan', 'id', 'plan_id');
    }

    public function partner()
    {
    	return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }

    public static function getStatus()
    {
        return [
            self::STATUS_PURPOSED => 'Proposé',
            self::STATUS_REFUSED => 'Refusé',
            self::STATUS_ACCEPTED => 'Accepté'
        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status])
            ? $this->getStatus()[$this->status]
            : null;
    }
}
