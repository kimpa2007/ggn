<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanZone extends Model
{
    protected $table = 'plans_zones';

    protected $fillable = [
        'id',
        'plan_id',
        'zone_id'
    ];

    public $timestamps = false;

    public function plan()
    {
    	return $this->hasOne('App\Models\PartnerPlan', 'id', 'plan_id');
    }

    public function zone()
    {
    	return $this->hasOne('App\Models\Zone', 'id', 'zone_id');
    }
}
