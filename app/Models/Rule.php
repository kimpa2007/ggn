<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    const RULE_PARTNER_CREDITS_GRANT_USERS = 'PARTNER_CREDITS_GRANT_USERS';

    const TYPE_PARTNER = 'PARTNER';
    const TYPE_USER = 'USER';

    protected $fillable = [
      'description',
      'rule',
      'type'
    ];

    public $timestamps = false;
}
