<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Models\ThemeTranslation;

class Theme extends Model
{
	use Translatable;

    public $translationModel = 'App\Models\ThemeTranslation';

    protected $table = 'themes';

    public $translatedAttributes = [
		'name',
		'slug',
		'meta_title',
        'meta_description'
		];

    protected $fillable = [
        'id',
        'position',
        'is_menu'        
    ];

	protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function contents()
    {
    	return $this->belongsToMany('App\Models\Content', 'contents_themes');
    }	

    public function scopeFindBySlug($query,$slug)
    {
        $translation = ThemeTranslation::where('slug', $slug)->first();
        return $query->find($translation->theme_id);
    }

}
