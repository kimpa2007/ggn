<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranslateTool extends Model
{
    protected $fillable = [
        'label',
        'text_fr',
        'text_en'
    ];

    public static function getAll(string $locale = null)
    {
        if(empty($locale)){
            return TranslateTool::all();
        } else {
            return TranslateTool::select('label','text_'.$locale)->get();
        }

    }

    public static function getTranslate(int $id)
    {
        return TranslateTool::where('id',$id)->first();
    }
}
