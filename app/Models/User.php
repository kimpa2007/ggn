<?php

namespace App\Models;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Acoustep\EntrustGui\Contracts\HashMethodInterface;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\ImageUpload;

use App\Repositories\ZoneRepository;

use App\Models\Zone;
use App\Models\Order;
use App\Models\PartnerVoucher;
use App\Models\UserZone;
use App\Models\Role;
use Hash;
use Auth;

class User extends Model implements
    AuthenticatableContract,
    CanResetPasswordContract,
    //ValidatingModelInterface,
    HashMethodInterface
{
    use Authenticatable,
        CanResetPassword,
        //ValidatingModelTrait,
        EntrustUserTrait,
        //SoftDeletes,
        Notifiable,
        ImageUpload;

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_REFUSED = 'REFUSED';

    protected $throwValidationExceptions = true;

    protected $imagesUpload = ['image'];

    protected $table = 'users';

    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'telephone',
        'image',
        'password',
        'status',
        'partner_id',
        'credits',
        'city',
        'postal_code',
        'address',
        'comment_on_signup'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $hashable = [
        'password'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // protected $rulesets = [
    //     'creating' => [
    //         'email'      => 'required|email|unique:users',
    //         'password'   => 'required',
    //     ],
    //     'updating' => [
    //         'email'      => 'required|email|unique:users',
    //         'password'   => '',
    //     ],
    // ];

    public function entrustPasswordHash()
    {
        $this->password = Hash::make($this->password);
        $this->save();
    }

    /*
    *   Relations
    */
    public function expertZones()
    {
        return $this->belongsToMany('App\Models\Zone', 'users_zones')
            ->where('type', UserZone::TYPE_EXPERT)
            ->withPivot('type', 'type');
    }

    public function userZones()
    {
        if ($this->partner()) {
            //has parnet return partner zones
        } else {
            //return null
          return null;
        }
    }

    public function receivedThreads()
    {
        return $this->hasMany('App\Models\Thread', 'recipient_id', 'id');
    }


    public function sentThreads()
    {
        return $this->hasMany('App\Models\Thread', 'sender_id', 'id');
    }

    public function sentQuestions()
    {
        //return only questions, not answers
        return $this->hasMany('App\Models\Thread', 'sender_id', 'id')
          ->whereNull('parent_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'user_id', 'id');
    }

    public function sendedThreads()
    {
        return $this->hasMany('App\Models\Thread', 'sender_id', 'id');
    }

    public function partner()
    {
        return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\UserProfile', 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'roles_users', 'user_id', 'role_id');
    }

    public function creditOperations()
    {
        return $this->hasMany('App\Models\CreditOperation', 'user_id', 'id');
    }

    public function notifications()
    {
        return $this->belongsToMany('App\Models\Notification', 'users_notifications', 'user_id', 'notification_id');
    }

    public function experts()
    {
        return $this->whereHas('roles', function ($query) {
            $query->where('name', 'expert');
        });
    }


    /**/

      public function contentViews()
      {
          return $this->hasMany('App\Models\ContentViewed', 'user_id', 'id');
      }

    /**
     * Get the user's full name.
     */
    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getCategoriesId()
    {
        if ($this->expertZones->isNotEmpty()) {
            $zonesId = $this->expertZones->map(function ($item, $key) {
                return $item->id;
            });

            return Country::WhereIn('zone_id', $zonesId)->get()->map(function ($item, $key) {
                return $item->id;
            });
        }

        return null;
    }

    /*
     * Return the expert zones into an array of ids
     */
    public function getExpertZonesIds()
    {
        if ($this->expertZones->isNotEmpty()) {
            $zonesId = $this->expertZones->map(function ($item, $key) {
                return $item->id;
            });

            return $zonesId;
        }

        return null;
    }

    /*
     * Return the expert zones names
     */
    public function getExpertZonesNames()
    {
        $zones = "";

        if ($this->expertZones->isNotEmpty()) {
            $zones = "";

            foreach ($this->expertZones as $zone) {
                $zones .= $zone->name.' ';
            }
        }

        return $zones;
    }

    /*
    *   Return translated status
    */
    public static function getStatus()
    {
        return [
            self::STATUS_ACTIVE => 'Activé',
            self::STATUS_REFUSED => 'Désactivé'
        ];
    }





    public function getRoleId()
    {
        $role = isset($this->roles) ? $this->roles : null;
        $role = $role ? $role->first() : null;
        return $role ? $role->id : null;
    }

    public function getRoleName()
    {
        $role = isset($this->roles) ? $this->roles : null;
        $role = $role ? $role->first() : null;
        return $role ? $role->display_name : null;
    }

    public function isZoneAllowed($zoneId)
    {

        if ($this->partner) {
            $availableZones = $this->partner->first()->getZonesIds();
            $result_array = array_intersect($zoneId, $availableZones);
            if (!empty($result_array)) {
                return true;
            }
        }
        return false;

    }

    /**
    * Return array of ids of zones
    */
    public function getAllowedZones()
    {
        $zones = [];

        if ($this->hasRole('user')) {
            if($this->partner_id){
                return $this->partner()->first()->getZonesIds();
            }
        } elseif ($this->hasRole('expert')) {
            //return $this->getExpertZonesIds();
            return ZoneRepository::getPublishedIds();
        } elseif ($this->hasRole('partner')) {
            if($this->partner_id){
                return $this->partner()->first()->getZonesIds();
            }
        } elseif ($this->hasRole(['admin','super-admin'])) {
            return ZoneRepository::getPublishedIds();
        }

        return $zones;
    }

    public function scopeWithStatus($query, $thestatus)
    {
        return $query->where('status', $thestatus);
    }
    public function scopeAtPartner($query, $thepartner)
    {
        return $query->where('partner_id', $thepartner);
    }

    public function newNotifications()
    {
        return $this->notifications()->wherePivot('status', UserNotification::STATUS_NEW)->get();
    }

    public function canPayOrderWithPartnerVoucher(Order $order)
    {
        if (!$this->partner) {
            return false;
        }

        $voucher = $this->partner->vouchers
            ->where('status', PartnerVoucher::STATUS_PENDING)
            ->first();

        return $voucher ? true : false;
    }

    public function hasAbonnementEnCoursValide(){
        $now = date('Y-m-d');

        $orders = PartnerOrder::where('partner_id', Auth::user()->partner_id)
                                ->where('start_at', '<=' , $now)
                                ->where('end_at', '>=', $now)
                                ->where('status', '=', PartnerOrder::STATUS_PAID)
                                ->get();
        if($orders->count()){
            return true;
        }
        return false;
    }



    public function canAccessContent($content=null)
    {
        //$country = Country::where('id', $content->country_id)->firstOrFail();
        //$zonesIds = $country->zones->pluck('zone_id')->toArray();

        /*
            pour l'instant ok a tout le monde si connecté, c'est le mode gratuit!
            return true;
        */



        //1 - a un compte super admin => ok
        if($this->hasRole(['super-admin']) or $this->hasRole(['expert']) ) {
            return true;
        }


        //2- current user a un abonnement en cours de validitié ? 
        if($this->hasAbonnementEnCoursValide() ){
            return true;
        }

        return false;
    }
}
