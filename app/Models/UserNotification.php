<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = 'users_notifications';

    const STATUS_NEW = 'NEW';
    const STATUS_READED = 'READED';

    protected $fillable = [
        'user_id',
        'notification_id',
        'read_at',
        'received_at',
        'status'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function notification()
    {
        return $this->hasOne('App\Models\Notification', 'id', 'notification_id');
    }
}
