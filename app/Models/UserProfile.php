<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Traits\ImageUpload;

class UserProfile extends Model
{
    use Translatable;

    public $translationModel = 'App\Models\UserProfileTranslation';

    public $translatedAttributes = [
        'content'
    ];

    protected $imagesUpload = ['image'];

    protected $table = 'users_profiles';

    protected $fillable = [
        'id',
        'user_id',
        'image',
        'order',
        'content'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
