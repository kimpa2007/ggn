<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;
use Dimsav\Translatable\Translatable;

class UserProfileTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = [
		'content'
    ];
}
