<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVoucher extends Model
{
    protected $table = 'users_vouchers';

    protected $fillable = [
        'id',
        'voucher_id',
        'user_id',
        'order_id',
        'spent_at',
    ];

    public $timestamps = false;

    protected $dates = [
        'created_at'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function order()
    {
        return $this->hasOne('App\Models\Order', 'id', 'order_id');
    }

    public function voucher()
    {
        return $this->hasOne('App\Models\PartnerVoucher', 'id', 'voucher_id');
    }
}
