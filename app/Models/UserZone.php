<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserZone extends Model
{
    protected $table = 'users_zones';

    const TYPE_EXPERT = 'EXPERT';

    protected $fillable = [
        'id',
        'zone_id',
        'user_id',
        'type'
    ];

    public $timestamps = false;
}
