<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ZoneRequested extends Model
{
    const STATUS_PENDING = 'PENDING';
    const STATUS_DONE = 'DONE';

    protected $table = 'zones_requested';

    protected $fillable = [
        'id',
        'partner_id',
        'zone_id',
        'status',
        'requested_at'
    ];

    public $timestamps = false;

    public function partner()
    {
    	return $this->hasOne('App\Models\Partner', 'id', 'partner_id');
    }

    public function zone()
    {
    	return $this->hasOne('App\Models\Zone', 'id', 'zone_id');
    }

    public static function getStatus()
    {
        return [
            self::STATUS_PENDING => 'En attente',
            self::STATUS_DONE => 'Accepté',

        ];
    }

    public function getStatusName()
    {
        return isset($this->getStatus()[$this->status])
            ? $this->getStatus()[$this->status]
            : null;
    }
}
