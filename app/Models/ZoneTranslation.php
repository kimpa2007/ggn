<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageUpload;
use Dimsav\Translatable\Translatable;

class ZoneTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
    	'name',
		'slug',
		'description',
		'legend',
		'copyright',
		'meta_title',
		'meta_description'
    ];
}
