<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Request;
use Redis;
use JWTAuth;
use Auth;
use Illuminate\Support\Facades\Input;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //exit();

        // $authHeader = JWTAuth::getToken();
        // if($authHeader){
        //     dd('header: ' . $authHeader);
        // }
        // //DOMAIN_CE
        // if( $authHeader && Redis::get( str_replace('bearer ', '', $authHeader ) ) ){
        //     $user = JWTAuth::parseToken()->authenticate();
        //     print_r($user);
        //     exit();
        //
        //     Auth::loginUsingId($user->id);
        //
        //     //dd(JWTAuth::getToken());
        //     Redis::del($token);
        //
        // //DOMAIN
        // }

        // CE style definition
        if(Request::server("SERVER_NAME") == env('WEBSITE_DOMAIN_CE')) {
            View::share('is_ce', 1);
            View::share('ce_style', 1);
        } else {
            View::share('is_ce', 0);
            View::share('ce_style', 0);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
