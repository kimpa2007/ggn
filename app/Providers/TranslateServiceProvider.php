<?php

namespace App\Providers;

use App\Models\TranslateTool;
use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Artisan;


class TranslateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $keys = $this->getKeys();
        View::share('translateKey',$keys);
    }

    private function getKeys(){

        //delete cache when switching language
        $locale = LaravelLocalization::getCurrentLocale();
        return TranslateTool::getAll($locale)->pluck('text_'.$locale,'label')->toArray();
        Cache::put('keys',$keys, Carbon::now()->addSecond(7)); //7 seconds is enough to load one page

    }


}
