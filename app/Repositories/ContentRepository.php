<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\Content;
use App\Models\Tag;
use App\Models\Zone;

use DB;
use Auth;

/**
 * Interface ContentRepository
 * @package namespace App\Repositories;
 */
class ContentRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\Content";
    }


    public function getPublished()
    {
        return Content::where([
        	'status' => Content::STATUS_PUBLISHED
        ])->orderBy('published_at','ASC');
    }

    /*public function getLatestNews()
    {
        return Content::where([
        	'latest_news' => 1
        ]);
    }*/


    public static function getPublishedIds()
    {

      $zones = Content::where([
        'status' => Content::STATUS_PUBLISHED
      ])->orderBy('order','ASC')->get();

      $contentId = $contents->map(function ($item, $key) {
          return $item->id;
      });

      return $contentId;
    }


    public function searchByTag($q)
    {
        $tag = Tag::where('name', 'LIKE', '%' . $q . '%')->get();

        $query = Content::where('status', Content::STATUS_PUBLISHED);

        if($tag->isEmpty()) {

            if ($q == trim($q) && strpos($q, ' ') !== false) {
              $q = explode(" ", $q);

              $tag = Tag::where('name', 'LIKE', $q[0] . '%')
                ->OrWhere('name', 'LIKE', $q[1] . '%')
                ->get();

                foreach($tag as $k => $t) {
                    if(strpos($t->name, ' ') !== false) {
                        $tag->forget($k);
                    }
                }
            }

            foreach($tag as $t) {
                $query->whereHas('tags', function ($query) use ($t) {
                    $query->where('tag_id', $t->id);
                });
            }

        } else {
            $query->whereHas('tags', function ($query) use ($tag) {
                $query->whereIn('tag_id', $tag->pluck('id')->toArray());
            });
        }

        return !$tag->isEmpty()
            ? $query->orderBy('published_at','DESC')
            : null;
    }


    public function searchByTheme($theme_id, $page = null, $limit = null)
    {
        $query = Content::where(['status' => Content::STATUS_PUBLISHED]);

       /* if (Auth::user() && (Auth::user()->hasRole('partner') || Auth::user()->hasRole('user'))) {
            // Get countries Ids of users zones
            $zonesIds = Auth::user()->getAllowedZones();
            $zones = Zone::whereIn('id', $zonesIds)->with('countries')->get();
            $countriesIds = [];
            foreach ($zones as $z) {
                foreach ($z->countries->pluck('id')->toArray() as $countryId) {
                    if (!in_array($countryId, $countriesIds)) {
                        $countriesIds[] = $countryId;
                    }
                }
            }

            $query->whereIn('country_id', $countriesIds);
        }*/

        $query->whereHas('themes', function($query) use ($theme_id) {
            $query->where('id', $theme_id );
        }) ;

        if($page){
            $query->offset($page * $limit);
        }
        if($limit){
            $query->limit($limit);
        }

        return $query->orderBy('published_at', 'desc')->get();
    }

    public function searchByCountryTheme($country_id, $theme_id = null, $page = null, $limit = null)
    {

        $query = Content::where('country_id', $country_id)->where(['status' => Content::STATUS_PUBLISHED])->orderBy('published_at','ASC');

        if($theme_id){
          $query->whereHas('themes', function($query) use ($theme_id)
                    {
                        $query->where('id', $theme_id );
                    }) ;
        }

        if($page){
          $query->offset($page * $limit);
        }

        if($limit){
          $query->limit($limit);
        }

        return $query->orderBy('published_at', 'desc')->get();

    }


}
