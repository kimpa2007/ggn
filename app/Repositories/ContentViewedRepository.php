<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\ContentViewed;
use DB;

/**
 * Interface ContentViewedRepository
 * @package namespace App\Repositories;
 */
class ContentViewedRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\ContentViewed";
    }

    public function getDatatableData()
    {
        return ContentViewed::with([
            'content'
        ]);
    }

}
