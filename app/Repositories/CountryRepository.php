<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\Country;
use DB;

/**
 * Interface CountryRepository
 * @package namespace App\Repositories;
 */
class CountryRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\Country";
    }

    public function getDatatableData()
    {
        return Country::with([
        	'zones'
        ]);
    }


    public function getPublished()
    {
        return Country::where([
        	'status' => Country::STATUS_PUBLISHED
        ])->orderBy('order','ASC');
    }

    public static function getPublishedIds()
    {

      $countries = Country::where([
        'status' => Country::STATUS_PUBLISHED
      ])->orderBy('order','ASC')->get();

      $countriesId = $countries->map(function ($item, $key) {
          return $item->id;
      });

      return $countriesId;
    }
    /* */
    public function getCountryTree($zones)
  	{

          $traverse = function (&$countriesTree,$countries, $fieldname,$level) use (&$traverse) {

  					$level++;

  		            foreach ($countries as $country) {

  		                $status = '';

  										array_push($countriesTree,array(
  											"name" => $country->name,
  											"value" => $country->id,
  											"parent" => $country->children->count() ? 1 : 0,
  											"level" => $level,
  											"status" => $status
  										));

  		                $traverse($countriesTree,$country->children, $fieldname,$level);
  		            }
	        };

	        $countriesTree = array();

					foreach($zones as $zone) {

              $level = 1;

	            array_push($countriesTree,array(
								"name" => $zone->name,
								"value" => '',
								"parent" => true,
								"level" => $level,
								"status" => ''
							));

              foreach($zone->countries as $country) {

                $level = 2;

              	if(!$country->parent_id) {

                  array_push($countriesTree,array(
                    "name" => $country->name,
                    "value" => $country->id,
                    "parent" => $country->children->count() ? 1 : 0,
                    "level" => $level,
                    "status" => ''
                  ));

              		//all parents
                  $traverse($countriesTree,Country::getTree($country->id), 'category_id',$level);
                }
              }
          }

  		return $countriesTree;
  	}

    /* */

}
