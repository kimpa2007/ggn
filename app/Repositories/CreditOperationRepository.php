<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\CreditOperation;
use DB;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class CreditOperationRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\CreditOperation";
    }

    public function getPartnerSubventionPetitions($partnerId)
    {
      return CreditOperation::with([
          'user',
          'order.product'
        ])
        ->where('type', CreditOperation::TYPE_PARTNER_SUBVENTION)
        ->where('partner_id',$partnerId)
        ->orderBy('done_at','DESC')->get();
    }

}
