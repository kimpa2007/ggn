<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\EmailTemplate;
use DB;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class EmailTemplateRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\EmailTemplate";
    }

}
