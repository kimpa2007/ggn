<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\Notification;
use App\Models\UserNotification;
use App\Models\Partner;
use DB;
use Auth;
use Carbon\Carbon;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class NotificationsRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }

    public function notificationsByPartner($id){

    	return Notification::byPartner($id)->get();
    }


    public function usersPerNotification($id){

        return Notification::findOrFail($id)->users()->withPivot('read_at','received_at','status')->get();
    }

    public function readNotification($id){
        Auth::user()->notifications()->updateExistingPivot($id, ['read_at'=>Carbon::now(),'status'=>UserNotification::STATUS_READED]);
        return 'ok';
    }

    

    public function store($data){
        $newNotif = new Notification($data);
        $newNotif->save();
        return $newNotif;
    }

    public function getDraftNotification($id){
        return Notification::findOrFail($id);
    }
}
