<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\PartnerOrder;
use DB;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class PartnerOrderRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PartnerOrder::class;
    }

    public function allByPartner($id){
    	return PartnerOrder::byPartner($id)->get();
    }
}
