<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\PartnerPlan;
use DB;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class PartnerPlanRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PartnerPlan::class;
    }


    public function allAcquirable(){
    	return PartnerPlan::all();
    }
}
