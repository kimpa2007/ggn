<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\Partner;
use App\Models\PartnerOrder;
use DB;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class PartnerRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\Partner";
    }


    public function getPlansOrders($id)
    {
        return $this->getOrders($id, PartnerOrder::TYPE_PLAN);
    }

    public function getCreditsOrders($id)
    {
        return $this->getOrders($id, PartnerOrder::TYPE_CREDIT);
    }

    public function getOrders($id, $type)
    {
        $partner = Partner::find($id);

        if($partner) {
            return $partner
                ->orders()
                ->where("type", $type)
                ->with('plan')
                ->get();
        }

        return null;
    }
}
