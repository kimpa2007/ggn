<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\PlanPurposed;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class PlanPurposedRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanPurposed::class;
    }

}
