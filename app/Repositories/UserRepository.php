<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Http\Requests\SubscriptionRequest;


use App\Models\User;
use App\Models\Role;
use DB;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\User";
    }

    public function getDatatableData()
    {
        return User::with([
            'roles',
            'partner',
            'expertZones',
            'profile'
        ])->select([
            'users.*',
            DB::raw("CONCAT(users.firstname,' ',users.lastname) as full_name"),
        ]);
    }

/*
    public function pendingUsersForPartner( $partnerId )
    {

        return User::with([
            'roles',
            'partner',
            'expertZones'
        ])->withStatus(
            'PENDING'
        )->atPartner( $partnerId
        );



    }
*/
    public function activeUsersForPartner( $partnerId )
    {

        return User::with([
            'roles',
            'partner',
            'expertZones'
        ])->withStatus(
            'ACTIVE'
        )->atPartner( $partnerId

        );
    }



    public function admins()
    {
      return User::whereHas('roles', function($query){
      		$query->where('name','admin');
      	});

    }
/*
    public function createOnSignup($data){
        $userRole = Role::where('name','user')->first();
        $data["email"] = $data["email_signup"];
        $user = new User( $data );
        $user->status = 'PENDING';
        $user->save();
        $user->attachRoles([$userRole->id]);
        $user->save();

        return $user;
    }
*/
}
