<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Models\Zone;
use App\Models\Content;
use DB;
use Auth;
/**
 * Interface ZoneRepository
 * @package namespace App\Repositories;
 */
class ZoneRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "App\\Models\\Zone";
    }

    public function getDatatableData()
    {
        return Zone::with([
        	'countries'
        ]);
    }

    public function getPublished()
    {
        return Zone::where([
        	'status' => Zone::STATUS_PUBLISHED
        ])->orderBy('order','ASC');
    }
/*
    public function getAuthorizedContentsByZoneId($id)
    {
      if (!Auth::user()) {
        return false;
      }
      		$zones = Auth::user()->getAllowedZones();
          foreach ($zones as $key => $value) {

          }
    }
*/
    public function getContentsByZoneId($id)
    {
        $zone = Zone::find($id);
        $contents = [];

        if(!$zone) {
            return null;
        }

        foreach ($zone->countries as $country) {
            $contentsCountry = $country->contentsCountry()
                ->where('status', Content::STATUS_PUBLISHED)
                ->orderBy('published_at', 'desc')
                ->get();

            foreach ($contentsCountry as $content) {
                $contents[] = $content;
            }
        }
        return $contents;
    }

    public static function getPublishedIds()
    {

      $zones = Zone::where([
        'status' => Zone::STATUS_PUBLISHED
      ])->orderBy('order','ASC')->get();

      $zonesId = $zones->map(function ($item, $key) {
          return $item->id;
      });

      return $zonesId;
    }

}
