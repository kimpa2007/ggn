<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

use Storage;

class FileService
{
    public function upload($file)
    {
        return $this->save($file);
    }

    public function save($file)
    {
        // Build filepath + filename
        $filePath = sprintf(
            "/public/uploads/%s/%s/%s/%s.%s",
            date('Y'),
            date('m'),
            date('d'),
            uniqid(rand(), false),
            $file->getClientOriginalExtension()
        );

        if(Storage::put($filePath, file_get_contents($file->getRealPath()))) {
            return $filePath;
        }

        return false;
    }

    public function delete($filePath)
    {
        return Storage::has($filePath) ? Storage::delete($filePath) : false;
    }

    public function move($uploadedFilePath, $filePath)
    {
        return Storage::move($uploadedFilePath, $filePath) ? true : false;
    }
}
