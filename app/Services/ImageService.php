<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;

use Storage;

class ImageService
{

	public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp'
    ];

	public static $messages = [
        'file.mimes' => 'Uploaded file is not in image format',
        'file.required' => 'Image is required'
    ];

    public function upload( $form_data , $resize = true, $resizeWidth = 900)
    {

        $validator = Validator::make($form_data, ImageService::$rules, ImageService::$messages);

        if ($validator->fails()) {

            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);

        }

        $photo = $form_data['file'];

        $originalName = $photo->getClientOriginalName();
        $extension = $photo->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        //$filename = $this->sanitize($originalNameWithoutExt);
        //$allowed_filename = $this->createUniqueFilename( $filename, $extension );

		$allowed_filename = uniqid(rand(), false).'.'.$extension;

		$prefix = date("Y").'/'.date("m").'/';

		if($resize){
			$result = $this->resized( $photo, $allowed_filename, $resizeWidth, $prefix);
		}
		else {
			$result = $this->original( $photo, $allowed_filename, $prefix );
		}

        if( !$result) {

			return Response::json([
                'error' => true,
                'message' => 'Server error while uploading',
                'code' => 500
            ], 500);

        }

        return Response::json([
            'error' => false,
            'code'  => 200,
            'filename' => "tmp/".$prefix.$allowed_filename,
            'storage_filename' => Storage::url("tmp/".$prefix.$allowed_filename)
        ], 200);

    }



    /**
     * Optimize Original Image
     */
    public function original( $photo, $filename , $prefix = '')
    {
        $manager = new ImageManager();
		//$image = $manager->make( $photo )->save(Config::get('images.full_size') . $filename );

		$image = $manager->make( $photo );

		// calculate md5 hash of encoded image
		$hash = md5($image->__toString());

		// use hash as a name
		$path = "images/{$hash}.jpg";

		// save it locally to ~/public/images/{$hash}.jpg
		$image->save(public_path($path));

		Storage::put(Config::get('images.tmp') . $prefix . $filename , $image->__toString());

		//delete temporal image
		$image->destroy();
		unlink($path);

		return true;
    }

    /**
     * Create Icon From Original
     */
    public function resized( $photo, $filename , $resizeWidth, $prefix = '')
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo )->resize($resizeWidth, null, function ($constraint) {
            $constraint->aspectRatio();
            });

        // calculate md5 hash of encoded image
		$hash = md5($image->__toString());

		// use hash as a name
		$path = "images/{$hash}.jpg";

		// save it locally to ~/public/images/{$hash}.jpg
		$image->save(public_path($path));


		Storage::put(Config::get('images.tmp') . $prefix . $filename , $image->__toString());

		//delete temporal image
		$image->destroy();
		unlink($path);

		return true;
    }

	public function move ($tmpFilename, $endPath)
	{
		//get tmp filename
		$filename = explode('/',$tmpFilename);
		$filename = $filename[sizeof($filename)-1];

		Storage::move(
			Config::get('images.basepath').$tmpFilename,
			Config::get('images.basepath').$endPath.$filename
		);

		return $endPath.$filename;
	}

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function delete( $filename)
    {


        if ( Storage::get(Config::get('images.basepath').$filename)){

            Storage::delete( Config::get('images.basepath').$filename );

        }

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }

	public function createUniqueFilename( $filename, $extension )
    {
        $full_size_dir = Config::get('images.dir');
        $full_image_path = $full_size_dir . $filename . '.' . $extension;

        if ( File::exists( $full_image_path ) )
        {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return $filename . '-' . $imageToken . '.' . $extension;
        }

        return $filename . '.' . $extension;
    }

    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
}

?>
