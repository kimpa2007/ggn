<?php
namespace App\Traits;

use App\Repositories\ImageUploadRepository;
use App;

trait ImageUpload
{
    public function getAttribute($key)
    {
        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
		if (in_array($key, $this->imagesUpload)) {
			$repository = App::make('\App\Repositories\ImageUploadRepository');
			$originalImage = isset($this->original[$key]) ? $this->original[$key] : null;

			if($value != $originalImage){
	            if($originalImage != null){
	                $repository->delete($originalImage);
	            }

	            if($value != "" && $value != null){
	                $storagePath = $repository->move($value, $this->table.'/');
	            }

				$value = isset($storagePath) ? $storagePath : null;
	        }
		}

        return parent::setAttribute($key, $value);
    }
}
