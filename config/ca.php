<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Club Apprendre config
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    
    'mail_company_email' => env('MAIL_COMPANY_EMAIL'),
    'mail_company_direction' => env('MAIL_COMPANY_DIRECTION'),
    'mail_company_name' => env('MAIL_COMPANY_NAME'),
    'domain_main' => env('WEBSITE_DOMAIN'),
    'domain_ce' => env('WEBSITE_DOMAIN_CE'),
    

    

];
