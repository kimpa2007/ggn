<?php

return [
    'defaultStyle' => [
        'color' => '#956a00',
        'opacity' => 0,
        'fillOpacity' => 0,
        'weight' => 0,
    ],
    'selectedStyle' => [
        'color' => '#956a00',
        //'color' => '#FFAB01', //opcion 2
        'opacity' => 0,
        'fillOpacity' => 0.5,
        //'fillOpacity' => 0.85, //opcion 2
        'weight' => 0.5,
    ],
];
