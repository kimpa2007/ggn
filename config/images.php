<?php

return [
    'basepath'	  => env('UPLOAD_BASEPATH', 'public/'),
    'dir'  		  => env('UPLOAD_IMAGES', 'public/images/upload/'),
    'tmp'  		  => env('UPLOAD_IMAGES_TMP', 'public/tmp/'),
    'full_size'   => env('UPLOAD_FULL_SIZE', public_path('images/full_size/')),
    'icon_size'   => env('UPLOAD_ICON_SIZE', public_path('images/icon_size/')),
];
