<?php

return [
    [
        'identifier' => 'GEOPOLITICS',
        'slug' => 'geopolitics',
        'name_en' => 'Geopolitics',
        'name_fr' => 'Géopolitique',
    ],
    [
        'identifier' => 'ANALYSIS',
        'slug' => 'analysis',
        'name_en' => 'Analysis',
        'name_fr' => 'Analyse',
    ],
    [
        'identifier' => 'ECONOMY',
        'slug' => 'economy',
        'name_en' => 'Economy',
        'name_fr' => 'Economie',
    ],
    [
        'identifier' => 'SOCIETY',
        'slug' => 'society',
        'name_en' => 'Society',
        'name_fr' => 'Société',
    ]
];
