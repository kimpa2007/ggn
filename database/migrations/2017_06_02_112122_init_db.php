<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Kalnoy\Nestedset\NestedSet;
use App\Models\Content;

class InitDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->string('image')->nullable();
            $table->string('status');
            $table->integer('order')->default(0);
            $table->boolean('partner_only')->default(0);

            $table->timestamps();
        });

        Schema::create('users_zones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('type');

            $table->foreign('zone_id')->references('id')->on('zones')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });


        Schema::create('users_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->longText('content');
            $table->string('image')->nullable();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });


        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->string('copyright')->nullable();
            $table->string('legend')->nullable();


            $table->longText('description')->nullable();
            $table->integer('order')->default(0);
            $table->string('status');
            $table->timestamps();
            NestedSet::columns($table);
        });

        Schema::create('countries_zones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');

            $table->integer('zone_id')->unsigned();
            $table->foreign('zone_id')->references('id')->on('zones');
        });


        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->string('type');
            $table->string('title');
            $table->string('theme')->nullable();
            $table->string('movie_on_top')->default(0)->nullable();
            $table->integer('latest_news')->default(0);
            $table->string('slug');
            $table->longText('description');
            $table->longText('content');


            $table->string('image')->nullable();
            $table->string('copyright')->nullable();
            $table->string('legend')->nullable();

            $table->string('image_2')->nullable();
            $table->string('copyright_2')->nullable();
            $table->string('legend_2')->nullable();

            $table->integer('order')->default(0);
            $table->integer('views')->default(0)->nullable();
            $table->string('status')->default(Content::STATUS_DRAFT)->change();

            $table->foreign('created_by')->references('id')->on('users');

            $table->foreign('country_id')->references('id')->on('countries');
            $table->timestamps();
        });

        Schema::create('contents_medias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->string('name');
            $table->string('value');
            $table->string('type');

            $table->foreign('content_id')->references('id')->on('contents')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('contents_viewed', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('content_id')->unsigned();
            $table->timestamp('viewed_at');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('content_id')->references('id')->on('contents');
        });




        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('type');
            $table->string('status');
            $table->integer('credits')->default(0)->nullable();

            $table->string('color1')->nullable();
            $table->string('color2')->nullable();
            $table->string('color3')->nullable();
            $table->string('logo')->nullable();
            $table->string('title')->nullable();

            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('users');

            $table->timestamps();

            $table->softDeletes();
        });

        Schema::table('users', function($table){
            $table->integer('credits')->default(0)->nullable();
            $table->integer('partner_id')->unsigned()->nullable();
            $table->foreign('partner_id')->references('id')->on('partners');
        });

        Schema::create('partners_plans', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('description')->nullable();
            $table->text('slug');
            $table->integer('price');
            $table->string('status');
            $table->string('type');
            $table->string('time');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamps();
        });

        Schema::create('plans_zones', function (Blueprint $table) {
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('partners_plans');

            $table->integer('zone_id')->unsigned();
            $table->foreign('zone_id')->references('id')->on('zones');
        });

        Schema::create('partners_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->string('type_payment');
            $table->date('paid_at')->nullable();
            $table->string('type');
            $table->string('amount');

            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();

            $table->integer('plan_id')->unsigned()->nullable();
            $table->foreign('plan_id')->references('id')->on('partners_plans');

            $table->integer('partner_id')->unsigned();
            $table->foreign('partner_id')->references('id')->on('partners');

            $table->timestamps();
        });


        Schema::create('plans_purposed', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');

            $table->integer('plan_id')->unsigned()->nullable();
            $table->foreign('plan_id')->references('id')->on('partners_plans');

            $table->integer('partner_id')->unsigned();
            $table->foreign('partner_id')->references('id')->on('partners');
        });

        Schema::create('credits_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quantity');
            $table->string('type');
            $table->timestamp('done_at')->nullable();
            $table->string('status')->nullable();

            $table->integer('partner_order_id')->unsigned()->nullable();
            $table->foreign('partner_order_id')->references('id')->on('partners_orders');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('partner_id')->unsigned()->nullable();
            $table->foreign('partner_id')->references('id')->on('partners');

            $table->timestamps();
        });

        Schema::create('emails_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable();
            $table->string('name');
            $table->text('template');
            $table->string('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('zones');
        Schema::dropIfExists('users_zones');
        Schema::dropIfExists('users_profiles');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('contents_types');
        Schema::dropIfExists('contents_viewed');
        Schema::dropIfExists('contents_medias');
        Schema::dropIfExists('contents');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('partners');
        Schema::dropIfExists('partners_plans');
        Schema::dropIfExists('plans_zones');
        Schema::dropIfExists('plans_purposed');
        Schema::dropIfExists('partners_orders');
        Schema::dropIfExists('credits_operations');
        Schema::dropIfExists('emails_templates');

        Schema::enableForeignKeyConstraints();
    }
}
