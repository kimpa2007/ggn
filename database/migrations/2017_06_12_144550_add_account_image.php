<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
		    $table->string('telephone')->nullable()->after('email');
		    $table->string('image')->nullable();
        $table->string('city',100)->nullable()->after('status');
        $table->string('postal_code',10)->nullable()->after('status');
        $table->string('address',250)->nullable()->after('status');
        $table->string('comment_on_signup')->nullable();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        $table->dropColumn('telephone');
        $table->dropColumn('image');
        $table->dropColumn('city');
        $table->dropColumn('postal_code');
        $table->dropColumn('address');

		});
    }
}
