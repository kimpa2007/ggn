<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id')->unsigned()->nullable();
            $table->string('title');
            $table->text('message');
            $table->timestamp('sent_at')->nullable();
            $table->string('status');

            $table->foreign('partner_id')->references('id')->on('partners');

            $table->timestamps();
        });

        Schema::create('users_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('notification_id')->unsigned()->nullable();
            $table->timestamp('read_at')->nullable();
            $table->timestamp('received_at')->nullable();
            $table->string('status');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('notification_id')->references('id')->on('notifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_notifications');
        Schema::dropIfExists('notifications');
    }
}
