<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoldeToCreditsOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits_operations', function($table){
            $table->dropColumn('quantity');
        });

        Schema::table('credits_operations', function($table){
            $table->integer('balance')->after('id');
            $table->integer('quantity')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits_operations', function($table){
            $table->dropColumn('quantity');
            $table->dropColumn('balance');
        });
        Schema::table('credits_operations', function($table){
            $table->string('quantity')->after('id');
        });
    }
}
