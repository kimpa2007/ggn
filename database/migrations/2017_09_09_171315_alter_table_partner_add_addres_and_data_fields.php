<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePartnerAddAddresAndDataFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function($table){
            $table->string('address')
                ->after('manager_id')
                ->nullable();

            $table->string('referent_data')
                ->after('manager_id')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function($table){
            $table->dropColumn('address');
            $table->dropColumn('referent_data');
        });
    }
}
