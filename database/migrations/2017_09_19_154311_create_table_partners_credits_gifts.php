<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePartnersCreditsGifts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_vouchers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('partner_id')->unsigned();
            $table->foreign('partner_id')->references('id')->on('partners');

            $table->string('amount');
            $table->string('quantity');
            $table->string('total_amount');
            $table->string('status');
            $table->string('remain');

            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_vouchers');
        Schema::dropIfExists('partners_vouchers');
    }
}
