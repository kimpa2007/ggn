<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateZoneRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones_requested', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id')->unsigned();
            $table->integer('zone_id')->unsigned();
            $table->string('status');
            $table->timestamp('requested_at')->default(Carbon::now());

            $table->foreign('zone_id')->references('id')->on('zones')
                ->onDelete('cascade');

            $table->foreign('partner_id')->references('id')->on('partners')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('zones_requested');

        Schema::enableForeignKeyConstraints();
    }
}
