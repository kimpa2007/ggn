<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PartnerDeleteManagerOncascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function($table)
        {
          $table->softDeletes();
        });

        Schema::table('partners', function($table){
           $table->dropForeign('partners_manager_id_foreign');
           $table->foreign('manager_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table)
        {
          $table->dropSoftDeletes();
        });
    }
}
