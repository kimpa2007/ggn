<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *ALTER TABLE `contents` ADD `theme_2` VARCHAR(250) NULL AFTER `theme`, ADD `theme_3` VARCHAR(250) NULL AFTER `theme_2`, ADD `theme_4` VARCHAR(250) NULL AFTER `theme_3`;
     *ALTER TABLE `custompages` CHANGE `content` `content` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
     */
    public function up()
    {
        Schema::table('contents', function($table) {
            $table->string('theme_2')->after('theme')->nullable();
            $table->string('theme_3')->after('theme')->nullable();
            $table->string('theme_4')->after('theme')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('contents', function($table){
        $table->dropColumn('theme_2');
        $table->dropColumn('theme_3');
        $table->dropColumn('theme_4');
        });
    }
}
