<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->longText('description');
            $table->longText('content');
            $table->string('contact')->nullable();
            $table->string('copyright', 250)->nullable();
            $table->string('legend', 250)->nullable();
            $table->string('copyright_2', 250)->nullable();
            $table->string('legend_2', 250)->nullable();            
            $table->string('locale')->index();
            $table->unique(['content_id','locale']);
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_translations');
    }
}
