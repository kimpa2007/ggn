<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertExistentTextToTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("insert into content_translations (content_id, title, slug, description, content, contact, legend, legend_2, copyright, copyright_2, locale) select id, title, slug, description, content, contact, legend, legend_2, copyright, copyright_2, 'en' from contents");

        \DB::statement("insert into content_translations (content_id, title, slug, description, content, contact, legend, legend_2, copyright, copyright_2, locale) select id, title, slug, description, content, contact, legend, legend_2, copyright, copyright_2, 'fr' from contents");

        // We drop the translation attributes in our main table: 
        Schema::table('contents', function ($table) {
            $table->dropColumn('title');
            $table->dropColumn('slug');
            $table->dropColumn('description');
            $table->dropColumn('content');
            $table->dropColumn('contact');
            $table->dropColumn('legend');
            $table->dropColumn('legend_2');
            $table->dropColumn('copyright');
            $table->dropColumn('copyright_2');
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::table('contents', function ($table) {
            $table->string('title')->after('type');
            $table->string('slug')->after('movie_on_top');
            $table->longText('description')->after('slug');
            $table->longText('content')->after('description');
            $table->string('contact')->after('content')->nullable();
            $table->string('copyright', 250)->after('image')->nullable();
            $table->string('legend', 250)->after('copyright')->nullable();
            $table->string('copyright_2', 250)->after('image_2')->nullable();
            $table->string('legend_2', 250)->after('copyright_2')->nullable(); 
        }); 

    }
}
