<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZoneTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->longText('description');
            $table->string('copyright', 250)->nullable();
            $table->string('legend', 250)->nullable();
            $table->string('locale')->index();
            $table->unique(['zone_id','locale']);
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');

        });

        \DB::statement("insert into zone_translations (zone_id, name, slug, description, legend, copyright, locale) select id, name, slug, description, '', '', 'en' from zones");
        \DB::statement("insert into zone_translations (zone_id, name, slug, description, legend, copyright, locale) select id, name, slug, description, '', '', 'fr' from zones");

        // We drop the translation attributes in our main table: 
        Schema::table('zones', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->dropColumn('description');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zone_translations');

        Schema::table('zone', function ($table) {
            $table->string('name')->after('id');
            $table->string('slug')->after('name');
            $table->longText('description')->after('slug');
        }); 

    }
}
