<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\CustomPage;
use App\Models\Language;

class CreateCustompageTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $customPages = DB::select('select * from custompages');

        // Remove custom pages table
        Schema::dropIfExists('custompages');

        // Rebuild custom page
        Schema::create('custompages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier')->unique();
            $table->timestamps();
        });

        Schema::create('custompage_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('custom_page_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->longText('content');
            $table->string('locale')->index();
            $table->unique(['custom_page_id','locale']);
            $table->foreign('custom_page_id')->references('id')->on('custompages')->onDelete('cascade');
        });

        foreach($customPages as $customPage)
        {
            $page = CustomPage::create([
                'identifier' => $customPage->slug
            ]);

            $page->fill([
                'en'  => [
                    'title' => $customPage->title,
                    'slug' => $customPage->slug,
                    'content' => $customPage->content,
                ]
            ]);
            //
            //
            // foreach (Language::all() as $lang) {
            //     $page->translateOrNew($lang->locale)->title = $customPage->title;
            //     $page->translateOrNew($lang->locale)->slug = $customPage->slug;
            //     $page->translateOrNew($lang->locale)->content = $customPage->content;
            // }
            $page->save();
        }

        // \DB::statement("insert into custompage_translations (custom_page_id, title, slug, content, locale) select id, title, slug, content, 'en' from custompages");
        // \DB::statement("insert into custompage_translations (custom_page_id, title, slug, content, locale) select id, title, slug, content, 'fr' from custompages");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custompage_translations');

        Schema::table('custompages', function ($table) {
            $table->string('title')->after('id');
            $table->string('slug')->after('title');
            $table->longText('content')->after('slug');
        });
    }
}
