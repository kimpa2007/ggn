<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_profile_id')->unsigned();
            $table->longText('content');
            $table->string('locale')->index();
            $table->unique(['user_profile_id','locale']);
            $table->foreign('user_profile_id')->references('id')->on('users_profiles')->onDelete('cascade');
        });

        \DB::statement("insert into user_profile_translations (user_profile_id, content, locale) select id, content, 'en' from users_profiles");
        \DB::statement("insert into user_profile_translations (user_profile_id, content, locale) select id, content, 'fr' from users_profiles");

        // We drop the translation attributes in our main table: 
        Schema::table('users_profiles', function ($table) {
            $table->dropColumn('content');
        }); 



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_translations');

        Schema::table('zone', function ($table) {
            $table->longText('content')->after('user_id');
        }); 

    }
}
