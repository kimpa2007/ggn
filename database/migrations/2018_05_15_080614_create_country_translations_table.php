<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->string('copyright', 250)->nullable();
            $table->string('legend', 250)->nullable();
            $table->string('locale')->index();
            $table->unique(['country_id','locale']);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');            
        });

         \DB::statement("insert into country_translations (country_id, name, slug, description, legend, copyright, locale) select id, name, slug, description, legend, copyright, 'en' from countries");
        \DB::statement("insert into country_translations (country_id, name, slug, description, legend, copyright, locale) select id, name, slug, description, legend, copyright, 'fr' from countries");

        // We drop the translation attributes in our main table: 
        Schema::table('countries', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->dropColumn('description');
            $table->dropColumn('copyright');
            $table->dropColumn('legend');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_translations');

        Schema::table('country', function ($table) {
            $table->string('name')->after('id');
            $table->string('slug')->after('name');
            $table->longText('description')->after('slug');
            $table->string('copyright')->after('image');
            $table->string('legend')->after('copyright');
        }); 
    }
}
