<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Theme;
use App\Models\Language;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('contents_themes', function (Blueprint $table) {
            $table->integer('content_id')->unsigned();
            $table->integer('theme_id')->unsigned();

            $table->foreign('content_id')->references('id')->on('contents')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('theme_id')->references('id')->on('themes')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['content_id', 'theme_id']);
        });

        Schema::create('theme_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('theme_id')->unsigned();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('meta_title')->nullable();
            $table->longText('meta_description')->nullable();
            $table->string('locale')->index();
            $table->unique(['theme_id','locale']);
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('cascade');
        });

        foreach(config('themes') as $k => $v) {
            $theme = Theme::create([
                'identifier' => $v["identifier"]
            ]);

            $theme->translateOrNew('en')->name = $v["name_en"];
            $theme->translateOrNew('en')->slug = $v["slug"];
            $theme->translateOrNew('en')->meta_title = $v["name_en"];
            $theme->translateOrNew('en')->meta_description = $v["name_en"];

            $theme->translateOrNew('fr')->name = $v["name_fr"];
            $theme->translateOrNew('fr')->slug = $v["slug"];
            $theme->translateOrNew('fr')->meta_title = $v["name_fr"];
            $theme->translateOrNew('fr')->meta_description = $v["name_fr"];

            $theme->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents_themes');
        Schema::dropIfExists('themes_translations');
        Schema::dropIfExists('themes');
    }
}
