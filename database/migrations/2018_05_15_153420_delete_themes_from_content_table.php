<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Content;
use App\Models\Theme;

class DeleteThemesFromContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Migrate themes
        $contents = DB::select('select * from contents');

        foreach($contents as $content) {
            $theme1 = Theme::whereTranslation('slug', strtolower($content->theme))->first();
            $theme2 = Theme::whereTranslation('slug', strtolower($content->theme_2))->first();
            $theme3 = Theme::whereTranslation('slug', strtolower($content->theme_3))->first();
            $theme4 = Theme::whereTranslation('slug', strtolower($content->theme_4))->first();

            $id1 = isset($theme1) ? $theme1->id : null;
            $id2 = isset($theme2) ? $theme2->id : null;
            $id3 = isset($theme3) ? $theme3->id : null;
            $id4 = isset($theme4) ? $theme4->id : null;

            $ids = [];

            if($id1) $ids[] = $id1;
            if($id2) $ids[] = $id2;
            if($id3) $ids[] = $id3;
            if($id4) $ids[] = $id4;

            $content = Content::find($content->id);
            $content->themes()->sync($ids);
        }



        Schema::table('contents', function ($table) {
            $table->dropColumn('theme');
            $table->dropColumn('theme_2');
            $table->dropColumn('theme_3');
            $table->dropColumn('theme_4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function($table) {
            $table->string('theme')/*->after('lastest_news')*/->nullable();
            $table->string('theme_2')->after('theme')->nullable();
            $table->string('theme_3')->after('theme')->nullable();
            $table->string('theme_4')->after('theme')->nullable();
        });
    }
}
