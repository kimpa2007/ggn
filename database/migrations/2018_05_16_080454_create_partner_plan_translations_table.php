<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerPlanTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_plan_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('partner_plan_id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->string('locale')->index();
            $table->unique(['partner_plan_id','locale']);
            $table->foreign('partner_plan_id')->references('id')->on('partners_plans')->onDelete('cascade');
        });

        \DB::statement("insert into partner_plan_translations (partner_plan_id, name, slug, description, locale) select id, name, slug, description, 'en' from partners_plans");
        \DB::statement("insert into partner_plan_translations (partner_plan_id, name, slug, description, locale) select id, name, slug, description, 'fr' from partners_plans");

        // We drop the translation attributes in our main table:
        Schema::table('partners_plans', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->dropColumn('description');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_plan_translations');

        Schema::table('partners_plans', function ($table) {
            $table->string('name')->after('id');
            $table->string('slug')->after('name');
            $table->longText('description')->after('slug');
        });
    }
}
