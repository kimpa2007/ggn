<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTranslateTools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('translate_tools', function (Blueprint $table) {
            $table->string('text_fr', 320)->change();
            $table->string('text_en', 320)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translate_tools', function (Blueprint $table) {
            $table->string('text_fr', 255)->change();
            $table->string('text_en', 255)->change();
        });
    }
}
