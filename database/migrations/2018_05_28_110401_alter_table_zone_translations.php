<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableZoneTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zone_translations', function($table){
            $table->string('meta_description')
                ->after('legend')
                ->nullable();

            $table->string('meta_title')
                ->after('legend')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zone_translations', function($table){
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
        });
    }
}
