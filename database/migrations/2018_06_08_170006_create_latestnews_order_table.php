<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLatestnewsOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('latestnews_order', function (Blueprint $table) {
          $table->integer('content_id')->unsigned();
          $table->foreign('content_id')->references('id')->on('contents');
          $table->integer('order_news')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('latestnews_order');
    }
}
