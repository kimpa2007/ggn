<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maps_legends', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icon');
            $table->string('color');
            $table->string('status');
            $table->string('key');
        });

        Schema::create('map_legend_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('map_legend_id')->unsigned();
            $table->string('name');

            $table->string('locale')->index();
            $table->unique(['map_legend_id','locale']);
            $table->foreign('map_legend_id')->references('id')->on('maps_legends')->onDelete('cascade');
        });


        Schema::create('contents_maps', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamp('start_at');
            $table->timestamp('end_at')->nullable();
            $table->string('latlng');
            $table->string('status');

            $table->integer('content_id')->unsigned();
            $table->foreign('content_id')->references('id')->on('contents');

            $table->integer('legend_id')->unsigned();
            $table->foreign('legend_id')->references('id')->on('maps_legends');
        });

        Schema::table('contents', function (Blueprint $table) {
            $table->boolean('has_map')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::dropIfExists('contents_maps');
        Schema::dropIfExists('maps_legends');
        Schema::dropIfExists('map_legend_translations');

        Schema::table('contents', function (Blueprint $table) {
            $table->dropColumn('has_map');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
