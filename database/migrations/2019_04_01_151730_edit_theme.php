<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Theme;

class EditTheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('themes', function($table){
            $table->integer('position')
                ->nullable();

            $table->boolean('is_menu')
                ->nullable();
        });

        DB::statement('UPDATE `theme_translations` SET `name`= "International Politics", `slug`="internacional-politics", `meta_title`="International politics"  WHERE id =1');
        DB::statement('UPDATE `theme_translations` SET `name`= "Politique Internationale", `slug`="politique-internacionale", `meta_title`="Politique Internationale"  WHERE id =2');

        DB::statement('UPDATE `themes` SET `is_menu`=true WHERE id IN (1,2,3,4,5)');
        DB::statement('UPDATE `themes` SET `position`=1 WHERE id = 1');
        DB::statement('UPDATE `themes` SET `position`=4 WHERE id = 2');
        DB::statement('UPDATE `themes` SET `position`=2 WHERE id = 3');
        DB::statement('UPDATE `themes` SET `position`=3 WHERE id = 4');
        DB::statement('UPDATE `themes` SET `position`=5 WHERE id = 5');

        DB::statement('UPDATE `theme_translations` SET `name`="Analyses" WHERE id = 4');

        //Santé (avec à côté un emplacement pour mettre le logo joint assurances de ladour)
        $theme = Theme::create([
            'id' => 6,
            'identifier' => 'sante',
            'position' => 6
        ]);
        $theme->translateOrNew('fr')->name = 'Santé';
        $theme->translateOrNew('fr')->slug = 'sante';
        $theme->translateOrNew('fr')->meta_title = 'Santé';
        $theme->translateOrNew('fr')->meta_description = 'Santé';
        $theme->save();
        
        //Culture
        $theme = Theme::create([
            'id' => 7,
            'identifier' => 'culture',
            'position' => 7
        ]);
        $theme->translateOrNew('fr')->name = 'Culture';
        $theme->translateOrNew('fr')->slug = 'culture';
        $theme->translateOrNew('fr')->meta_title = 'Culture';
        $theme->translateOrNew('fr')->meta_description = 'Culture';
        $theme->save();
        
        //Opinions
        $theme = Theme::create([
            'id' => 8,
            'identifier' => 'opinions',
            'position' => 8
        ]);
        $theme->translateOrNew('fr')->name = 'Opinions';
        $theme->translateOrNew('fr')->slug = 'opinions';
        $theme->translateOrNew('fr')->meta_title = 'Opinions';
        $theme->translateOrNew('fr')->meta_description = 'Opinions';
        $theme->save();

        DB::statement('UPDATE `themes` SET `position`=6 WHERE id = 6');
        DB::statement('UPDATE `themes` SET `position`=7 WHERE id = 7');
        DB::statement('UPDATE `themes` SET `position`=8 WHERE id = 8');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        if (Schema::hasColumn('themes', 'position'))
        {

            Schema::table('themes', function (Blueprint $table)

            {

                $table->dropColumn('position');

            });

        }

        if (Schema::hasColumn('themes', 'is_menu'))
        {

            Schema::table('themes', function (Blueprint $table)

            {

                $table->dropColumn('is_menu');

            });

        }

        DB::statement('DELETE FROM `theme_translations` WHERE theme_id IN (6,7,8) ');
        DB::statement('DELETE FROM `themes` WHERE id IN (6,7,8) ');
    }

}
