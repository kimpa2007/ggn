<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifPlanAddDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (!Schema::hasColumn('partners_plans', 'months'))
        {
            Schema::table('partners_plans', function($table){
                $table->integer('months')
                    ->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (Schema::hasColumn('partners_plans', 'months'))
        {

            Schema::table('partners_plans', function (Blueprint $table)
            {

                $table->dropColumn('months');

            });

        }
    }
}
