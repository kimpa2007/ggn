<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Creation des abonnements
        /*
            Je dois pouvoir gérer une abonnement à 3€/mois ou 30€/an dans le cas d’accord pour de grandes quantités d’abonnements via un tier, CPME, Réseau d’entreprises, …
        */

        // Abonnement Etudiant : 2€/mois sans engagement de durée.

        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by) VALUES (12, 2, 'ACTIVE', 'PUBLIC', 2, 1)");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale, slug) VALUES (15, 12, 'Abonnement Etudiant : 2€/mois', 'fr', '2e-mois')");

        // Abonnement Etudiant : 20€/an si paiement en une fois.
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by) VALUES (13, 20, 'ACTIVE', 'PUBLIC', 12, 1 )");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (16, 13, 'Abonnement Etudiant : 20€/an', 'fr')");

        // Abonnement standrad particulier : 6€/mois sans engagement de durée.
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by) VALUES (14, 6, 'ACTIVE', 'PUBLIC', 1, 1 )");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (17, 14, 'Abonnement standard particulier : 6€/mois', 'fr')");

        // Abonnement standrad particulier : 6€/mois sans engagement de durée.
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by) VALUES (15, 50, 'ACTIVE', 'PUBLIC', 12, 1 )");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (18, 15, 'Abonnement standard particulier :  50€/an', 'fr')");


        // Abonnement à 3€/mois , privée
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by) VALUES (16, 3, 'ACTIVE', 'PRIVATE', 1, 1 )");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (19, 16, 'Abonnement à 3€/mois', 'fr')");

         // Abonnement à 30€/an , privée
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by) VALUES (17, 3, 'ACTIVE', 'PRIVATE', 1, 1 )");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (20, 17, 'Abonnement à 30€/an', 'fr')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("delete from partners_plans where id IN (12,13,14,15)");
        \DB::statement("delete from partner_plan_translations where partner_plan_id IN (12,13,14,15)");
    }
}
