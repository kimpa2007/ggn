<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateThemesActif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         
        if (!Schema::hasColumn('themes', 'actif'))
        {
            Schema::table('themes', function($table){
                $table->integer('actif')->nullable();
            });
        }

        \DB::statement("UPDATE themes set actif=1");
        \DB::statement("UPDATE themes set actif=0 WHERE id = 8");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         
        if (Schema::hasColumn('themes', 'actif'))
        {

            Schema::table('themes', function (Blueprint $table)
            {
                $table->dropColumn('actif');

            });
        }
    }
}
