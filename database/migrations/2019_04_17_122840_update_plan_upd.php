<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePlanUpd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //ajouter colonne is_abonnement plan
        if (!Schema::hasColumn('partners_plans', 'is_abonnement'))
        {
            Schema::table('partners_plans', function($table){
                $table->integer('is_abonnement')
                    ->nullable();
            });
        }

        if (!Schema::hasColumn('partners_plans', 'is_etudiant'))
        {
            Schema::table('partners_plans', function($table){
                $table->integer('is_etudiant')
                    ->nullable();
            });
        }

        // ajouter cursus et ecole a planorder
        if (!Schema::hasColumn('partners_orders', 'ecole'))
        {
            Schema::table('partners_orders', function($table){
                $table->string('ecole')
                    ->nullable();
            });
        }

        if (!Schema::hasColumn('partners_orders', 'cursus'))
        {
            Schema::table('partners_orders', function($table){
                $table->string('cursus')
                    ->nullable();
            });
        }

        \DB::statement("UPDATE partners_plans SET is_abonnement=0");
        \DB::statement("UPDATE partners_plans SET is_abonnement=1 WHERE id IN (12,13,14,15,16,17)");
        \DB::statement("UPDATE partners_plans SET is_etudiant=1 WHERE id IN (12,13)");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('partners_plans', 'is_abonnement'))
        {

            Schema::table('partners_plans', function (Blueprint $table)
            {

                $table->dropColumn('is_abonnement');

            });

        }

        if (Schema::hasColumn('partners_plans', 'is_etudiant'))
        {

            Schema::table('partners_plans', function (Blueprint $table)
            {

                $table->dropColumn('is_etudiant');

            });

        }

          if (Schema::hasColumn('partners_orders', 'ecole'))
        {

            Schema::table('partners_orders', function (Blueprint $table)
            {

                $table->dropColumn('ecole');

            });

        }

          if (Schema::hasColumn('partners_orders', 'cursus'))
        {

            Schema::table('partners_orders', function (Blueprint $table)
            {

                $table->dropColumn('cursus');

            });

        }
    }
}
