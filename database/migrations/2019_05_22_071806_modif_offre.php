<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifOffre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (!Schema::hasColumn('partners_plans', 'has_promo_field'))
        {
            Schema::table('partners_plans', function($table){
                $table->integer('has_promo_field')
                    ->nullable();
            });
        }

         if (!Schema::hasColumn('partners_orders', 'promo_code'))
        {
            Schema::table('partners_orders', function($table){
                $table->string('promo_code')
                    ->nullable();
            });
        }

        // Abonnement avec code : 3€/mois si paiement en une fois.
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by, has_promo_field) VALUES (18, 3, 'ACTIVE', 'PUBLIC', 1, 1 ,1)");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (21, 18, 'Abonnement avec code : 3€/mois', 'fr')");

        // Abonnement avec code : 30€/an si paiement en une fois.
        \DB::statement("insert into partners_plans (id, price, status, type, months, created_by, has_promo_field) VALUES (19, 30, 'ACTIVE', 'PUBLIC', 12, 1, 1 )");
        \DB::statement("insert into partner_plan_translations (id, partner_plan_id, name, locale) VALUES (22, 19, 'Abonnement avec code : 30€/an', 'fr')");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if (Schema::hasColumn('partners_plans', 'has_promo_field'))
        {

            Schema::table('partners_plans', function (Blueprint $table)
            {

                $table->dropColumn('has_promo_field');

            });

           

        }
        
        if (Schema::hasColumn('partners_orders', 'promo_code'))
        {
            Schema::table('partners_orders', function (Blueprint $table)
            {
                $table->dropColumn('promo_code');

            });
        }
        \DB::statement("delete from partners_plans where id IN (18,19)");
        \DB::statement("delete from partner_plan_translations where partner_plan_id IN (18,19)");
    }
}
