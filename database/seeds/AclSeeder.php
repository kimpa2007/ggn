<?php

use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Models\Permission;
use App\Models\User;

class AclSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->make();
        $user->email = 'foo@bar.com';
        $user->save();

        $expert = factory(User::class)->make();
        $expert->email = 'expert@bar.com';
        $expert->save();

        $partner = factory(User::class)->make();
        $partner->email = 'partner@bar.com';
        $partner->save();

        $admin = factory(User::class)->make();
        $admin->email = 'admin@bar.com';
        $admin->save();

        $superAdmin = factory(User::class)->make();
        $superAdmin->email = 'superadmin@bar.com';
        $superAdmin->save();


		////////////////////////
		//		Roles
		////////////////////////

		//user
		$userRole = new Role();
		$userRole->name         = 'user';
		$userRole->display_name = 'User'; // optional
		$userRole->description  = 'User watch content'; // optional
		$userRole->save();

		//expert
		$expertRole = new Role();
		$expertRole->name         = 'expert';
		$expertRole->display_name = 'Expert'; // optional
		$expertRole->description  = 'Expert publish content'; // optional
		$expertRole->save();

		//association
		$partnerRole = new Role();
		$partnerRole->name         = 'partner';
		$partnerRole->display_name = 'Partner'; // optional
		$partnerRole->description  = 'Partner buy content register users'; // optional
		$partnerRole->save();

		//admin
		$adminRole = new Role();
		$adminRole->name         = 'admin';
		$adminRole->display_name = 'Admin'; // optional
		$adminRole->description  = 'Project manager'; // optional
		$adminRole->save();

		//superadmin
		$superadminRole = new Role();
		$superadminRole->name         = 'super-admin';
		$superadminRole->display_name = 'Super Admin'; // optional
		$superadminRole->description  = 'Manager users'; // optional
		$superadminRole->save();

		// role attach alias
		$user->attachRole($userRole);
		$expert->attachRole($expertRole);
		$partner->attachRole($partnerRole);
		$admin->attachRole($adminRole);
		$superAdmin->attachRole($superadminRole);

		////////////////////////
		//		Permissions
		////////////////////////

		/*
		$createPost = new Permission();
		$createPost->name         = 'create-post';
		$createPost->display_name = 'Create Posts'; // optional
		$createPost->description  = 'create new blog posts'; // optional
		$createPost->save();

		//add roles permision to create post
		$adminRole->attachPermission($createPost);
		//$owner->attachPermissions(array($createPost, $editUser));
		*/
    }
}
