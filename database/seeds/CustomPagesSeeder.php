<?php

use Illuminate\Database\Seeder;
use App\Models\CustomPage;

class CustomPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomPage::create([
            "title" => 'Mentions légales',
            "slug" => 'mentions-legales',
            "content" => 'Contenu pour les mentions légales'
        ]);
        CustomPage::create([
            "title" => 'CGV',
            "slug" => 'cgv',
            "content" => 'Contenu pour le CGV'
        ]);
        CustomPage::create([
            "title" => 'CGU',
            "slug" => 'cgu',
            "content" => 'Contenu pour le CGU'
        ]);
        CustomPage::create([
            "title" => 'Contact',
            "slug" => 'contact',
            "content" => 'Contenu pour le contact'
        ]);
    }
}
