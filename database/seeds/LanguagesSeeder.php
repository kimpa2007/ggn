<?php

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create([
            "locale" => 'fr',
            "name" => 'Français'
        ]);
        Language::create([
            "locale" => 'en',
            "name" => 'English'
        ]);
    }
}
