<?php

use Illuminate\Database\Seeder;

use App\Models\Permission;
use App\Models\Role;

class PartnerPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permManageCredits = Permission::where('name','manage-credits')->first();
        $creditManagerRole = new Role();
		$creditManagerRole->name         = 'credit-manager';
		$creditManagerRole->display_name = 'Credit Manager'; 
		$creditManagerRole->description  = 'Role to be able to manage partners credits'; 
		$creditManagerRole->save();
		$creditManagerRole->perms()->attach([ $permManageCredits->id ]);
		$creditManagerRole->save();


		$permBuyContent = Permission::where('name','buy-content')->first();
		$forfaitManagerRole = new Role();
		$forfaitManagerRole->name         = 'plan-manager';
		$forfaitManagerRole->display_name = 'Plan Manager'; 
		$forfaitManagerRole->description  = 'Role to be able to manage partners plans'; 
		$forfaitManagerRole->save();
		$forfaitManagerRole->perms()->attach([ $permBuyContent->id ]);
		$forfaitManagerRole->save();

		
		$permManageUsers = Permission::where('name','manage-users')->first();
		$socialManagerRole = new Role();
		$socialManagerRole->name         = 'social-manager';
		$socialManagerRole->display_name = 'Social Manager'; 
		$socialManagerRole->description  = 'Role to be able to manage other users'; 
		$socialManagerRole->save();
		$socialManagerRole->perms()->attach([ $permManageUsers->id ]);
		$socialManagerRole->save();



    }
}
