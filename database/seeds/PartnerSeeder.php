<?php

use Illuminate\Database\Seeder;

use App\Models\Zone;
use App\Models\User;
use App\Models\Thread;
use App\Models\Country;
use App\Models\Product;
use App\Models\UserZone;
use App\Models\Order;
use App\Models\Partner;
use App\Models\Rule;
use App\Models\PartnerOrder;
use App\Models\PartnerPlan;
use App\Models\PlanPurposed;

use Carbon\Carbon;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rule::create([
            'description' => 'Subvention de crédits',
            'rule' => Rule::TYPE_PARTNER,
            'type' => Rule::RULE_PARTNER_CREDITS_GRANT_USERS
        ]);

        $partner = Partner::create([
            'name' => 'Partner test',
            'type' => Partner::TYPE_NORMAL,
            'status' => Partner::STATUS_ACTIVE,
            'manager_id' => 3
        ]);

        // Setting users
        User::find(1)->update([
            "partner_id" => $partner->id,
            'status' => User::STATUS_ACTIVE
        ]);

        User::find(6)->update([
            "partner_id" => $partner->id,
            'status' => User::STATUS_ACTIVE
        ]);

        User::find(7)->update([
            "partner_id" => $partner->id,
            'status' => User::STATUS_PENDING
        ]);

        foreach(array_fill(0, 1000, 'user') as $i => $u) {
            $user = factory(User::class)->make();
            $user->partner_id = $partner->id;
            $user->status = User::STATUS_PENDING;
            $user->save();
        }

        //create Partner Plan and Partner Order to allow partner has zones

        User::find(3)->update([
            "partner_id" => $partner->id,
            'status' => User::STATUS_ACTIVE
        ]);

        $plan = PartnerPlan::create([
          'slug' => 'plan-1',
          'created_by' => 4,
          'name' => 'Plan',
          'description' => 'Description...',
          'price' => 100,
          'status' => PartnerPlan::STATUS_ACTIVE,
          'time' => 3,
          'type' => PartnerPlan::TYPE_PUBLIC
        ]);

        $plan->zones()->sync([1,2,3,4]);
        $plan->partnersPurposed()->sync([1 => [
          "status" => PlanPurposed::STATUS_PURPOSED
        ]]);

        PartnerOrder::create([
          'partner_id' => 1,
          'status' => PartnerOrder::STATUS_PAID,
          'type_payment' => PartnerOrder::PAYMENT_CB,
          'type' => PartnerOrder::TYPE_PLAN,
          'amount' => 100,
          'plan_id' => $plan->id,
          'start_at' => Carbon::now(),
          'end_at' => Carbon::now()->addMonths(2),
        ]);

    }
}
