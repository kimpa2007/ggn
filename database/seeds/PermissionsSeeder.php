<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permissions & roles
		$manageUsers = new Permission();
		$manageUsers->name         = 'manage-users';
		$manageUsers->display_name = 'Manage Users'; // optional
		$manageUsers->description  = 'Can manage users'; // optional
		$manageUsers->save();

		//Backstage
		$manageCredits = new Permission();
		$manageCredits->name         = 'manage-credits';
		$manageCredits->display_name = 'Manage Credits'; // optional
		$manageCredits->description  = 'Can change credits'; // optional
		$manageCredits->save();

    	//Api
    	$sendNotes = new Permission();
		$sendNotes->name         = 'send-notes';
		$sendNotes->display_name = 'Send Notifications'; // optional
		$sendNotes->description  = 'Can send notification emails to other users'; // optional
		$sendNotes->save();

		//App
    	$changePlans = new Permission();
		$changePlans->name         = 'change-plans';
		$changePlans->display_name = 'Change Plans'; // optional
		$changePlans->description  = 'Can change the plan'; // optional
		$changePlans->save();

		//Watch Content
    	$watch = new Permission();
		$watch->name         = 'watch-content';
		$watch->display_name = 'Watch Content'; // optional
		$watch->description  = 'Can watch content'; // optional
		$watch->save();

		//App
    	$publish = new Permission();
		$publish->name         = 'publish-content';
		$publish->display_name = 'Publish Content'; // optional
		$publish->description  = 'Can publish content'; // optional
		$publish->save();

		//App
    	$buyContent = new Permission();
		$buyContent->name         = 'buy-content';
		$buyContent->display_name = 'Buy Content'; // optional
		$buyContent->description  = 'Can buy content and account services & plans'; // optional
		$buyContent->save();




		$userRole = Role::find(1);
		$userRole->perms()->attach([
			$watch->id
			]);
		$userRole->save();



		$expertRole = Role::find(2);
		$expertRole->perms()->attach([
			$watch->id,
			$publish->id
			]);
		$expertRole->save();



		$allPermissions = [
			$watch->id,
			$publish->id,
			$manageUsers->id,
			$manageCredits->id,
			$sendNotes->id,
			$changePlans->id,
			$buyContent->id
		];

		$partnerRole = Role::find(3);
		$partnerRole->perms()->attach( $allPermissions );
		$partnerRole->save();

		$adminRole = Role::find(4);
		$adminRole->perms()->attach( $allPermissions );
		$adminRole->save();

		$superAdminRole = Role::find(5);
		$superAdminRole->perms()->attach( $allPermissions );
		$superAdminRole->save();





    }
}
