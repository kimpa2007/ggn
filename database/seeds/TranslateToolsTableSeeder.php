<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslateToolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('translate_tools')->truncate();

        $data = [
            //navbar
            ['label'=> 'front.navbar.contact_us','en'=>'Contact Us','fr'=>'Contact'],
            ['label'=> 'front.navbar.language','en'=>'Language','fr'=>'Langue'],
            ['label'=> 'front.navbar.account','en'=>'My Account','fr'=>'Mon Compte'],
            ['label'=> 'front.navbar.login','en'=>'Login','fr'=>'Connexion'],
            ['label'=> 'front.navbar.logout','en'=>'Logout','fr'=>'Déconnexion'],
            ['label'=> 'front.navbar.forgot_password','en'=>'I forgot my password','fr'=>'J\'ai oublié mon mot de passe'],

            //home
            ['label'=> 'front.home.lastest_news','en'=>'Latest News','fr'=>'Dernières actualités'],

            //footer
            ['label'=> 'front.footer.countries','en'=>'Countries','fr'=>'Pays'],
            ['label'=> 'front.footer.terms_conditions','en'=>'Terms and conditions','fr'=>'Conditions générales de ventes'],
            ['label'=> 'front.footer.rights','en'=>'all rights reserved','fr'=>'Tous droits réservés'],

            //countries
            ['label'=> 'front.countries.our_countries','en'=>'Our countries','fr'=>'Nos pays'],
            ['label'=> 'front.countries.see_more','en'=>'See More Countries','fr'=>'Voir plus de pays'],

            //termes présent sur tout le site
            ['label'=> 'front.general.home','en'=>'Home','fr'=>'Accueil'],
            ['label'=> 'front.general.analysis','en'=>'Analysis','fr'=>'Analyse'],
            ['label'=> 'front.general.contact','en'=>'Please Contact Us','fr'=>'Contactez-nous'],
            ['label'=> 'front.general.discover','en'=>'Discover','fr'=>'Découvrir'],
            ['label'=> 'front.general.contributors','en'=>'Contributors','fr'=>'Contributeurs'],
            ['label'=> 'front.general.search','en'=>'Search','fr'=>'Recherche'],
            ['label'=> 'front.general.see_content','en'=>'See content','fr'=>'Voir le contenu'],
            ['label'=> 'front.general.see_more','en'=>'See more','fr'=>'En voir plus'],
            ['label'=> 'front.general.see_more_areas','en'=>'See more areas','fr'=>'Voir plus de zones'],
            ['label'=> 'front.general.no_contents','en'=>'No contents found with this parameters','fr'=>'Aucun contenu trouvé avec ces paramètres'],
            ['label'=> 'front.general.load_more_content','en'=>'Load more content','fr'=>'Charger plus de contenus'],
            ['label'=> 'front.general.areas','en'=>'Areas','fr'=>'Zones'],
            ['label'=> 'front.general.filter','en'=>'Filter','fr'=>'Filtrer'],
            ['label'=> 'front.general.see','en'=>'See','fr'=>'Voir'],
            ['label'=> 'front.general.result_for','en'=>'Results for','fr'=>'Résultats pour'],
            ['label'=> 'front.general.no_results','en'=>'No results','fr'=>'Aucun résultat'],
            ['label'=> 'front.general.history','en'=>'History','fr'=>'Histoire'],
            ['label'=> 'front.general.title','en'=>'Title','fr'=>'Titre'],
            ['label'=> 'front.general.countries','en'=>'Countries','fr'=>'Pays'],
            ['label'=> 'front.general.credits','en'=>'My Credits','fr'=>'Mes crédits'],
            ['label'=> 'front.general.credit_in_progress','en'=>'Credits in progress','fr'=>'Crédits en cours'],
            ['label'=> 'front.general.historical_credits','en'=>'Historical credits','fr'=>'Historique des crédits'],
            ['label'=> 'front.general.see_all','en'=>'See all','fr'=>'Tout voir'],
            ['label'=> 'front.general.save','en'=>'Save','fr'=>'Sauvegarder'],

            //user
            ['label'=> 'front.user.info','en'=>'My informations','fr'=>'Mes informations'],
            ['label'=> 'front.user.doc','en'=>'My documents','fr'=>'Mes documents'],
            ['label'=> 'front.user.contact','en'=>'Contact us','fr'=>'Contactez-nous'],
            ['label'=> 'front.user.ok_read_understood','en'=>'Ok, read and understood!','fr'=>'J\'ai lu et compris'],
            ['label'=> 'front.user.manage_informations','en'=>'Manage my informations','fr'=>'Gérer mes informations'],
            ['label'=> 'front.user.recent_documents','en'=>'Recent documents','fr'=>'Mes documents récents'],
            ['label'=> 'front.user.modify_info','en'=>'Modify my informations','fr'=>'Modifier mes informations'],
            ['label'=> 'front.user.firstname','en'=>'First name','fr'=>'Prénom'],
            ['label'=> 'front.user.lastname','en'=>'Last name','fr'=>'Nom'],
            ['label'=> 'front.user.addresse','en'=>'Addresse','fr'=>'Adresse'],
            ['label'=> 'front.user.zip_code','en'=>'Zip Code','fr'=>'Code Postal'],
            ['label'=> 'front.user.city','en'=>'City','fr'=>'Ville'],
            ['label'=> 'front.user.phone','en'=>'Phone','fr'=>'Téléphone'],

            //contact
            ['label'=> 'front.contact.contact','en'=>'Contact us','fr'=>'Contactez-nous'],
            ['label'=> 'front.contact.name','en'=>'Name','fr'=>'Nom'],
            ['label'=> 'front.contact.email','en'=>'E-mail','fr'=>'E-mail'],
            ['label'=> 'front.contact.subject','en'=>'Subject','fr'=>'Sujet'],
            ['label'=> 'front.contact.message','en'=>'Message','fr'=>'Message'],
            ['label'=> 'front.contact.sending','en'=>'Sending...','fr'=>'En cours d\'envoi...'],
            ['label'=> 'front.contact.send','en'=>'Send','fr'=>'Envoyer'],


            //meta pages
            //zones
            [
                'label'=> 'meta.zones.title',
                'en'=>'GlobalGeoNews | All our areas',
                'fr'=>'GlobalGeoNews | Toutes nos régions'
            ],
            [
                'label'=> 'meta.zones.description',
                'en'=>'GlobalGeoNews | All our areas',
                'fr'=>'GlobalGeoNews | Toutes nos régions'
            ],

            //search
            [
                'label'=> 'meta.search.title',
                'en'=>'GlobalGeoNews | Geopolitical information and risk analysis website',
                'fr'=>'GlobalGeoNews | Site d\'information géopolitique et d\'analyse des risques'
            ],
            [
                'label'=> 'meta.search.description',
                'en'=>'GlobalGeoNews is the first geopolitical information and risk analysis website dedicated to international companies and institutions. Information especially around Mediterranean countries: Maghreb, Middle East and Asia. ',
                'fr'=>'Bienvenue sur GlobalGeoNews, plateforme Web géopolitique et de grands reportages centrés sur l´analyse risque au service des entreprises internationales et des institutionnels.'
            ],

            //home
            [
                'label'=> 'meta.home.title',
                'en'=>'GlobalGeoNews | Geopolitical information and risk analysis website',
                'fr'=>'GlobalGeoNews | Site d\'information géopolitique et d\'analyse des risques'
            ],
            [
                'label'=> 'meta.home.description',
                'en'=>'GlobalGeoNews is the first geopolitical information and risk analysis website dedicated to international companies and institutions. Information especially around Mediterranean countries: Maghreb, Middle East and Asia. ',
                'fr'=>'Bienvenue sur GlobalGeoNews, plateforme Web géopolitique et de grands reportages centrés sur l´analyse risque au service des entreprises internationales et des institutionnels.'
            ],


            //themes
            //geopolitics
            /*[
                'label'=> 'meta.geopolitics.title',
                'en'=>'GlobalGeoNews Geopolitics | Geopolitical information',
                'fr'=>'GlobalGeoNews Géopolitique | Information géopolitique'
            ],
            [
                'label'=> 'meta.geopolitics.description',
                'en'=>'GlobalGeoNews, daily geopolitical analysis and news by reporters and researchers.',
                'fr'=>'Retrouvez quotidiennement les analyses et informations géopolitiques des reporters et chercheurs qui collaborent avec GlobalGeoNews.'
            ],


            //analysis
            [
                'label'=> 'meta.analysis.title',
                'en'=>'GlobalGeoNews Analysis | Maghreb, Middle East and Asia',
                'fr'=>'Analyse de GlobalGeoNews | Maghreb, Moyen-Orient et Asie'
            ],
            [
                'label'=> 'meta.analysis.description',
                'en'=>'GlobalGeoNews, analysis focused on Maghreb, Middle East and Asia on a broad perspective.',
                'fr'=>'Retrouvez les analyses GlobalGeoNews centrées sur le Maghreb, le Moyen-Orient et l\'Asie dans un contexte international.'
            ],


            //economy
            [
                'label'=> 'meta.economy.title',
                'en'=>'GlobalGeoNews Economy | Financial news for risk analysis',
                'fr'=>'GlobalGeoNews Economie | Nouvelles financières pour l\'analyse des risques'
            ],
            [
                'label'=> 'meta.economy.description',
                'en'=>'GlobalGeoNews, economical and financial news intended for risk analysis and investment companies.',
                'fr'=>'Retrouvez les actualités économiques et financières GlobalGeoNews destinées aux entreprises dans leur politique d\'investissements.'
            ],

            //society
            [
                'label'=> 'meta.society.title',
                'en'=>'GlobalGeoNews Society | Reportages, analysis and written forecasting',
                'fr'=>'Société GlobalGeoNews | Reportages, analyses et prévisions écrites'
            ],
            [
                'label'=> 'meta.society.description',
                'en'=>'GlobalGeoNews Society, reportages, analysis and written forecasting.',
                'fr'=>'Reportages, analyses et prospectives sur des sujets d\'actualités de société sur notre plateforme Web GlobalGeoNews.'
            ],*/
        ];


        foreach($data as $insert) DB::table('translate_tools')->insert(
            [
                'label'=>$insert['label'],
                'text_en'=>$insert['en'],
                'text_fr'=>$insert['fr'],
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ]);
    }
}
