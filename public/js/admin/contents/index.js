$(function(){

	$(document).on('click','.remove-item',function(event){
		event.preventDefault();

		var itemId = $(event.target).data('id');

		dialog.confirm('Etes-vous sur de vouloir supprimer ce contenu ?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: routes.deleteItem,
                    data: {
                        _token: csrf_token,
                        id: itemId
                    }
                })
                .done(function(response) {

                	console.log(response)

                    if(!response.error) {
                        $('#table').DataTable().ajax.reload();

                        toastr.success('Ce contenu a été supprimé ', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });

	});

    var itemsTable = $('#table').DataTable({
    	rowReorder: false,
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
				serverSide : true,
				processing: true,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'title', name: 'title'},
			{data: 'published_at', name: 'published_at'},
			{data: 'movie_on_top', name: 'movie_on_top'},
			{data: 'latest_news', name: 'latest_news'},
			{data: 'country_name', name: 'country_name'},
			{data: 'themes', name: 'themes'},
			{data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
		initComplete: function () {

			this.api().columns('.filter-latest-news').every( function () {
				var column = this;
				var select = $('<select><option value="">- Choice -</option></select>')
					.append('<option value="1">Latest News</option>')
					.appendTo($(column.footer()).empty())
					.on('change', function () {
							column
								.search($(this).val(), false, false, true)
								.draw();
					});
			});

			this.api().columns('.filter-top-free').every( function () {
				var column = this;
				var select = $('<select><option value="">- Choice -</option></select>')
					.append('<option value="1">Free</option>')
					.appendTo($(column.footer()).empty())
					.on('change', function () {
							column
								.search($(this).val(), false, false, true)
								.draw();
					});
			});


			this.api().columns('.filter-themes').every( function () {
				var column = this;
				var themes = $('#table').find('.filter-themes').data('themes');
				var select = $('<select><option value="">- Choice -</option></select>');
				for(i = 0; i < themes.length; i++) {
					console.log(themes[i].name);
					select.append('<option value="' + themes[i].id + '">' + themes[i].name + '</option>');
				}

				select.appendTo($(column.footer()).empty())
				.on('change', function () {
						column
							.search($(this).val(), false, false, true)
							.draw();
				});
			});

		}

    });

});
