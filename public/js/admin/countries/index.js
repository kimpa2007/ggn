$(function() {


	$(document).on('click','.remove-item',function(event){
		event.preventDefault();

		var itemId = $(event.target).data('id');

		dialog.confirm('Etes-vous sur de vouloir supprimer ce pays ?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: routes.deleteItem,
                    data: {
                        _token: csrf_token,
                        id: itemId
                    }
                })
                .done(function(response) {

                	console.log(response)

                    if(!response.error) {
                        $('#table').DataTable().ajax.reload();

                        toastr.success('Cet pays a été supprimer ', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });

	});


	 var itemsTable = $('#table').DataTable({
	 	rowReorder: true,
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: false,
        serverSide: false,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
        		{data: 'order', name: 'order', width: "40"},
            {data: 'id', name: 'id', width: "40"},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        rowReorder: {
            dataSrc: 'order',
        }
    });

    itemsTable.on('row-reorder', function ( e, diff, edit ) {
    	var newOrder = [];

        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = itemsTable.row( diff[i].node ).data();

 			newOrder.push({
 				id : rowData['id'],
 				newOrder : diff[i].newData
 			});
        }

		if(newOrder.length > 0){

			$.ajax({
	            type: 'POST',
	            url: routes.updateOrder,
	            data: {
	            	_token: csrf_token,
	            	order : newOrder
	            },
	            dataType: 'html',
	            success: function(data){
	            	console.log(data);
	                var rep = JSON.parse(data);
	                if(rep.code == 200){
	                    //change
	                    toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {timeOut: 3000});
	                }
	                else if(rep.code == 400){
	                	//error
	                	toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {timeOut: 3000});
	                }
	                else {
	                	//nothing to change
	                }
	            }
			});
		}
    });

});
