$(function() {

    var table = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.data,
        columns: [
						{data: 'id', name: 'id', width: "40"},
						{data: 'partner', name: 'partner'},
						{data: 'date', name: 'date'},
						{data: 'amount', name: 'amount'},
						{data: 'type', name: 'type'},
						{data: 'status', name: 'status'},
						{data: 'action', name: 'action', orderable: false, searchable: false, width:'200'}
        ],
				order: [[ 2, 'desc' ]],
        initComplete: function () {
          // Add filters columns
          this.api().columns([1,4,5]).every(function() {
              var column = this;
              var select = $('<select><option value=""></option></select>')
                  .appendTo($(column.footer()).empty())
                  .on('change', function() {
                      var val = $.fn.dataTable.util.escapeRegex($(this).val());
                      column
                          .search(val ? '^' + val + '$' : '', true, false)
                          .draw();
                  });

              column.data().unique().sort().each(function( d, j ) {
                  if(d != null){
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                  }
              });
          });

          initEvents();

        }
    });

    initEvents = function(){
      $(document).on('click','#table .toggle-delete',function(e){
          var self = $(this);
          e.preventDefault();

          dialog.confirm('Etes-vous sur de vouloir cette commande ?', function(confirm){
              if(confirm) {
                  document.location.replace(self.attr('href'));
              }
          });
      });
    };

});
