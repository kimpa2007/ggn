$(function(){

    $(document).on('click','.toogle-delete',function(event){
    	event.preventDefault();

    	var url = $(event.target).data('url');

    	dialog.confirm('Etes-vous sur de vouloir supprimer cette légende ?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        _token: csrf_token,
                        _method: 'DELETE',
                    }
                })
                .done(function(response) {
                    if(!response.error) {
                        $('#table').DataTable().ajax.reload();
                        toastr.success('Cette légende a été supprimée ', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });

    });

    $('#table').DataTable({
    	rowReorder: false,
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
		serverSide : true,
		processing: true,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'icon', name: 'icon'},
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
		initComplete: function () {

		}
    });

});
