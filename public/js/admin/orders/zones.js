$(function() {

    var table = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.data,
        order: [[ 0, "desc" ]],
        columns: [
            {data: 'id', name: 'id'},
            {data: 'partner', name: 'partner'},
            {data: 'zone', name: 'zone'},
            {data: 'requested_at', name: 'requested_at'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function () {
            this.api().columns([4]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );

            initEvents(table);
        }
    });


    var initEvents = function(table) {
        $(document).on('click','#table .toggle-delete',function(e){
            var self = $(this);
            e.preventDefault();

            dialog.confirm('Etes-vous sur de vouloir cette commande ?', function(confirm){
                if(confirm) {
                    document.location.replace(self.attr('href'));
                }
            });
        });
    }
});
