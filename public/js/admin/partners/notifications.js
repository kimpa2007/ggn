$(function() {

    var table = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.data,
        columns: [
            {data: 'created_at', name: 'created_at'},
            {data: 'title', name: 'title'},
            {data: 'status', name: 'status'},
            {data: 'sent_at', name: 'sent_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function() {
            initEvents(table);
        }
    });

    var initEvents = function(table) {

    }
});
