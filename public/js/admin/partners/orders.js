$(function() {

    // Loading data
    var table = $('#table-plans').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.data.plans,
        columns: [
            {data: 'created_at', name: 'created_at'},
            {data: 'plan.name', name: 'plan.name'},
            {data: 'status', name: 'status'},
            {data: 'paid_at', name: 'paid_at'},
            {data: 'type_payment', name: 'type_payment'},
            {data: 'start_at', name: 'start_at'},
            {data: 'end_at', name: 'end_at'},
            {data: 'amount', name: 'amount'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function() {
            //initEvents(table);
        }
    });


    // Loading data
    var table = $('#table-credits').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.data.credits,
        columns: [
            {data: 'created_at', name: 'created_at'},
            {data: 'status', name: 'status'},
            {data: 'paid_at', name: 'paid_at'},
            {data: 'type_payment', name: 'type_payment'},
            {data: 'amount', name: 'amount'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function() {
        }
    });



});
