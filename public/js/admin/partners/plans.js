$(function() {
    var table = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.data,
        columns: [
            {data: 'created_at', name: 'created_at'},
            {data: 'plan.name', name: 'plan.name'},
            {data: 'plan.time', name: 'plan.time'},
            {data: 'start_at', name: 'start_at'},
            {data: 'end_at', name: 'end_at'},
            {data: 'amount', name: 'amount'},
            {data: 'status', name: 'status'},
            {data: 'zones', name: 'zones'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function() {
            initEvents(table);
        }
    });

    var initEvents = function(table) {

    }
});
