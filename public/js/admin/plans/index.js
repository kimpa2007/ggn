$(function(){


	$(document).on('click','.remove-item',function(event){
		event.preventDefault();

		var itemId = $(event.target).data('id');

		dialog.confirm('Etes-vous sur de vouloir supprimer cet contenu ?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: routes.deleteItem,
                    data: {
                        _token: csrf_token,
                        id: itemId
                    }
                })
                .done(function(response) {

                	console.log(response)

                    if(!response.error) {
                        $('#table').DataTable().ajax.reload();

                        toastr.success('Cet contenu a été supprimer ', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });

	});

    var itemsTable = $('#table').DataTable({
    	language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: false,
        serverSide: false,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'name', name: 'name'},
			{data: 'zone', name: 'zone'},
            {data: 'description', name: 'description'},
            {data: 'months', name:'months'},
			{data: 'price', name: 'price'},
			{data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function() {

            // Add filters columns
            this.api().columns([5]).every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function( d, j ) {
                    if(d != null){
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                    }
                });
            });

        }
    });

});
