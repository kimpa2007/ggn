$(function() {


    $(document).on('click','.remove-item',function(event){
        event.preventDefault();

        var itemId = $(event.target).data('id');

        dialog.confirm('Etes-vous sur de vouloir supprimer cet theme ?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: routes.deleteItem,
                    data: {
                        _token: csrf_token,
                        id: itemId
                    }
                })
                .done(function(response) {

                    if(!response.error) {
                        $('#table').DataTable().ajax.reload();

                        toastr.success('Cet theme a été supprimer ', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });

    });


     var table = $('#table').DataTable({
        "order": [[ 2, "asc" ]],
        rowReorder: true,
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: false,
        serverSide: false,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'name', name: 'name'},
            {data: 'position', name: 'position'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 2 },
            { orderable: false, targets: '_all' }
        ],
         rowReorder: {
            dataSrc: 'position'
        }
      
    });



      table.on('row-reorder', function ( e, diff, edit ) {

        var result = 'Reorder started on row: #'+edit.triggerRow.data()['id']+' ';

        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();

            var idTheme = rowData['id'];
            var newPosition = diff[i].newData;
            var oldPosition = diff[i].oldData;

            //console.log("Theme id: "+idTheme+ " passe de: "+oldPosition+" a: "+newPosition)
            ///theme/position/update

            $.ajax({
                url : '/admin/theme/position/update', 
                type : 'POST',
                data: {
                    'idTheme': idTheme, 
                    'newPosition':newPosition, 
                    'oldPosition': oldPosition,
                    _token: csrf_token,
                },
                dataType: 'json',
                success : function(code_html, statut){ // code_html contient le HTML renvoyé
                    //console.log("recharge table");
                }
            });


        }

    } );

});
