$(function(){
	$(document).on('click','.remove-item',function(event){
		event.preventDefault();
		var itemId = $(event.target).data('id');
		dialog.confirm('Etes-vous sur de vouloir supprimer cette traduction?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: routes.deleteItem + '/'+itemId,
                    data: {
                        _token: csrf_token,
                        _method: 'DELETE'
                    }
                })
                .done(function(response) {
                	
                	console.log(response)
                	
                    if(!response.error) {
                        $('#table').DataTable().ajax.reload();
                        
                        toastr.success('Cette traduction a été supprimée', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });
		
	});
	
    var itemsTable = $('#table').DataTable({
    	rowReorder: true,
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: false,
        serverSide: false,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'label', name: 'label'},
            {data: 'text_fr', name: 'text_fr'},
            {data: 'text_en', name: 'text_en'},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ],
        rowReorder: {
            dataSrc: 'label',
        }
    });
    
});