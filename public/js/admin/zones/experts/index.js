$(function() {


	 $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'full_name', name: 'full_name'},
			{data: 'date', name: 'date'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

});