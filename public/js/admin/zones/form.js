$(function() {

	$("input[name='slug']").slugify("input[name='name']");

    $( "#sortable1, #sortable2, #sortable3, #sortable4" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();

    var updatePartnerHiddens = function(){

    	$("#sortable2 input[type='hidden']").remove();

    	$.each($("#sortable2 li"), function(){
    		$("#sortable2").append('<input type="hidden" name="experts[]" value="'+$(this).data('id')+'" >')
    	});

    };

    $( "#sortable1, #sortable2" ).on('sortreceive',function(event, ui) {
    	updatePartnerHiddens();
    });

    $(".add-all").click(function(e){
    	e.preventDefault();

    	$("#sortable2").append($("#sortable1 li"));
    	updatePartnerHiddens();
    });

    $(".remove-all").click(function(e){
    	e.preventDefault();

    	$("#sortable1").append($("#sortable2 li"));
    	updatePartnerHiddens();
    });
		/* */
		var updateCountryHiddens = function(){

    	$("#sortable4 input[type='hidden']").remove();

    	$.each($("#sortable4 li"), function(){
    		$("#sortable4").append('<input type="hidden" name="countries[]" value="'+$(this).data('id')+'" >')
    	});

    };

    $( "#sortable3, #sortable4" ).on('sortreceive',function(event, ui) {
    	updateCountryHiddens();
    });

    $(".add-all-country").click(function(e){
    	e.preventDefault();

    	$("#sortable4").append($("#sortable3 li"));
    	updateCountryHiddens();
    });

    $(".remove-all-country").click(function(e){
    	e.preventDefault();

    	$("#sortable3").append($("#sortable4 li"));
    	updateCountryHiddens();
    });
		/* */

});
