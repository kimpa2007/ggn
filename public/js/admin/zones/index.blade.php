@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des zones
    </h1>

    <a href="#" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Order</th>
                <th>#</th>
                <th>Zone</th>
                <th>Pays</th>
                <th></th>
            </tr>
        </thead>
    </table>

</div>
@endsection

@push('javascripts')
<script>

var csrf_token = "{{csrf_token()}}";

var routes = {
	getData : '{{ action("Admin\Zone\ZoneController@getData") }}',
	deleteItem : '{{ action("Admin\Zone\ZoneController@delete") }}',
	updateOrder : '{{ action("Admin\Zone\ZoneController@updateOrder") }}'
};

</script>

{{ Html::script('js/admin/zones/index.js')}}

@endpush
