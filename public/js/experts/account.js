Dropzone.autoDiscover = false;

$(document).ready(function(){
	
	$(document).on('click','#remove-picture',function(event){
		
		event.preventDefault();
		
		$(".image-container.uploaded").css({display:'none'});
        $(".dropzone-container").css({display:'block'});
        $(".dropzone-container .message").css({opacity:1});
        $(".dropzone-container .progress-bar").css({display:"none"});
		
		$(".dropzone-avatar #image").val(null);
	});
	
	$(".submit-form").click(function(e) {
		
		e.preventDefault();
		
		if($("#uploading").length > 0 && $("#uploading").val() == 1){
			toastr.error('Image is uploading, please wait upload is complete', 'Erreur !', {timeOut: 3000});
		}
		else {
			//submit form
			$(".submit-form").closest("form").submit();	
		}
	});
	
	var imageDropzone = new Dropzone("#real-dropzone",{
	
	    uploadMultiple: false,
	    parallelUploads: 1,
	    maxFilesize: 8,
	    //previewsContainer: '#dropzonePreview',
	    //previewTemplate: document.querySelector('#preview-template').innerHTML,
	    addRemoveLinks: true,
	    dictRemoveFile: 'Remove',
	    dictFileTooBig: 'Image is bigger than 8MB',
	    url : routes.postUpload,
	
	    // The setting up of the dropzone
	    init:function() {
	
	        
	    },
	    sending: function(file, xhr, formData) {
		    formData.append("_token", csrf_token);
		},
	    uploadprogress : function(file,progress){
	    	console.log(file);
	    	console.log(progress);
	    	
	    	console.log("progress : "+progress);
	    	
	    	$(".dropzone-avatar #uploading").val(1);
	    	$(".dropzone-container .actions").css({opacity:0});
	    	
	    	$(".dropzone-avatar .progress-bar").css({
	    		display:"block",
	    		width:progress+"%"
	    	});
	    	
	    },
	    error: function(file, response) {
	        
	    },
	    success: function(file,done) {
	    	
	        //enable
	        
	        $("#image").val(done.filename);
	        
	        $(".image-container.uploaded .background-image").css({
	        	backgroundImage : "url("+done.storage_filename+")",
	        });
	        
	        $(".image-container.uploaded").css({display:'block'});
			$(".dropzone-container").css({display:'none'});
			
			$(".dropzone-avatar #uploading").val(0);
	        
	    }
	});
	
});
