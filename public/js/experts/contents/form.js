Dropzone.autoDiscover = false;

$(function() {
	
	$(document).on('click','.single-file-upload-toogle a', function(e) {
        e.preventDefault();
        
        console.log("button click!");
        
        $(this).parent().find(".input-file" ).click();
    });

    
    $(document).on('change','.single-file-upload-toogle .input-file',function(e){
    	
    	console.log("input change!");
    	
        var filename = $(this).val().split('\\').pop();
        $('.single-file-upload-toogle .filename').text(filename);
        
        $(".preview-link").slideUp();
    });
	
	
	$("#content-type-selector").change(function(e){
		console.log("select : "+$("#content-type-selector").val());	
		
		$(".type-content").css({display:"none"});
		if($("#content-type-selector").val() != null){
			$(".type-content.type-"+$("#content-type-selector").val()).css({display:"block"});
		}
		
	});
	
	$('#countrie-select').hierarchySelect({
		hierarchy: true,
		search: true,
		defaultValue:country_id,
		width:"100%"
	});
	
	$('#countrie-select a.select-option').click(function (e) {
	  //console.log(e);
	  e.preventDefault();
	  
	  $("input[name='country_id']").val($(this).data('value'));
	});
	
	$(".btn-status-option").click(function(event){
		event.preventDefault();
		
		var status = $(event.target).data('status');
		var itemId = $(event.target).data('id');
		
		//out.debug("status change! : "+status+" : id : "+produtId);
		
		$.ajax({
            type: 'POST',
            url: routes.changeStatus,
            data: {
            	_token: csrf_token,
            	itemId : itemId,
            	status : status
            },
            dataType: 'html',
            success: function(data){
            	console.log(data);
                var rep = JSON.parse(data);
                if(rep.code == 200){
                    //change 
                    $(".status-option").css({display:"none"});
                    $(".status-option.status-"+status).css({display:"block"});
                    
                    $(".btn-status-option i.fa").remove();
                    $(".btn-status-option.status-"+status).append('<i class="fa fa-check"></i>');
                    
                    toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {timeOut: 3000});
                }
                else if(rep.code == 400){
                	//error
                	toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {timeOut: 3000});
                }
                else {
                	//nothing to change
                }
            }
        });
		
	});
});

