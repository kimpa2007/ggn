$(function(){

    var itemsTable = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: false,
        serverSide: false,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'order', name: 'order'},
            {data: 'id', name: 'id', width: "40"},
            {data: 'title', name: 'title'},
						{data: 'countrie.name', name: 'countrie.name'},
            {data: 'views', name: 'views'},
						{data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function() {

            // Add filters columns
            this.api().columns([5]).every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function( d, j ) {
                    if(d != null){
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                    }
                });
            });

        }
    });



});
