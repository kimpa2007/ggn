$(function() {


	var updateSelectStatus = function(){
		if($("select[name='is_payed']").val() == 0){
			//non paye
			$("select[name='status'] > option").each(function(){

				if(this.value == options.engaged
					|| this.value == options.done){
					//this.attr()

					$(this).prop('disabled', true);
				}
			});
		}
		else {
			$("select[name='status'] > option").each(function(){
				$(this).prop('disabled', false);
			});
		}

	};

	$(document).on('change',"select[name='is_payed']",function(e){
		console.log("is payed!");
		updateSelectStatus();
	});

	$(document).on('click','.open-modal',function(event){
		event.preventDefault();

		$.each($(this).data(), function( key, value ) {

			console.log("key : "+key+", value : "+value);
			console.log($('#orderModal').find('[name="' + key + '"]'));

			var item = $('#orderModal').find('[name="' + key + '"]');

			if(item.is('input') || item.is('select') || item.is('textarea')){
				item.val(value);
			}
			else {
				item.html(value);
			}
		});

		updateSelectStatus();



		$('#orderModal').modal('show');



	});

	$('#order-form').on('submit', function(e){
		e.preventDefault();
		var form = $(this);

		$.ajax({
			method: form.attr('method'),
			url: form.attr('action'),
			data: form.serializeArray()
		})
		.done(function( response ) {
			console.log(response);
			if(response.success) {

				$('#table').DataTable().ajax.reload(function(response){
					table.refresh();
				}, false);
				$('#orderModal').modal("hide");
				toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {timeOut: 3000});
			}
		}).fail(function(response){
			toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {timeOut: 3000});
		});

	});

	$(document).on('click','.btn-status-option',function(event){
		event.preventDefault();

		var status = $(event.target).data('status');
		var itemId = $(event.target).data('id');

		//console.log("status change! : "+status+" : id : "+itemId);
		var root = $("#btn-status-"+itemId);

		$.ajax({
            type: 'POST',
            url: routes.changeStatus,
            data: {
            	_token: csrf_token,
            	itemId : itemId,
            	status : status
            },
            dataType: 'html',
            success: function(data){
            	console.log(data);
                var rep = JSON.parse(data);
                if(rep.code == 200){

                	//change
                    root.find(".status-option").css({display:"none"});
                    root.find(".status-option.status-"+status).css({display:"block"});

                    root.next().find(".btn-status-option i.fa").remove();
                    root.next().find(".btn-status-option.status-"+status).append('<i class="fa fa-check"></i>');

                    //update datatable
                    $('#table').DataTable().ajax.reload(function(response){
						table.refresh();
					}, false);

					toastr.success('Status changed successfully.', 'Sauvegardé !', {timeOut: 3000});
                }
                else if(rep.code == 400){
                	//error
                }
                else {
                	//nothing to change
                }
            }
        });

	});

	$('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: false,
        serverSide: false,
        pageLength: 50,
        ajax: routes.getData,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'product', name: 'product'},
			{data: 'username', name: 'username'},
			{data: 'date', name: 'date'},
			//{data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function () {
            this.api().columns([1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
   });

});
