Dropzone.autoDiscover = false;

$(function() {
	
    $(".btn-status-option").click(function(event){
		event.preventDefault();
		
		var status = $(event.target).data('status');
		var produtId = $(event.target).data('id');
		
		//out.debug("status change! : "+status+" : id : "+produtId);
		
		$.ajax({
            type: 'POST',
            url: routes.changeStatus,
            data: {
            	_token: csrf_token,
            	productId : produtId,
            	status : status
            },
            dataType: 'html',
            success: function(data){
            	console.log(data);
                var rep = JSON.parse(data);
                if(rep.code == 200){
                    //change 
                    $(".status-option").css({display:"none"});
                    $(".status-option.status-"+status).css({display:"block"});
                    
                    $(".btn-status-option i.fa").remove();
                    $(".btn-status-option.status-"+status).append('<i class="fa fa-check"></i>');
                    
                    toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {timeOut: 3000});
                }
                else if(rep.code == 400){
                	//error
                	toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {timeOut: 3000});
                }
                else {
                	//nothing to change
                }
            }
        });
		
	});
	
});

