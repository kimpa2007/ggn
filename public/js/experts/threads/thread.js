$(document).ready(function(){

	$(".load-more-children").click(function(e){

		e.preventDefault();

		if(page >= availablePages)
			return;

		page++;

		$.ajax({
		    url: routes.show,
		    type: "GET",
				data: {
					page : page
				},
			    success: function(response){
			    	console.log($(response).find('#children'));

			    	$("#children").prepend($(response).find('#children').html());

			    	if(page >= availablePages){
			    		$(".load-more-children").slideUp();
			    	}
			    },
			    error:function(msg){}
			});

		});

		$(document).on('click','.single-file-upload-toogle a', function(e) {
        e.preventDefault();
        $(this).parent().find( "input[name='file']" ).click();
    });

    $(document).on('change',".single-file-upload-toogle input[name='file']", function(e){
        var filename = $(this).val().split('\\').pop();
        $(this).parent().find('.filename').text(filename);
    });

		$(document).on('click',".edit-thread",function(e){
			e.preventDefault();

			var el = $(this);

			var threadId = el.data('thread-id');

			$('.thread-'+threadId+" .thread-content").css({display:'none'});
			$(".thread-"+threadId+" .response-block").css({display:'block'});

		});

		$(document).on('click','.close-edit',function(e){
			e.preventDefault();

			var el = $(this);

			var threadId = el.data('thread-id');

			$('.thread-'+threadId+" .thread-content").css({display:'block'});
			$(".thread-"+threadId+" .response-block").css({display:'none'});

		});

    $(document).on('click','.remove-thread-toogle', function(e){
        e.preventDefault();

        var el = $(this);

        bootbox.confirm({
            message: "Êtes-vous sur de vouloir supprimer ce message ?",
            buttons: {
                confirm: {
                    label: 'Oui',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Non',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
            	if(result){
                	window.location.href = el.attr('href');
                }
            }
        });
    });
});
