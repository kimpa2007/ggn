app.map = {

    leafletMap: null,
    map : null,
    modules: [],
    sateliteLayer: null,
    geoJsons: [],
    options: {},
    othersCountriesLabels: [{
        name : 'Monaco',
        id : 23,
        latlng: [43.73333, 7.41667],
        breakZoom: 5
    }],

    setLanguage: function(lang)
    {
        this.map._glMap.setLanguage(lang, true);
    },

    init: function(settings, key, locale)
    {
        $("#map").css({
            opacity: 0
        });

        var self = this;

        L.TopoJSON = L.GeoJSON.extend({
            addData: function(data) {
                var geojson, key;
                if (data.type === "Topology") {
                    for (key in data.objects) {
                        if (data.objects.hasOwnProperty(key)) {
                            geojson = topojson.feature(data, data.objects[key]);
                            L.GeoJSON.prototype.addData.call(this, geojson);
                        }
                    }
                    return this;
                }

                L.GeoJSON.prototype.addData.call(this, data);

                return this;
            }
        });

        L.topoJson = function(data, options) {
            return new L.TopoJSON(data, options);
        };

        jQuery.extend(self.options, settings);

        this.leafletMap = L.map('map', {
            maxZoom: 10,
            minZoom: 2,
            zoomControl: true,
            scrollWheelZoom: true,
        }).setView(
            [27.3803, 37.60071],
            2
        );

        this._loadCoutries();

        this.leafletMap.createPane('labels');
    	this.leafletMap.getPane('labels').style.zIndex = 650;
    	this.leafletMap.getPane('labels').style.pointerEvents = 'none';

         var gl = L.mapboxGL({
             accessToken: 'xxxx',
             style: 'https://maps.tilehosting.com/c/85e2cd41-3ae8-4127-aa88-93b37acaf493/styles/darkmatter/style.json?key=' + key,
         }).addTo(this.leafletMap);

         var gl = L.mapboxGL({
             accessToken: 'xxxx',
             style: 'https://maps.tilehosting.com/c/85e2cd41-3ae8-4127-aa88-93b37acaf493/styles/darkmatter-7cfd7/style.json?key=' + key,
         }).addTo(this.leafletMap);


        // this.map = L.mapboxGL({
        //     accessToken: 'xxxx',
        //     style: 'https://maps.tilehosting.com/c/85e2cd41-3ae8-4127-aa88-93b37acaf493/styles/positron/style.json?key=' + key,
        //     pane: 'labels'
        // }).addTo(this.leafletMap);

        this.map = L.mapboxGL({
            accessToken: 'xxxx',
            style: 'https://maps.tilehosting.com/c/85e2cd41-3ae8-4127-aa88-93b37acaf493/styles/darkmatter-7cfd7/style.json?key=' + key,
             pane: 'labels'
        }).addTo(this.leafletMap);

        this.setLanguage(locale);

        this.markerIcons = {};

        for (var key in this.options.legends) {
            var legend = this.options.legends[key];

            this.markerIcons[legend.id] = L.icon({
                iconUrl: legend.icon,
                iconSize: [23, 23], // size of the icon
                iconAnchor: [12, 12], // point of the icon which will correspond to marker's location
                popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
            });

            $("#map-legends").append(
                '<div class="map-legend" style="background-color:' + legend.color + ';">' +
                '<div class="icon"><img src="' + WEBROOT + legend.icon + '" width="22" /></div>' +
                '<div class="label">' + legend.name + '</div>' +
                '</div>'
            );
        }


        this._loadMarkers();
        this._setOthersCountriesLabels();
    },

    _setOthersCountriesLabels: function()
    {
        var self = this;
        var _markers = [];

        $.each(self.othersCountriesLabels, function(index, country) {
            var marker = L.marker(country.latlng, {
                className: 'countryLabel',
                icon: L.divIcon({
                    html: country.name,
                    className: 'countryLabel',
                    iconSize: [0, 0]
                })
            }).on('click', function(e) {
                window.location = "/country/" + country.id;
                e.preventDefault();
            });

            // L.DomEvent.addListener(marker.label, 'click', function(e) {
            //     alert('ok');
            //     e.preventDefault();
            //  }, marker);

            // marker.getLabel().on('click', function(e) {
            //     alert('ok');
            //     e.preventDefault();
            // });

            _markers[index] = new L.FeatureGroup();
            _markers[index].addLayer(marker);
        });

        self.leafletMap.on('zoomend', function() {
            $.each(self.othersCountriesLabels, function(index, country) {
                if (self.leafletMap.getZoom() > country.breakZoom){
                    self.leafletMap.addLayer(_markers[index]);
                } else {
                    self.leafletMap.removeLayer(_markers[index]);
                }
            });
        });
    },

    _loadMarkers: function()
    {
        for (var key in this.options.markers) {

            var marker = this.options.markers[key];

            if(this.markerIcons[marker.legend_id] !== undefined) {
                L.marker(marker.latlng, {
                        icon: this.markerIcons[marker.legend_id],
                        url: marker.url
                        //bubblingMouseEvents: true
                    }).on('click', function(e) {
                        //same tab
                        //window.location.href = e.target.options.url;

                        //new tab
                        var win = window.open(e.target.options.url, '_blank');
                        win.focus();

                    }).addTo(this.leafletMap);
            }

        }
    },

    _loadCoutries: function()
    {
        var self = this;

        self.leafletMap.createPane('labels');

        $.getJSON('/js/home/countries.topojson')
            .done(function(data) {

                self.definition = self._createDefinition();
                self.geoJSONdata = data;
                self.definition.addData(self.geoJSONdata);
                self.leafletMap.addLayer(self.definition, true);

                var selectedLayers = [];

                app.debug("country layers! =>");
                self.definition.eachLayer(function(layer) {
                    if (self._existInSelected(layer.feature.id)) {
                        layer.setStyle(self.options.selectedStyle);

                        layer.on('click', function() {
                            window.location.href = self._getCountryUrl(layer.feature.id);
                        });

                        selectedLayers.push(layer);
                    } else {
                        layer.setStyle(self.options.defaultStyle);
                    }
                });

                var group = L.featureGroup(selectedLayers);
                var width = $(window).width();
                var padding = [20, 20];
                var mobile = true;

                if (width >= 1170) {
                    padding = [parseInt(width / 4), 30];
                    mobile = false;
                }

                self.leafletMap.fitBounds(group.getBounds(), {
                    padding: padding
                });

                setTimeout(function() {
                    if (!mobile) {
                        self.leafletMap.panBy([padding[0] - 50, 0]);
                    }

                    $("#map").css({
                        opacity: 1
                    });
                    $(".spinner").css({
                        display: 'none'
                    });

                    self.leafletMap.invalidateSize();


                }, 1500);

            });
    },

    _existInSelected: function(countryId)
    {
        for (var key in this.options.countries) {
            var country = this.options.countries[key];

            if (country.iso.indexOf(countryId) > -1) {
                return true;
            }
        }
        return false;
    },

    _getCountryUrl: function(countryId)
    {
        for (var key in this.options.countries) {
            var country = this.options.countries[key];

            if (country.iso.indexOf(countryId) > -1) {
                return country.url;
            }
        }
        return null;
    },

    _createDefinition: function() {
        return new L.TopoJSON(null, {
            style: function(feature) {
                return feature.properties.layerStyle;
            },
            onEachFeature: function(feature, layer) {
                // does this feature have a property named popupContent?
                if (feature.properties.status == 1 && feature.properties.name !== undefined && feature.properties.name != "") {
                    layer.bindTooltip(feature.properties.name, {
                        sticky: true
                    });
                }

            }
        });
    },


    handleAddGeoJSON: function(geoJSON)
    {
        this.leafletMap.addLayer(geoJSON);
    },

    handleRemoveGeoJSON: function(geoJSON)
    {

        this.leafletMap.removeLayer(geoJSON);
    },

    handleUpdateGeoJSON: function(geoJSON)
    {},

    handleAddOverlay: function(overlay)
    {
        this.leafletMap.addLayer(overlay);
    },

    handleRemoveOverlay: function(overlay)
    {
        this.leafletMap.removeLayer(overlay);
    },

    handleMapEvent: function(mapEvent)
    {
        if (mapEvent === undefined) {
            return;
        }

        if (mapEvent.hasOwnProperty('panTo') && mapEvent.panTo != null) {
            this.leafletMap.flyTo(
                mapEvent.panTo.latlng,
                parseInt(mapEvent.panTo.zoom)
            );
        }

        if (this.sateliteLayer != null) {
        }
    }
};
