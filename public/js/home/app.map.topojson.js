app.map = {

    leafletMap: null,
    modules: [],
    sateliteLayer: null,

    geoJsons: [

    ],

    options: {

    },

    init: function(settings, key) {

        $("#map").css({
            opacity: 0
        });

        app.debug("map :: init");

        var self = this;

        L.TopoJSON = L.GeoJSON.extend({
            addData: function(jsonData) {
                if (jsonData.type === 'Topology') {
                    for (key in jsonData.objects) {
                        geojson = topojson.feature(jsonData, jsonData.objects[key]);
                        L.GeoJSON.prototype.addData.call(this, geojson);
                    }
                } else {
                    L.GeoJSON.prototype.addData.call(this, jsonData);
                }
            }
        });

        jQuery.extend(self.options, settings);

        this.leafletMap = L.map('map', {
            maxZoom: 6,
            minZoom : 2,
            zoomControl: true,
            scrollWheelZoom: false
        }).setView(
            [27.3803, 37.60071],
            2,
        );

        this.leafletMap.on('click', function(e) {
            console.log("Map center : " + self.leafletMap.getCenter());
            console.log("Map zoom : " + self.leafletMap.getZoom());
        });

        // var Stamen_Toner = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.{ext}', {
        //     attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        //     subdomains: 'abcd',
        //     minZoom: 0,
        //     maxZoom: 20,
        //     ext: 'png'
        // });

        // var layer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        //     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        // });

        var gl = L.mapboxGL({
            accessToken: 'xxxx',
            style: 'https://maps.tilehosting.com/c/85e2cd41-3ae8-4127-aa88-93b37acaf493/styles/darkmatter/style.json?key=' + key
            //style: 'https://maps.tilehosting.com/c/6b7d6aab-9dd2-4f6e-8998-8af6a4bbe8db/styles/darkmatter/style.json?key=drBIirAs6Q0u3ijcy2dO'
            //style: 'https://maps.tilehosting.com/c/6b7d6aab-9dd2-4f6e-8998-8af6a4bbe8db/styles/darkmatter-ad6e1/style.json?key=drBIirAs6Q0u3ijcy2dO'
        }).addTo(this.leafletMap);


        //this.leafletMap.addLayer(layer);

        /*
            L.gridLayer.googleMutant({
                type: 'roadmap',
                styles: custom_json
            }).addTo(this.leafletMap);
        */

        /*
            this.topoLayer = new L.TopoJSON();

            $.getJSON('js/home/countries.topo.json')
            .done(function(topoData){
                self.topoLayer.addData(topoData);
                self.topoLayer.addTo(self.leafletMap);
            });
        */

        this.markerIcons = {};

        for (var key in this.options.legends) {
            var legend = this.options.legends[key];

            this.markerIcons[legend.id] = L.icon({
                iconUrl: legend.icon,
                iconSize: [23, 23], // size of the icon
                iconAnchor: [12, 12], // point of the icon which will correspond to marker's location
                popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
            });

            $("#map-legends").append(
                '<div class="map-legend" style="background-color:' + legend.color + ';">' +
                '<div class="icon"><img src="' + WEBROOT + legend.icon + '" /></div>' +
                '<div class="label">' + legend.name + '</div>' +
                '</div>'
            );


        }

        this._loadCoutries();
        this._loadMarkers();

    },

    _loadMarkers: function() {

        for (var key in this.options.markers) {

            var marker = this.options.markers[key];

            L.marker(marker.latlng, {
                icon: this.markerIcons[marker.legend_id],
                url : marker.url
                //bubblingMouseEvents: true
            }).on('click',function(e){

              //same tab
              //window.location.href = e.target.options.url;

              //new tab
              var win = window.open(e.target.options.url, '_blank');
              win.focus();

            })
            .addTo(this.leafletMap)

        }

    },

    _loadCoutries: function() {

        var self = this;

        $.getJSON('/js/home/countries.topojson')
            .done(function(data) {

                self.definition = self._createDefinition();
                self.geoJSONdata = data;

                /*
                for (var key in self.geoJSONdata.objects['countries-2'].geometries) {
                    self.geoJSONdata.objects['countries-2'].geometries[key].status = 0;
                    self.geoJSONdata.objects['countries-2'].geometries[key].layerStyle = self.options.defaultStyle;

                    if (self._existInSelected(self.geoJSONdata.objects['countries-2'].geometries[key].id)) {
                        self.geoJSONdata.objects['countries-2'].geometries[key].status = 1;
                        self.geoJSONdata.objects['countries-2'].geometries[key].layerStyle = self.options.selectedStyle;
                    }

                }
                */

                console.log("Map :: Generate Topojson data => ",self.geoJSONdata);

                self.definition.addData(self.geoJSONdata);
                self.leafletMap.addLayer(self.definition);

                var selectedLayers = [];

                app.debug("country layers! =>");
                self.definition.eachLayer(function(layer) {

                    console.log("Map :: eachLayer => ",layer);

                    if (self._existInSelected(layer.feature.id)) {
                        //app.debug(layer);
                        layer.setStyle(self.options.selectedStyle);

                        layer.on('click', function() {
                            window.location.href = self._getCountryUrl(layer.feature.id);
                        });

                        selectedLayers.push(layer);
                    }
                    else {
                        layer.setStyle(self.options.defaultStyle);
                    }
                });

                //app.debug("Selected layers =>");
                //app.debug(selectedLayers);

                var group = L.featureGroup(selectedLayers);

                var width = $(window).width();
                var padding = [20, 20];
                var mobile = true;

                if (width >= 1170) {
                    padding = [parseInt(width / 4), 30];
                    mobile = false;
                }

                self.leafletMap.fitBounds(group.getBounds(), {
                    padding: padding
                });

                setTimeout(function() {
                    if (!mobile) {
                        self.leafletMap.panBy([padding[0] - 50, 0]);
                    }

                    $("#map").css({
                        opacity: 1
                    });
                    $(".spinner").css({
                        display: 'none'
                    });

                    self.leafletMap.invalidateSize();


                }, 1500);

            });
    },

    _existInSelected: function(countryId) {

        for (var key in this.options.countries) {
            var country = this.options.countries[key];

            if (country.iso.indexOf(countryId) > -1) {
                return true;
            }
        }
        return false;

    },

    _getCountryUrl: function(countryId) {

        for (var key in this.options.countries) {
            var country = this.options.countries[key];

            if (country.iso.indexOf(countryId) > -1) {
                return country.url;
            }
        }
        return null;

    },

    _createDefinition: function() {
        return new L.TopoJSON(null, {
            style: function(feature) {
                return feature.properties.layerStyle;
            },
            onEachFeature: function(feature, layer) {
                // does this feature have a property named popupContent?
                if (feature.properties.status == 1 && feature.properties.name !== undefined && feature.properties.name != "") {
                    layer.bindTooltip(feature.properties.name, {
                        sticky: true
                    });
                }

            }
        });
    },


    handleAddGeoJSON: function(geoJSON) {

        //this.geoJsons.push(geoJSON);

        this.leafletMap.addLayer(geoJSON);
    },

    handleRemoveGeoJSON: function(geoJSON) {

        this.leafletMap.removeLayer(geoJSON);
    },

    handleUpdateGeoJSON: function(geoJSON) {
        //this.leafletMap.removeLayer(this.geoJsons[0]);

        //this.leafletMap.addLayer(geoJSON);

        //this.geoJsons[0] = geoJSON;
    },

    handleAddOverlay: function(overlay) {
        this.leafletMap.addLayer(overlay);

        //this.leafletMap.invalidateSize();
    },

    handleRemoveOverlay: function(overlay) {
        this.leafletMap.removeLayer(overlay);

        //this.leafletMap.invalidateSize();
    },

    handleMapEvent: function(mapEvent) {

        app.debug("handleMapEvent :: ");
        app.debug(mapEvent);

        /*
        mapEvent = {
          panTo : [3,3],
          zoomTo : 6,
          overlay : null,
          layer : "SATELITE" //SATELLITE / ROAD
        };
        */

        if (mapEvent === undefined) {
            app.debug("Error:: mapEvent undefined!");
            return;
        }

        if (mapEvent.hasOwnProperty('panTo') && mapEvent.panTo != null) {

            app.debug("set view!");
            app.debug(mapEvent.panTo.zoom);

            this.leafletMap.flyTo(
                mapEvent.panTo.latlng,
                parseInt(mapEvent.panTo.zoom)
            );
        }

        //this.leafletMap.invalidateSize();
        if (this.sateliteLayer != null) {
            //this.sateliteLayer.redraw();
        }

    }

};
