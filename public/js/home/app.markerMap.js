app.markerMap = {

    leafletMap: null,
    modules: [],
    marker: null,
    sateliteLayer: null,
    geoJsons: [],
    options: {},

    moveTo: function(lat, lng) {
        this.leafletMap.panTo(new L.LatLng(lat, lng));
        this.leafletMap.invalidateSize();
    },

    moveMarkerTo: function(lat, lng) {
        this.marker.setLatLng(new L.LatLng(lat, lng));
        this.leafletMap.invalidateSize();
    },

    init: function(settings) {

        var self = this;

        jQuery.extend(self.options, settings);

        var center = [27.3803, 37.60071];

        if (this.options.center !== undefined) {
            center = this.options.center;
        }

        this.leafletMap = L.map('marker-map', {
            zoomControl: true,
            scrollWheelZoom: false
        }).setView(
            center,
            5
        );

        L.tileLayer('//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.leafletMap);

        this.marker = L.marker(center, {
            icon: L.icon({
                iconUrl: WEBROOT + 'images/marker-icon.png',
                iconSize: [25, 41], // size of the icon
                shadowSize: [41, 41], // size of the shadow
                iconAnchor: [12, 40], // point of the icon which will correspond to marker's location
                shadowAnchor: [12, 40], // the same for the shadow
                popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
            }),
            draggable: true
        }).addTo(this.leafletMap).on('dragend', function(e) {
            self.options.onMarkerChange(e.target.getLatLng());
            self.leafletMap.invalidateSize();
        });

        this.leafletMap.invalidateSize();

    }

};
