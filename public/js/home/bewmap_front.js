(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bewmap_hp = function() {
	this.initialize(img.bewmap_hp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,4799,2534);


(lib.yemenbtn = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AlxEhIhGkbIAQhGIAOgjQgGhTA6AnQBugSCKASQAlAzAfgzQAihPBMgoIEHgrIBsDaIhMAoQAPBVh7AYIhbAbQhpBWgwgKIhyA+Ig2AAIijBPg");
	this.shape.setTransform(44,30.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AlxEhIhGkbIAQhGIAOgjQgGhTA6AnQBugSCKASQAlAzAfgzQAihPBMgoIEHgrIBsDaIhMAoQAPBVh7AYIhbAbQhpBWgwgKIhyA+Ig2AAIijBPg");
	this.shape_1.setTransform(44,30.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AlxEhIhGkbIAQhGIAOgjQgGhTA6AnQBugSCKASQAlAzAfgzQAihPBMgoIEHgrIBsDaIhMAoQAPBVh7AYIhbAbQhpBWgwgKIhyA+Ig2AAIijBPg");
	this.shape_2.setTransform(44,30.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,88,61.2);


(lib.uaebtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AhsCCIhYhiQAgAQAxgXICQAAIAnhAIBnh/IAaBuIgcATIgNgcIgMAMIAKA+IAMAKIAAAXIgbAAIgeB9g");
	this.shape.setTransform(19.7,16.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AhsCCIhYhiQAgAQAxgXICQAAIAnhAIBnh/IAaBuIgcATIgNgcIgMAMIAKA+IAMAKIAAAXIgbAAIgeB9g");
	this.shape_1.setTransform(19.7,16.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AhsCCIhYhiQAgAQAxgXICQAAIAnhAIBnh/IAaBuIgcATIgNgcIgMAMIAKA+IAMAKIAAAXIgbAAIgeB9g");
	this.shape_2.setTransform(19.7,16.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39.3,33.5);


(lib.turkeybtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("ABGEeIgTghIAdgWQAKgsgtALIgoAXQg6gpguBBIgZAPIgeAAQg8AUgggpQhUg1g4AOIAAAlQhRAvggg9IgkgNIgVgPIgUAYIAAgPIANgZIg9AAQgIgYgbgTQAHgvgzgUQAhgYghgOIAAg/QAcgphRAXIAAgpIAogjICxgBIAAg5IAdgXICMAKQA+guBcgiQBmAGApgPQAWAkA8gCQATAnArgEQBoAnB8gTQBrAJAvgzIBGAAIAegOIBgBaQgRBKA7gRIAABXQA3BAgUA5IArA9IghAbIgKghIh6AAIgSASIgUgTIgtAXIhNAAIhVAjIhKAAIgkgVIhaAhIgegWIgUANIAUAtIgfAAIAAAbIgLAMgAsAipIAWgrIAZgUIgoghIBMgjQATAdAjgJQAeBBA4AFIgLALIgjAAIhxAug");
	this.shape.setTransform(76.9,30.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("ABGEeIgTghIAdgWQAKgsgtALIgoAXQg6gpguBBIgZAPIgeAAQg8AUgggpQhUg1g4AOIAAAlQhRAvggg9IgkgNIgVgPIgUAYIAAgPIANgZIg9AAQgIgYgbgTQAHgvgzgUQAhgYghgOIAAg/QAcgphRAXIAAgpIAogjICxgBIAAg5IAdgXICMAKQA+guBcgiQBmAGApgPQAWAkA8gCQATAnArgEQBoAnB8gTQBrAJAvgzIBGAAIAegOIBgBaQgRBKA7gRIAABXQA3BAgUA5IArA9IghAbIgKghIh6AAIgSASIgUgTIgtAXIhNAAIhVAjIhKAAIgkgVIhaAhIgegWIgUANIAUAtIgfAAIAAAbIgLAMgAsAipIAWgrIAZgUIgoghIBMgjQATAdAjgJQAeBBA4AFIgLALIgjAAIhxAug");
	this.shape_1.setTransform(76.9,30.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("ABGEeIgTghIAdgWQAKgsgtALIgoAXQg6gpguBBIgZAPIgeAAQg8AUgggpQhUg1g4AOIAAAlQhRAvggg9IgkgNIgVgPIgUAYIAAgPIANgZIg9AAQgIgYgbgTQAHgvgzgUQAhgYghgOIAAg/QAcgphRAXIAAgpIAogjICxgBIAAg5IAdgXICMAKQA+guBcgiQBmAGApgPQAWAkA8gCQATAnArgEQBoAnB8gTQBrAJAvgzIBGAAIAegOIBgBaQgRBKA7gRIAABXQA3BAgUA5IArA9IghAbIgKghIh6AAIgSASIgUgTIgtAXIhNAAIhVAjIhKAAIgkgVIhaAhIgegWIgUANIAUAtIgfAAIAAAbIgLAMgAsAipIAWgrIAZgUIgoghIBMgjQATAdAjgJQAeBBA4AFIgLALIgjAAIhxAug");
	this.shape_2.setTransform(76.9,30.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,153.8,60.2);


(lib.tunisiabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgjCiIhVhaIgegRIgPhWIBIhOIAAiUIASgdIAdgOIA6gmIAmATIAAAeQAhALAhgWIAKAKQhQA2A4A5QAvAzhKA/QhLAkBZA2IAgAAQgBApAuAAIAABDIhHAmIAAAUIgqAWQAnBkhXAZg");
	this.shape.setTransform(16.6,33.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgjCiIhVhaIgegRIgPhWIBIhOIAAiUIASgdIAdgOIA6gmIAmATIAAAeQAhALAhgWIAKAKQhQA2A4A5QAvAzhKA/QhLAkBZA2IAgAAQgBApAuAAIAABDIhHAmIAAAUIgqAWQAnBkhXAZg");
	this.shape_1.setTransform(16.6,33.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgjCiIhVhaIgegRIgPhWIBIhOIAAiUIASgdIAdgOIA6gmIAmATIAAAeQAhALAhgWIAKAKQhQA2A4A5QAvAzhKA/QhLAkBZA2IAgAAQgBApAuAAIAABDIhHAmIAAAUIgqAWQAnBkhXAZg");
	this.shape_2.setTransform(16.6,33.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,33.1,67.7);


(lib.syriabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AjGDsIgtgkIgPALIAAg+IAbgaIAAgZIAgAAQAOglgYgpIggAAIgPhTIAAgrIAQAPIALgMIAAgaIAgAAIgUgtIAUgOIAeAWIBaggIAkAUIBIAAIBVgjIBOAAIAtgWIAUASIg1A0IgoANIAXBGIgSCAIlEC+g");
	this.shape.setTransform(25.9,23.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AjGDsIgtgkIgPALIAAg+IAbgaIAAgZIAgAAQAOglgYgpIggAAIgPhTIAAgrIAQAPIALgMIAAgaIAgAAIgUgtIAUgOIAeAWIBaggIAkAUIBIAAIBVgjIBOAAIAtgWIAUASIg1A0IgoANIAXBGIgSCAIlEC+g");
	this.shape_1.setTransform(25.9,23.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AjGDsIgtgkIgPALIAAg+IAbgaIAAgZIAgAAQAOglgYgpIggAAIgPhTIAAgrIAQAPIALgMIAAgaIAgAAIgUgtIAUgOIAeAWIBaggIAkAUIBIAAIBVgjIBOAAIAtgWIAUASIg1A0IgoANIAXBGIgSCAIlEC+g");
	this.shape_2.setTransform(25.9,23.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,51.8,47.3);


(lib.spainbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AkeFUIgSg5IhgguIAAgkIAygtIgdgfIAegzIgmhCIAsAAIAAgrIAUgQIAAg/IA4gzIgngXIAQgRIgxAAIgRAVIhEAAIAQgcIg0AAIARgdIgRAAIAAgpIghgXIAkgUIAuAAIAdgjIAtARIFnAAQAuASAdgIIAgAAIAAAlIAkAAIBEAbIAxAAIAQgUIAoAUIAAAgIAngSIAAARIBmgSIAPAJIgQAVIAAAfIhCAkIgZAVIghAAIhHAgIAAAXIg0AwQhABAAuBOQg6AngNA+QhSALgZBGIgXgQIgaARIiPAAIgZATQguAMgUAogAHEAqIgXgZIAegRIAeAQIgSAag");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AkeFUIgSg5IhgguIAAgkIAygtIgdgfIAegzIgmhCIAsAAIAAgrIAUgQIAAg/IA4gzIgngXIAQgRIgxAAIgRAVIhEAAIAQgcIg0AAIARgdIgRAAIAAgpIghgXIAkgUIAuAAIAdgjIAtARIFnAAQAuASAdgIIAgAAIAAAlIAkAAIBEAbIAxAAIAQgUIAoAUIAAAgIAngSIAAARIBmgSIAPAJIgQAVIAAAfIhCAkIgZAVIghAAIhHAgIAAAXIg0AwQhABAAuBOQg6AngNA+QhSALgZBGIgXgQIgaARIiPAAIgZATQguAMgUAogAHEAqIgXgZIAegRIAeAQIgSAag");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AkeFUIgSg5IhgguIAAgkIAygtIgdgfIAegzIgmhCIAsAAIAAgrIAUgQIAAg/IA4gzIgngXIAQgRIgxAAIgRAVIhEAAIAQgcIg0AAIARgdIgRAAIAAgpIghgXIAkgUIAuAAIAdgjIAtARIFnAAQAuASAdgIIAgAAIAAAlIAkAAIBEAbIAxAAIAQgUIAoAUIAAAgIAngSIAAARIBmgSIAPAJIgQAVIAAAfIhCAkIgZAVIghAAIhHAgIAAAXIg0AwQhABAAuBOQg6AngNA+QhSALgZBGIgXgQIgaARIiPAAIgZATQguAMgUAogAHEAqIgXgZIAegRIAeAQIgSAag");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-49.4,-36,98.9,72.1);


(lib.sloveniabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgiAzIggANIghACIASgPIgIgUIgQAEIgMgUIAOgPIgWgIIARgaQBGAbAdgjIAqAAIA3gZQAUAUASARIgcAAIAAAMIgPAAIgjARIAFAgIgeAIIAHAOIgJAPg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgiAzIggANIghACIASgPIgIgUIgQAEIgMgUIAOgPIgWgIIARgaQBGAbAdgjIAqAAIA3gZQAUAUASARIgcAAIAAAMIgPAAIgjARIAFAgIgeAIIAHAOIgJAPg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgiAzIggANIghACIASgPIgIgUIgQAEIgMgUIAOgPIgWgIIARgaQBGAbAdgjIAqAAIA3gZQAUAUASARIgcAAIAAAMIgPAAIgjARIAFAgIgeAIIAHAOIgJAPg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.6,-6.8,25.3,13.7);


(lib.singaporebtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgmARIgYgUQA+gZAzAZIAMAUg");
	this.shape.setTransform(6.3,1.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgmARIgYgUQA+gZAzAZIAMAUg");
	this.shape_1.setTransform(6.3,1.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgmARIgYgUQA+gZAzAZIAMAUg");
	this.shape_2.setTransform(6.3,1.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,12.5,3.3);


(lib.saoudabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AmPGEQg6gDgihWIgWhGIAAhLIgTgsIhChVQhTgdAEhdIiWjTQgihGg1AMIAVhtIBTANIBDhEIA6gKIAAgiIAogOIhkhgICxg+QBVAIBQAjIE+DqICmAQIBBAAIAeA1IBJAAICMDDIAABFQA5AmAbA7IAnARQAQAlAbANIBXBhIDaAmIA3AyIgnC9IkLBoIkHAsQhNAngiBPQgfAzglgzQiKgShtASQg6gmAFBTIgNAiQiBiqh8i9g");
	this.shape.setTransform(91.5,74.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AmPGEQg6gDgihWIgWhGIAAhLIgTgsIhChVQhTgdAEhdIiWjTQgihGg1AMIAVhtIBTANIBDhEIA6gKIAAgiIAogOIhkhgICxg+QBVAIBQAjIE+DqICmAQIBBAAIAeA1IBJAAICMDDIAABFQA5AmAbA7IAnARQAQAlAbANIBXBhIDaAmIA3AyIgnC9IkLBoIkHAsQhNAngiBPQgfAzglgzQiKgShtASQg6gmAFBTIgNAiQiBiqh8i9g");
	this.shape_1.setTransform(91.5,74.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AmPGEQg6gDgihWIgWhGIAAhLIgTgsIhChVQhTgdAEhdIiWjTQgihGg1AMIAVhtIBTANIBDhEIA6gKIAAgiIAogOIhkhgICxg+QBVAIBQAjIE+DqICmAQIBBAAIAeA1IBJAAICMDDIAABFQA5AmAbA7IAnARQAQAlAbANIBXBhIDaAmIA3AyIgnC9IkLBoIkHAsQhNAngiBPQgfAzglgzQiKgShtASQg6gmAFBTIgNAiQiBiqh8i9g");
	this.shape_2.setTransform(91.5,74.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,183,149.4);


(lib.qatarbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgbA2IAAg8IAUhBIAhAgIAAAjQAHAmgWAmg");
	this.shape.setTransform(2.8,7.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgbA2IAAg8IAUhBIAhAgIAAAjQAHAmgWAmg");
	this.shape_1.setTransform(2.8,7.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgbA2IAAg8IAUhBIAhAgIAAAjQAHAmgWAmg");
	this.shape_2.setTransform(2.8,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,5.6,14.3);


(lib.palestinebtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgaAWQARgdALgeIAZAVIgVA2g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgaAWQARgdALgeIAZAVIgVA2g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgaAWQARgdALgeIAZAVIgVA2g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.6,-3.7,5.4,7.5);


(lib.pakistanbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AguJOQgFgxg2gUQgShIg+ALIiAAhIgjgeIgvAfIiQAAIAAhUIAug6IBKAAIAAg9IgwAAQAOhlhhgQIh0iAQDjBRDthRIgShbIAwguIA+AAIgPgUIBSgfIAVAcIAogkIgFhwQBngXhng3IBKAAQAvgWgbgnQA4gsglg+IgngiIAbghIBIgkIBqAAIArgWIBcAdQAwgDAFA6QAUAjAigOIA9AmIA3AAIARAYIg+AAIggAuIhaAcIhwghQgsAUARA4IAlAAIAABSIBCBEIAAAwIAaBTIg0A+IgmA3QgZBvhrBSIhXgeIhHBhIAaA8IA7AAIAABOIAmAAIAtBwQAPAihGgiIgqAhIhHAAIg5Aog");
	this.shape.setTransform(66.7,63.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AguJOQgFgxg2gUQgShIg+ALIiAAhIgjgeIgvAfIiQAAIAAhUIAug6IBKAAIAAg9IgwAAQAOhlhhgQIh0iAQDjBRDthRIgShbIAwguIA+AAIgPgUIBSgfIAVAcIAogkIgFhwQBngXhng3IBKAAQAvgWgbgnQA4gsglg+IgngiIAbghIBIgkIBqAAIArgWIBcAdQAwgDAFA6QAUAjAigOIA9AmIA3AAIARAYIg+AAIggAuIhaAcIhwghQgsAUARA4IAlAAIAABSIBCBEIAAAwIAaBTIg0A+IgmA3QgZBvhrBSIhXgeIhHBhIAaA8IA7AAIAABOIAmAAIAtBwQAPAihGgiIgqAhIhHAAIg5Aog");
	this.shape_1.setTransform(66.7,63.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AguJOQgFgxg2gUQgShIg+ALIiAAhIgjgeIgvAfIiQAAIAAhUIAug6IBKAAIAAg9IgwAAQAOhlhhgQIh0iAQDjBRDthRIgShbIAwguIA+AAIgPgUIBSgfIAVAcIAogkIgFhwQBngXhng3IBKAAQAvgWgbgnQA4gsglg+IgngiIAbghIBIgkIBqAAIArgWIBcAdQAwgDAFA6QAUAjAigOIA9AmIA3AAIARAYIg+AAIggAuIhaAcIhwghQgsAUARA4IAlAAIAABSIBCBEIAAAwIAaBTIg0A+IgmA3QgZBvhrBSIhXgeIhHBhIAaA8IA7AAIAABOIAmAAIAtBwQAPAihGgiIgqAhIhHAAIg5Aog");
	this.shape_2.setTransform(66.7,63.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,133.4,126.6);


(lib.omanbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("Ak/CzIELhnIAni8Ig3gzIAfh9IAbAAIAAgYIgMgKIgKg+IAMgNIANAdIAagUIAGAaQAxBiCOANIBQBbQA1AZhABTIggAmIgaAzQgsgjgWBrIARBLIg+AAIgeAmQgGA7hRgBIgcAiQAJBHhigUIhdAgg");
	this.shape.setTransform(32,39.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("Ak/CzIELhnIAni8Ig3gzIAfh9IAbAAIAAgYIgMgKIgKg+IAMgNIANAdIAagUIAGAaQAxBiCOANIBQBbQA1AZhABTIggAmIgaAzQgsgjgWBrIARBLIg+AAIgeAmQgGA7hRgBIgcAiQAJBHhigUIhdAgg");
	this.shape_1.setTransform(32,39.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("Ak/CzIELhnIAni8Ig3gzIAfh9IAbAAIAAgYIgMgKIgKg+IAMgNIANAdIAagUIAGAaQAxBiCOANIBQBbQA1AZhABTIggAmIgaAzQgsgjgWBrIARBLIg+AAIgeAmQgGA7hRgBIgcAiQAJBHhigUIhdAgg");
	this.shape_2.setTransform(32,39.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,64,79.5);


(lib.moroccobtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("An+GDIAMgIQB9ghB0hyQBNhZgwgjIAPhTIAxgtIAAgvQBNhaBCgHQBvhMAeh4IAqgbIBHBBIBoAAIAYgMQAeAVAnAAIAbAYIAACcIAWALIAAAXIAgAeIgUApQhegSgpASQgNAmhDAGIAMAcQAXAvg2gFIgqAbIgfAGQgkBMhcgKIgTAWIgeAAIg0AVIhiBFIAABag");
	this.shape.setTransform(51.1,38.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("An+GDIAMgIQB9ghB0hyQBNhZgwgjIAPhTIAxgtIAAgvQBNhaBCgHQBvhMAeh4IAqgbIBHBBIBoAAIAYgMQAeAVAnAAIAbAYIAACcIAWALIAAAXIAgAeIgUApQhegSgpASQgNAmhDAGIAMAcQAXAvg2gFIgqAbIgfAGQgkBMhcgKIgTAWIgeAAIg0AVIhiBFIAABag");
	this.shape_1.setTransform(51.1,38.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("An+GDIAMgIQB9ghB0hyQBNhZgwgjIAPhTIAxgtIAAgvQBNhaBCgHQBvhMAeh4IAqgbIBHBBIBoAAIAYgMQAeAVAnAAIAbAYIAACcIAWALIAAAXIAgAeIgUApQhegSgpASQgNAmhDAGIAMAcQAXAvg2gFIgqAbIgfAGQgkBMhcgKIgTAWIgeAAIg0AVIhiBFIAABag");
	this.shape_2.setTransform(51.1,38.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,102.2,77.5);


(lib.montenegrobtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AhEATIAFgpIANAAIAIggIAQAHIAAgYIA4AwIAnAGIAAANIgNAEIALATIgRAIIgUgRIgbA+Qgjgagkgbg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AhEATIAFgpIANAAIAIggIAQAHIAAgYIA4AwIAnAGIAAANIgNAEIALATIgRAIIgUgRIgbA+Qgjgagkgbg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AhEATIAFgpIANAAIAIggIAQAHIAAgYIA4AwIAnAGIAAANIgNAEIALATIgRAIIgUgRIgbA+Qgjgagkgbg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.9,-7.1,13.9,14.4);


(lib.monacobtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgTgBIAdgOIAKATIgZAMg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgTgBIAdgOIAKATIgZAMg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgTgBIAdgOIAKATIgZAMg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.9,-1.6,3.9,3.2);


(lib.maltabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgNAOIAAgbIAbAAIAAAbg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgNAOIAAgbIAbAAIAAAbg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgNAOIAAgbIAbAAIAAAbg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.4,-1.4,2.8,2.8);


(lib.malibtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AjKJ5QgYAdgTgdIAAAtQgzAXgdgpIgfAUIgfggIAAguIgiALIAagoIhIhuIgtAyIgvgVIgZAYIgagdIgyAcIALh4IgVgSIgYAOIAAgXIgXgNIAOgRIAAheIAyhOIA0ArIA1gcQAuAUATgbIFgAAIhQtcIDTAAIG5FEIAAAgIBIA+IAdAAIBRApIAABIIBegOQAIEohaBDIgZgNIgSAVIiTAAQgVAnhPgMQglAFgcgVQgqAwhHAVQgKAjghgTQgQAqgzATQAHAgggANQggghglAhIAAAvQgDA2goAHIglAOIgOB5IhHAog");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AjKJ5QgYAdgTgdIAAAtQgzAXgdgpIgfAUIgfggIAAguIgiALIAagoIhIhuIgtAyIgvgVIgZAYIgagdIgyAcIALh4IgVgSIgYAOIAAgXIgXgNIAOgRIAAheIAyhOIA0ArIA1gcQAuAUATgbIFgAAIhQtcIDTAAIG5FEIAAAgIBIA+IAdAAIBRApIAABIIBegOQAIEohaBDIgZgNIgSAVIiTAAQgVAnhPgMQglAFgcgVQgqAwhHAVQgKAjghgTQgQAqgzATQAHAgggANQggghglAhIAAAvQgDA2goAHIglAOIgOB5IhHAog");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.2)").s().p("AjKJ5QgYAdgTgdIAAAtQgzAXgdgpIgfAUIgfggIAAguIgiALIAagoIhIhuIgtAyIgvgVIgZAYIgagdIgyAcIALh4IgVgSIgYAOIAAgXIgXgNIAOgRIAAheIAyhOIA0ArIA1gcQAuAUATgbIFgAAIhQtcIDTAAIG5FEIAAAgIBIA+IAdAAIBRApIAABIIBegOQAIEohaBDIgZgNIgSAVIiTAAQgVAnhPgMQglAFgcgVQgqAwhHAVQgKAjghgTQgQAqgzATQAHAgggANQggghglAhIAAAvQgDA2goAHIglAOIgOB5IhHAog");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-71.8,-68.7,143.6,137.4);


(lib.libyabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AhVESIixBOQg5hNiFgIQgahxhvATIgihbIgagdQgggzAggQIAagYIgOhBIAPgzIgPguQAPhWgkhaQBYgagnhjIApgXIAAgUIBHgmIAAhDIBoAmQBQgVBEAtQBIADADBBIBWA8QB0AABPBQQB6AEADhbIgahJIA8g8QBagkBjAkIAoArIB6ARIANAeIgSAqIAUAxIgYA0IAdA+IAZOMIhWAAIABAug");
	this.shape.setTransform(66.6,65);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AhVESIixBOQg5hNiFgIQgahxhvATIgihbIgagdQgggzAggQIAagYIgOhBIAPgzIgPguQAPhWgkhaQBYgagnhjIApgXIAAgUIBHgmIAAhDIBoAmQBQgVBEAtQBIADADBBIBWA8QB0AABPBQQB6AEADhbIgahJIA8g8QBagkBjAkIAoArIB6ARIANAeIgSAqIAUAxIgYA0IAdA+IAZOMIhWAAIABAug");
	this.shape_1.setTransform(66.6,65);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AhVESIixBOQg5hNiFgIQgahxhvATIgihbIgagdQgggzAggQIAagYIgOhBIAPgzIgPguQAPhWgkhaQBYgagnhjIApgXIAAgUIBHgmIAAhDIBoAmQBQgVBEAtQBIADADBBIBWA8QB0AABPBQQB6AEADhbIgahJIA8g8QBagkBjAkIAoArIB6ARIANAeIgSAqIAUAxIgYA0IAdA+IAZOMIhWAAIABAug");
	this.shape_2.setTransform(66.6,65);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,133.3,130);


(lib.lebanonbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("Ag0BLIAjhrIAWgqIAhAAQAYApgOAkIghAAIAAAZIgsAvg");
	this.shape.setTransform(5.3,7.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("Ag0BLIAjhrIAWgqIAhAAQAYApgOAkIghAAIAAAZIgsAvg");
	this.shape_1.setTransform(5.3,7.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("Ag0BLIAjhrIAWgqIAhAAQAYApgOAkIghAAIAAAZIgsAvg");
	this.shape_2.setTransform(5.3,7.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10.6,15);


(lib.koweitbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AALBKIgdg0IhBAAIArhfIA7AAIARAeIAwB1g");
	this.shape.setTransform(8.4,7.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AALBKIgdg0IhBAAIArhfIA7AAIARAeIAwB1g");
	this.shape_1.setTransform(8.4,7.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AALBKIgdg0IhBAAIArhfIA7AAIARAeIAwB1g");
	this.shape_2.setTransform(8.4,7.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,16.8,14.9);


(lib.jordanbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AiiC5IAalBIAOgLIAtAlIAtAAICThWIAZAyIAAAeIAWAAIAAAfIiwA+IBjBfIgoAOIAAAiIg4AKIhEBDg");
	this.shape.setTransform(16.3,19.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AiiC5IAalBIAOgLIAtAlIAtAAICThWIAZAyIAAAeIAWAAIAAAfIiwA+IBjBfIgoAOIAAAiIg4AKIhEBDg");
	this.shape_1.setTransform(16.3,19.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AiiC5IAalBIAOgLIAtAlIAtAAICThWIAZAyIAAAeIAWAAIAAAfIiwA+IBjBfIgoAOIAAAiIg4AKIhEBDg");
	this.shape_2.setTransform(16.3,19.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32.5,39.4);


(lib.italybtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("ACqHpQgTgfgngMQgfgKgfgZIgXgEQgsgPAigYIATAAIAVgMQAdARBPAAQALgNAdAEIAggPQgeAtAEA0QAQAOgWAdgAEEFpIAUgbIAAgVIAXgeIgvhwIgdAAIgagVIgOgiIgiAAIgwgzIggAAQh8hChihmIgrhhIiMgcIg9ArIAAgcQhHAMAVhBQgzgcAzgSQAZgXglgYIANgWIBMgCIAOgRIAAgKIARgSIAMAHIAAATIAoAdIAZg/IAXAWIAcAAIAIgeIAYAGIAJgZIAiAHIATgRIBJgPIAtAuIBHAGIgSAaIAWAJIgOAQIAMAUIgGABIgUAAIg1ARQg4ALAmAzIgPAKQgIBIByA2QBQC1CAgaIgUApQBUAeBSAwQAkAVAGAcQAHASgbgEIgagoQgWADgUgWIgWAAIgmAgIAKBBIAfAUQAGAigagBQgfAOAMAfIgwAvIgFABQgHAAgCgLgAk7EUIgTgpIASgkIgTgQIAQgdIgagyQAsAFAjgpQA0A6gdAfIAGA+QACBEg4gpIAAAeg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("ACqHpQgTgfgngMQgfgKgfgZIgXgEQgsgPAigYIATAAIAVgMQAdARBPAAQALgNAdAEIAggPQgeAtAEA0QAQAOgWAdgAEEFpIAUgbIAAgVIAXgeIgvhwIgdAAIgagVIgOgiIgiAAIgwgzIggAAQh8hChihmIgrhhIiMgcIg9ArIAAgcQhHAMAVhBQgzgcAzgSQAZgXglgYIANgWIBMgCIAOgRIAAgKIARgSIAMAHIAAATIAoAdIAZg/IAXAWIAcAAIAIgeIAYAGIAJgZIAiAHIATgRIBJgPIAtAuIBHAGIgSAaIAWAJIgOAQIAMAUIgGABIgUAAIg1ARQg4ALAmAzIgPAKQgIBIByA2QBQC1CAgaIgUApQBUAeBSAwQAkAVAGAcQAHASgbgEIgagoQgWADgUgWIgWAAIgmAgIAKBBIAfAUQAGAigagBQgfAOAMAfIgwAvIgFABQgHAAgCgLgAk7EUIgTgpIASgkIgTgQIAQgdIgagyQAsAFAjgpQA0A6gdAfIAGA+QACBEg4gpIAAAeg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("ACqHpQgTgfgngMQgfgKgfgZIgXgEQgsgPAigYIATAAIAVgMQAdARBPAAQALgNAdAEIAggPQgeAtAEA0QAQAOgWAdgAEEFpIAUgbIAAgVIAXgeIgvhwIgdAAIgagVIgOgiIgiAAIgwgzIggAAQh8hChihmIgrhhIiMgcIg9ArIAAgcQhHAMAVhBQgzgcAzgSQAZgXglgYIANgWIBMgCIAOgRIAAgKIARgSIAMAHIAAATIAoAdIAZg/IAXAWIAcAAIAIgeIAYAGIAJgZIAiAHIATgRIBJgPIAtAuIBHAGIgSAaIAWAJIgOAQIAMAUIgGABIgUAAIg1ARQg4ALAmAzIgPAKQgIBIByA2QBQC1CAgaIgUApQBUAeBSAwQAkAVAGAcQAHASgbgEIgagoQgWADgUgWIgWAAIgmAgIAKBBIAfAUQAGAigagBQgfAOAMAfIgwAvIgFABQgHAAgCgLgAk7EUIgTgpIASgkIgTgQIAQgdIgagyQAsAFAjgpQA0A6gdAfIAGA+QACBEg4gpIAAAeg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-46.9,-48.9,93.9,97.8);


(lib.israelbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgyANIAgAQIAUg2IgXgVQAVg1AJg3IAYAAIASgVIAAA/IgTDyIgaAug");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgyANIAgAQIAUg2IgXgVQAVg1AJg3IAYAAIASgVIAAA/IgTDyIgaAug");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgyANIAgAQIAUg2IgXgVQAVg1AJg3IAYAAIASgVIAAA/IgTDyIgaAug");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.1,-17.6,10.3,35.2);


(lib.iraqbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("ABpF7Ik8jqQhRgkhUgHIAAgfIgXAAIAAgeIgZgxICyhoIARiCIgWhFIAogOIBGhFIB6AAIAKAgIAggbIBfCHIA8AAIgeAnQAqAlgqgFIgZA0QARAdgRAeIAgAeQAuAZgGAsQAZgKAwAvIAjAKIAoA4IAABIIAjALIAAAtIAlAQIAAAbIAKASIgHAgIgQAAIgJgWIgSgeIg8AAIgqBgg");
	this.shape.setTransform(42.5,39.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("ABpF7Ik8jqQhRgkhUgHIAAgfIgXAAIAAgeIgZgxICyhoIARiCIgWhFIAogOIBGhFIB6AAIAKAgIAggbIBfCHIA8AAIgeAnQAqAlgqgFIgZA0QARAdgRAeIAgAeQAuAZgGAsQAZgKAwAvIAjAKIAoA4IAABIIAjALIAAAtIAlAQIAAAbIAKASIgHAgIgQAAIgJgWIgSgeIg8AAIgqBgg");
	this.shape_1.setTransform(42.5,39.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("ABpF7Ik8jqQhRgkhUgHIAAgfIgXAAIAAgeIgZgxICyhoIARiCIgWhFIAogOIBGhFIB6AAIAKAgIAggbIBfCHIA8AAIgeAnQAqAlgqgFIgZA0QARAdgRAeIAgAeQAuAZgGAsQAZgKAwAvIAjAKIAoA4IAABIIAjALIAAAtIAlAQIAAAbIAKASIgHAgIgQAAIgJgWIgSgeIg8AAIgqBgg");
	this.shape_2.setTransform(42.5,39.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85.1,79);


(lib.iranbtn = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AG4KeQg+gWgThBQgfh3irB3Ikdh9IiSjiQhngKgWAtIgKgSIAAgbIglgQIAAgtIgjgLIAAhIIgpg4IgigKQgwgugZALQAGgtgugaIghgdQASgegSgeIAZgzQAqAEgqglIAfgnIg8AAIiKjDQAUg5g3hBIAAhYIB6B2IBQAAIBnhVIAoAkIgPA6IBEAhQAGBeBXACICuBaIB0gkIA6AQIAVgVIgWgnQA4gYA6gxQBFgUBNAKQAGAhBVARQA+ADA4A5QANgIBDAsQA2gQAGBEIASCCIgqA3QACBJAqgKQgYA5A0CEQCFAQg/BLIg0BCIB0CAQBgAQgNBlIAvAAIAAA8IhKAAIguA7IAABUg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AG4KeQg+gWgThBQgfh3irB3Ikdh9IiSjiQhngKgWAtIgKgSIAAgbIglgQIAAgtIgjgLIAAhIIgpg4IgigKQgwgugZALQAGgtgugaIghgdQASgegSgeIAZgzQAqAEgqglIAfgnIg8AAIiKjDQAUg5g3hBIAAhYIB6B2IBQAAIBnhVIAoAkIgPA6IBEAhQAGBeBXACICuBaIB0gkIA6AQIAVgVIgWgnQA4gYA6gxQBFgUBNAKQAGAhBVARQA+ADA4A5QANgIBDAsQA2gQAGBEIASCCIgqA3QACBJAqgKQgYA5A0CEQCFAQg/BLIg0BCIB0CAQBgAQgNBlIAvAAIAAA8IhKAAIguA7IAABUg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,153,51,0.008)").s().p("AG4KeQg+gWgThBQgfh3irB3Ikdh9IiSjiQhngKgWAtIgKgSIAAgbIglgQIAAgtIgjgLIAAhIIgpg4IgigKQgwgugZALQAGgtgugaIghgdQASgegSgeIAZgzQAqAEgqglIAfgnIg8AAIiKjDQAUg5g3hBIAAhYIB6B2IBQAAIBnhVIAoAkIgPA6IBEAhQAGBeBXACICuBaIB0gkIA6AQIAVgVIgWgnQA4gYA6gxQBFgUBNAKQAGAhBVARQA+ADA4A5QANgIBDAsQA2gQAGBEIASCCIgqA3QACBJAqgKQgYA5A0CEQCFAQg/BLIg0BCIB0CAQBgAQgNBlIAvAAIAAA8IhKAAIguA7IAABUg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88,-71.1,176,142.4);


(lib.greecebtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AAfEnIAAgVIBaANIAagGQAeAQAdgEIAAAOIAwgFIgFAPIhnAHQgsgkhHAHgAgmCOIgNAAIgPAUIgYghIgUAAIgFAOIgIgNIAAgtIgrgpIASgRIAggMIBYAmIAiAfIgLAIIgagRQgQgFgBAUIAeBKgAgSAnIATgRQhwgrgdAZIgUgRIgNAKIgdgrIAigJIgMgRIgfAFIgvg2IANgLIAAgXIAyg1IgHgdIAsAAIAygaIApAAIAVgRIA5AAIA7gaIAaAYIAvAHIA3AAIAQgKIgNgeIASgIIAoAhIgZAUIgWArIhdgZIgXAKIgbgJIgWAWIgYgGIgJAQIAMASIgHAPQgZALgbgVIAAgXQgqAAACA5IAoA4IgKAuIgZAJIAAAJQA9AWA9ApIAAAhQgggZgoAKgAAfgJQgUgXgfgUIAPgGIAoAfIAUAAIALAXg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AAfEnIAAgVIBaANIAagGQAeAQAdgEIAAAOIAwgFIgFAPIhnAHQgsgkhHAHgAgmCOIgNAAIgPAUIgYghIgUAAIgFAOIgIgNIAAgtIgrgpIASgRIAggMIBYAmIAiAfIgLAIIgagRQgQgFgBAUIAeBKgAgSAnIATgRQhwgrgdAZIgUgRIgNAKIgdgrIAigJIgMgRIgfAFIgvg2IANgLIAAgXIAyg1IgHgdIAsAAIAygaIApAAIAVgRIA5AAIA7gaIAaAYIAvAHIA3AAIAQgKIgNgeIASgIIAoAhIgZAUIgWArIhdgZIgXAKIgbgJIgWAWIgYgGIgJAQIAMASIgHAPQgZALgbgVIAAgXQgqAAACA5IAoA4IgKAuIgZAJIAAAJQA9AWA9ApIAAAhQgggZgoAKgAAfgJQgUgXgfgUIAPgGIAoAfIAUAAIALAXg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AAfEnIAAgVIBaANIAagGQAeAQAdgEIAAAOIAwgFIgFAPIhnAHQgsgkhHAHgAgmCOIgNAAIgPAUIgYghIgUAAIgFAOIgIgNIAAgtIgrgpIASgRIAggMIBYAmIAiAfIgLAIIgagRQgQgFgBAUIAeBKgAgSAnIATgRQhwgrgdAZIgUgRIgNAKIgdgrIAigJIgMgRIgfAFIgvg2IANgLIAAgXIAyg1IgHgdIAsAAIAygaIApAAIAVgRIA5AAIA7gaIAaAYIAvAHIA3AAIAQgKIgNgeIASgIIAoAhIgZAUIgWArIhdgZIgXAKIgbgJIgWAWIgYgGIgJAQIAMASIgHAPQgZALgbgVIAAgXQgqAAACA5IAoA4IgKAuIgZAJIAAAJQA9AWA9ApIAAAhQgggZgoAKgAAfgJQgUgXgfgUIAPgGIAoAfIAUAAIALAXg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.9,-32.4,51.8,64.9);


(lib.francebtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AHxGzIgQg5QgGgeA1gSIAAgYIASAAIAABdIgeAwgAhCFbIhQgiIgQAUIgxAAIhDgbIglAAIAAglIgfAAQAzgMgFhbIAQgUIgKgOIANg8IANAKIAJAaIATARIgYhGIgMgVIAAgtQglgGgjgnIAAgvIgZAAIAAgXIgVgPIggAAIg6gQIgiAAIgOgRIAaAAIAAgHIgQgJIASgIIgpgGIAbgSIA0AAIAqgTIA3AZIAXAAIAJAJIAMgHIAgAAIAIgJIgJgLIAAgxIgVgSIAAgQIAmAAIAGAcIBQALIA7gXIghgIQAfgbAxgDIAjgZQgQhPBZgDIAGAdIAmAAIAOAVIAZAFIAAAKIAjAFIAAAfIAcAAIAPgWIAKAAIAAAlIAMAAIAhAXIBFAGIAdAcIAPgFIAKALIAagHIAXAQIAjAAQg5A6ABBRIgqAAIAAAbIhJBBIAAAjIAogaIAhAAIgTAeIAYAAIgTAiQAlAXgZAXQgzASAzAcQgVBCBHgMIAAAbIgOAHIgKgUIgdAOIAMASQgyBUhlg5QiwgtgIB0IAOAJIhkASg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AHxGzIgQg5QgGgeA1gSIAAgYIASAAIAABdIgeAwgAhCFbIhQgiIgQAUIgxAAIhDgbIglAAIAAglIgfAAQAzgMgFhbIAQgUIgKgOIANg8IANAKIAJAaIATARIgYhGIgMgVIAAgtQglgGgjgnIAAgvIgZAAIAAgXIgVgPIggAAIg6gQIgiAAIgOgRIAaAAIAAgHIgQgJIASgIIgpgGIAbgSIA0AAIAqgTIA3AZIAXAAIAJAJIAMgHIAgAAIAIgJIgJgLIAAgxIgVgSIAAgQIAmAAIAGAcIBQALIA7gXIghgIQAfgbAxgDIAjgZQgQhPBZgDIAGAdIAmAAIAOAVIAZAFIAAAKIAjAFIAAAfIAcAAIAPgWIAKAAIAAAlIAMAAIAhAXIBFAGIAdAcIAPgFIAKALIAagHIAXAQIAjAAQg5A6ABBRIgqAAIAAAbIhJBBIAAAjIAogaIAhAAIgTAeIAYAAIgTAiQAlAXgZAXQgzASAzAcQgVBCBHgMIAAAbIgOAHIgKgUIgdAOIAMASQgyBUhlg5QiwgtgIB0IAOAJIhkASg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AHxGzIgQg5QgGgeA1gSIAAgYIASAAIAABdIgeAwgAhCFbIhQgiIgQAUIgxAAIhDgbIglAAIAAglIgfAAQAzgMgFhbIAQgUIgKgOIANg8IANAKIAJAaIATARIgYhGIgMgVIAAgtQglgGgjgnIAAgvIgZAAIAAgXIgVgPIggAAIg6gQIgiAAIgOgRIAaAAIAAgHIgQgJIASgIIgpgGIAbgSIA0AAIAqgTIA3AZIAXAAIAJAJIAMgHIAgAAIAIgJIgJgLIAAgxIgVgSIAAgQIAmAAIAGAcIBQALIA7gXIghgIQAfgbAxgDIAjgZQgQhPBZgDIAGAdIAmAAIAOAVIAZAFIAAAKIAjAFIAAAfIAcAAIAPgWIAKAAIAAAlIAMAAIAhAXIBFAGIAdAcIAPgFIAKALIAagHIAXAQIAjAAQg5A6ABBRIgqAAIAAAbIhJBBIAAAjIAogaIAhAAIgTAeIAYAAIgTAiQAlAXgZAXQgzASAzAcQgVBCBHgMIAAAbIgOAHIgKgUIgdAOIAMASQgyBUhlg5QiwgtgIB0IAOAJIhkASg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54.6,-44.6,109.2,89.4);


(lib.egyptbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AEJHBIq0AAIgUrNIgdg+IAYgzIgUgyIASgqIAPATIAlAAQBzgBB0ArQBOAkA8g8IBbgkIA/AMIAAAmQCfAVAogrIA6CkIgqCnQgqgagmgpIAAgkIhGhlIgWAlIErITIAOBkIgwAUIgcAyIgqAKIgPArg");
	this.shape.setTransform(47.7,47.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AEJHBIq0AAIgUrNIgdg+IAYgzIgUgyIASgqIAPATIAlAAQBzgBB0ArQBOAkA8g8IBbgkIA/AMIAAAmQCfAVAogrIA6CkIgqCnQgqgagmgpIAAgkIhGhlIgWAlIErITIAOBkIgwAUIgcAyIgqAKIgPArg");
	this.shape_1.setTransform(47.7,47.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AEJHBIq0AAIgUrNIgdg+IAYgzIgUgyIASgqIAPATIAlAAQBzgBB0ArQBOAkA8g8IBbgkIA/AMIAAAmQCfAVAogrIA6CkIgqCnQgqgagmgpIAAgkIhGhlIgWAlIErITIAOBkIgwAUIgcAyIgqAKIgPArg");
	this.shape_2.setTransform(47.7,47.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,95.4,94.8);


(lib.cyprus = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AhTALQAogFAXgYQAwACA4gbQgNAdgbAFIAMASIgbAAIgSAUIgXAAIgWAPg");
	this.shape.setTransform(8.4,4.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AhTALQAogFAXgYQAwACA4gbQgNAdgbAFIAMASIgbAAIgSAUIgXAAIgWAPg");
	this.shape_1.setTransform(8.4,4.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AhTALQAogFAXgYQAwACA4gbQgNAdgbAFIAMASIgbAAIgSAUIgXAAIgWAPg");
	this.shape_2.setTransform(8.4,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,16.8,8.8);


(lib.croatiabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("ABjCeIhQgtIgtAAQglgYgdghIAcgIQgtgTgIg2QgdgXgfgCIgYAmIgYgqIAKgKIAhgCIAggMIA4ARIAJgPIgHgPIAegIIgFggIAjgRIAPAAIAAgNIAbAAQB1BqAwg2IASAhIAQAAIgMAWIAdAKIgWAPIAAAPIgRAKQgIgihFAOIhsgRQgTAcglgYIAAAqIAkASIAAAiIBzBXIAGASIgEgEg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("ABjCeIhQgtIgtAAQglgYgdghIAcgIQgtgTgIg2QgdgXgfgCIgYAmIgYgqIAKgKIAhgCIAggMIA4ARIAJgPIgHgPIAegIIgFggIAjgRIAPAAIAAgNIAbAAQB1BqAwg2IASAhIAQAAIgMAWIAdAKIgWAPIAAAPIgRAKQgIgihFAOIhsgRQgTAcglgYIAAAqIAkASIAAAiIBzBXIAGASIgEgEg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("ABjCeIhQgtIgtAAQglgYgdghIAcgIQgtgTgIg2QgdgXgfgCIgYAmIgYgqIAKgKIAhgCIAggMIA4ARIAJgPIgHgPIAegIIgFggIAjgRIAPAAIAAgNIAbAAQB1BqAwg2IASAhIAQAAIgMAWIAdAKIgWAPIAAAPIgRAKQgIgihFAOIhsgRQgTAcglgYIAAAqIAkASIAAAiIBzBXIAGASIgEgEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.6,-16.2,45.3,32.4);


(lib.cambodiabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgZDFIhDgZQgGhAgjA2Qgkg7gHg8IgOAAIAAgfQgqgwACgxQAcggAYg1QBQgQBRAQIApAZIAbAAIAVAOIAgAAIghgnIAvgPIAhATIA4gfIgPAvIAYAXIgMAQIARAVQATA9gTA8IgRAAIhHAlIAAAXIgigRIgPASQgBAgApAXIAAAdIgZAAIgRgWIg9AKQAEAighAAQgHAAgKgBg");
	this.shape.setTransform(23.1,19.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgZDFIhDgZQgGhAgjA2Qgkg7gHg8IgOAAIAAgfQgqgwACgxQAcggAYg1QBQgQBRAQIApAZIAbAAIAVAOIAgAAIghgnIAvgPIAhATIA4gfIgPAvIAYAXIgMAQIARAVQATA9gTA8IgRAAIhHAlIAAAXIgigRIgPASQgBAgApAXIAAAdIgZAAIgRgWIg9AKQAEAighAAQgHAAgKgBg");
	this.shape_1.setTransform(23.1,19.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgZDFIhDgZQgGhAgjA2Qgkg7gHg8IgOAAIAAgfQgqgwACgxQAcggAYg1QBQgQBRAQIApAZIAbAAIAVAOIAgAAIghgnIAvgPIAhATIA4gfIgPAvIAYAXIgMAQIARAVQATA9gTA8IgRAAIhHAlIAAAXIgigRIgPASQgBAgApAXIAAAdIgZAAIgRgWIg9AKQAEAighAAQgHAAgKgBg");
	this.shape_2.setTransform(23.1,19.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.1,39.7);


(lib.bosniabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AAFBPIgFgSIh0hXIAAghIgkgSIAAgrQAlAYAUgdIBrASQBFgOAIAiIARgKIAWAHQgfA5A4ATIgbAFIAUAkIgoAIIAAAYIgQgHIgHAgIgOAAIgFAqIg7gvg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AAFBPIgFgSIh0hXIAAghIgkgSIAAgrQAlAYAUgdIBrASQBFgOAIAiIARgKIAWAHQgfA5A4ATIgbAFIAUAkIgoAIIAAAYIgQgHIgHAgIgOAAIgFAqIg7gvg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AAFBPIgFgSIh0hXIAAghIgkgSIAAgrQAlAYAUgdIBrASQBFgOAIAiIARgKIAWAHQgfA5A4ATIgbAFIAUAkIgoAIIAAAYIgQgHIgHAgIgOAAIgFAqIg7gvg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.3,-12.5,30.7,25.2);


(lib.bahreinbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgUAKIAAgwIApAeIgHAvg");
	this.shape.setTransform(2.1,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgUAKIAAgwIApAeIgHAvg");
	this.shape_1.setTransform(2.1,3.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgUAKIAAgwIApAeIgHAvg");
	this.shape_2.setTransform(2.1,3.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,4.1,7.8);


(lib.algeriabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AB6MTIhQgpIgeAAIhHg+IAAggIsjpNIgKgiIAAhaIBihEIA2gWIAdAAIATgWQBdALAjhMIAfgHIAqgbQA3AGgXgwIgNgcQBEgFANgoQApgSBeASIATgpIgggdIAAgYIgVgKIAAidIgbgXIBlg2QApgRAuAEIAog5ICsgTIAWgVICigKIAdAkIARgMIAMgPIAvAAIANgXQAzAYAhgYQAsAVA1AAIgSAdIAACVIhIBNIAPBYIAeAQIBWBaIAnCxQAlBbgQBWIAQAtIgQAzIAPBBIgbAYQgfAQAfAzIAbAdIAhBbQBvgSAbBxIlwDoIisCjIjTAhg");
	this.shape.setTransform(87.3,86);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AB6MTIhQgpIgeAAIhHg+IAAggIsjpNIgKgiIAAhaIBihEIA2gWIAdAAIATgWQBdALAjhMIAfgHIAqgbQA3AGgXgwIgNgcQBEgFANgoQApgSBeASIATgpIgggdIAAgYIgVgKIAAidIgbgXIBlg2QApgRAuAEIAog5ICsgTIAWgVICigKIAdAkIARgMIAMgPIAvAAIANgXQAzAYAhgYQAsAVA1AAIgSAdIAACVIhIBNIAPBYIAeAQIBWBaIAnCxQAlBbgQBWIAQAtIgQAzIAPBBIgbAYQgfAQAfAzIAbAdIAhBbQBvgSAbBxIlwDoIisCjIjTAhg");
	this.shape_1.setTransform(87.3,86);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AB6MTIhQgpIgeAAIhHg+IAAggIsjpNIgKgiIAAhaIBihEIA2gWIAdAAIATgWQBdALAjhMIAfgHIAqgbQA3AGgXgwIgNgcQBEgFANgoQApgSBeASIATgpIgggdIAAgYIgVgKIAAidIgbgXIBlg2QApgRAuAEIAog5ICsgTIAWgVICigKIAdAkIARgMIAMgPIAvAAIANgXQAzAYAhgYQAsAVA1AAIgSAdIAACVIhIBNIAPBYIAeAQIBWBaIAnCxQAlBbgQBWIAQAtIgQAzIAPBBIgbAYQgfAQAfAzIAbAdIAhBbQBvgSAbBxIlwDoIisCjIjTAhg");
	this.shape_2.setTransform(87.3,86);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,174.5,172);


(lib.albaniabtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgECAIgngyQgQhDAQhHIgdgTIAbg+IAUARIARgIIALAUIAXAJIAMAdIgGAFQgXBEA4AaIAIAeIgyA1IAAAXIgNALg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AgECAIgngyQgQhDAQhHIgdgTIAbg+IAUARIARgIIALAUIAXAJIAMAdIgGAFQgXBEA4AaIAIAeIgyA1IAAAXIgNALg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AgECAIgngyQgQhDAQhHIgdgTIAbg+IAUARIARgIIALAUIAXAJIAMAdIgGAFQgXBEA4AaIAIAeIgyA1IAAAXIgNALg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.2,-14.2,14.5,28.4);


(lib.afghanbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AnHF3IA0hCQBAhLiFgQQg1iFAYg5QgqAKgChIIAqg3IgNhkQAmAsArgHQBYAbgOhAQBugEADhmIARgxICkAAIAPAQIAoAAIAfAkIAQgaIA1gUIAdAaIAXgYIgVgWIA5AAIAAgmIBUgSIAAAbIAgAAQgZBMBDAgQA8gRAfguQAogMAMAcIBFgKIAAAhIgrAVIhqAAIhIAkIgbAiIAnAiQAlA+g4AsQAbAngvAVIhKAAQBnA3hnAWIAFBxIgpAkIgVgcIhSAfIAPAUIg+AAIgvAuIARBcQh2Aoh0AAQh0AAhxgog");
	this.shape.setTransform(54.6,41.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,51,51,0.4)").s().p("AnHF3IA0hCQBAhLiFgQQg1iFAYg5QgqAKgChIIAqg3IgNhkQAmAsArgHQBYAbgOhAQBugEADhmIARgxICkAAIAPAQIAoAAIAfAkIAQgaIA1gUIAdAaIAXgYIgVgWIA5AAIAAgmIBUgSIAAAbIAgAAQgZBMBDAgQA8gRAfguQAogMAMAcIBFgKIAAAhIgrAVIhqAAIhIAkIgbAiIAnAiQAlA+g4AsQAbAngvAVIhKAAQBnA3hnAWIAFBxIgpAkIgVgcIhSAfIAPAUIg+AAIgvAuIARBcQh2Aoh0AAQh0AAhxgog");
	this.shape_1.setTransform(54.6,41.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.008)").s().p("AnHF3IA0hCQBAhLiFgQQg1iFAYg5QgqAKgChIIAqg3IgNhkQAmAsArgHQBYAbgOhAQBugEADhmIARgxICkAAIAPAQIAoAAIAfAkIAQgaIA1gUIAdAaIAXgYIgVgWIA5AAIAAgmIBUgSIAAAbIAgAAQgZBMBDAgQA8gRAfguQAogMAMAcIBFgKIAAAhIgrAVIhqAAIhIAkIgbAiIAnAiQAlA+g4AsQAbAngvAVIhKAAQBnA3hnAWIAFBxIgpAkIgVgcIhSAfIAPAUIg+AAIgvAuIARBcQh2Aoh0AAQh0AAhxgog");
	this.shape_2.setTransform(54.6,41.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,109.2,83.1);


// stage content:
(lib.bewmap_1000 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	
        var url_country = window.location.protocol+'//'+window.location.host+ window.location.pathname.substr(0,3)+ '/country/';


		this.iran.addEventListener("click", fl_ClickToGoToWebPage);
		function fl_ClickToGoToWebPage() {
			window.open(url_country+"18", "_blank");
		}

		this.singapore.addEventListener("click", fl_GoToSingaporePage);
		function fl_GoToSingaporePage() {
			window.open(url_country+"55", "_blank");
		}

		this.cambodia.addEventListener("click", fl_GoToCambodiaPage);
		function fl_GoToCambodiaPage() {
			window.open(url_country+"54", "_blank");
		}

		this.pakistan.addEventListener("click", fl_GoToPakistanPage);
		function fl_GoToPakistanPage() {
			window.open(url_country+"57", "_blank");
		}

		this.afghan.addEventListener("click", fl_GoToAfghanPage);
		function fl_GoToAfghanPage() {
			window.open(url_country+"21", "_blank");
		}

		this.iraq.addEventListener("click", fl_GoToIraqPage);
		function fl_GoToIraqPage() {
			window.open(url_country+"19", "_blank");
		}

		this.syria.addEventListener("click", fl_GoToSyriaPage);
		function fl_GoToSyriaPage() {
			window.open(url_country+"42", "_blank");
		}

		this.jordan.addEventListener("click", fl_GoToJordanPage);
		function fl_GoToJordanPage() {
			window.open(url_country+"44", "_blank");
		}

		this.saouda.addEventListener("click", fl_GoToSaoudiPage);
		function fl_GoToSaoudiPage() {
			window.open(url_country+"46", "_blank");
		}

		this.koweit.addEventListener("click", fl_GoToKoweitPage);
		function fl_GoToKoweitPage() {
			window.open(url_country+"49", "_blank");
		}

		this.qatar.addEventListener("click", fl_GoToQatarPage);
		function fl_GoToQatarPage() {
			window.open(url_country+"52", "_blank");
		}

		this.bahrein.addEventListener("click", fl_GoToBahreinPage);
		function fl_GoToBahreinPage() {
			window.open(url_country+"47", "_blank");
		}

		this.uae.addEventListener("click", fl_GoToUAEPage);
		function fl_GoToUAEPage() {
			window.open(url_country+"48", "_blank");
		}

		this.oman.addEventListener("click", fl_GoToOmanPage);
		function fl_GoToOmanPage() {
			window.open(url_country+"50", "_blank");
		}

		this.yemen.addEventListener("click", fl_GoToYemenPage);
		function fl_GoToYemenPage() {
			window.open(url_country+"53", "_blank");
		}

		this.israel.addEventListener("click", fl_GoToIsraelPage);
		function fl_GoToIsraelPage() {
			window.open(url_country+"39", "_blank");
		}

		this.palestine.addEventListener("click", fl_GoToPalestinePage);
		function fl_GoToPalestinePage() {
			window.open(url_country+"40", "_blank");
		}

		this.lebanon.addEventListener("click", fl_GoToLebanonPage);
		function fl_GoToLebanonPage() {
			window.open(url_country+"41", "_blank");
		}

		this.turkey.addEventListener("click", fl_GoToTurkeyPage);
		function fl_GoToTurkeyPage() {
			window.open(url_country+"31", "_blank");
		}

		this.egypt.addEventListener("click", fl_GoToEgyptPage);
		function fl_GoToEgyptPage() {
			window.open(url_country+"38", "_blank");
		}

		this.libya.addEventListener("click", fl_GoToLibyaPage);
		function fl_GoToLibyaPage() {
			window.open(url_country+"37", "_blank");
		}

		this.tunisia.addEventListener("click", fl_GoToTunisiaPage);
		function fl_GoToTunisiaPage() {
			window.open(url_country+"36", "_blank");
		}

		this.algeria.addEventListener("click", fl_GoToAlgeriaPage);
		function fl_GoToAlgeriaPage() {
			window.open(url_country+"35", "_blank");
		}

		this.morocco.addEventListener("click", fl_GoToMoroccoPage);
		function fl_GoToMoroccoPage() {
			window.open(url_country+"34", "_blank");
		}

		this.mali.addEventListener("click", fl_GoToMaliPage);
		function fl_GoToMaliPage() {
			window.open(url_country+"58", "_blank");
		}

		this.cyprus.addEventListener("click", fl_GoToCyprusPage);
		function fl_GoToCyprusPage() {
			window.open(url_country+"33", "_blank");
		}

		this.greece.addEventListener("click", fl_GoToGreecePage);
		function fl_GoToGreecePage() {
			window.open(url_country+"30", "_blank");
		}

		this.albania.addEventListener("click", fl_GoToAlbaniaPage);
		function fl_GoToAlbaniaPage() {
			window.open(url_country+"29", "_blank");
		}



		this.montenegro.addEventListener("click", fl_GoToMontenegroPage);
		function fl_GoToMontenegroPage() {
			window.open(url_country+"28", "_blank");
		}

		this.bosnia.addEventListener("click", fl_GoToBosniaPage);
		function fl_GoToBosniaPage() {
			window.open(url_country+"27", "_blank");
		}

		this.croatia.addEventListener("click", fl_GoToCroatiaPage);
		function fl_GoToCroatiaPage() {
			window.open(url_country+"59", "_blank");
		}

		this.slovenia.addEventListener("click", fl_GoToSloveniaPage);
		function fl_GoToSloveniaPage() {
			window.open(url_country+"25", "_blank");
		}

		this.italy.addEventListener("click", fl_GoToItalyPage);
		function fl_GoToItalyPage() {
			window.open(url_country+"24", "_blank");
		}

		this.monaco.addEventListener("click", fl_GoToMonacoPage);
		function fl_GoToMonacoPage() {
			window.open(url_country+"23", "_blank");
		}

		this.france.addEventListener("click", fl_GoToFrancePage);
		function fl_GoToFrancePage() {
			window.open(url_country+"7", "_blank");
		}

		this.spain.addEventListener("click", fl_GoToSpainPage);
		function fl_GoToSpainPage() {
			window.open(url_country+"56", "_blank");
		}

		this.malta.addEventListener("click", fl_GoToMaltaPage);
		function fl_GoToMaltaPage() {
			window.open(url_country+"32", "_blank");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// btn
	this.mali = new lib.malibtn();
	this.mali.name = "mali";
	this.mali.parent = this;
	this.mali.setTransform(101.9,439.3,0.847,0.847,0,0,0,0.1,0.2);
	new cjs.ButtonHelper(this.mali, 0, 1, 2, false, new lib.malibtn(), 3);

	this.malta = new lib.maltabtn();
	this.malta.name = "malta";
	this.malta.parent = this;
	this.malta.setTransform(235,289.7,0.847,0.847,0,0,0,0.1,0.2);
	new cjs.ButtonHelper(this.malta, 0, 1, 2, false, new lib.maltabtn(), 3);

	this.spain = new lib.spainbtn();
	this.spain.name = "spain";
	this.spain.parent = this;
	this.spain.setTransform(116.9,257.9,0.847,0.847,0,0,0,0.1,0.1);
	new cjs.ButtonHelper(this.spain, 0, 1, 2, false, new lib.spainbtn(), 3);

	this.france = new lib.francebtn();
	this.france.name = "france";
	this.france.parent = this;
	this.france.setTransform(154.6,206.9,0.847,0.847,0,0,0,0.1,0.1);
	new cjs.ButtonHelper(this.france, 0, 1, 2, false, new lib.francebtn(), 3);

	this.monaco = new lib.monacobtn();
	this.monaco.name = "monaco";
	this.monaco.parent = this;
	this.monaco.setTransform(185.3,225.7,0.847,0.847,0,0,0,0.1,0.2);
	new cjs.ButtonHelper(this.monaco, 0, 1, 2, false, new lib.monacobtn(), 3);

	this.italy = new lib.italybtn();
	this.italy.name = "italy";
	this.italy.parent = this;
	this.italy.setTransform(221.5,241.3,0.847,0.847,0,0,0,0.1,0.2);
	new cjs.ButtonHelper(this.italy, 0, 1, 2, false, new lib.italybtn(), 3);

	this.slovenia = new lib.sloveniabtn();
	this.slovenia.name = "slovenia";
	this.slovenia.parent = this;
	this.slovenia.setTransform(237.1,207.2,0.847,0.847,0,0,0,0.1,0.1);
	new cjs.ButtonHelper(this.slovenia, 0, 1, 2, false, new lib.sloveniabtn(), 3);

	this.croatia = new lib.croatiabtn();
	this.croatia.name = "croatia";
	this.croatia.parent = this;
	this.croatia.setTransform(246.9,218.3,0.847,0.847,0,0,0,0.1,0.1);
	new cjs.ButtonHelper(this.croatia, 0, 1, 2, false, new lib.croatiabtn(), 3);

	this.bosnia = new lib.bosniabtn();
	this.bosnia.name = "bosnia";
	this.bosnia.parent = this;
	this.bosnia.setTransform(255.2,225.4,0.847,0.847,0,0,0,0.1,0.3);
	new cjs.ButtonHelper(this.bosnia, 0, 1, 2, false, new lib.bosniabtn(), 3);

	this.montenegro = new lib.montenegrobtn();
	this.montenegro.name = "montenegro";
	this.montenegro.parent = this;
	this.montenegro.setTransform(266.5,234.4,0.847,0.847,0,0,0,0.1,0.1);
	new cjs.ButtonHelper(this.montenegro, 0, 1, 2, false, new lib.montenegrobtn(), 3);

	this.albania = new lib.albaniabtn();
	this.albania.name = "albania";
	this.albania.parent = this;
	this.albania.setTransform(273,247.3,0.847,0.847,0,0,0,0.1,0.2);
	new cjs.ButtonHelper(this.albania, 0, 1, 2, false, new lib.albaniabtn(), 3);

	this.greece = new lib.greecebtn();
	this.greece.name = "greece";
	this.greece.parent = this;
	this.greece.setTransform(295.7,269.6,0.847,0.847,0,0,0,0.2,0.1);
	new cjs.ButtonHelper(this.greece, 0, 1, 2, false, new lib.greecebtn(), 3);

	this.israel = new lib.israelbtn();
	this.israel.name = "israel";
	this.israel.parent = this;
	this.israel.setTransform(380.3,325.6,0.847,0.847,0,0,0,0.1,0.3);
	new cjs.ButtonHelper(this.israel, 0, 1, 2, false, new lib.israelbtn(), 3);

	this.palestine = new lib.palestinebtn();
	this.palestine.name = "palestine";
	this.palestine.parent = this;
	this.palestine.setTransform(378.2,324.7,0.847,0.847,0,0,0,0.1,0.1);
	new cjs.ButtonHelper(this.palestine, 0, 1, 2, false, new lib.palestinebtn(), 3);

	this.cyprus = new lib.cyprus();
	this.cyprus.name = "cyprus";
	this.cyprus.parent = this;
	this.cyprus.setTransform(366.7,295.9,0.847,0.847,0,0,0,8.4,4.5);
	new cjs.ButtonHelper(this.cyprus, 0, 1, 2, false, new lib.cyprus(), 3);

	this.morocco = new lib.moroccobtn();
	this.morocco.name = "morocco";
	this.morocco.parent = this;
	this.morocco.setTransform(82.6,322.9,0.847,0.847,0,0,0,51.1,38.9);
	new cjs.ButtonHelper(this.morocco, 0, 1, 2, false, new lib.moroccobtn(), 3);

	this.algeria = new lib.algeriabtn();
	this.algeria.name = "algeria";
	this.algeria.parent = this;
	this.algeria.setTransform(144.3,353.3,0.847,0.847,0,0,0,87.3,86.1);
	new cjs.ButtonHelper(this.algeria, 0, 1, 2, false, new lib.algeriabtn(), 3);

	this.tunisia = new lib.tunisiabtn();
	this.tunisia.name = "tunisia";
	this.tunisia.parent = this;
	this.tunisia.setTransform(200.4,306.6,0.847,0.847,0,0,0,16.7,34);
	new cjs.ButtonHelper(this.tunisia, 0, 1, 2, false, new lib.tunisiabtn(), 3);

	this.libya = new lib.libyabtn();
	this.libya.name = "libya";
	this.libya.parent = this;
	this.libya.setTransform(256.5,367.3,0.847,0.847,0,0,0,66.7,65.1);
	new cjs.ButtonHelper(this.libya, 0, 1, 2, false, new lib.libyabtn(), 3);

	this.egypt = new lib.egyptbtn();
	this.egypt.name = "egypt";
	this.egypt.parent = this;
	this.egypt.setTransform(348.8,364.2,0.847,0.847,0,0,0,47.8,47.5);
	new cjs.ButtonHelper(this.egypt, 0, 1, 2, false, new lib.egyptbtn(), 3);

	this.turkey = new lib.turkeybtn();
	this.turkey.name = "turkey";
	this.turkey.parent = this;
	this.turkey.setTransform(378.8,264.7,0.847,0.847,0,0,0,76.9,30.3);
	new cjs.ButtonHelper(this.turkey, 0, 1, 2, false, new lib.turkeybtn(), 3);

	this.lebanon = new lib.lebanonbtn();
	this.lebanon.name = "lebanon";
	this.lebanon.parent = this;
	this.lebanon.setTransform(385.6,306,0.847,0.847,0,0,0,5.5,7.7);
	new cjs.ButtonHelper(this.lebanon, 0, 1, 2, false, new lib.lebanonbtn(), 3);

	this.yemen = new lib.yemenbtn();
	this.yemen.name = "yemen";
	this.yemen.parent = this;
	this.yemen.setTransform(481.7,452.2,0.847,0.847,0,0,0,44.1,30.8);
	new cjs.ButtonHelper(this.yemen, 0, 1, 2, false, new lib.yemenbtn(), 3);

	this.oman = new lib.omanbtn();
	this.oman.name = "oman";
	this.oman.parent = this;
	this.oman.setTransform(536.8,411.1,0.847,0.847,0,0,0,32,40);
	new cjs.ButtonHelper(this.oman, 0, 1, 2, false, new lib.omanbtn(), 3);

	this.uae = new lib.uaebtn();
	this.uae.name = "uae";
	this.uae.parent = this;
	this.uae.setTransform(521.9,382.9,0.847,0.847,0,0,0,19.7,16.8);
	new cjs.ButtonHelper(this.uae, 0, 1, 2, false, new lib.uaebtn(), 3);

	this.bahrein = new lib.bahreinbtn();
	this.bahrein.name = "bahrein";
	this.bahrein.parent = this;
	this.bahrein.setTransform(494.2,370.2,0.847,0.847,0,0,0,2.2,4.2);
	new cjs.ButtonHelper(this.bahrein, 0, 1, 2, false, new lib.bahreinbtn(), 3);

	this.qatar = new lib.qatarbtn();
	this.qatar.name = "qatar";
	this.qatar.parent = this;
	this.qatar.setTransform(500.7,375.4,0.847,0.847,0,0,0,2.9,7.4);
	new cjs.ButtonHelper(this.qatar, 0, 1, 2, false, new lib.qatarbtn(), 3);

	this.koweit = new lib.koweitbtn();
	this.koweit.name = "koweit";
	this.koweit.parent = this;
	this.koweit.setTransform(472.1,343.1,0.847,0.847,0,0,0,8.4,7.6);
	new cjs.ButtonHelper(this.koweit, 0, 1, 2, false, new lib.koweitbtn(), 3);

	this.saouda = new lib.saoudabtn();
	this.saouda.name = "saouda";
	this.saouda.parent = this;
	this.saouda.setTransform(458.2,383.4,0.847,0.847,0,0,0,91.5,74.9);
	new cjs.ButtonHelper(this.saouda, 0, 1, 2, false, new lib.saoudabtn(), 3);

	this.jordan = new lib.jordanbtn();
	this.jordan.name = "jordan";
	this.jordan.parent = this;
	this.jordan.setTransform(396.3,327.4,0.847,0.847,0,0,0,16.4,19.9);
	new cjs.ButtonHelper(this.jordan, 0, 1, 2, false, new lib.jordanbtn(), 3);

	this.syria = new lib.syriabtn();
	this.syria.name = "syria";
	this.syria.parent = this;
	this.syria.setTransform(406.5,298,0.847,0.847,0,0,0,25.9,23.8);
	new cjs.ButtonHelper(this.syria, 0, 1, 2, false, new lib.syriabtn(), 3);

	this.iraq = new lib.iraqbtn();
	this.iraq.name = "iraq";
	this.iraq.parent = this;
	this.iraq.setTransform(442,311.4,0.847,0.847,0,0,0,42.6,39.6);
	new cjs.ButtonHelper(this.iraq, 0, 1, 2, false, new lib.iraqbtn(), 3);

	this.afghan = new lib.afghanbtn();
	this.afghan.name = "afghan";
	this.afghan.parent = this;
	this.afghan.setTransform(604.2,306.4,0.847,0.847,0,0,0,54.6,41.6);
	new cjs.ButtonHelper(this.afghan, 0, 1, 2, false, new lib.afghanbtn(), 3);

	this.pakistan = new lib.pakistanbtn();
	this.pakistan.name = "pakistan";
	this.pakistan.parent = this;
	this.pakistan.setTransform(622.1,334.4,0.847,0.847,0,0,0,66.8,63.5);
	new cjs.ButtonHelper(this.pakistan, 0, 1, 2, false, new lib.pakistanbtn(), 3);

	this.cambodia = new lib.cambodiabtn();
	this.cambodia.name = "cambodia";
	this.cambodia.parent = this;
	this.cambodia.setTransform(902.6,478.1,0.847,0.847,0,0,0,23.2,20);
	new cjs.ButtonHelper(this.cambodia, 0, 1, 2, false, new lib.cambodiabtn(), 3);

	this.singapore = new lib.singaporebtn();
	this.singapore.name = "singapore";
	this.singapore.parent = this;
	this.singapore.setTransform(895.6,563.6,0.847,0.847,0,0,0,6.5,1.9);
	new cjs.ButtonHelper(this.singapore, 0, 1, 2, false, new lib.singaporebtn(), 3);

	this.iran = new lib.iranbtn();
	this.iran.name = "iran";
	this.iran.parent = this;
	this.iran.setTransform(512,315.9,0.847,0.847,0,0,0,0.1,0.3);
	new cjs.ButtonHelper(this.iran, 0, 1, 2, false, new lib.iranbtn(), 3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(153,0,0,0)").ss(1,1,1).p("AdTteIAAgcIg7AJQgKgYgiAKQgaAngzAPQg4gbAVhBIgbAAIAAgXIhIAQIAAAfIgwAAIASATIgTAVIgZgWIgtARIgNAWIgcgfIghAAIgOgNIiKAAIgPApQgDBWhcAEQAMA2hLgXQgkAGghglIAMBVIgkAuQACA+AjgJQgUAxAsBwQBxAOg1BAIgtA4IBjBsQBRANgLBWIAoAAIAAAzIg/AAIgnAxIAABHIB6AAIAogaIAeAZIBsgcQA1gJAPA8QAuASAFAqIBGAkIAwgiIA8AAIAkgcQA7AcgNgcIgmhfIggAAIAAhBIgyAAIgWgzIA8hSIBKAZQBahGAWhdIAggvIAsg1IgWhHIAAgoIg4g5IAAhGIgfAAQgPgwAlgRIBgAcIBMgXIAbgnIA0AAIgPgVIguAAIgzggQgdAMgSgeQgEgxgoADIhOgZIgkASIhaAAIg9AfIgXAcIAhAdQAfA0gvAmQAXAhgoASIg+AAQBWAvhWATIADBfIgiAfIgTgXIhFAZIAOARIg1AAIgpAnIAPBPQjJBDjAhDEBEWAT+QAsAIgEgkIAzgIIAPASIAVAAIAAgYQgjgUACgbIAMgPIAdAOIAAgTIA8ggIAOAAQARgzgRg0IgOgRIAKgOIgUgUIAMgoIgvAbIgcgQIgoANIAdAhIgcAAIgSgMIgWAAIgkgVQhEgOhFAOQgUAsgXAcQgCApAkApIAAAbIALAAQAGAyAfAyQAdguAFA3gEBCwAerQA0gWAsAWIAKARIhWAAgAsNsMIgQgbIAYgTQAJgmgmAJIgiAUQgzgjgmA3IgVANIgaAAQgzARgbgjQhHgtgwAMIAAAgQhEAogbg1IgfgKIgSgNIgRAUIAAgMIAMgVIg0AAQgHgVgXgPQAGgpgrgRQAcgUgcgLIAAg3QAYgihFATIAAgjIAigeICWgBIAAgwIAYgTIB3AJQA0goBOgcQBXAEAjgNQATAfAzgCQAQAiAlgEQBYAiBogQQBbAHAogrIA7AAIAagMIBRBMQgOA/AygPIBoBjIBEAAIBVhIIAiAfIgMAxIA5AcQAGBQBJACICTBMIBjgfIAxAOIASgSIgUghQAxgUAxgqQA7gRBBAIQAFAdBIAOQA1ADAvAwQALgHA5AlQAugNAEA6IAEAZAi9t0IgkgzQARgxgvg3IAAhKAlYtqIgRgQIgmAUIhCAAIhIAdIg+AAIgegRIhNAbIgZgTIgRAMIARAmIgbAAIAAAWIgJALIgOgNIAAAkIANBHIAbAAQAUAjgLAgIgcAAIAAAVIgWAWAlYtqIgsAsIgiALIATA8IgPBtIiXBYIh8BJIgnAAIgmgfIgMAKIAAg1Ai9t0IgcAXIgIgcIhoAAIgPAPACXkcIgJgPIAAgXIgggOIAAgmIgdgJIAAg9IgigvIgegJQgngogVAJQAFgmgngWIgcgYQAPgagPgZIAWgrQAjADgjgfIAZghIgzAAIhQhyA3OzfIBAgeQAQAZAegIQAaA2AvAFIgKAJIgdAAIhgAnIg2gNIASgkIAWgRIgigcIgQAGIALAaIgNAJIgvAAIgngHIgWgUIgzAWIgwAAAwIq7IApAbIATgMIAVAAIAOgRIAYAAIgLgQQAXgFALgYQgvAXgqgCQgTAVgiAFgAsyoiQgIAvgTAtIAVASIgSAuIgbgOAsyoiIAehbIAUgkAsyoiIAUAAIAQgRAsekwIgXAnIgwiLAskjuIAGhCIAQjOAs1kJIgkCOQgjgXgggiIAAgfIg7hVIgTAfID+HCIALBVIgoARIgYAqIgjAIIgOAkIhDgVIpKAAAskjuIBGAKIA6g5IAwgJIAAgcIAigMIhVhSICXg0IAAgZIgTAAIAAgaIgWgrAtNnGQgKAZgOAZA33msIANAQIAgAAQBigBBiAkQBCAfAygzIBOgfIA1ALIAAAgQCHARAjgkAskjuIgRBbQAtgJAcA7IB/CxQgDBQBGAZIA4BIIAQAlIAABAIATA7QAdBJAxADQBpCgBtCQAAVjbIiMgOIkNjGQhFgehHgHACDkBIgIgSIgPgaIgzAAIgkBSIA3AAIAaAsIA9AAIgghSIANAAIAHgbAEmgBIAAAoIAdAZIAGgpgAFhCCIAiAOQANAhAXAKQAbANAogTIB8AAIAhg3IBWhsIAWBdIAGAWQApBSB4AMIBEBNQAtAVg3BHIgaAgIgWAsQgmgdgTBaIAPA/Ig1AAIgZAgQgFAzhGgBIgXAcQAHA9hTgSIhOAcAFhCCIAAg0IARg3IAeAbIAAAeQAGAggTAgACjivIB3ClIAAA6QAxAgAWAyARtBcIkOgkQg1gSgQg2QgbhliRBlIjyhqIh8i/QhWgJgTAmAGnC7IBJBTIC5AfIAahqIAXAAIAAgTIgLgJIgIg1IALgKIAKAYIAXgREg0ygVcQAsgLgEhNIANgRIgIgLIALgzIAKAIIAIAXIARAOIgVg7IgKgSIAAgnQgfgGgeghIAAgnIgVAAIAAgUIgSgMIgbAAIgxgOIgdAAIgLgOIAVAAIAAgGIgNgIIAPgGIgjgGIAXgPIAsAAIAkgQIAvAVIASAAIAIAIIALgGIAbAAIAHgIIgJgJIAAgqIgQgPIAAgNIAfAAIAFAXIBEAKIAygUIgcgGQAagXAqgDIAdgWQgNhCBNgDIAEAZIAgAAIAMARIAWAFIAAAIIAeAEIAAAbIAXAAIANgTIAJAAIAAAgIAKAAIAbATIA7AFIAYAYIANgFIAIAKIAWgGIAUAOIAeAAQgxAxABBEIgjAAIAAAXIg+A3IAAAfIAigXIAcAAIgQAaIATAAIgFAKEg0ygVcIAcAAIAAAfIAeAAIA6AXIApAAIAOgRIAiARIAAAbIAhgPIAAAOIBWgQIANAIIgNATIAAAaIg5AeIgUASIgcAAIg9AbIAAAUIgsAoQg2A4AnBBQgxAigLAzQhGALgVA6IgTgOIgXAPIh6AAIgUAQQgnALgSAhIgfgRIgQgxIhRgnIAAgfIAqglIgZgaIAagsIggg3IAlAAIAAgmIARgNIAAg2IAwgrIghgUIANgOIgpAAIgPASIg6AAIAOgYIgsAAIAPgZIgPAAIAAgiIgcgUIAegRIAnAAIAZgdIAmAPIExAAQAnAOAYgGgEgvugUcIgNgIQAIhhCVAmQBWAwAqhIIAWgJIgIgSIgZAMIALAPEgxlgUmIAhAMEhCMgBvIAKgIQBqgcBihhQBBhKgogeIAMhGIAqgoIAAgnQBBhMA4gHQBfhAAZhmIAkgWIA8A3IBYAAIAUgLQAaATAhAAIBWguQAjgOAmADIAjgwICSgQIASgSICKgJIAZAfIAOgKIALgNIAnAAIALgTQArAUAdgUQAkASAtAAIAZgMIAyggIAgAQIAAAaQAcAJAcgTIAIAJQhDAtAvAxQAoArg/A1QhBAfBNAuIAbAAQAAAkAmgBIBYAgQBEgRA5AmQA9ACACA3IBJAzQBjAABEBEQBnADADhNIgXg+IAzgyQBMgfBUAfIAiAkIBoAOIAKAaIgOAjIARAqIgVAsIAZA0IARJfIAECjIhJAAIABAnIAIE1IhLAAIAAAmIgqA5IAOAeIggAaIANAnIgnAsIAzAIIAPBdQAvAPgfAhEgwYgQzIATAVIAQAAIAQgWIgZgNgEhCMgBvIgKAHQgUBJhUAzQgUBdhEBHIhKCXQgzAuAzBMIAXAkIgZArIAgCFIgdBuEg1vgLBIAXAUIAACEIASAJIAAAUIAbAZIgQAjQhQgQgjAQQgLAhg5AEIALAYQATApgugFIgkAXIgaAGQgeBAhOgJIgQATIgZAAIguASIhSA6IAABNEg4bACNIkyjfIgIgdEhCMgBvIE3AAEg4bACNIizAAIBELZIkrAAQgPAYgogRIgsAXIgtgkIgqBCIAABQIgMAPIAUAKIAAATIAUgLIASAPIgJBlIAqgXIAWAYIAVgUIAoASIAmgqIA9BdIgWAiIAdgKIAAAnIAaAcIAbgSQAYAjArgTIAAgmQAQAYAUgYIAAAtIA9ghIALhnIAggMQAigGACguIAAgnQAggcAcAcQAbgLgGgbQArgQAOgkQAcAQAIgeQA8gSAkgoQAXASAggFQBDAKARggIB9AAIAPgSIAVALQBNg5gHj8A80yXIAlAAIArgWIAiAAIATgOIgHgrIgqglIhEAGIAAAMIgXAAIgQANIgKgYIgUgIIgKgRIgJgQIALgEIAAgLIghgFIgxgpIAigHIgRgfIAXgEQgwgQAbgwIgTgHIgOAJQgHgdg7AMIhbgPQgRAYgfgUIAAAkIAfAQIAAAcIBiBKIAEAPEghngZXQBjBaAoguIAQAcIANAAIgKATIAZAIIgTANIAAAMEghngZXIgYAAIAAAKIgNAAIgdAPIAEAbIgaAHIAHAMIgIANIgvgOIgcALIgcABIgIAIIAUAkIAVghQAaADAZATQAGAuAmARIgXAHQAYAbAfAUIAoAAIBDAnQACACABABEgkugZaQA7AWAZgdIAkAAIAvgWQAQASAQAOA/m0dIAEgjIAMAAIAGgcIANAHIAAgVEgq8gWGIA0gkIB2AXIAlBSQBTBXBqA4IAbAAIAoAsIAdAAIAMAdIAWASIAYAAIAoBeIgTAaIAAASIgRAXQADANAJgFIApgoQgKgaAZgMQAXABgGgdIgagRIgIg3IAggbIATAAQARASATgCIAWAiQAWADgGgPQgFgYgfgRQhFgqhHgZIARgiQhtAWhDibQhigtAHg9IANgJQgggrAvgKIAugOIARAAIAFgBIgKgRIAMgNIgTgIIAPgVIg8gFIgngoIg+ANIgQAOIgdgGIgHAWIgUgFIgIAZIgXAAIgUgSIgVA1IghgZIAAgQIgLgGIgOAPIAAAJIgMAOIhBACA/m0dQgagTgZgVA9z0dIgOAHIgRgOIgXA0QgfgWgegXA9jw0IgNgMIghgqQgNg5ANg9QgMgIgMgIA9LzsIgFAEQgUA6AwAXA9jw0IALgKIAAgTIAqgtIgGgZA9jw0IAoAtIAagEIAKAPIgdAIIAZAkIAKgIIARAOQAZgVBgAlIgRAOQAjgIAbAVIAAgcQg0gkg0gSIAAgIIAVgHIAIgoIghgvQgDgwAkAAIAAATQAXASAWgJIAGgNIgKgPIAIgNIAUAFIATgTIAXAHIATgIIBPAVA8VunIAPgPIAbgKIBLAgIAeAbIgLAGIgWgOQgOgEAAAQIAZA/IgQgSIgMAAIgNARIgUgcIgRAAIgEALIgHgKIAAgngA6av6IANgFIAjAaIARAAIAJAUIgdgEQgSgUgbgRgA5trkIAAARQA8gGAlAfIBXgGIAEgNIgoAFIAAgNQgZAEgZgOIgWAFgEgksgYfIAOgDIAGAQIgPANEgq8gWGIAAgYQg8ALARg4QgqgYAqgPQAWgTgfgUIAKgTEgpXgTGIAagoIAAhPIgOAAIAAAVQguAPAFAZIAOAxgEgrIgWAIAMgGEgqTgSQQAlAEAegiQAsAxgZAZIAFA1QACA6gvgjIAAAaIgVAAIgQgkIAPgeIgQgOIAOgYgEgo+gE8IghiWIhJhMIgagOIgMhKIA9hCIAAh+IAPgYEglXgOQQgngNAegTIAQAAIASgLQAZAOBCABQAKgLAYADIAcgMQgaAlADAtQAOALgTAZIgdAAQgQgaghgKQgagJgbgVgEgjzgMQIAYAAIAAAYIgYAAgEgm1gIjIAAA5Ig8AgIAAARIgjATQAhBUhLAWEgmPADlQgWhgheAQIgdhNIgWgZQgbgrAbgNIAWgUIgMg3IANgrIgNgnQANhJgfhNEgmPADlIk3DFIiSCKIhjAPIhQANIAAg+IhEgjIgZAAIg9g0IAAgbIl2kTEhBvAOiIhOg6IAAgsIgvAAIhOgxQg2AxgvgxIgcBuIgzArIAkBEAKpEtIAuArIghCgIjiBYIBcC5AHUJQIjeAlQhCAigdBDQgaArgfgrQh0gQhdAQQgxghAEBGIgLAeIgOA8IA8DvIAqAPICJhDIAuAAIBhg1QApAJBYhIIBNgYQBpgUgNhJIBBgiEgmPADlQBwAGAxBCICVhCII2E/");
	this.shape.setTransform(463,367);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.008)").s().p("Ah0ADQhJgCgGhPQCyCvDChWIATAhIgSASIgxgOIhjAfg");
	this.shape_1.setTransform(490.7,277);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.iran},{t:this.singapore},{t:this.cambodia},{t:this.pakistan},{t:this.afghan},{t:this.iraq},{t:this.syria},{t:this.jordan},{t:this.saouda},{t:this.koweit},{t:this.qatar},{t:this.bahrein},{t:this.uae},{t:this.oman},{t:this.yemen},{t:this.lebanon},{t:this.turkey},{t:this.egypt},{t:this.libya},{t:this.tunisia},{t:this.algeria},{t:this.morocco},{t:this.cyprus},{t:this.palestine},{t:this.israel},{t:this.greece},{t:this.albania},{t:this.montenegro},{t:this.bosnia},{t:this.croatia},{t:this.slovenia},{t:this.italy},{t:this.monaco},{t:this.france},{t:this.spain},{t:this.malta},{t:this.mali}]}).wait(1));

	// Map
	this.instance = new lib.bewmap_hp();
	this.instance.parent = this;
	this.instance.setTransform(-2,-56,0.254,0.245);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(498,226.5,1219.8,622);
// library properties:
lib.properties = {
	id: '7682ACBAC7C81E4DACCAE862D8304DB4',
	width: 1000,
	height: 565,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"/js/home/bewmap_hp.png", id:"bewmap_hp"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7682ACBAC7C81E4DACCAE862D8304DB4'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;
