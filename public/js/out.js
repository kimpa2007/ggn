var out = {
	
	dev : true,
	
	debug : function(message){
		//only in dev	
   		if(out.dev && window.console){
	 		console.log(message);
	 	}
	},
	
	error : function(message){
		
   		if(window.console){
	 		console.error(message);
	 	}
   }
};