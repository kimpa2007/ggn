var table = $('#table').DataTable({
    language: {
        "url": "/plugins/datatables/locales/french.json"
    },
    processing: true,
    serverSide: false,
    pageLength: 10,
    ajax: routes.data,
    columns: [
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'solde', name: 'solde'},
        {data: 'action', name: 'action',width:'250', orderable: false, searchable: false},
    		{data: 'futur_solde', name: 'futur_solde'}
    ],
    createdRow: function( row, data, dataIndex ) {
            //console.log(data);
            $( row ).attr('id', 'user-'+data.id);
            $( row ).attr('data-id', data.id);
            $( row ).attr('data-credits', data.credits);
            $( row ).attr('data-input-value', 0);
            $( row ).attr('data-new-credits', data.credits);
    },
    initComplete: function() {
        initEvents(table);
    }
});


var initEvents = function(table) {

  var isNumber = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  var updateValue = function (itemId,newValue){
    var newValue = parseInt(newValue);

    $("#user-"+itemId).find('.credits-value').val(newValue);

    var credits = $("#user-"+itemId).data('credits');

    var newCredits = parseInt(credits) + parseInt(newValue);

    if(newCredits < 0){
      toastr.error('Les crédits utilisateurs ne peuvent pas être négatifs.','Erreur !',{timeOut: 3000});

      //set value to - all they have
      updateValue(itemId, -credits);
      return;
    }

    $("#user-"+itemId).find('td:nth-child(5)').html(newCredits);
    $("#user-"+itemId).attr('data-input-value',newValue);
    $("#user-"+itemId).attr('data-new-credits',newCredits);

    //update the table data
    var d = table.row("#user-"+itemId).data();
    d.newCredits = newCredits;
    d.newValue = newValue;
    //table.row("#user-"+itemId).data(d);

    checkHasCredits();

  };

  var checkHasCredits = function(){
    var total = 0;

    //get all rows with selector changed
    var items = table.rows('.changed').every(function(){
        var item = this.data();
        total += item.newValue;
    });

    //console.log(total);
    var difference = PARTNER_CREDITS-total;
    $("#futur-solde").html(difference);

    if(total <= PARTNER_CREDITS){
      //ok
      return true;
    }
    else {
      toastr.error('Pas assez de crédits pour effectuer cette action.','Problème !',{timeOut: 3000});
    }

    return false;

  };

  $(document).on('click','.toggle-plus', function(e){
        var self = $(this);
        e.preventDefault();

        var id = self.data('id');
        $("#user-"+id).addClass("changed");

        var currentVal = $("#user-"+id).find('.credits-value').val();

        updateValue(id,parseInt(currentVal) + 1);

  });

  $(document).on('click','.toggle-minus', function(e){
        var self = $(this);
        e.preventDefault();

        var id = self.data('id');
        $("#user-"+id).addClass("changed");

        var currentVal = parseInt($("#user-"+id).find('.credits-value').val());

        updateValue(id,parseInt(currentVal) - 1);

  });

  $(document).on('change','.credits-value', function(e){
        var self = $(this);
        e.preventDefault();

        var id = self.data('id');
        $("#user-"+id).addClass("changed");

        var currentVal = $("#user-"+id).find('.credits-value').val();

        if (isNumber(currentVal)){
          updateValue(id,parseInt(currentVal));
        }
        else {
          toastr.error('Le nombre doit être un nombre entier', 'Erreur !', {timeOut: 3000});
          updateValue(id,0);
        }
  });

  $(document).on('click','.save-changes',function(e){
    e.preventDefault();

    var $btn = $('.save-changes');
    $btn.button('loading');


    if(checkHasCredits()){

        var users = [];

        //get all rows with selector changed
        var items = table.rows('.changed').data();

        table.rows('.changed').every(function(){
            var item = this.data();

            users.push({
              id:item.id,
              current_credits:parseInt(item.credits),
              add_value:item.newValue,
              new_credits:item.newCredits
            })
        });

        var users = JSON.stringify(users);
        users = users.replace(/"/g, '&quot;')

        //console.log(users);

        $('<form action="'+routes.saveCredits+'" method="POST"/>')
            .append($('<input type="hidden" name="_token" value="' + csrf_token + '">'))
            .append($('<input type="hidden" name="users" value="' + users + '">'))
            .appendTo($(document.body)) //it has to be added somewhere into the <body>
              .submit();

    }
    else {
      $btn.button('reset');
    }

  });



  $(document).on('click','.open-same-credits',function(e){
    e.preventDefault();

    var $btn = $('.open-same-credits');


    bootbox.prompt({
        title: "Définir le montant à envoyer à chaque utilisateur",
        inputType: 'number',
        callback: function (result) {
            if(result == null)
              return;

            //check if partner has enaugh credits
            //console.log("table length : "+table.rows().data().length);
            var total = table.rows().data().length * result;
            //console.log(total);
            if(total > PARTNER_CREDITS){
              toastr.error('Pas assez de crédits pour effectuer cette action.', 'Erreur !', {
                  timeOut: 3000
              });
              return;
            }

            $btn.button('loading');
            $(".save-changes").addClass("disabled");

            $('<form action="'+routes.saveSameCredits+'" method="POST"/>')
                .append($('<input type="hidden" name="_token" value="' + csrf_token + '">'))
                .append($('<input type="hidden" name="quantity" value="' + result + '">'))
                .appendTo($(document.body)) //it has to be added somewhere into the <body>
                .submit();

        }
    });

  });


};
