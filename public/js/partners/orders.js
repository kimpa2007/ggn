var table = $('#table').DataTable({
    language: {
        "url": "/plugins/datatables/locales/french.json"
    },
    processing: true,
    serverSide: false,
    pageLength: 10,
    ajax: routes.data,
    columns: [
        {data: 'id', name: 'id'},
        {data: 'created_at', name: 'created_at'},
        {data: 'type', name: 'type'},
        {data: 'plan', name: 'plan'},        
        {data: 'start_at', name: 'start_at'},
        {data: 'end_at', name: 'end_at'},
      /*  {data: 'content', name: 'content'},*/
        {data: 'amount', name: 'amount'},
        {data: 'status', name: 'status'},
        //{data: 'action', name: 'action', width:'250', orderable: false, searchable: false}
    ],
    initComplete: function() {
        initEvents(table);
    }
});


var initEvents = function(table) {

    $('.toggle-cancel').on('click', function(e){
        e.preventDefault();
        var el = $(this);


        bootbox.confirm({
          message: "Êtes-vous sur de vouloir annuler cette commande ?",
          buttons: {
              confirm: {
                  label: 'Oui',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'Non',
                  className: 'btn-secdonary'
              }
          },
          callback: function (result) {
              if(result) {
                  window.location = el.attr('href');
              }
          }
        });
    });
};
