$(function() {

    // Loading data
    var table = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: false,
        pageLength: 10,
        ajax: routes.data,
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'full_name', name: 'full_name'},
			      {data: 'email', name: 'email'},
            {data: 'status', name: 'status', orderable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function() {
            //initEvents(table);
            $('#table tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            } );

            $(".select-all").click(function(e){
              e.preventDefault();

              $(this).toggleClass('selected');

              table.rows('tr').every(function(){
                  var item = this.data();
                  if($(".select-all").hasClass('selected'))
                    $(this.node()).addClass("selected");
                  else
                    $(this.node()).removeClass("selected");
              });


            });

            $('.multiple-accept').click( function (e) {
                e.preventDefault();

                var $btn = $('.multiple-accept');
                $btn.button('loading');

                var users = [];

                //get all rows with selector changed
                var items = table.rows('.selected').data();

                table.rows('.selected').every(function(){
                    var item = this.data();

                    users.push({
                      id:item.id
                    })
                });

                var users = JSON.stringify(users);
                users = users.replace(/"/g, '&quot;')

                $('<form action="'+routes.activeMultiple+'" method="POST"/>')
                    .append($('<input type="hidden" name="_token" value="' + csrf_token + '">'))
                    .append($('<input type="hidden" name="users" value="' + users + '">'))
                    .appendTo($(document.body)) //it has to be added somewhere into the <body>
                      .submit();

                //alert( table.rows('.selected').data().length +' row(s) selected' );

            } );

            $('.multiple-cancel').click( function (e) {

              e.preventDefault();

              dialog.confirm('Etes-vous sur de vouloir refuser cet utilisateur ?', function(result){
                if(result) {

                  var $btn = $('.multiple-cancel');
                  $btn.button('loading');

                  var users = [];

                  //get all rows with selector changed
                  var items = table.rows('.selected').data();

                  table.rows('.selected').every(function(){
                      var item = this.data();

                      users.push({
                        id:item.id
                      })
                  });

                  var users = JSON.stringify(users);
                  users = users.replace(/"/g, '&quot;')

                  $('<form action="'+routes.refuseMultiple+'" method="POST"/>')
                      .append($('<input type="hidden" name="_token" value="' + csrf_token + '">'))
                      .append($('<input type="hidden" name="users" value="' + users + '">'))
                      .appendTo($(document.body)) //it has to be added somewhere into the <body>
                        .submit();
                }
              });

            } );
        }
    });

    $(document).on('click','.trigger-active',function(e){

      e.preventDefault();

      var id = $(e.target).data('id');

      $.ajax({
          method: 'POST',
          url: routes.active,
          data: {
              _token: csrf_token,
              id: id
          }
      })
      .done(function(response) {
          if(response.success) {
              $('#table').DataTable().ajax.reload(function(response){
                  //initEvents();
              }, false);
              toastr.success('Utilisateur accepté', 'Accepté !', {timeOut: 3000});
          } else {
              toastr.error('Une erreur s\'est produite lors de l\'activation', 'Erreur !', {timeOut: 3000});
          }
      }).fail(function(response){
          toastr.error('Une erreur s\'est produite lors de l\'activation', 'Erreur !', {timeOut: 3000});
      });

    });


    $(document).on('click','.trigger-refuse',function(e){
        e.preventDefault();

        var id = $(e.target).data('id');

        dialog.confirm('Etes-vous sur de vouloir refuser cet utilisateur ?', function(result){
            if(result) {
                $.ajax({
                    method: 'POST',
                    url: routes.refused,
                    data: {
                        _token: csrf_token,
                        id: id
                    }
                })
                .done(function(response) {
                    if(response.success) {
                        $('#table').DataTable().ajax.reload(function(response){
                            //initEvents();
                        }, false);
                        toastr.success('Cet utilisateur a été refusé ', 'Succès !', {timeOut: 3000});
                    } else {
                        toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                    }
                }).fail(function(response){
                    toastr.error('Une erreur s\'est produite', 'Erreur !', {timeOut: 3000});
                });
            }
        });

    });


    var initEvents = function(table) {

    }

});
