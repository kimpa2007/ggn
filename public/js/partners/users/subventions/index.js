var table = $('#table').DataTable({
    language: {
        "url": "/plugins/datatables/locales/french.json"
    },
    processing: true,
    serverSide: true,
    pageLength: 10,
    ajax: routes.data,
    columns: [
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'product', name: 'product'},
        {data: 'price', name: 'price'},
    		{data: 'date', name: 'date'},
        {data: 'action', name: 'action', width:'230px', orderable: false, searchable: false}
    ],
    initComplete: function() {
        initEvents(table);
    }
});

var initEvents = function(table) {

  $(document).on('click',".toggle-accept",function(event){

    event.preventDefault();

    var link = $(event.target).closest('.toggle-accept');

    var price = link.data('price');

    bootbox.prompt({
        title: "Set the amount to send to User",
        inputType: 'number',
        value: price,
        callback: function (result) {
            if(result == null)
              return;

            $.ajax({
              type: 'POST',
              url: link.attr('href'),
              data : {
                _token: csrf_token,
                amount : result,
              },
              dataType: 'json',
              success: function(response) {

                  if (response.code == 200) {
                      toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {
                          timeOut: 3000
                      });

                      window.location.reload();

                  } else if (response.code == 400) {
                      //error
                      toastr.error(response.message, 'Erreur !', {
                          timeOut: 3000
                      });
                  } else {
                      //nothing to change
                  }

              },
              error: function(data) {
                var errors = data.responseJSON;
                console.log(errors);

                if(errors.hasOwnProperty('amount')){

                  toastr.error(errors.amount[0], 'Erreur !', {
                      timeOut: 3000
                  });

                }
                else if (errors.code == 400) {
                    //error
                    toastr.error(errors.message, 'Erreur !', {
                        timeOut: 3000
                    });
                }
                else {

                  toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {
                      timeOut: 3000
                  });

                }

              }
            })

        }
    });

  });

  $(document).on('click','.toggle-delete', function(e){
        var self = $(this);
        e.preventDefault();

        dialog.confirm('Etes-vous sur de vouloir cette commande ?', function(confirm){
            if(confirm) {
                document.location.replace(self.attr('href'));
                //send
            }
        });
    });


};
