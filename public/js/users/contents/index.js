var table = $('#table-orders').DataTable({
    language: {
        "url": "/plugins/datatables/locales/french.json"
    },
    processing: true,
    serverSide: true,
    pageLength: 10,
    ajax: routes.data,
    //responsive: true,
    columns: [
        {data: 'date', name: 'date'},
        {data: 'title', name: 'title'},
        {data: 'description', name: 'description',width:'250'},
    		{data: 'countrie', name: 'countrie'},
        {data: 'action', name: 'action', width:'100', orderable: false, searchable: false}
    ],
    initComplete: function() {
        initEvents(table);
    }
});


var initEvents = function(table) {
  console.log("hello!");
};
