$(function(){

  var page = 1;
	var availablePages = Math.ceil(count / itemsPerPage);

  var countriesArray = [];
  var filtersArray = [];

  var selectedCountry = null;

  if(initCountryId != null)
    selectedCountry = initCountryId;

  var interval = null;

  /**
  *	Load the content related to query
  */
  var loadContent = function(){

    var countries = selectedCountry != null ?
      $.merge(countriesArray,[selectedCountry]) : countriesArray;

    $.ajax({
        url: routes.loadContent,
        type: "GET",
        data: {
          page : page,
          countries : countries,
          filters : filtersArray
        },
          success: function(response){

            //update avaiable pages
            count = $(response).find('#count').data('count');
            availablePages = Math.ceil(count / itemsPerPage);

            $("#contents-grid").append($(response).find('#contents-grid').html());

            if(parseInt(page) >= parseInt(availablePages)){
              $(".see-more-container").css({display:'none'});
            }
            else {
              $(".see-more-container").css({display:'block'});
            }

            if(count == 0){
              $(".contents-message").css({display:'block'});
            }
            else {
              $(".contents-message").css({display:'none'});
            }

            $(".contents-grid-container").slideDown();

          },
          error:function(msg){}
      });

  };

  /**
  *	Return all content to empty values
  */
  var resetContent = function(callback){
    $(".contents-grid-container").slideUp();
    page = 1;

    clearTimeout(interval);

    interval = setTimeout(function(){
        $("#contents-grid").empty();
        callback();
    },500);

  };

  if($(".select-countrie.active").length > 0){
    //enable menu

    var countrie = $(".select-countrie.active");
    var countryId = countrie.data('cat-id');
    var subCategories = countrie.data('cat-sub');

    if(subCategories.length > 0){
      //fill subcategories
      $(".sub-countries ul").empty();
      for(var key in subCategories){
        var li = '<li>'+
          '<a href="#" class="btn btn-sm select-sub-countrie" data-cat-id="'+subCategories[key].id+'" >'+subCategories[key].name+'</a>'+
          '</li>';
        $(".sub-countries ul").append(li);
      }
      $(".countries").addClass('open');
      $(".sub-countries").slideDown();
    }
  }

  $(".select-category").click(function(e){

    e.preventDefault();

    var countrie = $(e.target).closest('.select-category');
    var countryId = countrie.data('cat-id');
    var subCategories = countrie.data('cat-sub');

    countriesArray = new Array();

    //remove selected countrie
    if(countrie.hasClass('active')){

      $('.select-category').removeClass('active');
      //close submenu
      $('.sub-countries').slideUp();
      $(".countries").removeClass('open');

      selectedCountry = null;

    }
    else {
      $('.select-countrie').removeClass('active');
      countrie.addClass('active');

      selectedCountry = countryId;

      if(subCategories.length > 0){
        //fill subcategories
        $(".sub-countries ul").empty();
        for(var key in subCategories){
          var li = '<li>'+
            '<a href="#" class="btn btn-sm select-sub-countrie" data-cat-id="'+subCategories[key].id+'" >'+subCategories[key].name+'</a>'+
            '</li>';
          $(".sub-countries ul").append(li);
        }
        $(".countries").addClass('open');
        $(".sub-countries").slideDown();
      }
      else {
        //close submenu
        $('.sub-countries').slideUp();
        $(".countries").removeClass('open');
      }

      //countriesArray.push(countrie.data('cat-id'));
    }

    resetContent(loadContent);

  });


  $(document).on('click',".select-sub-countrie",function(e){

    e.preventDefault();

    var countrie = $(e.target).closest('.select-sub-countrie');
    var countryId = countrie.data('cat-id');

    //remove selected countrie
    if(countrie.hasClass('active')){

      //remove from array of countries
      var index = countriesArray.indexOf(countryId);
      if(index != -1)
        countriesArray.splice(index, 1);

      countrie.removeClass('active');
    }
    else {
      countrie.addClass('active');
      countriesArray.push(countryId);
    }

    console.log(countriesArray);

    resetContent(loadContent);

  });



  $(".filter-type").change(function(){

    if($(this).is(':checked')){
        //add to array
        filtersArray.push($(this).val());
    }
    else {
        //remove to array
        var index = filtersArray.indexOf($(this).val());
        if(index != -1)
          filtersArray.splice(index,1);
    }

    console.log(filtersArray);

    resetContent(loadContent);

  });

  $(".load-more-content").click(function(e){

    e.preventDefault();

    if(page >= availablePages)
      return;

    page++;

    loadContent();

    });

  $(".flip-container").click(function(e){
    e.preventDefault();

    var gridItem = $(e.target).closest('.flip-container');
    console.log(gridItem);

    if(gridItem.hasClass("disabled")){
      return;
    }

    $(".flip-container").removeClass("selected");
    gridItem.addClass("selected");

    var timeToWait = 0;
    if($(".grid-item-countries").hasClass('open')){
      timeToWait = 650;
      $(".grid-item-countries").removeClass('open');
    }

    setTimeout(function() {

      var container = gridItem.data('cat-container');
      var categoriesList = gridItem.data('countries');

      var countries = categoriesList.split(',');

      console.log(countries);

      $("."+container).empty();

      for(var i=0;i<countries.length;i++){
        $('.'+container).append('<a href="#">'+countries[i]+'</a>');
      }

      $('.'+container).delay(500).addClass('open');


    }, timeToWait);

  });


  /**
  *   Question form submit
  */

  $('.question-form').submit(function() {

      $(".question-form .submit-button").css({
          pointerEvents:'none',
          opacity:0.3
        });

      $.ajax({
          data: $(this).serialize(),
          type: $(this).attr('method'),
          url: $(this).attr('action'),
          success: function(response) {
              //console.log(response);
              $(".question-form a.submit-button").remove();

              $(".question-form a.submit-button").prepend('<i class="fa fa-spinner"></i> ');
              $(".question-form a.submit-button").css({pointerEvents:'none'});

              toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {timeOut: 3000});
          },
          error:function(response){

              $(".question-form a.submit-button").css({
                pointerEvents:'auto',
                opacity:1
              });

              console.error(response.responseJSON);

              toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {timeOut: 3000});
          }
      });

      return false;
  });


});
