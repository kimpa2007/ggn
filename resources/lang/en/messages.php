<?php
    return [
        'front' => [
            'login' => [
                'login_register' => 'Login / Register',
                'login' => 'Login',
                'register' => 'Register',
                'field' => [
                    'email' =>'Email',
                    'password' => 'Password',
                    'password_confirmation' => 'Password confirmation',
                    'firstname' => 'Firstname',
                    'lastname' => 'Lastname',
                ]
            ],
            'articles' => [
                'read_more' => 'Read more'
            ],            
            'user' => [
                'orders' => [
                    'list' => 'List of orders',
                    'field' => [
                        'date' => 'Date',
                        'type' => 'Type',
                        'plan' => 'Plan',
                        'start' => 'Start',
                        'end' => 'End',
                        'content' => 'Contenu',
                        'amount' => 'Amount',
                        'status' => 'Status'
                    ]
                ],
            
                'menu' => [
                    'customer_account' => "Customer account",
                    'informations' => 'Informations',
                    'orders' => 'Orders',
                    'users' => 'Users',
                    'contact' => 'Contact',
                ]
            ],
            'menu' => [
                'carte' => 'Map'
            ]
        ]
    ]
;

