<?php
    return [
        'front' => [
            'login' => [
                'login_register' => 'Connexion / Abonnement',
                'login' => 'Connexion',
                'register' => 'Inscription',
                'field' => [
                    'email' =>'E-mail',
                    'password' => 'Mot de passe',
                    'password_confirmation' => 'Confirmation du mot de passe',
                    'firstname' => 'Prénom',
                    'lastname' => 'Nom',
                ]
            ],
            'articles' => [
                'read_more' => 'Si vous souhaitez lire la suite de l\'article, veuillez vous inscrire et vous abonnez.'
            ],
            'user' => [
                'orders' => [
                    'list' => 'Liste des commandes',
                    'field' => [
                        'date' => 'Date',
                        'type' => 'Type',
                        'plan' => 'Abonnement',
                        'start' => 'Début',
                        'end' => 'Fin',
                        'content' => 'Contenu',
                        'amount' => 'Prix',
                        'status' => 'État de la commande'
                    ]
                ],
                'menu' => [
                    'customer_account' => "Compte client",
                    'informations' => 'Informations',
                    'orders' => 'Commandes',
                    'users' => 'Utilisateurs',
                    'contact' => 'Contact',
                    'carte' => 'Carte'
                ]
            ],
            'menu' => [
                'carte' => 'Carte'
            ]
        ],
        'abonnement' => [
            'abonnezvous' => 'Abonnez-vous'
        ]

    ]
;

