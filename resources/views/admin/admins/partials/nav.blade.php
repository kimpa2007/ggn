

<div class="header">
	@if(isset($user))
    	<h1>{{$user->firstname}} {{$user->lastname}}</h1>
    @else
    	<h1>Neuf administrateur</h1>
    @endif
    
</div>



@if(isset($user))
<div class="nav tabs-inline">
	<ul class="tabs-inline">

		<li class="{{ Request::is('*account') ? 'active' : '' }}">
            <a href="{{action('Admin\Admins\AccountController@show',['id' => $user->id])}}">
            	<span class="text">
                	Compte
                </span>
            </a>
        </li>

       
        

	</ul>
</div>
@endif