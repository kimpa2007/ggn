@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('plugins/hierarchy-select/hierarchy-select.min.css') !!}
@endpush

@section('content')

{!!
	Form::open([
		'action' => 'Admin\Content\ContentController@save',
		'method' => 'POST',
		'enctype' => 'multipart/form-data'
	])
!!}

@php 
	//$langues = App\Models\Language::all()
	$langues = App\Models\Language::where('id', 1)->get();
@endphp 

<div class="header">
    <h1>{{ $content->name or 'Création de contenu' }}</h1>

    <div class="actions">
    	@if(isset($content))
    		@include('expert.partials.status',[
		    	"statusOptions" => App\Models\Content::getStatus(),
		    	"item" => $content
		    ])

			<a href="{{ LaravelLocalization::getLocalizedURL('fr', route('content-preview',$content->id)) }}" class="btn btn-status dropdown-toggle" target="_blank">Preview FR</a>
			{{--<a href="{{ LaravelLocalization::getLocalizedURL('en', route('content-preview',$content->id)) }}" class="btn btn-status dropdown-toggle" target="_blank">Preview EN</a>--}}
			<a href="https://www.globalgeonews.com/en/content-preview/{{$content->id}}" class="btn btn-status dropdown-toggle" target="_blank">Preview EN</a>

		@endif
		<input type="submit" value="Enregistrer" class="btn btn-primary submit-form" style="margin-top: 12px !important;"/>
    </div>
</div>
@php
	$types = App\Models\Content::getTypes();
@endphp


<div class="body">
	<div class="col-md-8">
	    	<div class="row lang-fields-group">
		    	<h4>Titre</h4>
		    	<div class="lateral-bar">
		    		
			    	@foreach ( $langues as $lang)
				    	<div class="col-md-12">
					    	<div class="form-group label-floating">
									<label class="control-label">Titre {{$lang->name}}</label>
									<input type="text" name="title[{{$lang->locale}}]" value="{{ isset($content) ? $content->translate($lang->locale)->title : old('title.'.$lang->locale, 'Titre ' . $lang->name) }}" class="form-control" style="font-weight:bold;font-size:20px">
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<br clear="all">

			<div class="row lang-fields-group">
				<h4>Description</h4>
				<div class="lateral-bar">
			    	@foreach ($langues as $lang)
					    <div class="col-md-12">
							<div class="form-group label-floating row">
					            <label class="control-label">Description {{$lang->name}}</label>
					            <textarea name="description[{{$lang->locale}}]" class="form-control" id="editor1_{{$lang->locale}}" rows="10" cols="80">{{  isset($content) ? $content->translate($lang->locale)->description : old('description.'.$lang->locale, 'Description ' . $lang->name) }}</textarea>
					        </div>
					    </div>
				    @endforeach
			    </div>
			</div>
			<br clear="all">

			<div class="row lang-fields-group">
			    <div class="form-group label-floating type-content type-{{App\Models\Content::TYPE_MOVIE}}" @if(isset($content) && $content->type == App\Models\Content::TYPE_MOVIE) style="display:block;" @endif >
		            <label class="control-label">{{$types[App\Models\Content::TYPE_MOVIE]}} </label>
		            <!-- filemanager -->
		            <br clear="all">
					<div class="input-group">
				    	<span class="input-group-btn">
						    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
						        <i class="fa fa-picture-o"></i> Choose
						    </a>
				    	</span>
				    	<br />
						<input name="content[{{App\Models\Content::TYPE_MOVIE}}]" id="thumbnail" type="text" class="form-control"  value="@if(isset($content) && $content->type == App\Models\Content::TYPE_MOVIE) {{$content->content}} @endif" />
			  		</div>

		   		</div>

				<div class="form-group label-floating type-content type-{{App\Models\Content::TYPE_RICHTEXT}}" @if(isset($content) && $content->type == App\Models\Content::TYPE_RICHTEXT) style="display:block;" @endif >
					<h4>Texte</h4>
					<div class="lateral-bar">
			    		@foreach ($langues as $lang)
					    	<div class="col-md-12 m-top-25">
					            <label class="control-label">{{$types[App\Models\Content::TYPE_RICHTEXT]}} {{$lang->name}}</label>
					            <textarea name="content[{{App\Models\Content::TYPE_RICHTEXT}}][{{$lang->locale}}]" class="form-control" id="editor2_{{$lang->locale}}" rows="10" cols="80">
									{{  (isset($content) && $content->type == App\Models\Content::TYPE_RICHTEXT) ? $content->translate($lang->locale)->content : old('content.RICHTEXT.'.$lang->locale) }}

					            </textarea>
		            			<br clear="all">

					    	</div>
				    	@endforeach
				    </div>
		   		</div>
			</div>
			<br clear="all">

			<div class="row lang-fields-group">
				<h4>Contact</h4>
				<div class="lateral-bar">
			    	@foreach ($langues as $lang)
					    <div class="col-md-12">
				    		<label class="control-label">Contact {{$lang->name}}</label>
							 <textarea name="contact[{{$lang->locale}}]" class="form-control" id="editor3_{{$lang->locale}}" rows="10" cols="80">
								 {{ isset($content) ? $content->translate($lang->locale)->contact : old('contact.'.$lang->locale) }}
							 </textarea>
						</div>
			    	@endforeach
			    </div>
			</div>
	    </div>

	<div class="col-md-4">

	    	<div class="row">
				<div class="col-md-12">
					<div class="form-group label-floating">
						<label class="control-label">Publiée le</label>
						{{
							Form::text(
								'published_at',
								isset($content->published_at) ? $content->published_at: null,
								[
									'class' => 'datetimepicker'
								]
							)
						}}
					</div>
				 </div>
			</div>


	    	<div class="row">
	    		<div class="col-md-12">
	                <div class="form-group label-floating">
	                    <label class="control-label">Type</label>
	                    {{
	                        Form::select(
	                            'type',
	                            [null => ''] + $types,
	                            isset($content) ? $content->type : null,
	                            [
	                                'class' => 'form-control',
	                                'id' => 'content-type-selector'
	                            ]
	                        )
	                    }}
	                </div>
	    		</div>
	    	</div>

	    	<div class="row">

				<div class="col-md-12">
					<div class="form-group label-floating">
							<label class="control-label">Latest News </label>
							<select name="latest_news" class="form-control">
								<option value="1" {{isset($content) && $content->latest_news == 1 ? 'selected' : ''}} @if(old('latest_news') == '1') selected @endif>Oui</option>
								<option value="0" {{isset($content) && $content->latest_news == 0 ? 'selected' : ''}} @if(!isset($content)) selected @endif @if(old('latest_news') == '0') selected @endif>Non</option>
						</select>
					</div>
				</div>

	    	</div>


	    	<div class="row">
				<div class="col-md-12">
					<div class="form-group label-floating">
						<label class="control-label">Auteurs</label>

							{!!
								Form::select(
									'created_by',
									$experts->pluck('full_name', 'id')->toArray(),
									isset($content->created_by) ? $content->created_by : Auth::user()->id,
									[
										'class' => 'form-control',
										'placeholder' => '---'
									]
								)
							!!}
					</div>
				</div>
			 	<div class="col-md-12">
					<div class="form-group form-hierchical-select">
							<label>Pays</label>
						@if(isset($countriesTree))
							<select name="country_id" class="form-control">
								<option selected="true" disabled="disabled">---</option>
								@foreach($countriesTree as $countrie)
									<option value="{{$countrie['id']}}" {{isset($content) && $countrie['id'] == $content->country_id ? 'selected' : ''}} @if(old('country_id') == $countrie['id']) selected @endif @if($countrie['latlng']) data-latlng="{{ json_encode(json_decode($countrie['latlng']), JSON_HEX_TAG) }}" @endif>{{$countrie['name']}}</option>
								@endforeach
							</select>
						@endif
					</div>
					<input class="hidden hidden-field" name="search_form[countrie]" readonly aria-hidden="true" type="text"/>
				</div>
				<div class="col-md-12">
					<div class="form-group label-floating">
						<label class="control-label">Content Free home </label>

						<select name="movie_on_top" class="form-control">
							<option value="1" {{isset($content) && $content->movie_on_top == 1 ? 'selected' :  ''}} @if(old('movie_on_top') == '1') selected @endif >Oui</option>
							<option value="0" {{isset($content) && $content->movie_on_top == 0 ? 'selected' :  ''}} @if(old('movie_on_top') == '0') selected @endif >Non</option>
						</select>
					</div>
				</div>


			</div>
			<br clear="all">


    		<div class="row lang-fields-group">
			    <div class="col-md-12">
					<h4>Image 1</h4>
			        {{ Form::hidden('content_id', isset($content) ? $content->id : null) }}
					@if (!Auth::user()->hasRole('super-admin'))
						{{ Form::hidden('created_by', Auth::user()->id) }} ?>
					@endif
			        {{ Form::hidden('order', isset($content) ? $content->order : null) }}

					@include('components.dropzone-image',[
						'image' => isset($content) && isset($content->image) ?
							$content->image : null,
						'size' => 'banner',
						'id' => 'dropzone-1',
						'name' => 'image',
						'resizeWidth' => 1000
					])
				</div>
				<div class="col-md-12">
					 @if (isset($content->image))
					 	<img src="{{Storage::url($content->image)}}" class="img-responsive">
					@endif
				</div>
				<div class="col-md-12">
					<h4>Légende</h4>
					<div class="lateral-bar">
			    		@foreach ($langues as $lang)
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Légende {{$lang->locale}}</label>
									<input type="text" name="legend[{{$lang->locale}}]" value="{{  isset($content) ? $content->translate($lang->locale)->legend: old('legend.'.$lang->locale) }}" class="form-control">
								</div>
							</div>
						@endforeach
					</div>
					<h4>Copyright</h4>
					<div class="lateral-bar">
			    		@foreach ($langues as $lang)
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Copyright {{$lang->locale}}</label>
									<input type="text" name="copyright[{{$lang->locale}}]" value="{{  isset($content) ? $content->translate($lang->locale)->copyright : old('copyright.'.$lang->locale) }}" class="form-control">
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<br clear="all">

			<div class="row lang-fields-group">
				<div class="col-md-12">
					<h4>Image 2</h4>
					@include('components.dropzone-image',[
						'image' => isset($content) && isset($content->image_2) ?
						$content->image_2 : null,
						'size' => 'banner',
						'id' => 'dropzone-2',
						'name' => 'image_2',
						'resizeWidth' => 1000
					])
			 	</div>
				<div class="col-md-12">
					@if (isset($content->image_2))
						<img src="{{Storage::url($content->image_2)}}" class="img-responsive">
					@endif
				</div>
				<div class="col-md-12">

					<h4>Légende</h4>
					<div class="lateral-bar">
			    		@foreach ($langues as $lang)
						 	<div class="col-md-12">
								<div class="form-group label-floating">
								 	<label class="control-label">Legende {{$lang->locale}}</label>
									<input type="text" name="legend_2[{{$lang->locale}}]" value="{{ isset($content) ? $content->translate($lang->locale)->legend_2 : old('legend_2.'.$lang->locale) }}" class="form-control">
								</div>
							</div>
						@endforeach
					</div>

					<h4>Copyright</h4>
					<div class="lateral-bar">
			    		@foreach ($langues as $lang)
				 			<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Copyright {{$lang->locale}}</label>
									<input type="text" name="copyright_2[{{$lang->locale}}]" value="{{ isset($content) ? $content->translate($lang->locale)->copyright_2 : old('copyright_2.'.$lang->locale) }}" class="form-control">
								</div>
					 		</div>
					 	@endforeach
					</div>
				</div>
	 		</div>
 			<br clear="all">

			<div class="row">
				<div class="col-md-12">
					<h4>Thèmes associés</h4>
					<div class="form-group label-floating">
						@php
							$themes = App\Models\Theme::all();

						@endphp
						@foreach($themes as $th)
							@php
								$checked = '';
                                if(is_array(old('theme')) && in_array($th->id, old('theme'))) $checked = 'checked';
							@endphp
							<div>
								{{
									Form::checkbox(
											'theme[]',
											$th->id,
											isset($content) ? $content->themes->contains($th->id) : $checked
									)
								}}
								<label for="checkbox">{{ $th->translate('fr')->name }}</label>
							</div>
						@endforeach
					</div>
				</div>
	    	</div>

	   	</div>

		<br clear="all">
<div class="row">
	<div class="col-md-12">
		<h4>Afficher sur la carte</h4>

			<label>{!! Form::radio('has_map', 1, isset($content) && $content->has_map == 1 ? true : false, ['id' => 'has_map_yes']) !!} Oui</label>
			<label>{!! Form::radio('has_map', 0, !isset($content) || (isset($content) && !$content->has_map) == 1 ? true : false, ['id' => 'has_map_no']) !!} Non</label>
			<br />
			<script>
				$('input[name="has_map"]').on('click', function(e){
					if($(this).val() == 1) {
						$('.map-options').show();
					} else {
						$('.map-options').hide();
					}
				})
			</script>
			<div class="map-options">

				<label>Position :</label>

				@php
					$latlng = isset($countrie['latlng']) ? json_encode(json_decode($countrie['latlng']), JSON_HEX_TAG) : null;
				@endphp

				{!!
					Form::text(
						'latlng',
						(isset($content)) && $content->map ? $content->map->latlng : $latlng,
						[
							'readonly' => true,
							//'style' => 'display: none'
						]
					)
				!!}
				<div id="marker-map">
					</div>

				<label>Légende :</label>

				<select name="legend_id" placeholder="---">
					<option value="">---</option>
					@foreach(App\Models\MapLegend::all() as $legend)
						<option value="{{$legend->id}}" data-key="{{$legend->key}}" @if( ((isset($content)) && isset($content->map)) && $content->map->legend->id == $legend->id ) selected @endif>{{$legend->name}}</option>
					@endforeach()
				</select>

				<br />
				<label class="control-label">Afficher du :</label>
				{{
					Form::text(
						'map_display_from',
						(isset($content)) && $content->map ? $content->map->start_at : Carbon\Carbon::today(),
						[
							'class' => 'datepicker-map'
						]
					)
				}}
				<br />
				<label class="control-label">Au : </label>
				{{
					Form::text(
						'map_display_to',
						(isset($content)) && $content->map ? $content->map->end_at : Carbon\Carbon::today()->addDays(3),
						[
							'class' => 'datepicker-map'
						]
					)
				}}
			</div>
			<hr />

			<script>
				@if((!isset($content)) || ((isset($content)) && $content->has_map == 0))
					setTimeout(function(){
						$('.map-options').hide();
					}, 2000);
				@endif
			</script>

	</div>
</div>
		<div class="row">
			<div class="col-md-12">
				<h4>Mots clés associés</h4>

				<div class="col-md-12">
						<div class="row">
							<div class="col-md-4">
								<input type="text" name="filters" id="tags-filters" value="" placeholder="Recherche rapide" class="form-control" />
							</div>
						</div>
					</div>

					<script>
						$('input[name="filters"]').keyup(function(){
							var valThis = $(this).val().toLowerCase();
							if(valThis == ""){
								$('.tag').show();
							} else {
								$('.tag').each(function(){
									var text = $(this).find('.text').text().toLowerCase();
									(text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
								});
							};
						});
					</script>

				@foreach(App\Models\Tag::all() as $tag)
					@php
						$checked = is_array(old('tags')) && in_array($tag->id, old('tags')) ? 'checked' : null;
					@endphp
					<div class="col-md-4 tag">
						<label>
							{{
								Form::checkbox(
									'tags[]',
									$tag->id,
									isset($content) ? in_array($tag->id, $content->tags->pluck('id')->toArray()) : $checked
								)
							}}
							<span class="text">{{ $tag->name }}</span>
						</label>
					</div>
				@endforeach()
			</div>
		</div>

			</div>
		</div>
	    </div>
	</div>
</div>

{!! Form::close() !!}

@endsection

@push('stylesheets')
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<link rel="stylesheet" media="all" href="{{ asset('css/leaflet.css')}}" />

	<style>

	#marker-map {
	    background-color: #000;
	    width: 100%;
	    height: 400px;
	    position: relative;
	    z-index: 0;
	}

	.dropzone-1 {
		width: 100px;
		height: 100px;
		min-height: 0px !important;
	}
	</style>
@endpush


@push('javascripts-libs')
	<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

	{!! Html::script('js/libs/leaflet.js') !!}
	{!! Html::script('plugins/ckeditor/ckeditor.js') !!}
	{!! Html::script('plugins/hierarchy-select/hierarchy-select.js') !!}
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
	{!! Html::script('/js/home/app.js') !!}
	{!! Html::script('/js/home/app.markerMap.js') !!}

	<script>
		var WEBROOT = "{{asset('')}}";
	</script>
@endpush

@push('javascripts')

	@php
		$lat = '46.2276';
		$lng = '2.2137';

		if(isset($content)) {
			$latlng = isset($content->country->latlng) ? json_decode($content->country->latlng) : null;
			$lat = isset($latlng->lat) ? $latlng->lat : '46.2276';
			$lng = isset($latlng->lng) ? $latlng->lng : '2.2137';
		}
	@endphp


	<script>
		$(document).ready(function(){

			app.markerMap.init({
				center : [{{$lat}},{{$lng}}],
				locale : 'fr-FR',
				onMarkerChange : function(latLng){
					$('input[name="latlng"]').val(JSON.stringify(latLng));
				}
			});

			$('select[name=country_id]').on('change', function(e){
				var geo = $(this).find(':selected').attr('data-latlng');
				var geo = geo !== undefined ? JSON.parse(geo) : null;

				if(geo.lat && geo.lng) {
					app.markerMap.moveTo(geo.lat, geo.lng);
					app.markerMap.moveMarkerTo(geo.lat, geo.lng);
				}
			});

			// if($('input[name="latlng"]').val()) {
			// 	var geo = $('input[name="latlng"]').val() ? JSON.parse($('input[name="latlng"]').val()) : null;
			// 	if(geo.lat && geo.lng) {
			// 		app.markerMap.moveTo(geo.lat, geo.lng);
			// 		app.markerMap.moveMarkerTo(geo.lat, geo.lng);
			// 	}
			// }

			$('select[name="latest_news"]').on("change", function(e){
				if($(this).val() == 1) {
					$('.map-options').show();
					$('#has_map_yes').attr('checked', 'checked');
					$('#has_map_no').attr('checked', '');
					$('select[name="legend_id"]').val($('select[name="legend_id"]').find('option[data-key="HOT_NEWS"]').val());
				}
			});
		});

	</script>

 <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script>

$('input[name="filters"]').keyup(function(){
	var valThis = $(this).val().toLowerCase();
	if(valThis == ""){
		$('.tag').show();
	} else {
		$('.tag').each(function(){
			var text = $(this).find('.text').text().toLowerCase();
			(text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
		});
	};
});


$('#lfm').filemanager('file');

    CKEDITOR.disableAutoInline = true;
        @foreach ($langues as $lang)
	       CKEDITOR.inline( 'editor1_{{$lang->locale}}' );
        @endforeach


    @foreach ($langues as $lang)
		CKEDITOR.replace( 'editor2_{{ $lang->locale }}', {
		 toolbar: [
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			{ name: 'editing', groups: [ 'find', 'selection' ] },
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
			{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			'/',
			{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			{ name: 'others', items: [ '-' ] },
			{ name: 'about', items: [ 'About' ] }
		]
		});
		CKEDITOR.replace( 'editor3_{{ $lang->locale }}', {
		 toolbar: [
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			{ name: 'editing', groups: [ 'find', 'selection' ] },
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			{ name: 'links', items: [ 'Link', 'Unlink' ] },
			{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			{ name: 'others', items: [ '-' ] }

		]
		});
	@endforeach
		/**/

    var csrf_token = "{{ csrf_token() }}";

    var country_id = null;
    @if(isset($content))
    	country_id = {{$content->country_id}};
    @endif

    var routes = {
    	changeStatus : "{{action('Admin\Content\ContentController@changeStatus')}}"
    };

	$('.datepicker-map').daterangepicker({
		singleDatePicker: true,
		timePicker: false,
	   showDropdowns: true,
	   timePickerIncrement: 5,
	   locale: {
            format: 'YYYY-MM-DD'
        }
	});

	$('.datetimepicker').daterangepicker({
		singleDatePicker: true,
		timePicker: true,
	   showDropdowns: true,
	   timePickerIncrement: 5,
	   locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        }
	});
</script>

{!! Html::script('/js/admin/contents/form.js') !!}


@endpush
