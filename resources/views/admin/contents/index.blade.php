@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Contenus
    </h1>

    <a href="{{ action('Admin\Content\ContentController@create') }}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Titre</th>
                <th>Date de publication</th>
                <th class="filter-top-free">Free Home</th>
                <th class="filter-latest-news">Latest News</th>
        		<th>Countrie</th>
                <th class="filter-themes" data-themes="{{ json_encode(App\Models\Theme::all()) }}">Themes</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
  </table>

</div>
@endsection

@push('javascripts')

<script>

	var routes = {
		getData : '{{ action("Admin\Content\ContentController@getData") }}',
		deleteItem : '{{ action("Admin\Content\ContentController@delete") }}'
    };

    var csrf_token = "{{ csrf_token() }}";

</script>

{!! Html::script('/js/admin/contents/index.js') !!}

@endpush
