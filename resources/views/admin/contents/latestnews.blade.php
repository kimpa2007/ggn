@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Latest News
    </h1>
    <a href="{{ action('Admin\Content\ContentController@create') }}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
  <div id="contentTrie">
    <ul class="ui-sortable" style="list-style: none;">
    	@php
        foreach ($contents as $value) {
          echo '	<li id="'.$value['id'].'" >
          <div class="row  bg-white" style=" border:1pt solid rgb(85, 85, 85);">
          <div class="col-md-10">'.$value['id'].' - <a href="' . action('Admin\Content\ContentController@show', ['id' => $value['id']]) . '"> '.$value['title'].'</a></div>
        	    <div class="col-md-2">
           <img src="'.Storage::url($value['image']).'" class="img-responsive">
            </div>
            </li>
           ';
        }
      @endphp
    </ul>
</div>

</div>
@endsection

@push('javascripts')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
$(document).ready(function(){
					$(function() {
						$("#contentTrie ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
								/*var order = $(this).sortable("serialize");
								$.post("{{ action("Admin\Content\ContentController@updateOrderLatestnews") }}", order, function(theResponse){
									$("#contentRight").html(theResponse);
								});*/
                $.ajax({
          				type: 'POST',
          				url: routes.updateOrder,
          				data: {
          					_token: csrf_token,
          					order : $(this).sortable('toArray')
          				},
          				dataType: 'html',
          				success: function(data){
          					console.log(data);
          						var rep = JSON.parse(data);
          						if(rep.code == 200){
          								toastr.success('Enregistrement effectué avec succès.', 'Sauvegardé !', {timeOut: 3000});
          						}
          						else if(rep.code == 400){
          							toastr.error('Une erreur s\'est produite lors de l\'enregistrement', 'Erreur !', {timeOut: 3000});
          						}
          						else {
          							//nothing to change
          						}
          				}
          			});

							}
						});
					});

				});

        var routes = {
          updateOrder : '{{ action("Admin\Content\ContentController@updateOrderLatestnews") }}',
          };
          var csrf_token = "{{ csrf_token() }}";

</script>

{!! Html::script('/js/admin/contents/latestnewsindex.js') !!}

@endpush
