@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
	{!! Html::style('/css/jquery-ui.min.css') !!}
@endpush

@section('content')

@include('admin.countries.partials.nav')
<div class="body">
    <div class="row">
        <div class="col-md-12 form-container">

        </div>
    </div>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Ordre</th>
                <th>#</th>
                <th>Titre</th>
				<th>Countrie</th>
				<th>Views</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Filtrer par : </th>
                <th></th>
                <th></th>
				<th></th>
				<th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts-libs')

@endpush

@push('javascripts')
    <script>

    	var routes = {
    		getData : '{{ action("Admin\Country\CountryController@getContentsData", $countrie->id) }}',
    		deleteItem : '{{ action("Admin\Content\ContentController@delete") }}',
        	updateOrder : '{{ action("Admin\Content\ContentController@updateOrder") }}'
        };

        var csrf_token = "{{ csrf_token() }}";

    </script>

    {!! Html::script('/js/admin/countries/contents.js') !!}
@endpush
