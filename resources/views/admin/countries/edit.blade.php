@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
	{!! Html::style('/css/jquery-ui.min.css') !!}
@endpush

@section('content')

@include('admin.countries.partials.nav')

<div class="body">
    <div class="row">
        <div class="col-md-12 form-container">
        	{!! Form::open(['url' => action('Admin\Country\CountryController@save'), 'files'=>true, 'method' => 'POST']) !!}
	    		{{ csrf_field() }}

        		<input type="hidden" name="id" value="{{ $countrie->id or '' }}" >


			    <div class="col-md-8">
			    	<div class="row lang-fields-group">
				    	<h4>Nom</h4>
				    	<div class="lateral-bar">
					    	@foreach ( App\Models\Language::all() as $lang)
						    	<div class="col-md-12">
							    	<div class="form-group label-floating">
											<label class="control-label">Nom {{$lang->name}}</label>
											<input type="text" name="name[{{$lang->locale}}]" value="{{ isset($countrie) && !count($errors) ? $countrie->translate($lang->locale)->name : old('name.'.$lang->locale) }}" class="form-control" >
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<br clear="all">

					<div class="row lang-fields-group">
				    	<h4>Slug</h4>
				    	<div class="lateral-bar">
					    	@foreach ( App\Models\Language::all() as $lang)
						    	<div class="col-md-12">
							    	<div class="form-group label-floating">
											<label class="control-label">Slug {{$lang->name}}</label>
											<input type="text" name="slug[{{$lang->locale}}]" value="{{ isset($countrie) && !count($errors) ? $countrie->translate($lang->locale)->slug : old('slug.'.$lang->locale) }}" class="form-control">
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<br clear="all">

					<div class="row lang-fields-group">
						<h4>Description</h4>
						<div class="lateral-bar">
					    	@foreach ( App\Models\Language::all() as $lang)
							    <div class="col-md-12">
									<div class="form-group label-floating row">
							            <label class="control-label">Description {{$lang->name}}</label>
							            <textarea name="description[{{$lang->locale}}]" class="form-control" id="editor1_{{$lang->locale}}" rows="10" cols="80">{{  isset($countrie) ?$countrie->translate($lang->locale)->description : old('description.'.$lang->locale) }}</textarea>
							        </div>
							    </div>
						    @endforeach
					    </div>
					</div>
					<br clear="all">

					<div class="row lang-fields-group">
						<div class="col-md-6">
							<div>
								<h4>Image</h4>
				        		@include('components.dropzone-image',[
						        	'image' => isset($countrie) && isset($countrie->image) ? $countrie->image : null,
						        	'size' => 'banner',
						        	'id' => 'dropzone-1',
						        	'name' => 'image'
						        ])
						    </div>
						   
							<br clear="all">

							<div class="row lang-fields-group">
						    	<h4>Legend</h4>
						    	<div class="lateral-bar">
							    	@foreach ( App\Models\Language::all() as $lang)
								    	<div class="col-md-12">
									    	<div class="form-group label-floating">
													<label class="control-label">Legned {{$lang->name}}</label>
													<input type="text" name="legend[{{$lang->locale}}]" value="{{ isset($countrie) ? $countrie->translate($lang->locale)->legend : '' }}" class="form-control">
											</div>
										</div>
									@endforeach
								</div>
							</div>
							<br clear="all">
							<div class="row lang-fields-group">
						    	<h4>Copyright</h4>
						    	<div class="lateral-bar">
							    	@foreach ( App\Models\Language::all() as $lang)
								    	<div class="col-md-12">
									    	<div class="form-group label-floating">
													<label class="control-label">Copyright {{$lang->name}}</label>
													<input type="text" name="copyright[{{$lang->locale}}]" value="{{ isset($countrie) ? $countrie->translate($lang->locale)->copyright : '' }}" class="form-control">
											</div>
										</div>
									@endforeach
								</div>
							</div>
							<br clear="all">					
		                </div>
						<div class="col-md-6">
							 @if (isset($countrie->image))
							 	<img src="{{Storage::url($countrie->image)}}" class="img-responsive">
							@endif
						</div>
					</div>
				</div>
				<div class="col-md-4">
				<div class="form-group label-floating text-right">
		                <input type="submit" value="Save" class="btn btn-primary submit-form"/>
		            </div>

					<div class="form-group label-floating">
	                    <label class="control-label">Status</label>
	                    <select class="form-control" name="status">

	                    	@foreach( App\Models\Country::getStatus() as $key => $value )
	                    		<option value="{{$key}}" {{isset($countrie) && $countrie->status == $key ? 'selected' : ''}} >{{$value}}</option>
	                    	@endforeach
	                    </select>
	                	<span class="material-input"></span>
	                </div>

					<div class="form-group label-floating">
						<label class="control-label">Alignement de la photo</label>
						<select name="image_align" class="form-control">
							<option value="top" {{isset($countrie) && $countrie->image_align == 'top' ? 'selected' : ''}}>Haut</option>
							<option value="center" {{isset($countrie) && $countrie->image_align == 'center' ? 'selected' : ''}}>Center</option>
							<option value="bottom" {{isset($countrie) && $countrie->image_align == 'bottom' ? 'selected' : ''}}>Bas</option>
						</select>
					</div>


				</div>            

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
	{!! Html::script('plugins/ckeditor/ckeditor.js') !!}

	{!! Html::script('/js/libs/speakingurl.js') !!}
	{!! Html::script('/js/libs/slugify.js') !!}
	{!! Html::script('/js/libs/jquery-ui.min.js') !!}
@endpush

@push('javascripts')
	<script>

		CKEDITOR.disableAutoInline = true;

		@foreach ( App\Models\Language::all() as $lang)
	    	CKEDITOR.inline( 'editor1_{{ $lang->locale }}' );
	    @endforeach

	    var csrf_token = "{{csrf_token()}}";

	    var countryId = {{isset($countrie) ? $countrie->id : 'null'}};
	    var parentId = {{isset($countrie) && $countrie->parent_id != null ? $countrie->parent_id : 'null'}};


	</script>

	{!! Html::script('/js/admin/countries/form.js') !!}


@endpush
