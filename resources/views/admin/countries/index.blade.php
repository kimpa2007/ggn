@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des pays
    </h1>

    <a href="{{action('Admin\Country\CountryController@create')}}" class="btn btn-primary">Ajouter</a>
</div>

<div class="body">

  <table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Order</th>
                <th>#</th>
                <th>Pays</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
@endsection

@push('javascripts-libs')

	{{ Html::script('js/libs/jquery-sortable-min.js')}}

@endpush

@push('javascripts')
<script>

var csrf_token = "{{csrf_token()}}";

var routes = {
	getData : '{{ action("Admin\Country\CountryController@getData") }}',
	deleteItem : '{{ action("Admin\Country\CountryController@delete") }}',
	updateOrder : '{{ action("Admin\Country\CountryController@updateOrder") }}'
};

</script>

{{ Html::script('js/admin/countries/index.js')}}

@endpush
