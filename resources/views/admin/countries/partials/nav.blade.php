<div class="header">
	@if(isset($countrie))
    	<h1>{{$countrie->name}}</h1>
    @else
    	<h1>New countrie</h1>
    @endif
</div>

@if(isset($countrie))
<div class="nav tabs-inline">
	<ul class="tabs-inline">

		<li class="{{ Request::is('*countries*') && !Request::is('*contents*') ? 'active' : '' }}">
              <a href="{{action('Admin\Country\CountryController@show', ['id' => $countrie->id])}}">
            	<span class="text">
                	Pays
                </span>
            </a>
        </li>
        <li class="{{ Request::is('*contents*') ? 'active' : '' }}">
            <a href="{{ action('Admin\Country\CountryController@contents', ['id' => $countrie->id]) }}">
                <span class="text">
                	Contenu
                </span>
            </a>
        </li>
	</ul>
</div>
@endif
