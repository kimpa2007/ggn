@extends('layouts.app')

@section('content')

{!!
    Form::open([
        'url' => isset($page)
            ? route('admin.custompage.update', $page->id)
            : route('admin.custompage.store'),
        'method' => isset($page) ? 'PUT' : 'POST'
    ])
!!}

<div class="header">
    <h1>{{ isset($page) ? 'Edition page' : 'Nouvelle page'}}</h1>
    <div class="actions">
		<input type="submit" value="Enregistrer" class="btn btn-primary submit-form" style="margin-top: 12px !important;"/>
    </div>
</div>


<div class="body">
    <div class="row">
        <div class="col-md-12 ">

            <div class="col-md-8 bg-white form-container">
                <div class="row lang-fields-group">
                    <h4>Titre</h4>
                    <div class="lateral-bar">
                        @foreach ( App\Models\Language::all() as $lang)
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Titre en {{$lang->name}}</label>
                                    <input type="text" name="title[{{$lang->locale}}]" value="{{ (isset($page)) && $page->translate($lang->locale) ? $page->translate($lang->locale)->title : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <br clear="all">

                <div class="row lang-fields-group">
                    <h4>Slug</h4>
                    <div class="lateral-bar">
                        @foreach ( App\Models\Language::all() as $lang)
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Slug en {{$lang->name}}</label>
                                    <input type="text" name="slug[{{$lang->locale}}]" value="{{ (isset($page)) && $page->translate($lang->locale) ? $page->translate($lang->locale)->slug : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <br clear="all">

                <div class="row lang-fields-group">
                    <h4>Content</h4>
                    <div class="lateral-bar">
                        @foreach ( App\Models\Language::all() as $lang)
                            <div class="col-md-12">
                                <label class="control-label">Content {{$lang->locale}}</label>
                                <div class="form-group label-floating row">

                                    <textarea name="content[{{$lang->locale}}]" class="form-control" id="editor1_{{$lang->locale}}" rows="10" cols="80">{{  (isset($page)) && $page->translate($lang->locale) ?$page->translate($lang->locale)->content : '' }}</textarea>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <br clear="all">
            </div>

            <div class="col-md-4">
                <div class="bg-white form-container">
                    <div class="form-group">
                        <h4>Identifier</h4>
                        <label class="control-label">Identifiant unique de la page :</label>
                        <input type="text" name="identifier" value="{{ isset($page) ? $page->identifier : '' }}" class="form-control">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@push('javascripts')
    {!! Html::script('plugins/ckeditor/ckeditor.js') !!}
    <script>
        $(function(){
            CKEDITOR.disableAutoInline = true;
            @foreach ( App\Models\Language::all() as $lang)
                CKEDITOR.replace( 'editor1_{{ $lang->locale }}', {
                     toolbar: [
                        { name: 'document', groups: ['source', 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                        { name: 'editing', groups: [ 'find', 'selection' ] },
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
                        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                        '/',
                        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                        { name: 'others', items: [ '-' ] },
                        { name: 'about', items: [ 'About' ] }
                    ]
                    });
            @endforeach


        });
    </script>
@endpush
