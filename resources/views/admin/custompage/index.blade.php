@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des pages statiques
    </h1>

    <a href="{{route('admin.custompage.create')}}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Page</th>
                <th>Slug</th>
                <th></th>
            </tr>
        </thead>
    </table>

</div>
@endsection

@push('javascripts')
<script>

var csrf_token = "{{csrf_token()}}";

var routes = {
	getData : '{{ route("admin.custompage.data") }}',
    deleteItem : '{{ route("admin.custompage.index") }}'
};

</script>

{{ Html::script('js/admin/custom/index.js')}}

@endpush
