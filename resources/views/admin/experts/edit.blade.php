@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
@endpush

@section('content')

@include('admin.experts.partials.nav')


<div class="body">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 bg-white form-container">

        	<!-- Dropzone Avatar -->

        	{!! Form::open(['url' => action('Admin\Expert\AccountController@save'), 'files'=>true, 'method' => 'POST']) !!}


	    		{{ csrf_field() }}

	        	@include('components.dropzone-image',[
					'image' => isset($user) && isset($user->image) ?
						$user->image : null,
					'size' => 'avatar',
					'id' => 'dropzone-1',
					'name' => 'image',
					'resizeWidth' => 500
				])

				<input type="hidden" name="id" value="{{ $user->id or '' }}" >

	            <div class="row">
	                <div class="col-md-6">
	                    <div class="form-group label-floating">
	                        <label class="control-label">Prénom</label>
	                        <input type="text" name="firstname" value="{{ $user->firstname or '' }}" class="form-control">
	                    </div>
	                </div>

	                <div class="col-md-6">
	                    <div class="form-group label-floating">
	                        <label class="control-label">Nom</label>
	                        <input type="text" name="lastname" value="{{ $user->lastname or '' }}" class="form-control"/>
	                    </div>
	                </div>
	            </div>

				<div class="row">
					<div class="col-md-6">
		                <div class="form-group label-floating">
		                    <label class="control-label">Zone</label>
		                    {{
		                        Form::select(
		                            'zone_id',
		                            App\Models\ZoneTranslation::where('locale','fr')->pluck('name', 'zone_id'),
		                            isset($user) ? $user->expertZones()->first()->id : null,
		                            [
		                                'class' => 'form-control'
		                            ]
		                        )
		                    }}
		                </div>
		            </div>

		        	<div class="col-md-6">
		        		<div class="form-group label-floating">
			                <label class="control-label">E-mail</label>
			                <input type="text" name="email" value="{{ $user->email or '' }}" class="form-control"/>
			            </div>
		        	</div>
				</div>


	            <div class="form-group label-floating">
	                <label class="control-label">Mot de passe</label>
	                <input type="password" name="password" value="" class="form-control"/>
	            </div>

	            <div class="form-group label-floating">
	                <label class="control-label">Confirmation du mot passe</label>
	                <input type="password" name="password_confirmation" value="" class="form-control"/>
	            </div>

	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush
