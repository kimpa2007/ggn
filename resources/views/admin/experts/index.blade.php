@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
       Liste des contributeurs
    </h1>

    <a href="{{ action('Admin\Expert\AccountController@create') }}" class="btn btn-primary">Ajouter</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Order</th>
                <th>Nom/Prénom</th>
                <th>Zone</th>
				        <th>Ajouté le</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts')
<script>

var csrf_token = "{{csrf_token()}}";

var routes = {
	getData : '{{ action("Admin\Expert\AccountController@getData") }}',
	deleteItem : '{{ action("Admin\Expert\AccountController@delete") }}',
  updateOrder : '{{ action("Admin\Expert\AccountController@updateOrder") }}'
};

</script>

{{ Html::script('js/admin/experts/index.js')}}

@endpush
