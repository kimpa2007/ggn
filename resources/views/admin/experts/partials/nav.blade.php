

<div class="header">
	@if(isset($user))
    	<h1>{{$user->firstname}} {{$user->lastname}}</h1>
    @else
    	<h1>Nouveau contributor</h1>
    @endif

</div>


@if(isset($user))
<div class="nav tabs-inline">
	<ul class="tabs-inline">

		<li class="{{ Request::is('*account') ? 'active' : '' }}">
            <a href="{{action('Admin\Expert\AccountController@show',['id' => $user->id])}}">
            	<span class="text">
                	Compte
                </span>
            </a>
        </li>
        <li class="{{ Request::is('*profile') ? 'active' : '' }}">
            <a href="{{ action('Admin\Expert\ProfileController@index', ['id' => $user->id]) }}">
                <span class="text">
                	Fiche
                </span>
            </a>
        </li>
	</ul>
</div>
@endif
