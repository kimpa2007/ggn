@extends('layouts.app')

@section('content')

@include('admin.experts.partials.nav')

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
        	{!! Form::open(['url' => action('Admin\Expert\ProfileController@save'), 'files'=>true, 'method' => 'POST']) !!}
	    		
	    		{{ csrf_field() }}
	    		
	    		<input type="hidden" name="id" value="{{ $user->id or '' }}" >
	    		
	    		<div class="row">
	    			<div class="col-md-3">
	    				
	    				@include('components.dropzone-image',[
							'image' => isset($user->profile) && isset($user->profile->image) ? 
								$user->profile->image : null,
							'size' => 'avatar',
							'id' => 'dropzone-1',
							'name' => 'image',
							'resizeWidth' => 500
						])
	    				
					</div>
					<div class="row lang-fields-group">
		                <h4>Contenu</h4>
		                <div class="lateral-bar">
		                    @foreach ( App\Models\Language::all() as $lang)
		                        <div class="col-md-12">
		                            <div class="form-group label-floating">
		                                <label class="control-label">Contenu en {{$lang->name}}</label>
		                                {{
					                        Form::textarea(
					                            'content['.$lang->locale.']',
					                            $user->profile && null !== $user->profile->translate($lang->locale) ? $user->profile->translate($lang->locale)->content : '',
					                            [
					                                'id' => 'editor_'.$lang->locale,
					                                'placeholder'=> 'Ecrivez votre description ici...'
					                            ]
					                        )
					                    }}
		                            </div>
		                        </div>
		                    @endforeach
		                </div>
		            </div>
                	<br clear="all">
					
				</div>

	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush

@push('javascripts')
<script src="/plugins/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.disableAutoInline = true;
    @foreach ( App\Models\Language::all() as $lang)
    	CKEDITOR.inline( 'editor_{{$lang->locale}}' );
    @endforeach	
</script>



@endpush
