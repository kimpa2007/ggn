@extends('layouts.app')

@section('content')

<div class="body dashboard" id="table_wrapper" >

  <div class="row">
    <div class="col-md-3">
      <div class="small-block">
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <div class="block-body">
          <div class="number">
            {{$users->count()}}
          </div>
          <div class="text">
            Users
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="small-block">
        <div class="icon">
          <i class="fa fa-user-md"></i>
        </div>
        <div class="block-body">
          <div class="number">
            {{$experts->count()}}
          </div>
          <div class="text">
            Contributeurs
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="small-block">
        <div class="icon">
          <i class="fa fa-user-o"></i>
        </div>
        <div class="block-body">
          <div class="number">
            {{$partners->count()}}
          </div>
          <div class="text">
            Clients
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="small-block">
        <div class="icon">
          <i class="fa fa-th-list"></i>
        </div>
        <div class="block-body">
          <div class="number">
            {{$zones->count()}}
          </div>
          <div class="text">
            Zones
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">

    <div class="col-md-6">
      <h4><i class="fa fa-file-o"></i> &nbsp; Contenus ({{$contents->count()}})</h4>
      <div class="table-container">
        <table class="table" style="background:#FFF;">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Nom</th>
                      <th>Date</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($contents->get() as $content )
                <tr class="table-row" onclick="window.document.location='{{route('admin.content.show',["id" => $content->id])}}';">
                    <td>{{$content->id}}</td>
                    <td>{{$content->title}}</td>
                    <td>{{$content->created_at->format('d/m/Y')}}</td>
                </tr>
                @endforeach

              </tbody>
          </table>
        </div>
    </div>

    <div class="col-md-6">

    </div>

      <div class="col-md-6">

      </div>

      <div class="col-md-6">

      </div>

    <div class="col-md-6">
    </div>

  </div>

</div>

@endsection

@push('javascripts')

@endpush
