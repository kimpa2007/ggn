@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des factures
    </h1>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Partenaire</th>
                <th>Requested at</th>
                <th>Montant</th>
                <th>Type</th>
                <th>Statut</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ route("admin.invoices.data") }}',
    };
</script>
{!! Html::script('/js/admin/invoices/index.js') !!}
@endpush
