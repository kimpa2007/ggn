@extends('layouts.app')

@section('content')
    <div class="header">
        <h1>
            Map legend
        </h1>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 bg-white form-container">
                {!!
                    Form::open([
                        'url' => isset($mapLegend)
                            ? action('Admin\MapLegend\MapLegendController@update',$mapLegend)
                            : action('Admin\MapLegend\MapLegendController@store'),
                        'method' => 'POST',
                        'class' => 'form',
                        'enctype' => 'multipart/form-data'
                    ])
                !!}

                @if(isset($mapLegend))
                    <input type="hidden" name="_method" value="PUT" />
                @endif

                <div class="form-group">
                    <strong>Icon</strong> :
                    @include('components.dropzone-image',[
          						'image' => isset($mapLegend) && isset($mapLegend->icon) ? $mapLegend->icon : null,
          						'size' => 'icon',
          						'id' => 'dropzone-1',
          						'name' => 'icon',
          						'resizeWidth' => 32
          					])
                </div>

                <div class="form-group label-floating">
                        <label class="control-label">Couleur</label>

                        {!!
                            Form::text(
                                'color',
                                isset($mapLegend) ? $mapLegend->color : old('color'),
                                [
                                    'class' => 'form-control'
                                ]
                            )
                        !!}

                        {{-- <input type="text" name="title[{{$lang->locale}}]" value="{{ isset($content) ? $content->translate($lang->locale)->title : old('title.'.$lang->locale) }}" class="form-control" style="font-weight:bold;font-size:20px"> --}}
                </div>


                <div class="form-group label-floating">
                    <label class="control-label">Cléé</label>

                    {!!
                        Form::text(
                            'key',
                            isset($mapLegend) ? $mapLegend->key : old('key'),
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>

                <div class="row lang-fields-group">
    		    	<h4>Nom</h4>
    		    	<div class="lateral-bar">
    			    	@foreach ( App\Models\Language::all() as $lang)
    				    	<div class="col-md-12">
    					    	<div class="form-group label-floating">
    									<label class="control-label">Nom {{$lang->name}}</label>

                                        {!!
                                            Form::text(
                                                'name['.$lang->locale.']',
                                                isset($mapLegend) ? $mapLegend->translate($lang->locale)->name : old('name.'.$lang->locale),
                                                [
                                                    'class' => 'form-control'
                                                ]
                                            )
                                        !!}

    									{{-- <input type="text" name="title[{{$lang->locale}}]" value="{{ isset($content) ? $content->translate($lang->locale)->title : old('title.'.$lang->locale) }}" class="form-control" style="font-weight:bold;font-size:20px"> --}}
    							</div>
    						</div>
    					@endforeach
    				</div>
    			</div>
    			<br clear="all">

                <div class="form-group">
                    <strong>Status</strong> :
                    {!!
                        Form::select(
                            'status',
                            App\Models\MapLegend::getStatus(),
                            isset($mapLegend) ? $mapLegend->status : null,
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>


                <p align="right">
                {!!
                    Form::submit('Save', [
                        'class' => 'btn btn-primary'
                    ])
                !!}
                </p>


                {!! Form::close() !!}
            </div>
        </div>

    </div>
@endsection

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
	{!! Html::style('/css/jquery-ui.min.css') !!}

@endpush

@push('javascripts-libs')
    {!! Html::script('/js/dropzone.min.js') !!}
    {!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush
