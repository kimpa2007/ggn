@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Map legends
    </h1>

    <a href="{{ action('Admin\MapLegend\MapLegendController@create') }}" class="btn btn-primary">Create</a>
</div>

<div class="body">
    <table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Icon</th>
                <th>Name</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
@endsection

@push('javascripts')
    <script>
    	var routes = {
    		getData : '{{ action("Admin\MapLegend\MapLegendController@getData") }}'
        };
        var csrf_token = "{{ csrf_token() }}";
    </script>

    {!! Html::script('/js/admin/mapslegends/index.js') !!}
@endpush
