@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des commandes de crédits
    </h1>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Marque blanche</th>
                <th>Commande du</th>
                <th>Payée le</th>
                <th>Paiement</th>
                <th>Crédits</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Filtrer par : </th>
                <th></th>
                <th></th>
        				<th></th>
        				<th></th>
                <th></th>
        				<th></th>
        				<th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ route("admin.orders.credit.data") }}',
    };
</script>
{!! Html::script('/js/admin/orders/credits.js') !!}
@endpush
