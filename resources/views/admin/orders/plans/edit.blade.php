@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        @if(isset($order))
            Commande de l'offre "{{ $order->plan->name }}" de {{ $order->partner->name }}
        @endif
    </h1>
</div>


@if(isset($order))
    @include('admin.partners.nav', [
        "partner" => $order->partner
    ])
@endif

<div class="body">


    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-primary">

                <div class="panel-heading">
                    Commande #{{$order->id}}
                </div>

                <div class="panel-body list-group">

                    <div class="list-group-item">
                        <strong>Offre </strong> :
                        {{$order->plan->name}} 
                    </div>

                    @if ($order->ecole != '')
                        <div class="list-group-item">
                            <strong>École </strong> :
                            {{ $order->ecole }}
                        </div>
                    @endif

                    @if ($order->cursus != '')
                        <div class="list-group-item">
                            <strong>Cursus </strong> :
                            {{ $order->cursus }}
                        </div>
                    @endif

                    @if ($order->promo_code != '')
                        <div class="list-group-item">
                            <strong>Code Promo </strong> :
                            {{ $order->promo_code }}
                        </div>
                    @endif

                    {!!
                        Form::open([
                            'url' => route('admin.orders.plan.save', $order),
                            'method' => 'POST',
                            'class' => 'form',
                            'enctype' => 'multipart/form-data'
                        ])
                    !!}

                    {{ Form::hidden('partner_id', $order->partner_id) }}
                    {{ Form::hidden('plan_id', $order->plan_id) }}
                    {{ Form::hidden('type', $order->type) }}

                    <div class="list-group-item">
                        <strong>Création</strong> :
                        {{ isset($order->created_at) ? $order->created_at->format('d/m/Y H:i:s') : null }}
                    </div>

                    <div class="list-group-item">
                        <strong>Mise à jour</strong> :
                        {{ isset($order->updated_at) ? $order->updated_at->format('d/m/Y H:i:s') : null }}
                    </div>

                    <div class="list-group-item">
                        <strong>Status</strong> :
                        {{
                            Form::select(
                                'status',
                                App\Models\PartnerOrder::getStatus(),
                                isset($order) ? $order->status : null
                            )
                        }}
                    </div>

                    <div class="list-group-item">
                        <strong>Type de paiement </strong> :
                        {{
                            Form::select(
                                'type_payment',
                                App\Models\PartnerOrder::getTypePayment(),
                                isset($order) ? $order->type_payment : null
                            )
                        }}
                    </div>

                    <div class="list-group-item">
                        <strong>Payé le </strong> :
                        {{
                            Form::text(
                                'paid_at',
                                isset($order->paid_at) ? $order->paid_at->format('d/m/Y') : null,
                                [
                                    'class' => 'datepicker'
                                ]
                            )
                        }}
                    </div>

                    <div class="list-group-item">
                        <strong>Montant </strong> :
                        {{ $order->amount or '' }}&euro;
                    </div>

                    <div class="list-group-item">
                        <strong>Date de début </strong> :
                        {{
                            Form::text(
                                'start_at',
                                isset($order->start_at) ? $order->start_at->format('d/m/Y') : null,
                                [
                                    'class' => 'datepicker',
                                    'data-date-format' => 'dd/mm/yyyy'
                                ]
                            )
                        }}
                    </div>

                    <div class="list-group-item">
                        <strong>Date de fin </strong> :
                        {{
                            Form::text(
                                'end_at',
                                isset($order->end_at) ? $order->end_at->format('d/m/Y') : null,
                                [
                                    'class' => 'datepicker',
                                    'data-date-format' => 'dd/mm/yyyy'
                                ]
                            )
                        }}
                    </div>


                    <div class="list-group-item">
                        <strong>Facture </strong> :

                        <div class="single-file-upload-toogle text-left">
                            <a class="btn btn-sm btn-link"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
                            <span class="filename"></span>
                            {{ Form::file('file', ["style" => "display: none;"]) }}

                        </div>

                        @if(isset($order->invoice))
                        <div class="">
                          <a href="{{Storage::url($order->invoice)}}" target="_blank"><i class="fa fa-file-o"></i> Voir facture </a>
                        </div>
                        @endif

                    </div>

                    <div class="text-center">
                        <input type="submit" value="OK" class="btn btn-primary" />
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="panel panel-success">
                <div class="panel-heading">
                    Offre : "{{$order->plan->name}}"
                    <strong class="pull-right">{{$order->plan->price}} &euro;</strong>            
                </div>
                <div class="panel-body list-group">
                   

                    <h4>Zones</h4>
                    @foreach($order->plan->zones as $t)
                        <li class="list-group-item">
                            <a href="{{ action('Admin\Zone\ZoneController@show', $t->id) }}">{{ $t->name }}</a>
                        </li>
                    @endforeach()
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascripts')

<script>
  $(function(){
    $(document).on('click','.single-file-upload-toogle a', function(e) {
        e.preventDefault();
        $(this).parent().find( "input[name='file']" ).click();
    });

    $(document).on('change',".single-file-upload-toogle input[name='file']", function(e){
        var filename = $(this).val().split('\\').pop();
        $('.single-file-upload-toogle .filename').text(filename);
    });
  });
</script>

@endpush
