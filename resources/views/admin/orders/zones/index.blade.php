@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des commandes de zones
    </h1>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Partenaire</th>
                <th>Zone</th>
                <th>Requested at</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ route("admin.orders.zones.data") }}',
    };
</script>
{!! Html::script('/js/admin/orders/zones.js') !!}
@endpush
