@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('plugins/hierarchy-select/hierarchy-select.min.css') !!}
@endpush

@section('content')

<div class="header">
    <h1>{{ $template->name or 'Nouveau template' }}</h1>
</div>


<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">

			{!!
		        Form::open([
		            'url' => isset($template)
						? route('admin.parameters.emails.update', $template)
						: route('admin.parameters.emails.create'),
		            'method' => 'POST',
		            'enctype' => 'multipart/form-data'
		        ])
		    !!}

			@if(isset($template))
			<input type="hidden" name="_method" value="PUT">
			@endif

		    <div class="form-group label-floating">
		        <label>Nom</label>
		        {{
		            Form::text(
		                'name',
		                isset($template) ? $template->name : null,
		                [
		                    'class' => 'form-control'
		                ]
		            )
		        }}
		    </div>


			<div class="form-group label-floating">
		        <label>Sujet</label>
		        {{
		            Form::text(
		                'subject',
		                isset($template) ? $template->subject : null,
		                [
		                    'class' => 'form-control'
		                ]
		            )
		        }}
		    </div>

		    <div class="form-group label-floating">
		        <label>Clé</label>
		        {{
		            Form::text(
		                'key',
		                isset($template) ? $template->key : null,
		                [
		                    'class' => 'form-control'
		                ]
		            )
		        }}
		    </div>

		    <div class="form-group label-floating">
		        <label>Template</label>
		        {{
		            Form::textarea(
		                'template',
		                isset($template) ? $template->template : null,
		                [
		                    'class' => 'form-control',
		                    'rows' => '20'
		                ]
		            )
		        }}
		    </div>

		    <div class="form-group label-floating">
		        <input type="submit" value="Sauvegarder" class="btn btn-primary submit-form" />
		    </div>

			{!! Form::close() !!}

		</div>
	</div>
</div>
@endsection

@push('javascripts')
@endpush
