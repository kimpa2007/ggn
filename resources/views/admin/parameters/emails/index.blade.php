@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Templates e-mail
    </h1>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Clé</th>
                <th></th>
            </tr>
        </thead>
    </table>

    <a href="{{ route('admin.parameters.emails.new') }}" class="btn btn-primary">Créer</a>
</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
	var routes = {
		data : '{{ route("admin.parameters.emails.data") }}',
    };
</script>
{!! Html::script('/js/admin/config/emails.js') !!}
@endpush
