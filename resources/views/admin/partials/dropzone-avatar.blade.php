@php
	$varName = isset($varName) ? $varName : 'image';
@endphp

<div class="dropzone-avatar">

	<input type="hidden" id="image" name="image" value="{{$item->$varName or ''}}">
	<input type="hidden" id="uploading" name="uploading" value="0">

	<div class="dropzone-container" @if(isset($item) && $item->$varName != null) style="display:none;" @endif>

		<div id="real-dropzone" class="dropzone image-container">

			<div class="progress-bar"></div>
			<div class="background-image" style="background-image:url('{{asset('images/default-avatar.png')}}')"></div>

			<div class="dz-message"></div>

            <div class="fallback">
                <input name="image" type="file" multiple />
            </div>

            <div class="actions">
    			<p>Glissez une image ou<br><span class="link"><i class="fa fa-upload"></i> &nbsp; cliquez-ici</span></p>
    		</div>
    	</div>

	</div>


	<div class="image-container uploaded" @if(!isset($item) || (isset($item) && !$item->$varName)) style="display:none;" @endif>
		<div class="background-image" @if(isset($item)) style="background-image:url('{{ Storage::url($item->$varName)}}')" @endif>
		</div>
		<div class="actions">
			<a href="" class="btn btn-table" id="remove-picture"><i class="fa fa-trash"></i> &nbsp; Supprimer </a>
		</div>
	</div>

</div>

<!-- End drop zone -->
