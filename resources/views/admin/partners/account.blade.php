@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name or 'Ajouter un client' }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">

            {!!
                Form::open([
                    'url' => isset($partner)
                        ? action('Admin\Partner\PartnerController@update', ['id' => $partner->id])
                        : action('Admin\Partner\PartnerController@create'),
                    'method' => 'POST'
                ])
            !!}

			       {{ Form::hidden('id', isset($partner) ? $partner->id : null) }}
            <h2>Données du client</h2>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group label-floating">
                        <label class="control-label">Nom</label>
                        <input type="text" name="name" value="{{ $partner->name or old('name') }}" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group label-floating">
    	                <label class="control-label">Type</label>
                        {{
              						Form::select(
              							'type',
              							App\Models\Partner::getTypes(),
              							isset($partner) ? $partner->type : old('type'),
              							[
              								'class' => 'form-control'
              							]
              						)
              					}}
    	            </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group label-floating">
    	                <label class="control-label">Status</label>
                        {{
              						Form::select(
              							'status',
              							App\Models\Partner::getStatus(),
              							isset($partner) ? $partner->status : old('status'),
              							[
              								'class' => 'form-control'
              							]
              						)
              					}}
    	            </div>
                </div>
            </div>


            <div class="form-group label-floating">
                <label class="control-label">Adresse du client</label>
                {{
                    Form::textarea(
                        'address',
                        isset($partner) ? $partner->address : old('address'),
                        [
                            'class' => 'form-control',
                            'rows' => 3
                        ]
                    )
                }}
            </div>

            <div class="form-group label-floating">
                <label class="control-label">Coordonnées du référent</label>
                {{
                    Form::textarea(
                        'referent_data',
                        isset($partner) ? $partner->referent_data : old('referent_data'),
                        [
                            'class' => 'form-control',
                            'rows' => 3
                        ]
                    )
                }}
            </div>

            @if (!isset($partner) and !isset($partner->manager) and !empty($partner->manager))

                <h2>Compte du manager</h2>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Prénom</label>
                            <input type="text" name="firstname" value="{{ isset($partner) ? $partner->manager->firstname : old('firstname') }}" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Nom</label>
                            <input type="text" name="lastname" value="{{ isset($partner) ? $partner->manager->lastname : old('lastname') }}" class="form-control">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group label-floating">
                            <label class="control-label">E-mail</label>
                            <input type="text" name="email" value="{{ isset($partner) ? $partner->manager->email : old('email') }}" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Mot de passe</label>
                            <input type="password" name="password" value="Ggn2018" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Confirmation du mot passe</label>
                            <input type="password" name="password_confirmed" value="Ggn2018" class="form-control"/>
                        </div>
                    </div>
                </div>
            @endif

            <div class="form-group label-floating text-center">
                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

</div>

@if(isset($partner))
    <div class="body">
        <div class="row">
            <div class="col-md-12 bg-white">
                <br />
                Cliquez sur le bouton ci-dessous pour supprimer le client <strong>{{ $partner->name }}</strong>.
                {!!
                    Form::open([
                        'url' => action('Admin\Partner\PartnerController@delete', ["id" => $partner->id]),
                        'method' => 'POST',
                        'class' => 'form-inline'
                    ])
                !!}
                <input type="submit" value="Supprimer" class="btn btn-sm btn-danger toogle-delete" />
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @push('javascripts')
    {!! Html::script('/js/admin/partners/account.js') !!}
    @endpush
@endif

@endsection
