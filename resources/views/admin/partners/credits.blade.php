@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">

            <h2>Crédit en cours : {{ $partner->credits or '0' }}&euro;</h2>

            {!!
                Form::open([
                    'url' => action('Admin\Partner\CreditController@save', ['id' => $partner->id]),
                    'method' => 'POST',
                    'class' => 'form-inline'
                ])
            !!}

            <div class="form-group label-floating">
                <label class="control-label">Ajouter des crédits</label>
                {{ Form::number('quantity', null, ['class' => 'form-control'] )}}
            </div>


            <div class="form-group label-floating text-center">
                <input type="submit" value="Ajouter" class="btn btn-primary submit-form"/>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

</div>


<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
            <h2>Commande de crédits</h2>

            <table class="table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Payé le</th>
                        <th>Quantité (&euro;)</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($partner->orders->where('type', \App\Models\PartnerOrder::TYPE_CREDIT)->all() as $order)
                    <tr>
                        <td>{{ $order->created_at->format('d/m/Y') }}</td>
                        <td>{{ $order->paid_at ? $order->paid_at->format('d/m/Y') : null }}</td>
                        <td>{{ $order->amount }}</td>
                        <td>{{ $order->getStatusName() }}</td>
                        <td>
                            <a href="{{route('admin.orders.credit.show', $order)}}">Voir la commande</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
            <h2>Historique crédits</h2>

            <table id="table-credits" class="table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Quantité (&euro;)</th>
                        <th>Solde (&euro;)</th>
                        <th>Type</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection


@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : '{{ action("Admin\Partner\CreditController@getCreditOperationData", ["id" => $partner->id]) }}',
        };
    </script>

	{!! Html::script('/js/admin/partners/credits.js') !!}
@endpush
