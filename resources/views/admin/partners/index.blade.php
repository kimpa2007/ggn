@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des clients
    </h1>
        <a href="{{ action('Admin\Partner\PartnerController@newPartner') }}" class="btn btn-primary">Ajouter</a>
</div>

<div class="body">
  <div id="app"></div>
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Type</th>
                <th>Administrateur</th>
                <th>Créé le</th>
                <th></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>

            </tr>
        </tfoot>

    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ action("Admin\Partner\PartnerController@getData") }}',
    };
</script>
{!! Html::script('/js/admin/partners/index.js') !!}
@endpush
