<div class="nav tabs-inline">
	<ul class="tabs-inline">
        <li class="{{ Request::is('admin/partners/*/account') ? 'active' : '' }}">
            <a href="{{action('Admin\Partner\PartnerController@show', ['id' => $partner->id])}}">
            	<span class="text">
                	Compte
                </span>
            </a>
        </li>

        <li class="{{ Request::is('admin/partners/*/users') ? 'active' : '' }}">
            <a href="{{action('Admin\Partner\UserController@index', ['id' => $partner->id])}}">
            	<span class="text">
                	Users
                </span>
            </a>
        </li>

        <li class="{{ Request::is('admin/partners/*/plans') ? 'active' : '' }}">
            <a href="{{action('Admin\Partner\PlanController@show', ['id' => $partner->id])}}">
            	<span class="text">
                	Abonnements
                </span>
            </a>
        </li>

		<li class="{{ Request::is('admin/partners/*/orders') ? 'active' : '' }}{{ Request::is('admin/orders/*') ? 'active' : '' }}">
			<a href="{{ route('admin.partner.orders', $partner->id) }}">
            	<span class="text">
                	Commandes
                </span>
            </a>
        </li>

	</ul>
</div>
