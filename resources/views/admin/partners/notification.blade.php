@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">

            {!!
                Form::open([
                    'url' => isset($notification) ?
                        route('admin.partner.notifications.update', [
                            'id' => $partner->id,
                            'notification' => $notification
                        ]) :
                        route('admin.partner.notifications.save', [
                            'id' => $partner->id
                        ])
                    ,
                    'method' => 'POST'
                ])
            !!}

            {{ Form::hidden('partner_id', $partner->id) }}

            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="form-group label-floating">
                        <label class="control-label">Titre</label>
                        {{
                            Form::text(
                                'title',
                                isset($notification) ? $notification->title : null,
                                [
                                    'class' => 'form-control'
                                ]
                            )
                        }}
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Message</label>
                        {{
                            Form::textarea(
                                'message',
                                isset($notification) ? $notification->message : null,
                                [
                                    'class' => 'form-control',
                                    'rows' => 2
                                ]
                            )
                        }}
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Status</label>
                        {{
                            Form::select(
                                'status',
                                \App\Models\Notification::getStatus(),
                                isset($notification) ? $notification->status : [],
                                [
                                    'class' => 'form-control',
                                ]
                            )
                        }}
                    </div>
                </div>
            </div>

            <div class="form-group label-floating text-center">
                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection


@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : '{{ action("Admin\Partner\CreditController@getCreditOperationData", ["id" => $partner->id]) }}',
        };
    </script>

	{!! Html::script('/js/admin/partners/credits.js') !!}
@endpush
