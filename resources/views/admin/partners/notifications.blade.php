@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@include('admin.partners.nav', [
    "partner" => $partner
])

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
        	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Titre</th>
                        <th>Status</th>
                        <th>Envoyé le</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            <a href="{{ route('admin.partner.notifications.create', $partner->id) }}" class="btn btn-primary">Créer</a>
        </div>
    </div>
</div>
@endsection


@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : '{{ route("admin.partner.notifications.data", $partner->id) }}',
        };
    </script>

	{!! Html::script('/js/admin/partners/notifications.js') !!}
@endpush
