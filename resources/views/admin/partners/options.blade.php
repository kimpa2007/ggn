@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">

            {!!
                Form::open([
                    'url' => action('Admin\Partner\OptionController@save', ['id' => $partner->id]),
                    'method' => 'POST'
                ])
            !!}

            @include('components.dropzone-image',[
                'image' => isset($partner) && isset($partner->logo) ? $partner->logo : null,
                'size' => 'avatar',
                'id' => 'dropzone-1',
                'name' => 'logo'
            ])

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group label-floating">
                        <label class="control-label">Couleur 1</label>
                        <input type="text" name="color1" value="{{ $partner->color1 or '' }}" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group label-floating">
                        <label class="control-label">Couleur 2</label>
                        <input type="text" name="color2" value="{{ $partner->color2 or '' }}" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group label-floating">
                        <label class="control-label">Couleur 3</label>
                        <input type="text" name="color3" value="{{ $partner->color3 or '' }}" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group label-floating">
                <label class="control-label">Titre ou slogan</label>
                {{
                    Form::textarea(
                        'title',
                        isset($partner) ? $partner->title : null,
                        [
                            'class' => 'form-control',
                            'rows' => 2
                        ]
                    )
                }}
            </div>

            <div class="form-group label-floating text-center">
                <input type="submit" value="Enregistrer" class="btn btn-primary submit-form"/>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

</div>
@endsection

@push('javascripts-libs')
    {!! Html::script('/js/dropzone.min.js') !!}
    {!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush
