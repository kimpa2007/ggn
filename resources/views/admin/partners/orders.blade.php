@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
            <h2>Commande d'abonnement</h2>

            <table id="table-plans" class="table">
                <thead>
                    <tr>
                        <th>Créé le</th>
                        <th>Abonnement</th>
                        <th>Status</th>
                        <th>Payé le</th>
                        <th>Type de paiement</th>
                        <th>Début</th>
                        <th>Fin</th>
                        <th>Prix (&euro;)</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
            <h2>Commande de crédits</h2>

            <table id="table-credits" class="table">
                <thead>
                    <tr>
                        <th>Créé le</th>
                        <th>Status</th>
                        <th>Payé le</th>
                        <th>Type de paiement</th>
                        <th>Crédits (&euro;)</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection


@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : {
                plans : '{{ route("admin.partner.orders.data.plans", $partner->id) }}',
                credits : '{{ route("admin.partner.orders.data.credits", $partner->id) }}',
            }
        };
    </script>

	{!! Html::script('/js/admin/partners/orders.js') !!}
@endpush
