@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif



<div class="body">

  <div class="row">
      <div class="col-md-12 bg-white form-container">

          {!!
              Form::open([
                  'url' => route('admin.partner.plan.create',["id" => $partner->id]),
                  'method' => 'POST',
              ])
          !!}

          <div class="row">
            <div class="col-md-6">
              <div class="form-group label-floating">
                  <label class="control-label">Ajouter des abonnements</label>
                  {{
                    Form::select(
                      'plan_id',
                      \App\Models\PartnerPlanTranslation::where('locale','fr')->pluck('name','partner_plan_id'),
                      null,
                      [
                        'class' => 'form-control'
                      ]
                    )
                  }}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group" style="margin-top: -5px;">
                  <label class="control-label">Date de début </label>
                  {{
                      Form::text(
                          'start_at',
                          null,
                          [
                              'class' => 'datepicker form-control',
                              'data-date-format' => 'dd/mm/yyyy'
                          ]
                      )
                  }}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="text-center">
                <input type="submit" value="Ajouter" class="btn btn-primary submit-form"/>
            </div>
          </div>

          {!! Form::close() !!}
      </div>
  </div>
</div>
<div class="body">


	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Date</th>
                <th>Nom</th>
                <th>Durée (mois)</th>
                <th>Début</th>
                <th>Fin</th>
                <th>Prix (&euro;)</th>
                <th>Statut</th>
                <th>Zones</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
@endsection



@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ action("Admin\Partner\PlanController@getData", ["id" => $partner->id]) }}',
    };
</script>
{!! Html::script('/js/admin/partners/plans.js') !!}
@endpush
