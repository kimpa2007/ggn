@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@if(isset($partner))
    @include('admin.partners.nav', [
        "partner" => $partner
    ])
@endif

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
        {!!
            Form::open([
                'url' => route('admin.partner.rules.save', $partner->id),
                'method' => 'POST'
            ])
        !!}

        {{ Form::hidden('partner_id', $partner->id) }}

        <div class="form-group">
            <ul class="list-group">
                @foreach(App\Models\Rule::all() as $rule)
                <li class="list-group-item">
                    {{ Form::checkbox('rules[]', $rule->id, $partner->rules->contains($rule), ["id" => 'checkbox']) }}
                    <label for="checkbox">{{ $rule->description }}</label>
                </li>
                @endforeach()
            </ul>
        </div>


        <div class="form-group label-floating text-center">
            <input type="submit" value="Enregistrer" class="btn btn-primary submit-form"/>
        </div>

        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
