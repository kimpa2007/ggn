@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
         {{ $partner->name }}
    </h1>
</div>

@include('admin.partners.nav', [
    "partner" => $partner
])

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom/Prénom</th>
                <th>E-mail</th>
				<th>Téléphone</th>
                <th>Créé le</th>
				<th>Statut</th>
                <th></th>
            </tr>
        </thead>

    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ action("Admin\Partner\UserController@getData", ["id" => $partner->id]) }}',
    };
</script>
{!! Html::script('/js/admin/partners/users.js') !!}
@endpush
