@extends('layouts.app')


@section('content')
<div class="header">
    <h1>
        Créer un abonnement
    </h1>
</div>

@php
    $langues =  App\Models\Language::where('id', 1)->get();
@endphp

<div class="body">
    <div class="row">
		<div class="col-md-12 form-container">

                {!!
                    Form::open([
                        'url' => isset($plan)
                            ? route('admin.plan.update', ['id' => $plan->id])
                            : route('admin.plan.save'),
                        'method' => 'POST'
                    ])
                !!}

                <div class="col-xs-8">
                    <div class="row lang-fields-group">
                        <h4>Nom</h4>
                        <div class="lateral-bar">
                            @foreach ($langues as $lang)
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nom en {{$lang->name}}</label>
                                        <input type="text" name="name[{{$lang->locale}}]" value="{{ isset($plan) ? $plan->translate($lang->locale)->name : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <br clear="all">

                    <div class="row lang-fields-group">
                        <h4>Description</h4>
                        <div class="lateral-bar">
                            @foreach ($langues as $lang)
                                <div class="col-md-12">
                                    <div class="form-group label-floating row">
                                        <label class="control-label">Description {{$lang->locale}}</label>
                                        <textarea name="description[{{$lang->locale}}]" class="form-control" id="editor1_{{$lang->locale}}" rows="10" cols="80">{{  isset($plan) ?$plan->translate($lang->locale)->description : '' }}</textarea>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <br clear="all">

                </div>

                <div class="col-xs-4">
                    <div class="form-group label-floating text-right">
                        <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Status</label>
                        {{
                            Form::select(
                                'status',
                                App\Models\PartnerPlan::getStatus(),
                                isset($plan) ? $plan->status : null,
                                [
                                    'class' => 'selectpicker',
                                ]
                            )
                        }}
                    </div>


                  <!--  <div class="form-group label-floating">
                        <label class="control-label">Durée</label>
                        {{
                            Form::select(
                                'time',
                                \App\Models\PartnerPlan::getTimes(),
                                isset($plan) ? $plan->time : null,
                                [
                                    'class' => 'form-control'
                                ]
                            )
                        }}
                    </div>-->


                    <div class="form-group label-floating">
                        <label class="control-label">Durée (en mois)</label>
                        <input name="months" value="{{ isset($plan) ? $plan->months: '' }}" class="form-control">
                    </div>


                    <div class="form-group label-floating">
                        <label class="control-label">Prix</label>
                        {{
                            Form::text(
                                'price',
                                isset($plan->price) ? $plan->price : null,
                                [
                                    'class' => 'form-control'
                                ]
                            )
                        }}
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Zones</label>
                        {{
                                    Form::select(
                                        'zones[]',
                                        \App\Models\ZoneTranslation::where('locale','fr')->pluck('name','zone_id'),
                                        isset($plan) ? $plan->zones : [],
                                        [
                                            'class' => 'selectpicker',
                                              'multiple' => 'multiple',
                                              'data-header' => "Séléctionnez les zones"
                                        ]
                                    )
                                }}
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Type</label>
                        {{
                            Form::select(
                                'type',
                                App\Models\PartnerPlan::getTypes(),
                                isset($plan) ? $plan->type : null,
                                [
                                    'class' => 'selectpicker',
                                ]
                            )
                        }}
                    </div>
         
                </div>
                <br clear="all">


				<label class="control-label">Clients</label>

		        @php
		        	$allPartners = \App\Models\Partner::pluck('name', 'id')->toArray();
		        	$selectedPartners = isset($plan) ? $plan->partnersPurposed->pluck('name','id')->toArray() : [];
		        	$difference = array_diff_assoc($allPartners,$selectedPartners);
		        @endphp

		        @include('components.connected-lists',[
		        	'others' => $difference,
		        	'selected' => $selectedPartners,
		        	'array_name' => 'partners_purposed'
		        ])


                {!! Form::close() !!}
            </div>
        </div>

</div>
@endsection


@push('stylesheets')
	{!! Html::script('plugins/ckeditor/ckeditor.js') !!}
    {!! Html::style('/plugins/jquery-lwmultiselect/jquery.lwMultiSelect.css') !!}
    {!! Html::style('/css/jquery-ui.min.css') !!}
@endpush

@push('javascripts-libs')
	{!! Html::script('/plugins/jquery-lwmultiselect/jquery.lwMultiSelect.min.js') !!}
	{!! Html::script('/js/libs/jquery-ui.min.js') !!}
@endpush

@push('javascripts')

    <script>

    	CKEDITOR.disableAutoInline = true;
        @foreach ( $langues as $lang)
	       CKEDITOR.inline( 'editor1_{{$lang->locale}}' );
        @endforeach

        $("#partners-select").lwMultiSelect({
            addAllText:'Tous ajouter',
            removeAllText:'Tous supprimer',
            selectedLabel:'',
            onChange: function() {
            }
        });
    </script>
@endpush
