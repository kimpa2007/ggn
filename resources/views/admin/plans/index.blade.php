@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des abonnements
    </h1>

    <a href="{{ route('admin.plan.form') }}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Zone</th>
                <th>Description</th>
                <th>Nº Jour</th>
                <th>Prix</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>

    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	getData : '{{ route("admin.plan.data") }}',
    	deleteItem : '{{route("admin.plan.delete")}}'
    };
</script>
{!! Html::script('/js/admin/plans/index.js') !!}
@endpush
