@extends('layouts.app')

@section('content')
<div class="header">
    <h1>Tags</h1>
</div>

<div class="body">

    {{-- REMOVE TAGS FORM  --}}

    <div class="row">
        <div class="col-md-offset-1 col-md-10 bg-white form-container">
            {!!
                Form::open([
                    'url' => route('admin.tags.delete'),
                    'method' => 'POST'
                ])
            !!}
            <div class="col-xs-5">
                <h3>Mots à garder</h3>
                {!!
                    Form::select(
                        'from[]',
                        App\Models\Tag::pluck('name', 'id'),
                        null,
                        [
                            'multiple' => 'multiple',
                            'size' => 8,
                            'class' => 'form-control',
                            'id' => 'search'
                        ]
                    )
                !!}
            </div>

            <div class="col-xs-2">
                <br />
                <br />
                <button type="button" id="search_rightAll" class="btn btn-primary btn-sm btn-block"><i class="glyphicon glyphicon-forward"></i></button>
                <button type="button" id="search_rightSelected" class="btn btn-primary btn-sm  btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                <button type="button" id="search_leftSelected" class="btn btn-primary btn-sm  btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                <button type="button" id="search_leftAll" class="btn btn-primary btn-sm btn-block"><i class="glyphicon glyphicon-backward"></i></button>
            </div>

            <div class="col-xs-5">
                <h3>Mots à supprimer</h3>
                {!!
                    Form::select(
                        'to_remove[]',
                        [],
                        null,
                        [
                            'multiple' => 'multiple',
                            'size' => 8,
                            'class' => 'form-control',
                            'id' => 'search_to'
                        ]
                    )
                !!}
            </div>

            <div class="col-md-12 text-center">
                {!!
                    Form::submit('Enregistrer', [
                        'class' => 'btn btn-primary'
                    ])
                !!}
            </div>

            {!!
                Form::close()
            !!}
        </div>
    </div>

    {{-- ADD TAG FORM  --}}
    <hr />
    <div class="row">
        <div class="col-md-offset-1 col-md-10 bg-white form-container">
            {!!
                Form::open([
                    'url' => route('admin.tags.create'),
                    'method' => 'POST',
                ])
            !!}
            <div class="col-md-6">
                <h3>Ajouter un mot </h3>
                {!!
                    Form::text('name', null, [
                        'class' => 'form-control',
                    ])
                !!}
            </div>
            <div class="col-md-12 text-center">
                {!!
                    Form::submit('Enregistrer', [
                        'class' => 'btn btn-primary'
                    ])
                !!}
            </div>
            {!!
                Form::close()
            !!}
        </div>
    </div>

    {{-- UPDATE TAGS FORM  --}}
    <hr />
    <div class="row">
        <div class="col-md-offset-1 col-md-10 bg-white form-container">
            {!!
                Form::open([
                    'url' => route('admin.tags.update'),
                    'method' => 'POST',
                ])
            !!}

            <div class="col-md-6">
                <h3>Modifier des mots</h3>
                @foreach(App\Models\Tag::all() as $tag)
                    {!!
                        Form::text('tags['.$tag->id.']', $tag->name, [
                            'class' => 'form-control',
                        ])
                    !!}
                @endforeach()
            </div>

            <div class="col-md-12 text-center">
                {!!
                    Form::submit('Enregistrer', [
                        'class' => 'btn btn-primary'
                    ])
                !!}
            </div>

            {!!
                Form::close()
            !!}
        </div>
    </div>

</div>
@endsection


@push('javascripts-libs')
	{!! Html::script('/plugins/multiselect/multiselect.min.js') !!}
@endpush

@push('javascripts')
<script>
jQuery(document).ready(function($) {
    $('#search').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        },
        fireSearch: function(value) {
            return value.length > 1;
        }
    });
});
</script>
@endpush
