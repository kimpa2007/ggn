@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/jquery-ui.min.css') !!}
@endpush

@section('content')

@php
	$langues = App\Models\Language::where('id', 1)->get();
@endphp

<div class="header">
    <h1>{{ isset($theme) ? $theme->translate('fr')->name : 'Création de theme' }}</h1>
</div>

<div class="body">
    <div class="row">
        <div class="col-md-12 form-container">

        	<!-- Droptheme Avatar -->

        	{!! Form::open(['url' => action('Admin\Theme\ThemeController@save'), 'files'=>true, 'method' => 'POST']) !!}


	    		{{ csrf_field() }}

        		<input type="hidden" name="id" value="{{ $theme->id or '' }}" >

        		<div class="col-xs-8">
        			<div class="row lang-fields-group">
				    	<h4>Position</h4>
				    	<div class="lateral-bar">
					    	<div class="col-md-12">
						    	<div class="form-group label-floating">
									<input type="number" name="position" value="{{ $theme->position or '' }}" >
								</div>
							</div>
						</div>
					</div>


	        		<div class="row lang-fields-group">
				    	<h4>Nom</h4>
				    	<div class="lateral-bar">
					    	@foreach ($langues  as $lang)
						    	<div class="col-md-12">
							    	<div class="form-group label-floating">
										<label class="control-label">Nom en {{$lang->name}}</label>
										<input type="text" name="name[{{$lang->locale}}]" value="{{ isset($theme) ? $theme->translate($lang->locale)->name : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
									</div>
								</div>
							@endforeach
						</div>
					</div>

					<br clear="all">

					<div class="row lang-fields-group">
				    	<h4>Meta Title</h4>
				    	<div class="lateral-bar">
					    	@foreach ($langues  as $lang)
						    	<div class="col-md-12">
							    	<div class="form-group label-floating">
										<label class="control-label">Meta Title en {{$lang->name}}</label>
										<input type="text" name="meta_title[{{$lang->locale}}]" value="{{ isset($theme) ? $theme->translate($lang->locale)->meta_title : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<br clear="all">

					<div class="row lang-fields-group">
						<h4>Meta Description</h4>
						<div class="lateral-bar">
					    	@foreach ($langues as $lang)
							    <div class="col-md-12">
									<div class="form-group label-floating row">
							            <label class="control-label">Meta Description {{$lang->locale}}</label>
							            <textarea name="meta_description[{{$lang->locale}}]" class="form-control" id="editor1_{{$lang->locale}}" rows="10" cols="80">{{  isset($theme) ?$theme->translate($lang->locale)->meta_description : '' }}</textarea>
							        </div>
							    </div>
						    @endforeach
					    </div>
					</div>
					<br clear="all">

				</div>						


				<!-- -->
	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/libs/jquery-ui.min.js') !!}
@endpush


