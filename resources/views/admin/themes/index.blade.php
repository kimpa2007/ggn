@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des themes
    </h1>

    <a href="{{action('Admin\Theme\ThemeController@create')}}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Theme</th>
                <th>Position</th>
                <th></th>
            </tr>
        </thead>
    </table>

</div>
@endsection

@push('javascripts')
<script>

var csrf_token = "{{csrf_token()}}";

var routes = {
	getData : '{{ action("Admin\Theme\ThemeController@getData") }}',
	deleteItem : '{{ action("Admin\Theme\ThemeController@delete") }}',
};

console.log(routes);
</script>

{{ Html::script('js/admin/themes/index.js')}}

@endpush
