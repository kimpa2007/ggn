@extends('layouts.app')

@section('content')
    <div class="header">
        <h1>
            Ajouter une traduction
        </h1>

        <a href="{{route('admin.translate.index')}}" class="btn btn-primary">Retour</a>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-md-12 bg-white form-container">
                @isset($translate)
                    {!! Form::open(['method'=>'put','url'=>route('admin.translate.save.edit',$translate)]) !!}
                @else
                    {!! Form::open(['method'=>'post','url'=>route('admin.translate.save')]) !!}
                @endisset

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Key</label>
                            <input type="text" name="label" value="{{ $translate->label or old('name') }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Texte fr</label>
                            <input type="text" name="text_fr" value="{{ $translate->text_fr or old('name') }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Texte en</label>
                            <input type="text" name="text_en" value="{{ $translate->text_en or old('name') }}" class="form-control">
                        </div>
                    </div>


                </div>
                        <div class="form-group label-floating text-left">
                            <input type="submit" value="Valider" class="btn btn-primary submit-form"/>
                        </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>


@endsection
