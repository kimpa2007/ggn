@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Traductions des termes dans le site
    </h1>

    <a href="{{route('admin.translate.create')}}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Key</th>
                <th>Français - FR</th>
                <th>English - EN</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>

</div>
@endsection

@push('javascripts')
    <script>

        var csrf_token = "{{csrf_token()}}";

        var routes = {
            getData : '{{ route( "admin.translate.getData" ) }}',
            deleteItem : '{{ route( "admin.translate.delete" ) }}'
        };

    </script>

    {{ Html::script('js/admin/translate/index.js')}}

@endpush
