@extends('layouts.app')

@section('content')
<div class="header">
    <h1>{{ $user->full_name or 'Créer un compte' }}</h1>
</div>

@if(isset($user))
	@include('admin.users.nav')
@endif

<div class="body">
    <div class="row">

        <div class="col-md-12 bg-white form-container">

        	<!-- Dropzone Avatar -->
        	{!!
                Form::open([
        					'url' => isset($user)
        						? route('admin.users.update', $user)
        						: route('admin.users.save'),
                  'files' => true,
        					'method' => 'POST'
                ])
            !!}

				{{ Form::hidden('id', isset($user) ? $user->id : null) }}

        @if(isset($user) && $user->getRoleId() == 2)
        				@include('components.dropzone-image',[
        					'image' => isset($user) && isset($user->image) ? $user->image : null,
        					'size' => 'avatar',
        					'id' => 'dropzone-1',
        					'name' => 'image'
        				])
        @endif
				<div class="row">
          <div class="col-md-6">

                <div class="form-group label-floating">
                    <label class="control-label">Clients</label>
                    {{
                        Form::select(
                            'partner_id',
                            App\Models\Partner::all()->prepend(['id' => '', 'name' => '---'])->pluck('name', 'id'),
                            (isset($user)) && isset($user->partner) ? $user->partner->id : null,
                            [
                                'class' => 'form-control'
                            ]
                        )
                    }}
                </div>
            </div>


				</div>

	            <div class="row">
	                <div class="col-md-6">
	                    <div class="form-group label-floating">
	                        <label class="control-label">Nom</label>
	                        <input type="text" name="lastname" value="{{ $user->lastname or old('lastname') }}" class="form-control">
	                    </div>
	                </div>

	                <div class="col-md-6">
	                    <div class="form-group label-floating">
	                        <label class="control-label">Prénom</label>
	                        <input type="text" name="firstname" value="{{ $user->firstname or old('firstname') }}" class="form-control"/>
	                    </div>
	                </div>
	            </div>


                <div class="row">
	                <div class="col-md-6">
        				<div class="form-group label-floating">
        	                <label class="control-label">E-mail</label>
        	                <input type="text" name="email" value="{{ $user->email or old('email') }}" class="form-control"/>
        	            </div>
                    </div>

	                <div class="col-md-6">
        	            <div class="form-group label-floating">
        	                <label class="control-label">Téléphone</label>
        	                <input type="text" name="telephone" value="{{ $user->telephone or old('telephone') }}" class="form-control"/>
        	            </div>
                    </div>
	            </div>


                <div class="row">
                  <div class="col-md-6">
                   <div class="form-group label-floating">
                     <label class="control-label">Role</label>
             {{
               Form::select(
                 'role_id',
                 App\Models\Role::pluck('display_name', 'id'),
                 isset($user) ? $user->getRoleId() : null,
                 [
                   'class' => 'form-control'
                 ]
               )
             }}
                 </div>
         </div>
         <div class="col-md-6">
                   <div class="form-group label-floating">
                     <label class="control-label">Status</label>
             {{
               Form::select(
                 'status',
                 App\Models\User::getStatus(),
                 isset($user) ? $user->status : null,
                 [
                   'class' => 'form-control'
                 ]
               )
             }}
                 </div>
         </div>
                </div>


	            <div class="form-group label-floating">
	                <label class="control-label">Mot de passe</label>
	                <input type="password" name="password" value="" class="form-control"/>
	            </div>

	            <div class="form-group label-floating">
	                <label class="control-label">Confirmation du mot passe</label>
	                <input type="password" name="password_confirmation" value="" class="form-control"/>
	            </div>

	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
          </div>
      @if(isset($user))
        <div class="body">
            <div class="row">
                <div class="col-md-12 bg-white">
                  <br />
                  Cliquez sur le bouton ci-dessous pour supprimer l'utilisateur.
        {!!
                        Form::open([
                            'url' => route('admin.users.delete', $user),
                            'method' => 'POST',
                            'class' => 'delete'
                        ])
                    !!}
                      {{ Form::hidden('id', isset($user) ? $user->id : null) }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Supprimer le compte" class="btn btn-danger toogle-delete" />
                    {{ Form::close() }}
                  </div>
              </div>
          </div>
      @endif

</div>
@endsection

@push('javascripts-libs')
    {!! Html::script('/js/dropzone.min.js') !!}
    {!! Html::script('/js/libs/jquery.imageUploader.js') !!}
    {!! Html::script('/js/admin/partners/account.js') !!}
@endpush

@push('javascripts')
@endpush
