@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
@endpush

@section('content')

<div class="header">
    <h1>{{ $user->full_name }}</h1>
</div>

@include('admin.users.nav')

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">

            <h2>Crédit en cours : {{ $user->credits or '0' }}&euro;</h2>

            {!!
                Form::open([
                    'url' => route('admin.users.credits.save', $user),
                    'method' => 'POST',
                    'class' => 'form-inline'
                ])
            !!}

            <div class="form-group label-floating">
                <label class="control-label">Ajouter des crédits</label>
                {{ Form::number('quantity', $user->credits, ['class' => 'form-control']) }}
            </div>


            <div class="form-group label-floating text-center">
                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

</div>


<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
            <h2>Historique crédits</h2>

            <table id="table-credits" class="table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Quantité (&euro;)</th>
                        <th>Type</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection


@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : '{{ route("admin.users.credits.operations", $user) }}',
        };
    </script>

	{!! Html::script('/js/admin/users/credits.js') !!}
@endpush
