@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Users
    </h1>

    <a href="{{ action('Admin\User\AccountController@create') }}" class="btn btn-primary">Ajouter</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom/Prénom</th>
                <th>E-mail</th>
				<th>Téléphone</th>
                <th>Role</th>
				<th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Filtrer par : </th>
                <th></th>
                <th></th>
				<th></th>
				<th></th>
				<th>Status</th>
                <th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts')
<script>

$(function(){

    $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: '{{ action("Admin\User\AccountController@getData", ["type" => $type]) }}',
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
  			{data: 'telephone', name: 'telephone'},
  			{data: 'role', name: 'role'},
  			{data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function() {

            // Add filters columns
            this.api().columns([4,5]).every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function( d, j ) {
                    if(d != null){
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                    }
                });
            });

        }
    });

});

</script>
@endpush
