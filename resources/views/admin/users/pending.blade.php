@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Liste des comptes Users en attente de validation
    </h1>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Prénom/Nom</th>
                <th>E-mail</th>
				<th>Téléphone</th>
                <th>Enregistrer le</th>
                <th>Association</th>
				<th>Statut</th>
                <th></th>
            </tr>
        </thead>

    </table>

</div>
@endsection

@push('javascripts')
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : '{{ action("Admin\User\PendingAccountController@getData") }}',
    	active : "{{action('Admin\User\PendingAccountController@setActive')}}",
        refused : "{{action('Admin\User\PendingAccountController@setRefused')}}",
        partner : "{{action('Admin\User\PendingAccountController@setPartner')}}"
    };
</script>
{!! Html::script('/js/admin/users/pending.js') !!}
@endpush
