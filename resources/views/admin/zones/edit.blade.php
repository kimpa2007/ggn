@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
	{!! Html::style('/css/jquery-ui.min.css') !!}
@endpush

@section('content')


@include('admin.zones.partials.nav')

<div class="body">
    <div class="row">
        <div class="col-md-12 form-container">

        	<!-- Dropzone Avatar -->

        	{!! Form::open(['url' => action('Admin\Zone\ZoneController@save'), 'files'=>true, 'method' => 'POST']) !!}


	    		{{ csrf_field() }}

        		<input type="hidden" name="id" value="{{ $zone->id or '' }}" >

        		<div class="col-xs-8">
	        		<div class="row lang-fields-group">
				    	<h4>Nom</h4>
				    	<div class="lateral-bar">
					    	@foreach ( App\Models\Language::all() as $lang)
						    	<div class="col-md-12">
							    	<div class="form-group label-floating">
										<label class="control-label">Nom en {{$lang->name}}</label>
										<input type="text" name="name[{{$lang->locale}}]" value="{{ isset($zone) ? $zone->translate($lang->locale)->name : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
									</div>
								</div>
							@endforeach
						</div>
					</div>

					<br clear="all">

					<div class="row lang-fields-group">
				    	<h4>Slug</h4>
				    	<div class="lateral-bar">
					    	@foreach ( App\Models\Language::all() as $lang)
						    	<div class="col-md-12">
							    	<div class="form-group label-floating">
										<label class="control-label">Slug en {{$lang->name}}</label>
										<input type="text" name="slug[{{$lang->locale}}]" value="{{ isset($zone) ? $zone->translate($lang->locale)->slug : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<br clear="all">

					<div class="row lang-fields-group">
						<h4>Description</h4>
						<div class="lateral-bar">
					    	@foreach ( App\Models\Language::all() as $lang)
							    <div class="col-md-12">
									<div class="form-group label-floating row">
							            <label class="control-label">Description {{$lang->locale}}</label>
							            <textarea name="description[{{$lang->locale}}]" class="form-control" id="editor1_{{$lang->locale}}" rows="10" cols="80">{{  isset($zone) ?$zone->translate($lang->locale)->description : '' }}</textarea>
							        </div>
							    </div>
						    @endforeach
					    </div>
					</div>
					<br clear="all">

					<div class="row lang-fields-group">
						<h4>Meta title</h4>
						<div class="lateral-bar">
							@foreach ( App\Models\Language::all() as $lang)
								<div class="col-md-12">
									<div class="form-group label-floating row">
										<label class="control-label">Meta Title {{$lang->locale}}</label>
										<input type="text" name="meta_title[{{$lang->locale}}]" value="{{ isset($zone) ? $zone->translate($lang->locale)->meta_title : '' }}" class="form-control" style="font-weight:bold;font-size:20px">

									</div>
								</div>
							@endforeach
						</div>

						<h4>Meta description</h4>
						<div class="lateral-bar">
							@foreach ( App\Models\Language::all() as $lang)
								<div class="col-md-12">
									<div class="form-group label-floating row">
										<label class="control-label">Meta Description {{$lang->locale}}</label>
										<input type="text" name="meta_description[{{$lang->locale}}]" value="{{ isset($zone) ? $zone->translate($lang->locale)->meta_description : '' }}" class="form-control" style="font-weight:bold;font-size:20px">

									</div>
								</div>
							@endforeach
						</div>
					</div>
					<br clear="all">


		        	<div class="row">

		        		<div class="col-md-6">
			        		@include('components.dropzone-image',[
					        	'image' => isset($zone) && isset($zone->image) ? $zone->image : null,
					        	'size' => 'banner',
					        	'id' => 'dropzone-1',
					        	'name' => 'image'
					        ])

					        <div class="row lang-fields-group">
								<h4>Legend</h4>
								<div class="lateral-bar">
							    	@foreach ( App\Models\Language::all() as $lang)
									    <div class="col-md-12">
											<div class="form-group label-floating row">
									            <label class="control-label">Legend {{$lang->locale}}</label>
									            <input type="text" name="legend[{{$lang->locale}}]" value="{{ isset($zone) ? $zone->translate($lang->locale)->legend : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
									        </div>
									    </div>
								    @endforeach
							    </div>
							</div>

							<div class="row lang-fields-group">
								<h4>Copyright</h4>
								<div class="lateral-bar">
							    	@foreach ( App\Models\Language::all() as $lang)
									    <div class="col-md-12">
											<div class="form-group label-floating row">
									            <label class="control-label">Copyright {{$lang->locale}}</label>
									            <input type="text" name="copyright[{{$lang->locale}}]" value="{{ isset($zone) ? $zone->translate($lang->locale)->copyright : '' }}" class="form-control" style="font-weight:bold;font-size:20px">
									        </div>
									    </div>
								    @endforeach
							    </div>
							</div>
		                </div>
	            	</div>
				</div>

        		<div class="col-xs-4">
        			<div class="form-group label-floating">
	                    <label class="control-label">Status</label>
	                    <select class="form-control" name="status">

	                    	@foreach( App\Models\Zone::getStatus() as $key => $value )
	                    		<option value="{{$key}}" {{isset($zone) && $zone->status == $key ? 'selected' : ''}} >{{$value}}</option>
	                    	@endforeach
	                    </select>
	                	<span class="material-input"></span>
	                </div>
        		</div>
        		<br clear="all">





		        @if(isset($zone))
		        <label class="control-label">Corespondant</label>
		        <div class="row connected-lists">

		        	<div class="col-md-1">
		        	</div>

		        	<div class="connected-list left-list col-md-4">
			        	<ul id="sortable1" class="connectedSortable">

			        		@foreach($zone->otherExperts() as $expert)
								<li class="ui-state-default" data-id="{{$expert->id}}">{{$expert->firstname}} {{$expert->lastname}}</li>
							@endforeach
						</ul>
					</div>
					<div class="middle-list col-md-2">
						<div class="content">
							<a href="#" class="add-all">Ajouter tous > </a>

							<a href="" class="remove-all">< Retirer tous </a>
						</div>

					</div>

					<div class="connected-list right-list col-md-4 selected-items">

						<ul id="sortable2" class="connectedSortable">


						@foreach($zone->experts as $expert)
							<input type="hidden" name="experts[]" value="{{$expert->id}}" >
							<li class="ui-state-default" data-id="{{$expert->id}}">{{$expert->firstname}} {{$expert->lastname}}</li>
						@endforeach

						</ul>
					</div>

		        </div>
		        @endif
						<!-- Pays -->
						@if(isset($zone))
						<label class="control-label">Pays</label>
						<div class="row connected-lists">

							<div class="col-md-1">
							</div>

							<div class="connected-list left-list col-md-4">
								<ul id="sortable3" class="connectedSortable">
									@foreach($zone->otherCountries() as $countrie)
										<li class="ui-state-default" data-id="{{$countrie->id}}">{{$countrie->name}} </li>
									@endforeach
						</ul>
						</div>
						<div class="middle-list col-md-2">
						<div class="content">
							<a href="#" class="add-all-country">Ajouter tous > </a>

							<a href="" class="remove-all-country">< Retirer tous </a>
						</div>

						</div>

						<div class="connected-list right-list col-md-4 selected-items">

						<ul id="sortable4" class="connectedSortable">


						@foreach($zone->countries as $countrie)
							<input type="hidden" name="countries[]" value="{{$countrie->id}}" >
							<li class="ui-state-default" data-id="{{$countrie->id}}">{{$countrie->name}}</li>
						@endforeach

						</ul>
						</div>

						</div>
						@endif

						<!-- -->
	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
	{!! Html::script('plugins/ckeditor/ckeditor.js') !!}

	{!! Html::script('/js/libs/speakingurl.js') !!}
	{!! Html::script('/js/libs/slugify.js') !!}
	{!! Html::script('/js/libs/jquery-ui.min.js') !!}
@endpush

@push('javascripts')
	<script>

		CKEDITOR.disableAutoInline = true;
        @foreach ( App\Models\Language::all() as $lang)
	       CKEDITOR.inline( 'editor1_{{$lang->locale}}' );
        @endforeach


	</script>

	{!! Html::script('/js/admin/zones/form.js') !!}


@endpush
