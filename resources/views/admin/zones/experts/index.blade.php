@extends('layouts.app')

@section('content')

@include('admin.zones.partials.nav')

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Date</th>
                <th></th>
            </tr>
        </thead>
    </table>

</div>
@endsection

@push('javascripts')
<script>


var csrf_token = "{{csrf_token()}}";

var routes = {
	getData : '{{ action("Admin\Zone\ExpertController@getData",["id" => $zone->id]) }}'
};

</script>

{{ Html::script('js/admin/zones/experts/index.js')}}

@endpush
