

<div class="header">
	@if(isset($zone))
    	<h1>{{$zone->name}}</h1>
    @else
    	<h1>New zone</h1>
    @endif

</div>



@if(isset($zone))
<div class="nav tabs-inline">
	<ul class="tabs-inline">

		<li class="{{ Request::is('*show') ? 'active' : '' }}">
            <a href="{{action('Admin\Zone\ZoneController@show',['id' => $zone->id])}}">
            	<span class="text">
                	Détails
                </span>
            </a>
        </li>

        <li class="{{ Request::is('*experts') ? 'active' : '' }}">
            <a href="{{ action('Admin\Zone\ExpertController@index', ['id' => $zone->id]) }}">
            	<span class="text">
                	Contributeurs
               </span>
            </a>
        </li>

        <li class="{{ Request::is('*stats') ? 'active' : '' }}">
            <a href="{{ action('Admin\Zone\StatsController@index', ['id' => $zone->id]) }}">
                <span class="text">
                	Statistiques
                </span>
            </a>
        </li>

	</ul>
</div>
@endif
