@extends('layouts.frontend')

@section('title', translate::key('front.contact.contact') )

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item">{{ translate::key('front.contact.contact') }}</li>
		</ol>
	</div>
</div>


<div class="horizontal-container bg-with">

	<div class="horizontal-inner-container">

		<div class="contact">

			@if($errors->any())
				<div class="alert alert-danger">
							<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
							</ul>
					</div>
			@endif

			@if(Session::get('alert-type') == "success")
				<div class="alert alert-success">
						<ul>
								<li>{{ Session::get('message') }}</li>
						</ul>
				</div>
			@endif


			{{Form::open([
					'url' => route('contact.send'),
					'method' => 'POST',
					'id' => 'contact-form'
				])}}

				<div class="form-group {{$errors->has('name')?'has-error':''}}">
					<label for="name" class="control-label">{{ translate::key('front.contact.name') }} *</label>
					<input type="text" name="name" class="form-control" value="{{$user ? $user->getFullNameAttribute() : old('name')}} " />
				</div>

				<div class="form-group {{$errors->has('email')?'has-error':''}}">
					<label for="email" class="control-label">{{ translate::key('front.contact.email') }} *</label>
					<input type="text" name="email" class="form-control" value="{{$user ? $user->email : old('email')}} " />
				</div>

				<div class="form-group {{$errors->has('subject')?'has-error':''}}">
					<label for="subject" class="control-label">{{ translate::key('front.contact.subject') }} *</label>
					<input type="text" name="subject" class="form-control" value="{{old('subject')}}" />
				</div>

				<div class="form-group {{$errors->has('message')?'has-error':''}}">
					<label for="comment" class="control-label">{{ translate::key('front.contact.message') }} *</label>
					<textarea name="comment" rows="5" class="form-control">{{old('comment')}}</textarea>
				</div>

				<div class="submit-container text-center">
					<input type="submit" class="btn btn-primary" value="{{ translate::key('front.contact.send') }}" data-sending="{{ translate::key('front.contact.sending') }}">
				</div>

			{{Form::close()}}

		</div>

	</div>
</div>


@endsection

@push('javascripts')

<script>
	$(function(){
		$('#contact-form').submit(function(){
			var input = $('#contact-form').find('input[type="submit"]');
			input.css({pointerEvents:'none', opacity:0.3}).val(input.data('sending'));

		});
	});
</script>

@endpush
