@extends('layouts.frontend',[
	"is_product" => 1,
	"banner_image" => Storage::url($content->countrie->image)
])

@section('title',$content->title)

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
		  <li class="breadcrumb-item"><a href="{{route('country',['countryId' => $content->countrie->id ])}}">{{ $content->countrie->name }}</a></li>
		  <li class="breadcrumb-item">{{$content->title}}</li>
		</ol>
	</div>
</div>
<div class="horizontal-container bg-white user-content">

	<div class="horizontal-inner-container">

		<div class="fiche-produit-content">
			<div class="row">
				<div class="col-sm-2 text-center">
					{{ $expert->firstname or '' }} {{ $expert->lastname or '' }}
					@if($expert->image)
					<img src="{{Storage::url($expert->image)}}" class="img-circle img-responsive" alt="{{ $expert->firstname or '' }} {{ $expert->lastname or '' }}">
					@endif
					<br />
						@if($expert->profile)
						{!! str_limit(strip_tags($expert->profile->content), $limit = 30,'...')!!}
						@endif
						<br />
						<a href="{{route('contributor',['id'=>$expert->id])}}">See more</a>


			</div>
					<div class="col-sm-10">
			<h1>
				{{$content->title}}
			</h1>
			@if($content->image)
			<div class="row">
				<div class="col">
					<img src="{{Storage::url($content->image)}}" class="img-responsive">
					<div class="text-right">{{isset($content->legend) ? $content->legend : ''}} {{isset($content->copyright) ? $content->copyright : ''}}</div>
				</div>
			</div>
			<br />
			@endif
			{!! $content->description !!}
			@if($content->type == \App\Models\Content::TYPE_RICHTEXT)
				<div class="row">
					<div class="col">
				{!! $content->content !!}
			</div>
		</div>
				<br />
					@if($content->image)
						<div class="row">
							<div class="col">
								<img src="{{Storage::url($content->image_2)}}" class="img-responsive">
								<div class="text-right">{{isset($content->legend_2) ? $content->legend_2 : ''}} {{isset($content->copyright_2) ? $content->copyright_2 : ''}}</div>
							</div>
						</div>
					@endif
			@elseif($content->type == \App\Models\Content::TYPE_MOVIE)
				<div class="text-center row">
				<video id="my-video" class="video-js" controls preload="auto" width="100%" controls controlsList="nodownload" poster="@if($content->image_2){{Storage::url($content->image_2)}}@else {{Storage::url('video.png')}}@endif" data-setup="{}">
			     <source src="{!!$link!!}" type="video/mp4">;
			     <p class="vjs-no-js">
			       To view this video please enable JavaScript, and consider upgrading to a web browser that
			       <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			     </p>
  		</video>
			</div>
			@endif
			@if(isset($content->contact))
			<div class="row">
				<h3>Contacts</h3>
				{!! $content->contact !!}
			</div>
			@endif
		</div>
	</div>
</div>
	</div>

</div>

@endsection

@push('javascripts')

@endpush
