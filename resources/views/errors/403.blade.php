@extends('layouts.frontend')

@section('title','For more content please contact us')

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
		</ol>
	</div>
</div>


<div class="horizontal-container bg-with block-2">

	<div class="horizontal-inner-container">
		<div class="row">
					<div class="col text-center">
						<h2>For more content please contact <br /> <a href="mailto:info@globalgeonews.com" class="h2">info@globalgeonews.com</h2>
					</div>
		</div>
	</div>
</div>
@endsection
