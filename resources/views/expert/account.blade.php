@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('/css/dropzone.min.css') !!}
@endpush

@section('content')

<div class="header">
    <h1>My account</h1>
</div>

<div class="body">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 bg-white form-container">

        	<!-- Dropzone Avatar -->



        	{!! Form::open(['url' => action('Expert\AccountController@save'), 'files'=>true, 'method' => 'POST']) !!}


	    		{{ csrf_field() }}

		        @include('components.dropzone-image',[
						'image' => isset($user) && isset($user->image) ?
							$user->image : null,
						'size' => 'avatar',
						'id' => 'dropzone-1',
						'name' => 'image',
						'resizeWidth' => 500
				])


	            <div class="row">
	                <div class="col-md-6">
	                    <div class="form-group label-floating">
	                        <label class="control-label">Last name</label>
	                        <input type="text" name="firstname" value="{{ $user->firstname or '' }}" class="form-control">
	                    </div>
	                </div>

	                <div class="col-md-6">
	                    <div class="form-group label-floating">
	                        <label class="control-label">First name</label>
	                        <input type="text" name="lastname" value="{{ $user->lastname or '' }}" class="form-control"/>
	                    </div>
	                </div>
	            </div>

				<div class="form-group label-floating">
	                <label class="control-label">Type</label>
	                <p class="" style="padding-top:10px;font-weight:400">
	                	Contributors @foreach($user->expertZones as $zone) {{$zone->name}} @endforeach
	                </p>
	            </div>

				<div class="form-group label-floating">
	                <label class="control-label">E-mail</label>
	                <input type="text" name="email" value="{{ $user->email or '' }}" class="form-control"/>
	            </div>

	            <div class="form-group label-floating">
	                <label class="control-label">Password</label>
	                <input type="password" name="password" value="" class="form-control"/>
	            </div>

	            <div class="form-group label-floating">
	                <label class="control-label">Confirm password</label>
	                <input type="password" name="confirm_password" value="" class="form-control"/>
	            </div>

	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush
