@extends('layouts.app')

@push('stylesheets')
	{!! Html::style('plugins/hierarchy-select/hierarchy-select.min.css') !!}
@endpush

@section('content')

<div class="header">
    <h1>{{ $content->name or 'Content creation' }}</h1>

    <div class="actions">
    	@if(isset($content))
    		@include('expert.partials.status',[
		    	"statusOptions" => App\Models\Content::getStatus(),
		    	"item" => $content
		    ])
		@endif
    </div>

</div>


<div class="body">

	{!!
        Form::open([
            'action' => 'Expert\ContentController@save',
            'method' => 'POST',
            'enctype' => 'multipart/form-data'
        ])
    !!}

		<div class="row">
		    <div class="col-md-2">

		        {{ Form::hidden('content_id', isset($content) ? $content->id : null) }}
		        {{ Form::hidden('created_by', Auth::user()->id) }}
		        {{ Form::hidden('order', isset($content) ? $content->order : null) }}

				@include('components.dropzone-image',[
					'image' => isset($content) && isset($content->image) ?
						$content->image : null,
					'size' => 'banner',
					'id' => 'dropzone-1',
					'name' => 'image',
					'resizeWidth' => 1000
				])
			</div>
			<div class="col-md-10">
				@if (isset($content->image))
					<img src="{{Storage::url($content->image)}}" class="img-responsive">
				@endif
			</div>
		</div>
			<div class="row">
				<div class="col-md-7">
								<div class="form-group label-floating">
										<label class="control-label">Legend</label>
										<input type="text" name="legend" value="{{ $content->legend or '' }}" class="form-control">
								</div>
						 </div>
			<div class="col-md-5">
										 <div class="form-group label-floating">
												 <label class="control-label">Copyright</label>
												 <input type="text" name="copyright" value="{{ $content->copyright or '' }}" class="form-control">
										 </div>
			</div>
		</div>
			<div class="row">
				<div class="col-md-7">
		            <div class="form-group label-floating">
		                <label class="control-label">Title</label>
		                <input type="text" name="title" value="{{ $content->title or '' }}" class="form-control">
		            </div>
	           </div>

	           <div class="col-md-5">
	                <div class="form-group form-hierchical-select">
	                    <label>Country</label>


	                    <input type="hidden" name="country_id" value="{{ $content->country_id or '' }}" />

	            <div class="btn-group hierarchy-select" data-resize="auto" id="countrie-select">
						  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						      <span class="selected-label pull-left">&nbsp;</span>
						      <span class="caret"></span>
						      <span class="sr-only">Toggle Dropdown</span>
						  </button>
						  <div class="dropdown-menu open">
						  	  <div class="hs-searchbox">
						          <input type="text" class="form-control" autocomplete="off">
						      </div>
						      <ul class="dropdown-menu inner" role="menu">
						      	@if(isset($countriesTree))
						      	  @foreach($countriesTree as $countrie)
						      	  <li class="@if($countrie['parent']) dropdown-header @endif level-{{$countrie['level']}}" data-value="{{$countrie['value']}}" data-level="{{$countrie['level']}}">
						      	  	@if($countrie['parent'])
						      	  		<span class="text">{{$countrie['name']}}</span>
						      	  	@else
						      	  		<a href="#" class="select-option" data-value="{{$countrie['value']}}">{{$countrie['name']}}</a>
						      	  	@endif
						      	  </li>
						      	  @endforeach
								@endif
						      </ul>
						  </div>
						  <input class="hidden hidden-field" name="search_form[countrie]" readonly aria-hidden="true" type="text"/>
						</div>


	                </div>
	            </div>

           </div>

			<div class="form-group label-floating">
	            <label class="control-label">Description</label>
	            <textarea name="description" class="form-control" id="editor1" rows="10" cols="80">
	                {{ $content->description or '' }}
	            </textarea>
	        </div>

	        <div class="row">
				<div class="col-md-5">
	                <div class="form-group label-floating">
	                    <label class="control-label">Type</label>

	                    @php
	                    	$types = App\Models\Content::getTypes();
	                    @endphp

	                    {{
	                        Form::select(
	                            'type',
	                            [null => ''] + $types,
	                            isset($content) ? $content->type : null,
	                            [
	                                'class' => 'form-control',
	                                'id' => 'content-type-selector'
	                            ]
	                        )
	                    }}
	                </div>
	            </div>
	        </div>

					<div class="form-group label-floating type-content type-{{App\Models\Content::TYPE_MOVIE}}" @if(isset($content) && $content->type == App\Models\Content::TYPE_MOVIE) style="display:block;" @endif >
			            <label class="control-label">{{$types[App\Models\Content::TYPE_MOVIE]}} </label>
			            <!-- filemanager -->
									<div class="input-group">
								    <span class="input-group-btn">
								      <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
								        <i class="fa fa-picture-o"></i> Choose
								      </a>
								    </span><br />
										<input name="content[{{App\Models\Content::TYPE_MOVIE}}]" id="thumbnail" type="text" class="form-control"  value="@if(isset($content) && $content->type == App\Models\Content::TYPE_MOVIE) {{$content->content}} @endif" />
								  </div>
									<!-- / filemanager -->

			   </div>

			<div class="form-group label-floating type-content type-{{App\Models\Content::TYPE_RICHTEXT}}" @if(isset($content) && $content->type == App\Models\Content::TYPE_RICHTEXT) style="display:block;" @endif >
	            <label class="control-label">{{$types[App\Models\Content::TYPE_RICHTEXT]}}</label>
	            <textarea name="content[{{App\Models\Content::TYPE_RICHTEXT}}]" class="form-control" id="editor2" rows="10" cols="80">
	                @if(isset($content) && $content->type == App\Models\Content::TYPE_RICHTEXT) {{$content->content}} @endif
	            </textarea>
							<div class="row">
									<div class="col-md-2">

									@include('components.dropzone-image',[
										'image' => isset($content) && isset($content->image_2) ?
										$content->image_2 : null,
										'size' => 'banner',
										'id' => 'dropzone-2',
										'name' => 'image_2',
										'resizeWidth' => 1000
									])
								</div>
								<div class="col-md-10">
									 @if (isset($content->image_2))
										<img src="{{Storage::url($content->image_2)}}" class="img-responsive">
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-md-7">
												<div class="form-group label-floating">
														<label class="control-label">Legende</label>
														<input type="text" name="legend_2" value="{{ $content->legend_2 or '' }}" class="form-control">
												</div>
										 </div>
							<div class="col-md-5">
														 <div class="form-group label-floating">
																 <label class="control-label">Copyright</label>
																 <input type="text" name="copyright_2" value="{{ $content->copyright_2 or '' }}" class="form-control">
														 </div>
							</div>
						</div>
	        </div>



	        <div class="form-group label-floating">
	            <input type="submit" value="Sauvegarder" class="btn btn-primary submit-form" />
	        </div>


	    </div>
	</div>

	{!! Form::close() !!}

</div>
@endsection

@push('javascripts-libs')
	{!! Html::script('plugins/ckeditor/ckeditor.js') !!}
	{!! Html::script('plugins/hierarchy-select/hierarchy-select.js') !!}
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush

@push('javascripts')
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script>
    CKEDITOR.disableAutoInline = true;
    CKEDITOR.inline( 'editor1' );
		CKEDITOR.replace( 'editor2', {
		 toolbar: [
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
			{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
			{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			'/',
			{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			{ name: 'others', items: [ '-' ] },
			{ name: 'about', items: [ 'About' ] }
		]
		});

    var csrf_token = "{{ csrf_token() }}";

    var country_id = null;
    @if(isset($content))
    	country_id = {{$content->country_id}};
    @endif

    var routes = {
    	changeStatus : "{{action('Expert\ContentController@changeStatus')}}"
    };

</script>

{!! Html::script('/js/experts/contents/form.js') !!}


@endpush
