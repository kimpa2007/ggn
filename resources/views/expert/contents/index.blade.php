@extends('layouts.app')

@section('content')
<div class="header">
    <h1>
        Contenus
    </h1>

    <a href="{{ action('Expert\ContentController@create') }}" class="btn btn-primary">Créer</a>
</div>

<div class="body">
	<table id="table" class="table" style="background:#FFF;margin-top: 20px;">
        <thead>
            <tr>
                <th>Order</th>
                <th>#</th>
                <th>Title</th>
        				<th>Country</th>
        				<th>Views</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Filter by: </th>
                <th></th>
                <th></th>
        				<th></th>
        				<th></th>
                <th></th>
        				<th></th>
            </tr>
        </tfoot>
    </table>

</div>
@endsection

@push('javascripts')

<script>

	var routes = {
		getData : '{{ action("Expert\ContentController@getData") }}',
    	updateOrder : '{{ action("Expert\ContentController@updateOrder") }}'
    };

    var csrf_token = "{{ csrf_token() }}";

</script>

{!! Html::script('/js/experts/contents/index.js') !!}

@endpush
