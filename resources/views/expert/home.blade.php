@extends('layouts.app')

@section('content')

<div class="body dashboard" id="table_wrapper" >

  <div class="row">

    <div class="col-md-6">
      <h4><i class="fa fa-file-o"></i> &nbsp; Contents ({{$contents->count()}})</h4>
      <div class="table-container">
        <table class="table" style="background:#FFF;">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Date</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($contents->get() as $content )
                <tr class="table-row" onclick="window.document.location='{{route('expert.contents.show',["id" => $content->id])}}';">
                    <td>{{$content->id}}</td>
                    <td>{{$content->title}}</td>
                    <td>{{$content->created_at->format('d/m/Y')}}</td>
                </tr>
                @endforeach

              </tbody>
          </table>
        </div>
    </div>

      <div class="col-md-6">

      </div>


    <div class="col-md-6">
    </div>

  </div>

</div>

@endsection

@push('javascripts')

@endpush
