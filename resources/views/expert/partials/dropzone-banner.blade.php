<div class="dropzone-banner">

	<input type="hidden" id="image" name="image" value="{{$item->image or ''}}">
	<input type="hidden" id="uploading" name="uploading" value="0">

	<div class="dropzone-container" @if( isset($item) && $item->image) style="display:none;" @endif>

		<div id="image-dropzone" class="dropzone image-container">

			<div class="progress-bar"></div>
			<div class="background-image"></div>

			<div class="dz-message"></div>

            <div class="fallback">
                <input name="image" type="file" multiple />
            </div>

            <div class="message">
				<p>Drop an image or<br><span class="link"><i class="fa fa-upload"></i> &nbsp; click here</span></p>
    		</div>
    	</div>
	</div>

	<div class="image-container uploaded" @if(!isset($item) || (isset($item) && !$item->image)) style="display:none;" @endif>
		<div class="background-image" @if(isset($item)) style="background-image:url('{{ Storage::url($item->image)}}')" @endif>
		</div>
		<div class="actions">
			<a href="" class="btn btn-table" id="remove-picture"><i class="fa fa-trash"></i> &nbsp; Delete </a>
		</div>
	</div>

</div>
