@if(isset($items))
    @if(ceil($items->total() / $items->perPage()) > 1)
    <div class="text-center">
        <ul class="pagination pagination-primary">
            <li><a href="{{ $items->previousPageUrl() }}"><</a></li>
            @for ($i = 1; $i < ceil($items->total() / $items->perPage()) + 1; $i++)
                <li class="@if($items->currentPage() == $i) active @endif">
                    <a href="{{ $items->url($i) }}">{{ $i }}</a>
                </li>
            @endfor()
            <li><a href="{{ $items->nextPageUrl() }}">></a></li>
        </ul>
    </div>
    @endif
@endif
