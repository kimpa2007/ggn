@extends('layouts.app')

@section('content')

<div class="header">
    <h1>My profile</h1>
</div>

<div class="body">
    <div class="row">
        <div class="col-md-12 bg-white form-container">
        	{!! Form::open(['url' => action('Expert\ProfileController@save'), 'files'=>true, 'method' => 'POST']) !!}
	    		{{ csrf_field() }}

	    		<div class="row">
	    			<div class="col-md-3">

				        @include('components.dropzone-image',[
							'image' => isset($user->profile) && isset($user->profile->image) ?
								$user->profile->image : null,
							'size' => 'avatar',
							'id' => 'dropzone-1',
							'name' => 'image',
							'resizeWidth' => 500
						])


					</div>
					<div class="col-md-9">

		                <div class="form-group label-floating">
		                    <label class="control-label">Content</label>
		                    {{
		                        Form::textarea(
		                            'content',
		                            $user->profile ? $user->profile->content : '',
		                            [
		                                'id' => 'editor1',
		                                'placeholder'=> 'Write your description here...'
		                            ]
		                        )
		                    }}
		                </div>
	                </div>
				</div>

	            <div class="form-group label-floating text-center">
	                <input type="submit" value="Envoyer" class="btn btn-primary submit-form"/>
	            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@push('javascripts-libs')
	{!! Html::script('/js/dropzone.min.js') !!}
	{!! Html::script('/js/libs/jquery.imageUploader.js') !!}
@endpush

@push('javascripts')
<script src="/plugins/ckeditor/ckeditor.js"></script>
<script>

    CKEDITOR.disableAutoInline = true;
    CKEDITOR.inline( 'editor1' );
</script>

@endpush
