@extends('layouts.frontend',[
	"is_product" => 1,
	"banner_image" => Storage::url($content->countrie->image),
 	"og_title" => $content->translate(LaravelLocalization::getCurrentLocale())->title,
	"og_image" => Storage::url($content->image),
	"og_description" => $content->translate(LaravelLocalization::getCurrentLocale())->description,
	"og_url" => Request::url(),

])

@section('title', $content->translate(LaravelLocalization::getCurrentLocale())->title)
@section('metaDescription', str_limit(strip_tags($expert->profile->translateOrDefault(LaravelLocalization::getCurrentLocale())->content), 30,'...') )


@section('content')

<style>
	.IN-widget {
		vertical-align: top !important;
	}
</style>

<div id="fb-root"></div>

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item"><a href="{{route('country',['countryId' => $content->countrie->id ])}}">{{ $content->countrie->translate(LaravelLocalization::getCurrentLocale())->name }}</a></li>
		  <li class="breadcrumb-item">{{$content->translate(LaravelLocalization::getCurrentLocale())->title}}</li>
		</ol>
	</div>
</div>

<div class="horizontal-container bg-white user-content">
	<div class="horizontal-inner-container">
		<div class="fiche-produit-content">
			<div class="row">
				<div class="col-sm-2 text-center">
					{{ $expert->firstname or '' }} {{ $expert->lastname or '' }}
					@if($expert->image)
					<img src="{{Storage::url($expert->image)}}" class="img-circle img-responsive" alt="{{ $expert->firstname or '' }} {{ $expert->lastname or '' }}">
					@endif
					<br />
						@if($expert->profile)
							{!! str_limit(strip_tags($expert->profile->translateOrDefault(LaravelLocalization::getCurrentLocale())->content), 30,'...')!!}
						@endif
						<br />
						<a href="{{route('contributor',['id'=>$expert->id])}}">{{ translate::key('front.general.see_more') }}</a>
			</div>
					<div class="col-sm-10">
			<h1>
				{{$content->translateOrDefault(LaravelLocalization::getCurrentLocale())->title}}
			</h1>


			<div class="row">
				<div class="fb-share-button" data-href="{{ route('content-free',$content->id) }}" data-layout="button_count" data-size="small">
					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ route('content-free',$content->id) }}4&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Partager</a>
				</div>

				<a href="http://twitter.com/share?text={{ $content->translate(LaravelLocalization::getCurrentLocale())->title }}&url={{ route('content-free',$content->id) }}" class="twitter-hashtag-button" data-show-count="false">Tweeter</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

				<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: fr_FR</script>
				<script type="IN/Share" data-url="{{ route('content-free',$content->id) }}" data-counter="top"></script>

				<a class="btn-blue" href="mailto:?subject={{ $content->translate(LaravelLocalization::getCurrentLocale())->title }}&amp;body=Pour en savoir plus: <a href='{{ route('content-free',$content->id) }}'>{{ route('content-free',$content->id) }}</a>" title="Envoyer par email">
					<i class="fa fa-envelope-o" aria-hidden="true"></i> Envoyer par email
				</a>
			</div>



			

			@if($content->type == \App\Models\Content::TYPE_RICHTEXT)
				@if($content->image)
					<div class="row">
						<div class="col">
							<img src="{{Storage::url($content->image)}}" class="img-responsive">
							<div class="text-right">{{isset($content->translate(LaravelLocalization::getCurrentLocale())->legend) ? $content->translate(LaravelLocalization::getCurrentLocale())->legend : ''}} {{isset($content->translate(LaravelLocalization::getCurrentLocale())->copyright) ? $content->translate(LaravelLocalization::getCurrentLocale())->copyright : ''}}</div>
						</div>
					</div>
					<br />
				@endif
			@endif
			<div class="row">
				<div class="col">
			{!! $content->translateOrDefault(LaravelLocalization::getCurrentLocale())->description !!}
			</div>
		</div>
			@if($content->type == \App\Models\Content::TYPE_RICHTEXT)
				<div class="row">
					<div class="col">
						{!! $content->translate(LaravelLocalization::getCurrentLocale())->content !!}
					</div>
				</div>
					@if($content->image)
						<div class="row">
							<div class="col">
								<img src="{{Storage::url($content->image_2)}}" class="img-responsive">
								<div class="text-right">{{isset($content->translate(LaravelLocalization::getCurrentLocale())->legend_2) ? $content->translate(LaravelLocalization::getCurrentLocale())->legend_2 : ''}} {{isset($content->translate(LaravelLocalization::getCurrentLocale())->copyright_2) ? $content->translate(LaravelLocalization::getCurrentLocale())->copyright_2 : ''}}</div>
							</div>
						</div>
					@endif
			@elseif($content->type == \App\Models\Content::TYPE_MOVIE)
				<div class="text-center row">
				<video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="800" height="400" controls controlsList="nodownload" poster="@if($content->image_2){{Storage::url($content->image_2)}}@else {{Storage::url('video.png')}}@endif" data-setup='{"fluid": true}'>
			     <source src="{!!$link!!}" type="video/mp4">;
			     <p class="vjs-no-js">
			       To view this video please enable JavaScript, and consider upgrading to a web browser that
			       <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			     </p>
  		</video>
			</div>
			<div class="text-center row">
						<p><br /><small><em>{{ translate::key('content.movie.reproduction.prohibited') }}</em></small></p>
			</div>
			@endif

			@if($content->translate(LaravelLocalization::getCurrentLocale())->contact && $content->translate(LaravelLocalization::getCurrentLocale())->contact != '')
				<div class="row">
					<h3>Contacts</h3>
					{!! $content->translate(LaravelLocalization::getCurrentLocale())->contact !!}
				</div>
			@endif
		{{--	<div class="row">
				<p><br />
				@if(!empty($isfree))
					{{ translate::key('content.sharring.free') }} : {{ $isfree }}
				@else
					{{ translate::key('content.not.sharring.free') }}
				@endif
			</p>
			</div>
		--}}
		</div>

	</div>
</div>
	</div>

</div>
@endsection

@push('javascripts')
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2&appId=1258205060999385&autoLogAppEvents=1"></script>
	<script type="text/javascript">
		$(document).ready(function () {
	    //Disable cut copy paste
	    $('body').bind('cut copy paste', function (e) {
	    	alert("Reproduction interdite, tous droits réservés.")
	        e.preventDefault();
	    });
	   
	    //Disable mouse right click
	    $("body").on("contextmenu",function(e){
	    	alert("Reproduction interdite, tous droits réservés.")
	        return false;
	    });
	});
</script>
@endpush
