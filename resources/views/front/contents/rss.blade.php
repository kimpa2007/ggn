<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<atom:link href="https://www.globalgeonews.com/rss.xml" rel="self" type="application/rss+xml" />
		<description>Quotidien digital d'informations centré sur trois piliers : grands reportages, analyses et interviews</description>
		<language>fr</language>
		<link>https://www.globalgeonews.com</link>
		<pubDate>{{ date('r') }}</pubDate>
		<title>GlobalGeoNews / L’actualité du monde réel</title>
		<image>
			<url>https://www.globalgeonews.com/images/front/logo.jpg</url>
		    <title>GlobalGeoNews</title>
		    <link>https://www.globalgeonews.com</link>
		</image>

		@foreach($contents as $content)
			@php
              $title = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title);
              $expert = \App\User::find($content->created_by);
              $theme =  $content->themes->first();
              $theme_name = "";
              if(!empty($theme)){
	              $theme_name = ($theme->name);
	          }
          	@endphp

			<item>
				<guid isPermaLink="false"><![CDATA[ {{ route('user.content.show',$content->id) }} ]]></guid>
				<title><![CDATA[ {{ str_limit($title, $limit = 240,'...') }} ]]></title>
				<link><![CDATA[ {{ route('user.content.show',$content->id) }} ]]></link>
				<pubDate>{{ $content->published_at->format('r') }}</pubDate>
				<description>
					<![CDATA[ {!! $content->translateOrDefault(LaravelLocalization::getCurrentLocale())->description !!}]]>
				</description>
				
				@if($theme_name != '')<category>{{ $theme_name }}</category>@endif

			</item>
		@endforeach
	</channel>
</rss>