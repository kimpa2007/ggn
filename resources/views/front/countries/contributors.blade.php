@php
	$zone = $country ? $country->zones->first() : null;
@endphp

@extends('layouts.frontend')

@section('title',$country->name)
@php
	if($country->description){
        $metaDescription = str_limit(strip_tags($country->description), $limit = 250,'...');
	} else {
        $metaDescription ='';
	}
@endphp

@section('metaDescription',$metaDescription)
@section('header-banner')
	@if($country->image)
		<div class="background-image" style="background-image:url('{{ Storage::url($country->image) }}'); background-position: center {{$country->image_align or ''}};">
			<div class="legend">{{$country->legend or ''}}</div>
		</div>
	@endif

	<div class="background-image-hover"></div>
	<div class="horizontal-inner-container header-content">
		<div class="title">
			<h1>@yield('title')</h1>
			<hr class="white">
		</div>
	</div>
@endsection

@section('content')
<div class="page-country">

    <div class="horizontal-container  bg-black nav">
        <ul>
            @foreach (App\Models\Theme::all() as $theme)
            <li>
                <a href="{{ route('country.theme', [
                    'id' => $country->id,
                    'theme_slug' => $theme->translate(LaravelLocalization::getCurrentLocale())->slug
                    ]) }}" class="{{ Request::is('*' . $theme->translate(LaravelLocalization::getCurrentLocale())->slug . '*') ? 'active' : '' }}">
                    {{ $theme->translate(LaravelLocalization::getCurrentLocale())->name or '' }}
                </a>
            </li>
            @endforeach

            @if($zone)
            <li>
                <a href="{{ route('country.contributors', $country->id) }}" class="{{ Request::is('*contributors*') ? 'active' : '' }}">
                    Contributors
                </a>
            </li>
            @endif
        </ul>
    </div>

	<div class="horizontal-container bg-black">
		<div class="contents-container">
			<div class="horizontal-inner-container contents-grid-container">
                <div class="row country-container">
                    @foreach($contributors as $contributor)
                        <div class="grid-item col-md-3">

            				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
            					<div class="flipper">
            						<div class="front">
            							<!-- front content -->
            							<div class="background-image" style="background-image:url('{{ Storage::url($contributor->image)}}')"></div>
            							<div class="background-hover"></div>
            								<div class="grid-item-content">
            								<h3>
            									{{ $contributor->firstname or '' }} {{ $contributor->lastname or '' }}
            								</h3>
            							</div>
            						</div>
            						<div class="back">
            							<!-- back content -->
            							<div class="background-image" style="background-image:url('{{ Storage::url($contributor->image)}}')"></div>
            							<div class="background-hover-turn"></div>
            							<div class="grid-item-content-turn">
            								<h4>{{ $contributor->firstname or '' }} {{ $contributor->lastname or '' }}</h4>
            								<p>

            								</p>

            									<div class="button-container">
            										<a href="{{route('contributor',['id'=> $contributor->id])}}" class="btn btn-primary">DISCOVER</a>
            									</div>

            							</div>
            						</div>
            					</div>
            				</div>

            			</div>
                    @endforeach()
                </div>
			</div>
		</div>
	</div>

</div>

@endsection


@push('javascripts')

@endpush
