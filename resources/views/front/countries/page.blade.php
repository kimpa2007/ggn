@php
	$zone = $country ? $country->zones->first() : null;
@endphp

@extends('layouts.frontend')

@section('title',$country->translate(LaravelLocalization::getCurrentLocale())->name)
@php
	if($country->translate(LaravelLocalization::getCurrentLocale())->description){
		$metaDescription = str_limit(strip_tags($country->translate(LaravelLocalization::getCurrentLocale())->description), $limit = 250,'...');
	} else {
		$metaDescription ='';
	}
@endphp

@section('metaDescription',$metaDescription)
@section('header-banner')
	@if($country->image)
		<div class="background-image" style="background-image:url('{{ Storage::url($country->image) }}'); background-position: center {{$country->image_align or ''}};">
			<div class="legend">{{$country->translate(LaravelLocalization::getCurrentLocale())->legend or ''}}</div>
		</div>
	@endif

	<div class="background-image-hover"></div>
	<div class="horizontal-inner-container header-content">
		<div class="title">
			<h1>@yield('title')</h1>
			<hr class="white">
		</div>
	</div>
@endsection

@section('content')
<div class="page-country">

	<div class="horizontal-container  bg-black nav">
		<ul>
			@foreach (App\Models\Theme::all() as $theme)
			<li>
				<a href="{{ route('country.theme', [
					'id' => $country->id,
					'theme_slug' => $theme->translate(LaravelLocalization::getCurrentLocale())->slug
					]) }}" class="{{ Request::is('*' . $theme->translate(LaravelLocalization::getCurrentLocale())->slug . '*') ? 'active' : '' }}">
					{{ $theme->translate(LaravelLocalization::getCurrentLocale())->name or '' }}
				</a>
			</li>
			@endforeach

			@if($zone)
			<li>
			    <a href="{{ route('country.contributors', $country->id) }}" class="{{ Request::is('*contributors*') ? 'active' : '' }}">
					Contributors
				</a>
			</li>
			@endif
		</ul>
	</div>

	<div class="horizontal-container bg-black">

		{{-- <div class="horizontal-inner-container">
			{!! $country->translate(LaravelLocalization::getCurrentLocale())->description !!}
		</div> --}}


		{{-- <div class="filters">
			<div class="horizontal-inner-container">
				<ul>
					<li>
						Filter:
					</li>

					<li>
						<input type="checkbox" id="filter-2" class="filter-type" name="filter-2" value="{{App\Models\Content::TYPE_MOVIE}}">
						<label for="filter-2">Video</label>
					</li>
					<li>
						<input type="checkbox" id="filter-3" class="filter-type" name="filter-3" value="{{App\Models\Content::TYPE_RICHTEXT}}">
						<label for="filter-3">Article</label>
					</li>
				</ul>
			</div>
		</div> --}}

		<div class="contents-container">
			<div class="horizontal-inner-container contents-grid-container">

				<div class="contents-message" style="{{ $count == 0 ? 'display:block;' : 'display:none;' }}">
					{{ translate::key('front.general.no_contents') }}
				</div>

				<div class="row content-wrapper">
					@foreach($contents as $content)
					<div class="grid-item col-md-3">
						<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
						<div class="front">
						  <!-- front content -->
							@if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
						  <div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
						  <div class="background-hover"></div>
						    <div class="grid-item-content">
						    @php
						        $title = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title);
						    @endphp
						    <h3>
						      {{ str_limit($title, $limit = 30,'...')}}
						    </h3>
						  </div>
						</div>
						<div class="back">
						  <!-- back content -->
						  <div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
						  <div class="background-hover-turn"></div>
						  <div class="grid-item-content-turn">
						    <h4>{{ $content->translate(LaravelLocalization::getCurrentLocale())->title}}</h4>
						    <p>

						      @php
						        //remove all html tags from description
						        $description = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->description);
						      @endphp
						      {{ str_limit($description, $limit = 150,'...')}}
						      @if(Auth::user())
						        <div class="button-container">
						          <a href="{{route('user.content.show',['id'=>$content->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
						        </div>
						      @endif
						    </p>


						  </div>
						</div>
						</div>
						</div>
					</div>
					@endforeach
			    </div>

				<div id="count" data-count="{{$count}}"></div>

				@if($count > $itemsPerPage)
					<div class="see-more">
						<a href="#" class="btn btn-secondary" id="toogle-more">{{ translate::key('front.general.see_more') }}</a>
					</div>
				@endif
			</div>
		</div>
	</div>

</div>

@endsection


@push('javascripts')
<script>
	var itemsPerPage = {{ $itemsPerPage }};
	var count = {{ $count }};
	var page = 1;
	var availablePages = Math.ceil(count / itemsPerPage);

	$(function(){
		$("#toogle-more").click(function(e){

			e.preventDefault();

			if(page >= availablePages) {
				return;
			}

			$.ajax({
				url: "?",
				type: "GET",
				data: {
					page : page
				},
				success: function(response){
					page++;
					$(".content-wrapper").append($(response).find('.content-wrapper').html());

					if(parseInt(page) >= parseInt(availablePages)){
						$("#toogle-more").parent().fadeOut();
					}
				},
				error:function(msg){}
			});
		});
	});
</script>
@endpush
