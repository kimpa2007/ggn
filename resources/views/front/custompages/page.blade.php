@extends('layouts.frontend')

@section('title', $page->translate(LaravelLocalization::getCurrentLocale())->title )

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item">{{ $page->translate(LaravelLocalization::getCurrentLocale())->title }}</li>
		</ol>
	</div>
</div>


<div class="horizontal-container bg-white block-2">

	<div class="horizontal-inner-container">

		{!! $page->translate(LaravelLocalization::getCurrentLocale())->content !!}

	</div>
</div>


@endsection

@push('javascripts')

<script>
	$(function(){

	});
</script>

@endpush
