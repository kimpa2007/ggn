@extends('layouts.frontend')
@section('title',translate::key('front.user.createlogin')) 
@section('metaDescription',translate::key('front.user.createlogin'))

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item">{{ __('messages.front.login.login_register') }}</li>
		</ol>
	</div>
</div>
<div class="user-account">
	<div class="horizontal-inner-container">
		<div>
			@if(Session::has('flash_message'))
				<div class="row">
				  <div class="col-md-12">
				   <ul class="flash-msg-list">
						<li class="flash-msg success">{!! session('flash_message') !!}</li>
					</ul>
				  </div>
				</div>
	      	@endif
	      	@if($errors->any())
				<div class="row">
				    <div class="col-md-12">
			             <ul class="flash-msg-list">
			                {!! implode('', $errors->all('<li class="flash-msg">:message</li>')) !!}
			            </ul>
				    </div>
				</div>
			@endif 

			@if (isset($errors) && $errors->has('password'))
					<div class="alert alert-danger control-label">{{ $errors->first('password') }}</div>
			@endif
			@if (isset($errors) && $errors->has('email'))
					<div class="alert alert-danger">{{ $errors->first('email') }}</v>
			@endif

		</div>

		<div class="row">	
			<div class="col-md-6">

				<form role="form" method="POST" action="/login">

					{{ csrf_field() }}

					<div class="section-title">
						<h2>{{ __('messages.front.login.login') }}<br/><br/></h2>
						<hr class="decoration" />
					</div>

					<div class="row {{ isset($errors) && $errors->has('email') ? ' has-error' : '' }}">
	                	<div class="col-md-4" >
	                    	<label class="control-label">{{ __('messages.front.login.field.email') }}</label>
	                	</div>
	                   	<div class="col-md-8">
	                    	<input type="text" required name="email" value="{{ isset($partner) ? $partner->manager->email : old('email') }}" class="form-control"/>
	                    </div>
	                </div>

	                <div class="row {{ isset($errors) && $errors->has('password') ? ' has-error' : '' }}">

	                	<div class="col-md-4">
	                    	<label class="control-label">{{ __('messages.front.login.field.password') }}</label>
	                	</div>
	                   	<div class="col-md-8">
	                    	<input type="password" name="password" value="" required class="form-control"/>
	                    </div>
	                </div>

	                <div class="row text-center">
	                	<div class="col-md-4" ></div>
	                	<div class="col-md-8">
	               			<a class="mgb-15" href="{{ route('password.request') }}">{{ translate::key('front.navbar.forgot_password') }}</a>
		                	<input type="submit" value="{{ translate::key('front.user.login') }}" class="btn btn-primary submit-form"/>
		                </div>
	            	</div>
				</form>

			</div>

			<div class="col-md-6">
				<div class="section-title">
						<h2>Inscrivez-vous et choisissez votre formule d'abonnement</h2>
					<hr class="decoration" />
				</div>

			
				{!!
	                Form::open([
	                    'url' =>  action('User\UserController@create'),
	                    'method' => 'POST'
	                ])
	            !!}

				<div class="section-body">
	                    <div class="row ">
	                    	<div class="col-md-4">
	                        	<label class="control-label">{{ __('messages.front.login.field.firstname') }}</label>
	                    	</div>
		                   	<div class="col-md-8">
		                        <input type="text" name="firstname" value="{{ isset($partner) ? $partner->manager->firstname : old('firstname') }}" class="form-control"/>
		                    </div>
		                </div>

		                <div class="row ">
	                    	<div class="col-md-4">
	                        	<label class="control-label">{{ __('messages.front.login.field.lastname') }}</label>
	                    	</div>
		                   	<div class="col-md-8">
		                        <input type="text" name="lastname" value="{{ isset($partner) ? $partner->manager->lastname : old('lastname') }}" class="form-control"/>
		                    </div>
		                </div>

		                <div class="row ">
	                    	<div class="col-md-4">
	                        	<label class="control-label">{{ __('messages.front.login.field.email') }}</label>
	                    	</div>
		                   	<div class="col-md-8">
	                        	<input type="text" name="email" value="{{ isset($partner) ? $partner->manager->email : old('email') }}" class="form-control"/>
		                    </div>
		                </div>

		                <div class="row ">
	                    	<div class="col-md-4">
	                        	<label class="control-label">{{ __('messages.front.login.field.password') }}</label>
	                    	</div>
		                   	<div class="col-md-8">
	                        	<input type="password" name="password" value="" class="form-control"/>
		                    </div>
		                </div>

		                <div class="row ">
	                    	<div class="col-md-4">
	                        	<label class="control-label">{{ __('messages.front.login.field.password_confirmation') }}</label>
	                    	</div>
		                   	<div class="col-md-8">
	                        	<input type="password" name="password_confirmed" value="" class="form-control"/>
		                    </div>
		                </div>

		                <div class="row text-center mgb-15">
		                	<div class="col-md-4" ></div>
	                		<div class="col-md-8">
			                	<input type="submit" value="Choisir un abonnement" class="btn btn-primary submit-form"/>
			                </div>
			            </div>
	            </div>
	            {!! Form::close() !!}				
			</div>
		</div>
	</div>
</div>


@endsection

@push('javascripts')
	<script>
	</script>
@endpush
