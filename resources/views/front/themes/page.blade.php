@extends('layouts.frontend')
@section('title',$theme->translate(LaravelLocalization::getCurrentLocale())->meta_title)
@section('metaDescription',$theme->translate(LaravelLocalization::getCurrentLocale())->meta_description)
@section('content')

<div class="horizontal-container bg-black block-2 container-content">
	<div class="horizontal-inner-container">
		<h2>{{ $theme->translate(LaravelLocalization::getCurrentLocale())->name or '' }}
			@if($theme->id == 6 )
           		<span class="text-blanc">Assurances de l’Adour</span>
  		    @endif 
  		</h2>
		<div class="seprator" style="height:20px;"></div>
		<div class="row content-wrapper">
			@foreach($contents as $content)
				<div class="grid-item col-md-3">

					<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
						<div class="flipper">

							<div class="front">
								<!-- front content -->
								@if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
								<div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
								<div class="background-hover"></div>
									<div class="grid-item-content">
									@php
										//remove all html tags from description
										$name = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title);
									@endphp
									<h3>
										{{ str_limit($name, $limit = 50,'...')}}
									</h3>
								</div>
							</div>

							<div class="back">
								<!-- back content -->
								<div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
								<div class="background-hover-turn"></div>
								<div class="grid-item-content-turn">
									<h4>{{ $content->name }}</h4>
									<p>
										{{ str_limit(strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->description), $limit = 150, '...')}}
								{{--		@if(Auth::user())                                  
                                    
	                                    	@if(Auth::user()->canAccessContent())
		                                      <div class="button-container">
		                                        <a href="{{route('user.content.show',['id'=>$content->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
		                                      </div>
		                                    @else
		                                      <div class="button-container">
		                                          <a href="{{ route('offer.show') }}" class="btn btn-primary">{{ __('messages.abonnement.abonnezvous') }}</a>
		                                      </div>
		                                    @endif
		                                @else
		                                  <div class="button-container">
		                                      <a href="{{ route('user.login') }}" class="btn btn-primary">{{ __('messages.abonnement.abonnezvous') }}</a>
		                                  </div>
		                                @endif
		                        --}}
		                        		 <div class="button-container">
	                                        <a href="{{route('user.content.show',['id'=>$content->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
	                                      </div>

									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>

		@if($count > $itemsPerPage)
			<div class="see-more">
				<a href="#" class="btn btn-secondary" id="toogle-more">{{ translate::key('front.general.see_more') }}</a>
			</div>
		@endif

		<div class="seprator" style="height:20px;"></div>
	</div>
</div>

@endsection

@push('javascripts')
	<script>
		var itemsPerPage = {{ $itemsPerPage }};
		var count = {{ $count }};
		var page = 1;
		var availablePages = Math.ceil(count / itemsPerPage);

		$(function(){
			$("#toogle-more").click(function(e){
				e.preventDefault();
				if(page >= availablePages) {
					return;
				}

				$.ajax({
					url: "{{ route('theme', $theme["slug"]) }}",
					type: "GET",
					data: {
						page : page
					},
					success: function(response){
						page++;
						$(".content-wrapper").append($(response).find('.content-wrapper').html());
						if(parseInt(page) >= parseInt(availablePages)){
							$("#toogle-more").parent().fadeOut();
						}
					},
					error:function(msg){}
				});
			});
		});
	</script>
@endpush
