@php
    $image = '';
@endphp
@extends('layouts.frontend',[
	"is_small" => 1,
	"banner_image" => $image
])

@section('title','GlobalGeoNews Contributors | Our reporters and researchers')
@section('metaDescription','iscover our reporters and researchers and all our team who contribute to GlobalGeoNews actualities. daily geopolitical news by reporters and researchers. Découvrez les journalistes reporters, chercheurs, analystes et toute l´équipe qui contribuent GlobalGeoNews.')
@section('content')
    <div class="breadcrumbs-container">
        <div class="horizontal-inner-container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
                <li class="breadcrumb-item">{{ translate::key('front.general.contributors') }}</li>
            </ol>
        </div>
    </div>
    <div class="horizontal-container block-2">
        <div class="horizontal-inner-container">
            <h2>{{ translate::key('front.general.contributors') }}</h2>

            <div class="seprator" style="height:20px;"></div>
            <div class="row country-container">
                @foreach($users as $user)
                    <div class="grid-item col-md-3">
                        <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                            <div class="flipper">
                                <div class="front">
                                    <!-- front content -->
                                    <div class="background-image" style="background-image:url('{{ Storage::url($user->image)}}')"></div>
                                    <div class="background-hover"></div>
                                    <div class="grid-item-content">
                                        <h3>
                                            {{ $user->firstname or '' }} {{ $user->lastname or '' }}
                                        </h3>
                                    </div>
                                </div>
                                <div class="back">
                                    <!-- back content -->
                                    <div class="background-image" style="background-image:url('{{ Storage::url($user->image)}}')"></div>
                                    <div class="background-hover-turn"></div>
                                    <div class="grid-item-content-turn">
                                        <h4>{{ $user->firstname or '' }} {{ $user->lastname or '' }}</h4>
                                        <p></p>
                                        <div class="button-container">
                                            <a href="{{route('contributor',['id'=> $user->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="seprator" style="height:20px;"></div>
        </div>
    </div>
@endsection

@push('javascripts')

@endpush
