@php
    if(isset($user->profile->image)) {
        $image = Storage::url($user->profile->image);
    } else {
        $image = '';
    }
@endphp
@extends('layouts.frontend',[
	"is_small" => 1,
	"banner_image" => $image
])

@php
    if($user->profile){
     $metaDescription = str_limit(strip_tags($user->profile->content), $limit = 250,'...');
 }else {
     $metaDescription = '';
 }
@endphp

@section('title',$user->firstname.' '.$user->lastname)
@section('metaDescription',$metaDescription)
@section('content')

    <div class="breadcrumbs-container">
        <div class="horizontal-inner-container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{route('contributors')}}">{{ translate::key('front.general.contributors') }}</a></li>
                <li class="breadcrumb-item">{{$user->firstname}} {{$user->lastname}}</li>
            </ol>
        </div>
    </div>

    <div class="horizontal-container bg-white user-content">
        <div class="horizontal-inner-container">
            <div class="fiche-produit-content">
                <div class="container">
                    <h1>
                        {{$user->firstname}} {{$user->lastname}}
                    </h1>
                    <div class="row">
                        <div class="col-sm-3 text-center">
                            @if(isset($user->image))
                                <img src="{{Storage::url($user->image)}}" class="img-responsive" alt="{{$user->name}} {{$user->lastname}}">
                            @endif
                        </div>
                        <div class="col-sm-8">
                            @if($user->profile)
                                {!! $user->profile->content !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascripts')

@endpush
