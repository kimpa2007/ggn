@extends('layouts.frontend',[
	"is_home" => 1,
	"banner_image" => asset('images/front/ggn-logo-2.png'),
	"og_title" => translate::key('meta.home.title'),
	"og_image" => asset('images/front/ggn-linkedin.png'),
	"og_description" => translate::key('meta.home.description'),
	"og_url" => Request::url()
])

@section('title', translate::key('meta.home.title') )
@section('metaDescription', translate::key('meta.home.description'))

@php
	$zonesIds = Auth::user()
		? Auth::user()->getAllowedZones()
		: App\Models\Zone::where('status',App\Models\Zone::STATUS_PUBLISHED)->get()->pluck('id');
	
	$themes = App\Models\Theme::where('actif', 1)->orderBy('position', 'ASC')->get();		
@endphp

@section('content')

	{{--@include ('home.partials.map') --}}

	@include ('home.partials.free')
	@include ('home.partials.latest-news')

	@foreach ($themes as $theme)
		@include ('home.partials.theme', array('theme' => $theme))
	@endforeach

	{{--@if($zonesIds)
		@foreach ($zonesIds as $zoneId)
			@include ('home.partials.zone-content', array('zoneId'=>$zoneId))
		@endforeach
	@endif--}}


@endsection

@push('stylesheets')
	{{Html::style('plugins/slick/slick.css')}}
	{{Html::style('plugins/slick/slick-zone.css')}}
	{{Html::style('/vendor/owlcarousel/assets/owl.carousel.min.css')}}
	{{Html::style('/vendor/owlcarousel/assets/owl.theme.default.min.css')}}
@endpush

@push('javascripts-libs')
	{{Html::script('plugins/slick/slick.js')}}
	{{Html::script('plugins/jquery-rotate/jQueryRotate.js')}}
	{{Html::script('/vendor/owlcarousel/owl.carousel.min.js')}}
@endpush

@push('javascripts')
<script>

	$(document).ready(function(){
		$('.content-slider').owlCarousel({
		    loop:false,
		    margin:4,
		    nav:true,
			dots: false,
			navClass: ['prev', 'next'],
			navContainer: false,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:4
		        }
		    }
		});
	});
</script>
@endpush
