<div class="horizontal-container bg-black block-2">

	<div class="horizontal-inner-container">

		<h2>{{ translate::key('front.countries.our_countries') }}</h2>
		<div class="seprator" style="height:20px;"></div>
		<div class="row country-container">

			@foreach($contries as $country)

			<div class="grid-item col-md-3">

				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<!-- front content -->
							@if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
							<div class="background-image" style="background-image:url('{{ Storage::url($country->image)}}')"></div>
							<div class="background-hover"></div>
								<div class="grid-item-content">
								@php
									//remove all html tags from description
									$name = strip_tags($country->translate(LaravelLocalization::getCurrentLocale())->name);
								@endphp
								<h3>
									<i class="fa {{$country->icon}}"></i>
									{{ str_limit($name, $limit = 50,'...')}}
								</h3>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="background-image" style="background-image:url('{{ Storage::url($country->image)}}')"></div>
							<div class="background-hover-turn"></div>
							<div class="grid-item-content-turn">
								<h4>{{ $country->translate(LaravelLocalization::getCurrentLocale())->name}}</h4>
								<p>

									@php
										//remove all html tags from description
										$description = strip_tags($country->translate(LaravelLocalization::getCurrentLocale())->description);
									@endphp
									{{ str_limit($description, $limit = 150,'...')}}
								</p>

								@if(Auth::user())
									<div class="button-container">
										<a href="{{route('contries.show',['id'=>$country->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
									</div>
								@endif

							</div>
						</div>
					</div>
				</div>

			</div>

			@endforeach

		</div>

		@if($country_count > $itemsPerPage)

			<div class="see-more-container-country">
				<a href="#" class="btn btn-secondary see-more-country">{{ translate::key('front.countries.our_countries') }}</a>
			</div>

		@endif

		<div class="seprator" style="height:20px;"></div>


	</div>
</div>
@push('javascripts')
<script>
	var itemsPerPage = {{$itemsPerPage}};
	var count = {{$country_count}};
	var pageCountry = 1;
	var availablePages = Math.ceil(count / itemsPerPage);
	var routes = {
		loadContent : "{{route('home')}}"
	};
	$(function(){
		$(".see-more-country").click(function(e){
			e.preventDefault();
			if(pageCountry >= availablePages)
	      return;
			pageCountry ++;
			$.ajax({
					url: routes.loadContent,
					type: "GET",
					data: {
						pageCountry : pageCountry
					},
						success: function(response){
							$(".country-container").append($(response).find('.country-container').html());
							if(parseInt(pageCountry) >= parseInt(availablePages)){
								$(".see-more-container-country").fadeOut();
							}
						},
						error:function(msg){}
				});
		});
	});
</script>
@endpush
