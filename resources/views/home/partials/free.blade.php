@php
  $contentRepository = App::make('\App\Repositories\ContentRepository');
  $videos = $contentRepository->getPublished()->get();
  $videos = $videos->sortByDesc(function ($video, $key) {
      return $video->published_at != NULL ? strtotime($video->published_at) : 0;
  })->slice(0, 4);
@endphp

@if($videos)
  <div class="horizontal-container adecouvrir block-2">
      <div class="horizontal-inner-container">
          <h2 style="color: #000; margin-bottom: 5px">À découvrir</h2>
          <div class="owl-carousel owl-theme content-slider">
              @foreach($videos as $i => $content)
                  <div class="item">
                      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                          <div class="front">
                            <!-- front content -->
                            @if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
                            <div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
                            <div class="background-hover"></div>
                              <div class="grid-item-content">
                              @php
                                  $title = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title);
                              @endphp
                              <h3>
                                {{ str_limit($title, $limit = 50,'...')}}
                              </h3>
                            </div>
                          </div>
                          <div class="back">
                            <!-- back content -->
                            <div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
                            <div class="background-hover-turn"></div>
                            <div class="grid-item-content-turn">
                              <h4>{{ $content->translate(LaravelLocalization::getCurrentLocale())->title}}</h4>
                              <p>

                                @php
                                  //remove all html tags from description
                                  $description = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->description);
                                @endphp
                                {{ str_limit($description, $limit = 105,'...')}}
                                 @if(Auth::user())
                                      <div class="button-container">
                                      {{--  <a href="{{ route('user.content.show',['id'=>$content->id]) }}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a> --}}
                                        <a href="{{ route('content-free',$content->id) }}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>

                                      </div>
                                    @else
                                    <div class="button-container">
                                        <a href="{{ route('content-free',$content->id) }}" class="btn btn-primary">En libre accès</a>
                                    </div>
                                  @endif
                              </p>


                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              @endforeach
          </div>
      </div>
  </div>
@endif

@push('stylesheets')
    <link rel="stylesheet" media="all" href="{{ asset('css/leaflet.css')}}" />
    <link href="https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.1/mapbox-gl.css" rel='stylesheet' />
    <style>
      .adecouvrir .next span, .adecouvrir .prev span{
        color: grey !important;
      }
      }
    </style>
@endpush

@push('javascripts-libs')
    {{Html::script('//code.createjs.com/createjs-2015.11.26.min.js')}}
    <script src="{{asset('js/libs/leaflet.js')}}"></script>
    <script src="https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.1/mapbox-gl.js"></script>
    <script src="{{asset('js/libs/leaflet-mapbox-gl.js')}}"></script>
    <script src="https://cdn.klokantech.com/openmaptiles-language/v1.0/openmaptiles-language.js"></script>
    <script src="https://unpkg.com/topojson@3"></script>
    <script src="{{asset('js/home/app.js')}}"></script>
    <script src="{{asset('js/home/app.map.js')}}"></script>
    <script src="{{asset('js/home/app.markerMap.js')}}"></script>
@endpush
