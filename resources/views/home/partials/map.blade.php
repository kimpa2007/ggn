@php
$contentRepository = App::make('\App\Repositories\ContentRepository');
$videos = $contentRepository->getPublished()->get();
$videos = $videos->sortByDesc(function ($video, $key) {
    return $video->published_at != NULL ? strtotime($video->published_at) : 0;
})->slice(0, 4);

@endphp
<div style="clear:both"></div>
<div class="home-map">

    {{-- <div class="spinner">
      <div class="sk-fading-circle">
        <div class="sk-circle1 sk-circle"></div>
        <div class="sk-circle2 sk-circle"></div>
        <div class="sk-circle3 sk-circle"></div>
        <div class="sk-circle4 sk-circle"></div>
        <div class="sk-circle5 sk-circle"></div>
        <div class="sk-circle6 sk-circle"></div>
        <div class="sk-circle7 sk-circle"></div>
        <div class="sk-circle8 sk-circle"></div>
        <div class="sk-circle9 sk-circle"></div>
        <div class="sk-circle10 sk-circle"></div>
        <div class="sk-circle11 sk-circle"></div>
        <div class="sk-circle12 sk-circle"></div>
      </div>
    </div>

    <div id="map">
    </div>--

    <div id="map-legends">
    </div>
    --}}


    {{-- fake --}}
    <div id="map" style="width: 66%; display: block;">
      <img style="width: 100%" src="/images/carte.jpg">
    </div>

    <div class="map-videos">
        @php
            $p = 1;
        @endphp

        @foreach($videos as $i => $content)
            <div class="video-item">
                <div class="item">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                  <div class="flipper">
                    <div class="front" style="width: 95%;height: 210px;">
                      <!-- front content -->
                      @if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
                      <div class="background-image" style="background-image:url('{{ Storage::url($content->image) }}')"></div>
                      <div class="background-hover"></div>
                        <div class="grid-item-content">
                        <h3>{{ str_limit(strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title), $limit = 30,'...')}}</h3>
                      </div>
                    </div>
                    <div class="back" style="width: 95%;height: 210px;">
                      <!-- back content -->
                      <div class="background-image" style="background-image:url('{{ Storage::url($content->image) }}')"></div>
                      <div class="background-hover-turn"></div>
                      <div class="grid-item-content-turn">
                        <h4>{{ str_limit(strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title), $limit = 30,'...')}}</h4>
                        <p>
                            {{ str_limit(strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->description), $limit = 80,'...')}}

                            @if(Auth::user())
                              <div class="button-container">
                                <a href="{{ route('user.content.show',['id'=>$content->id]) }}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
                              </div>
                            @else
                            <div class="button-container">

                              {{--  @if($p <= 2)--}}
                                    <a href="{{ route('content-free',$content->id) }}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
                              {{--  @else
                                    <a href="{{ route('user.content.show', $content->id) }}" class="btn btn-primary">Please <br />contact US</a>
                                @endif--}}
                            </div>
                          @endif
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            </div>

            @php
                $p++;
            @endphp
        @endforeach
    </div>
</div>

@push('stylesheets')
    <link rel="stylesheet" media="all" href="{{ asset('css/leaflet.css')}}" />
    <link href="https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.1/mapbox-gl.css" rel='stylesheet' />
@endpush

@push('javascripts-libs')
    {{Html::script('//code.createjs.com/createjs-2015.11.26.min.js')}}
    <script src="{{asset('js/libs/leaflet.js')}}"></script>
    <script src="https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.1/mapbox-gl.js"></script>
    <script src="{{asset('js/libs/leaflet-mapbox-gl.js')}}"></script>
    <script src="https://cdn.klokantech.com/openmaptiles-language/v1.0/openmaptiles-language.js"></script>
    <script src="https://unpkg.com/topojson@3"></script>
    <script src="{{asset('js/home/app.js')}}"></script>
    <script src="{{asset('js/home/app.map.js')}}"></script>
    <script src="{{asset('js/home/app.markerMap.js')}}"></script>
@endpush

@push('javascripts')
<script>
  var WEBROOT = "{{asset('')}}";

  $(function(){
    $.ajax({
        type: 'GET',
        url: '{{ route('map.data') }}',
        data: {
            _token: "@csrf",
        },
        success: function(data){
           // app.map.init(data, '{{ env('MAP_TILER_KEY') }}', '{{ LaravelLocalization::getCurrentLocale() }}');
        }
    });
  });
</script>
@endpush
