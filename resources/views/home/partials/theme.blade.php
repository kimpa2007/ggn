@php
    $theme_content = $content_search->searchByTheme($theme->id, null, $itemsPerPage);
@endphp

<div class="horizontal-container bg-black block-2">

  <div class="horizontal-inner-container">
    <h2>
      {{ $theme->name }} 

      @if($theme->id == 6 )
            <span class="text-blanc">Assurances de l’Adour</span>
      @endif 
    </h2>

    

    <div class="seprator" style="height:20px;"></div>
    <div class="row analysis-container">

      <div class="owl-carousel owl-theme content-slider">
                @foreach($theme_content as $content)
                    <div class="item">
                        <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                          <div class="flipper">
                            <div class="front">
                              <!-- front content -->
                @if($content->type == \App\Models\Content::TYPE_MOVIE)
                  <div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>
                @endif
                              <div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
                              <div class="background-hover"></div>

                                <div class="grid-item-content">
                @php
                                    $title = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->title);
                                @endphp
                                <h3>
                                  {{ str_limit($title, $limit = 50,'...')}}
                                </h3>
                              </div>
                            </div>
                            <div class="back">
                              <!-- back content -->
                              <div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
                              <div class="background-hover-turn"></div>
                              <div class="grid-item-content-turn">
                                <h4>{{ $content->translate(LaravelLocalization::getCurrentLocale())->title}}</h4>
                                <p>

                                  @php
                                    //remove all html tags from description
                                    $description = strip_tags($content->translate(LaravelLocalization::getCurrentLocale())->description);
                                  @endphp
                                  {{ str_limit($description, $limit = 110,'...')}}
                                  {{--@if(Auth::user())                                  
                                    
                                    @if(Auth::user()->canAccessContent())
                                      <div class="button-container">
                                        <a href="{{route('user.content.show',['id'=>$content->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
                                      </div>
                                    @else
                                      <div class="button-container">
                                          <a href="{{ route('offer.show') }}" class="btn btn-primary">{{ __('messages.abonnement.abonnezvous') }}</a>
                                      </div>
                                    @endif
                                  @else
                                  <div class="button-container">
                                      <a href="{{ route('user.login') }}" class="btn btn-primary">{{ __('messages.abonnement.abonnezvous') }}</a>
                                  </div>
                                  @endif--}}
                                  <div class="button-container">
                                    <a href="{{route('user.content.show',['id'=>$content->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
                                  </div>
                                </p>


                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                @endforeach
            </div>

    </div>


    <div class="seprator" style="height:20px;"></div>


  </div>
</div>
@push('javascripts')

@endpush
