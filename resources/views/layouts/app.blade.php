<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GGN</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" media="all" href="{{ asset('css/backend-style.css')}}" />
        <link rel="stylesheet" media="all" href="{{ asset('css/font-awesome/css/font-awesome.min.css')}}" />

        <link rel="stylesheet" media="all" href="{{ asset('plugins/toastr/toastr.min.css')}}" />

        @stack('stylesheets')

		    <script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>

        <!-- Datatables -->
        <!--<link rel="stylesheet" type="text/css" href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}">-->
        <link rel="stylesheet" type="text/css" href="{{ asset('/plugins/datatables/datatables.min.css') }}">
        <!--<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>-->
        <!--<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>-->
        <script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>

        <!-- Bootbox -->
        <script src="{{ asset('/plugins/bootbox/bootbox.min.js') }}"></script>

        <!-- Toastr -->
        <script src="{{ asset('/plugins/toastr/toastr.min.js') }}"></script>

        <!-- Bootstrap select -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/plugins/bootstrap-select/bootstrap-select.min.css') }}">
        <script src="{{ asset('/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
        <script src="/js/app.js"></script>

        <!-- Dialog -->
        <script src="/js/libs/dialog.js"></script>

		<style type="text/css">
			#sidebar-left{overflow: auto;}
		</style>

    </head>

    <body>

    	@yield('modal')
		<section id="wrapper">
	        <aside class="sidebar" id="sidebar-left">
	        	<nav>

	        		<div class="logo">
	        			<img src="/images/logo.png" alt="GGN admin" />
	        		</div>

	        		<ul class="nav nav-stacked left-menu" id="stacked-menu">

	        			@if(Auth::user()->hasRole('user'))
	        				<li class="{{ Request::is('user/account*') ? 'active' : '' }}">
		        				<a href="{{ route('user.index') }}">
		        					<i class="fa fa-home"></i>
		        					<span class="text">
		        						Home
		        					</span>
		        				</a>
		        			</li>
	        			@endif

	        			@role(['expert'])
		        			@include('layouts.sidebar.expert')
		        		@endrole

                        @role(['admin','super-admin'])
                        	@include('layouts.sidebar.admin')
                        @endrole

	        		</ul>
	        	</nav>
	        </aside>

	        <section id="main">
	        	<header>

	        		<!-- header actions -->
		        	<div class="navbar-collapse" id="example-navbar-transparent">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
                        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
  									Hello, {{Auth::user()->firstname}}
  									<b class="caret"></b>
  								<div class="ripple-container"></div></a>
                            	<ul class="dropdown-menu dropdown-menu-right default-padding">
                                	<li class="dropdown-header"></li>
                                	<li>

                                		@if(Auth::user()->hasRole('expert'))
	                                		<a href="{{ route('expert.account') }}">
            					        					<i class="fa fa-user-circle-o"></i>
            					        						&nbsp;My account
            					        				</a>
            				        				@endif
            				        				@role(['admin','super-admin'])
            				        					<a href="{{ route('admin.account') }}">
            					        					<i class="fa fa-user-circle-o"></i>
            					        						&nbsp;My account
            					        				</a>
            				        				@endrole


                                	</li>
                                	<li>
                                		<a href="{{ url('/logout') }}"
	                                        onclick="event.preventDefault();
	                                                 document.getElementById('logout-form').submit();">
	                                        <i class="fa fa-sign-out"></i> &nbsp; Logout
	                                    </a>
	                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                                        {{ csrf_field() }}
	                                    </form>
                                    </li>
                                </ul>
                        	</li>
						</ul>
					</div>

	        	</header>
                @if(isset($errors))
                    @if (count($errors) > 0)
            		    <div class="alert alert-danger">
            		        <ul>
            		            @foreach ($errors->all() as $error)
            		                <li>{{ $error }}</li>
            		            @endforeach
            		        </ul>
            		    </div>
            		@endif
                @endif

                @if(Session::has('notify_error'))
                    <div class="alert alert-danger">
                        {{ Session::get('notify_error') }}
                    </div>
                @endif

                @if(Session::has('notify_success'))
                    <div class="alert alert-success">
                        {{ Session::get('notify_success') }}
                    </div>
                @endif

	        	<section id="content">
	        		@yield('content')
	        	</section>
	        </section>

        </section>

		@stack('javascripts-libs')

        @stack('javascripts')
    </body>
</html>
