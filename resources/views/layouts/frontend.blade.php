<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <meta name="description" content="@yield('metaDescription')">

        <meta property="og:title" content="{!! $og_title or '' !!}" />
        <meta property="og:image" content="{!! $og_image or '' !!}" />
        <meta property="og:description" content="{!! isset($og_description) ? strip_tags($og_description) : null !!}" />
        <meta property="og:url" content="{!! $og_url or '' !!}" />

        <link rel="stylesheet" media="all" href="{{ asset('css/font-awesome/css/font-awesome.min.css')}}?t={{time()}}" />
        <link rel="stylesheet" media="all" href="{{ asset('plugins/toastr/toastr.min.css')}}?t={{time()}}" />
        <link rel="stylesheet" media="all" href="{{ asset('fonts/iconmoon/iconmoon.css')}}?t={{time()}}" />

        <link href="https://vjs.zencdn.net/7.1.0/video-js.css" rel="stylesheet">

        @stack('stylesheets')
        <link rel="stylesheet" media="all" href="{{ asset('css/frontend-style.css')}}?t={{time()}}" />
      <script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
      <!-- Toastr -->
        <script src="{{ asset('/plugins/toastr/toastr.min.js') }}"></script>
        <!-- Bootbox -->
        <script src="{{ asset('/plugins/bootbox/bootbox.min.js') }}"></script>
        <script src="/js/app.js"></script>
        <!-- Dialog -->
        <script src="{{ asset('js/libs/dialog.js')}}"></script>

        <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
        {!! Html::script('/js/libs/videojs/videojs-ie8.min.js') !!}
        {{-- <script src="//vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script> --}}

        <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/talentid/ggn-v2.css') }}" />
    </head>
    <body>

      @yield('modal')

    <div class="mobile-menu">
      @include ('layouts.partials.header-nav-mobile')
    </div>

    <section id="wrapper">
      <section id="main">
        @if(isset($is_home))
          @include('layouts.partials.header-home')
                @elseif(isset($is_small))
                  @include('layouts.partials.header-small')
        @else
          @include('layouts.partials.header-default')
        @endif

        <section id="content">
            @yield('content')
                </section>
        </section>
    </section>

    @include('layouts.partials.footer')

    @stack('javascripts-libs')

    <script>
      $(function(){

        var CSRF_TOKEN = "{{csrf_token()}}";

        $(".open-login").click(function(e){

          if(CSRF_TOKEN == ""){
            return true;
          }

          e.preventDefault();

          if($('.login-form').hasClass('open')){
            $('.login-form').removeClass('open');
          }
          else {
            $('.login-form').addClass('open');
          }
        });

        $("a.mobile-open").click(function(e){
          e.preventDefault();
          console.log("hello")

          //$(".mobile-menu").css({top:0});
          $(".mobile-menu").show();


        });
        $(".close-button").click(function(e){
          console.log("close");
          $(".mobile-menu").hide();
        });

        $(".mobile-menu a").click(function(e){
          console.log("hell2")

          if(!$(e.target).hasClass('open-login')){
            $(".mobile-menu").hide();

          }
        });

        $(".open-user-menu").click(function(){
          $(".user-nav").toggleClass('open');
        });

        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}", 'Info', {timeOut: 3000});
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}", 'Warning', {timeOut: 3000});
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}", 'Success', {timeOut: 3000});
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}", 'Error', {timeOut: 3000});
                    break;
            }
          @endif


      });
    </script>

    @stack('javascripts')      

      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138328625-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-138328625-1');
      </script>
    </body>
</html>
