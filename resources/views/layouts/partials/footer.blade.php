@php
    $zones =  App\Models\Zone::where('status',App\Models\Zone::STATUS_PUBLISHED)->get();
    $countries =  App\Models\Country::where('status',App\Models\Country::STATUS_PUBLISHED)->limit(6)->orderby('order')->get();
@endphp

<div class="seprator bg-black" style="height:20px;"></div>
<footer class="horizontal-container bg-black">
    <div class="horizontal-inner-container">
       {{-- <div class="row">
            <div class="col-md-4 text-left">
                <h4>{{ translate::key('front.footer.countries') }}</h4>
                <ul>
                    @foreach ($countries as $country)
                        <li><a href="{{route('country',['countryId' => $country->id ])}}">{{ $country->translate(LaravelLocalization::getCurrentLocale())->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4">
                <br/> <img src="{{asset('images/front/logo-footer.png')}}" srcset="{{asset('images/front/logo-footer-2x.png')}} 2x" alt="">
            </div>
            <div class="col-md-4 text-left">
                <h4>{{ translate::key('front.general.areas') }}</h4>
                <ul>
                    @foreach ($zones as $zone)
                        <li>{{ $zone->translate(LaravelLocalization::getCurrentLocale())->name }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        --}}

        <div class="row">
           
            <div class="col-md-12 text-center">
                <br/> <img src="{{asset('images/front/logo-footer.png')}}" srcset="{{asset('images/front/logo-footer-2x.png')}} 2x" alt="">
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-12">

                @php
                    $page = App\Models\CustomPage::where('identifier', 'cgu')->first();
                @endphp

                @if($page)
                    @php
                        $slug = $page->translate(LaravelLocalization::getCurrentLocale()) ? $page->translate(LaravelLocalization::getCurrentLocale())->slug : null;
                    @endphp
                    @if($slug)
                        <a href="{{ route('custompage', $slug) }}">CGU</a>
                    @endif
                @endif

                @php
                    $page_cgv = App\Models\CustomPage::where('identifier', 'cgv')->first();
                @endphp

                @if($page_cgv)
                    @php
                        $slug = $page_cgv->translate(LaravelLocalization::getCurrentLocale()) ? $page_cgv->translate(LaravelLocalization::getCurrentLocale())->slug : null;
                    @endphp
                    @if($slug)
                       - <a href="{{ route('custompage', $slug) }}">CGV</a>
                    @endif
                @endif

                - <a href="mailto:info@globalgeonews.com">info@globalgeonews.com</a> © BEW @php echo date("Y"); @endphp
                - {{ translate::key('front.footer.rights') }}
                <br />{{ translate::key('front.legal.mention.adresse') }}

            </div>
        </div>
    </div>
    <div class="seprator bg-black" style="height:20px;"></div>
</footer>
