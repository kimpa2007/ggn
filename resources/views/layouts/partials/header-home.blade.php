

<header class="horizontal-container">

	<div class="horizontal-container">
		<nav>
			<div class="logo">
				<a href="{{ env('APP_URL') }}/{{ app()->getLocale() }}">
					{{-- <img src="{{asset('images/front/ggn-logo.png')}}" srcset="{{asset('images/front/ggn-logo-2x.png')}} 2x" alt="GGN" /> --}}
					<img src="{{asset('images/front/logo.jpg')}}" srcset="{{asset('images/front/logo.jpg')}}"  alt="GGN" />											
				</a>
			</div>
			<div class="desktop-nav">
				@include ('layouts.partials.header-nav')
				{{-- <ul>
					@foreach ($themes as $value)
						@if($theme->id == 6)
							<li>
								<a href="{{route('theme',['id'=> $value])}}">Santé</a>
							</li>
						@else
							<li>
								<a href="{{route('theme',['id'=> $value])}}">{{$value}}</a>
							</li>
						@endif
					@endforeach
					<li>
						<a href="#">Contributors</a>
					</li>
				</ul> --}}
			</div>
			<div class="mobile-nav">
				<ul>
					<li>
						<a href="#" class="mobile-open"><i class="fa fa-bars"></i></a>
					</li>
				</ul>
			</div>

		</nav>
	</div>
</header>
