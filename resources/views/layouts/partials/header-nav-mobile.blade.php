
<ul class="right">

	@if(isset(Auth::user()->id))
		<li class="login-buttons">
			@if(Auth::user()->hasRole('partner'))
				<a class="btn-left" href="{{route('partner.home')}}">{{ translate::key('front.navbar.account') }}</a>
			@elseif(Auth::user()->hasRole('expert'))
				<a class="btn-left" href="{{route('expert.account')}}">{{ translate::key('front.navbar.account') }}</a>
			@elseif(Auth::user()->hasRole(['admin','super-admin']))
				<a class="btn-left" href="{{route('admin.account')}}">{{ translate::key('front.navbar.account') }}</a>
			@else
				<a class="btn-left" href="{{route('user.home')}}">{{ translate::key('front.navbar.account') }}</a>
			@endif
			<a class="btn-right" href="{{route('logout')}}">{{ translate::key('front.navbar.logout') }}</a>
		</li>
	@else

		<li class="primary">
			<a class="btn-rouge" href="{{ route('user.login')}}">{{ __('messages.front.login.login_register') }}</a>
		</li>
			      <a href="#" class="close-button">X</a>

	@endif


</ul>

<ul>
	@php
		$locale = \Illuminate\Support\Facades\Session::get('locale');
		$themes = App\Models\Theme::where('is_menu', 1)->where('actif', 1)->orderBy('position', 'ASC')->get();		

	@endphp
	@foreach ($themes as $theme)
		@if($theme->id == 6)
			<li>
				<a href="{{route('theme', $theme->slug)}}">Santé</a>
			</li>
		@else
			<li>
				<a href="{{route('theme', $theme->slug)}}">{{$theme->name}}</a>
			</li>
		@endif
	@endforeach
	<li>
		<a href="{{route('contributors')}}">{{ translate::key('front.general.contributors') }}</a>
	</li>
	<li>
		<a href="{{route('map.show')}}"><i style="color: red" class="red-marker fa fa-map-marker" aria-hidden="true"></i> CARTE</a>
	</li>

	<li class="{{ Request::is('contact') ? 'active' : '' }}" >
		<a href="{{route('contact')}}">{{ translate::key('front.navbar.contact_us') }} </a>
	</li>

	{{-- <li>
		@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
			<a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
				<img src="{{asset('images/front/flags/'.$localeCode.'.png')}}"  alt="GGN" /> {{ $localeCode }}
			</a>
		@endforeach
	</li>--}}

	<li>
		{!!
			Form::open([
				'url' => route('search'),
				'method' => 'GET',
				'class' => 'search-form'
			])
		!!}

		{!!
			Form::text('q', Request::get('q'), [
				'class' => 'search-input',
				'placeholder' => translate::key('front.general.search')
			])
		!!}

		{!!
			Form::submit('OK', [
				'class' => 'search-submit'
			])
		!!}

		{!!
			Form::close()
		!!}
	</li>
</ul>


<script>
    $(function() {
        $("a[href='#menuExpand']").click(function(e) {
            $(".menu").toggleClass("menuOpen");
            e.preventDefault();
        });
    });
</script>
