{{-- @php
	$themes = App\Models\Content::getThemes();
@endphp --}}

<ul>
	@php
		$locale = \Illuminate\Support\Facades\Session::get('locale');
		$themes = App\Models\Theme::where('is_menu', 1)->where('actif', 1)->orderBy('position', 'ASC')->get();		
	@endphp

	@foreach ($themes as $theme)
		@if($theme->id==6)
			<li>
				<a href="{{route('theme', $theme->slug)}}">Santé</a>
			</li>
		@else
			<li>
				<a href="{{route('theme', $theme->slug)}}">{{$theme->name}}</a>
			</li>
		@endif
	@endforeach

	@php
		$locale = \Illuminate\Support\Facades\Session::get('locale');
		$page = App\Models\CustomPage::where('id', 4)->get()->first();	
	@endphp
	<li>
		<a href="{{route('custompage', $page->slug)}}">{{$page->title}}</a>
	</li>


	<li>
		<a href="{{route('contributors')}}">{{ translate::key('front.general.contributors') }} </a>
	</li>
	<li>
		<a href="{{route('map.show')}}"><i style="color: red" class="red-marker fa fa-map-marker" aria-hidden="true"></i></a>
	</li>
</ul>




<ul class="right">
	{!!
		Form::open([
			'url' => route('search'),
			'method' => 'GET',
			'class' => 'search-form'
		])
	!!}

	{!!
		Form::text('q', Request::get('q'), [
			'class' => 'search-input',
			'placeholder' => translate::key('front.general.search')
		])
	!!}

	{!!
		Form::submit('OK', [
			'class' => 'search-submit'
		])
	!!}


	{!!
		Form::close()
	!!}
	
	{{--<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{  translate::key('front.navbar.language') }} <span class="caret"></span></a>
		<ul class="dropdown-menu">
			@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
				<li>
					<a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
						<img src="{{asset('images/front/flags/'.$localeCode.'.png')}}" srcset="{{asset('images/front/ggn-logo-2-2x.png')}} 2x"  alt="GGN" /> {{ $properties['native'] }}
					</a>
				</li>
			@endforeach
		</ul>
	</li>--}}

	<li class="{{ Request::is('contact') ? 'active' : '' }}" >
		<a href="{{route('contact')}}">{{ translate::key('front.navbar.contact_us') }}</a>
	</li>

	@if(isset(Auth::user()->id))

		<li class="login-buttons">

			@if(Auth::user()->hasRole('partner'))
				<a class="btn-left" href="{{route('partner.home')}}">{{ translate::key('front.navbar.account') }}</a>
			@elseif(Auth::user()->hasRole('expert'))
				<a class="btn-left" href="{{route('expert.account')}}">{{ translate::key('front.navbar.account') }}</a>
			@elseif(Auth::user()->hasRole(['admin','super-admin']))
				<a class="btn-left" href="{{route('admin.account')}}">{{ translate::key('front.navbar.account') }}</a>
			@else
				<a class="btn-left" href="{{route('user.home')}}">{{ translate::key('front.navbar.account') }}</a>
			@endif

			<a class="btn-right" href="{{route('logout')}}">{{ translate::key('front.navbar.logout') }}</a>
		</li>

	@else

		<li class="primary">
			<a class="btn-rouge" href="{{ route('user.login')}}">{{ __('messages.front.login.login_register') }}</a>
		</li>


	@endif

</ul>


<style>
	.login-buttons{
		padding-top: 0px;
		margin: 0;
	}

	.login-buttons a {
	    display: block;
	    height: 29px;
	    font-size: 11px !important;
	    width: 100%;
		padding: 7px 6px;
	    text-align: center;
	}
	header nav {
    	padding: -1px 19px;
	}
</style>
