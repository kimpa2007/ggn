<header class="horizontal-container default">

	<div class="bg-white" style="z-index:15;">
		<div class="horizontal-container">
			<nav>
				<div class="logo">
					<a href="https://www.globalgeonews.com/{{ app()->getLocale() }}">
						<img src="{{asset('images/front/ggn-logo-2.png')}}" srcset="{{asset('images/front/ggn-logo-2-2x.png')}} 2x"  alt="GGN" />
					</a>
				</div>

				<div class="desktop-nav">
					@include ('layouts.partials.header-nav')
				</div>
				<div class="mobile-nav">
					<ul>
						<li>
							<a href="#" class="mobile-open"><i class="fa fa-bars"></i></a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	</div>
</header>
