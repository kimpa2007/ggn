<div id="accordion">

	<li class="{{ Request::is('expert/dashboard*') ? 'active' : '' }}">
		<a href="{{ action('Admin\HomeController@index') }}">
			<i class="fa fa-home"></i>
			<span class="text">
				Accueil
			</span>
		</a>
	</li>

	<li class=>
	    <a href="#" data-target="#item1" data-parent="#accordion"  data-toggle="collapse" data-parent="#stacked-menu">
	        <i class="fa fa-users"></i>
	        <span class="text">
	            Clients
	            <span class="caret"></span>
	        </span>
	    </a>

	    <ul class="nav nav-stacked collapse left-submenu {{ Request::is('admin/users*') || Request::is('admin/partners*') || Request::is('admin/admins*') ? 'in' : '' }}" id="item1">
	        <li class="{{ Request::is('admin/partners*') ? 'active' : '' }}">
	        	<a href="{{ action('Admin\Partner\PartnerController@index') }}">
	        		<i class="fa fa-user-o"></i>
	        		<span class="text">
	        			Clients
	        		</span>
	        	</a>
	        </li>
	      </ul>

	</li>

	<li class="Request::is('admin/zones*')  || Request::is('admin/contents*') || Request::is('admin/countries*') ? 'active' : '' }}">
	    <a href="#" data-target="#item2" data-parent="#accordion" data-toggle="collapse" data-parent="#stacked-menu">
	        <i class="fa fa-files-o"></i>
	        <span class="text">
	            Contenu
	            <span class="caret"></span>
	        </span>
	    </a>
		    <ul class="nav nav-stacked collapse left-submenu {{ Request::is('admin/contents*') || Request::is('admin/experts*') ||  Request::is('admin/zones*') || Request::is('admin/countries*') || Request::is('admin/custompage*') ? 'in' : '' }}" id="item2">
	        <li class="{{ Request::is('admin/contents*') ? 'active' : '' }}">
	        	<a href="{{action('Admin\Content\ContentController@index')}}">
	        		<i class="fa fa-file-o"></i>
	        		<span class="text">
	        			Contenus
	        		</span>
	        	</a>
	        </li>
			<li class="{{ Request::is('admin/latestnews*') ? 'active' : '' }}">
			 <a href="{{action('Admin\Content\ContentController@indexLatestnews')}}">
				 <i class="fa fa-fire"></i>
				 <span class="text">
					 Latest News
				 </span>
			 </a>
			</li>
			<li class="{{ Request::is('admin/experts*') ? 'active' : '' }}">
			 <a href="{{action('Admin\Expert\AccountController@index')}}">
				 <i class="fa fa-user-md"></i>
				 <span class="text">
					 Contributeurs
				 </span>
			 </a>
			</li>
			<li class="{{ Request::is('admin/maplegend*') ? 'active' : '' }}">
			 <a href="{{action('Admin\MapLegend\MapLegendController@index')}}">
				 <i class="fa fa-map"></i>
				 <span class="text">
					 Map legend
				 </span>
			 </a>
			</li>
	      </ul>

	</li>


	<li class="{{ Request::is('admin/suivi*') || Request::is('admin/invoices*') || Request::is('admin/orders*') ? 'active' : '' }}">
	    <a href="#" data-target="#item3" data-parent="#accordion" data-toggle="collapse" data-parent="#stacked-menu">
	        <i class="fa fa-check-square-o"></i>
	        <span class="text">
	            Suivi Commandes
	            <span class="caret"></span>
	        </span>
	    </a>

	    <ul class="nav nav-stacked collapse left-submenu {{ Request::is('admin/suivi*') || Request::is('admin/invoices*') || Request::is('admin/orders*') ? 'in' : '' }}" id="item3">
					<li class="{{ Request::is('admin/orders/plan*') ? 'active' : '' }}">
	        	<a href="{{ route('admin.orders.plan.index') }}">
	        		<i class="fa fa-shopping-cart"></i>
	        		<span class="text">
	        			Commandes abonnements
	        		</span>
	        	</a>
	        </li>

	        <li class="{{ Request::is('admin/invoices*') ? 'active' : '' }}">
	        	<a href="{{ route('admin.invoices')}}">
	        		<i class="fa fa-file-o"></i>
	        		<span class="text">
	        			Factures
	        		</span>
	        	</a>
	        </li>
	      </ul>

	</li>

	<li class="{{ Request::is('admin/parameters*') ? 'active' : '' }}">
	    <a href="#" data-target="#item4" data-parent="#accordion" data-toggle="collapse" data-parent="#stacked-menu">
	        <i class="fa fa-cog"></i>
	        <span class="text">
	            Paramètres
	            <span class="caret"></span>
	        </span>
	    </a>

	    <ul class="nav nav-stacked collapse left-submenu" id="item4">
				<li class="{{ Request::is('admin/plans*') ? 'active' : '' }}">
					 <a href="{{action('Admin\Tag\TagController@index')}}">
						 <i class="fa fa-file-o"></i>
						 <span class="text">
							 Mot clef
						 </span>
					 </a>
				 </li>
				<li class="{{ Request::is('admin/plans*') ? 'active' : '' }}">
					 <a href="{{action('Admin\Plan\PartnerPlanController@index')}}">
						 <i class="fa fa-file-o"></i>
						 <span class="text">
							 Offres abonnements
						 </span>
					 </a>
				 </li>
				<li class="{{ Request::is('admin/zones*') ? 'active' : '' }}">
					<a href="{{action('Admin\Zone\ZoneController@index')}}">
						<i class="fa fa-th-list"></i>
						<span class="text">
							Zones
						</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/themes*') ? 'active' : '' }}">
					<a href="{{action('Admin\Theme\ThemeController@index')}}">
						<i class="fa fa-th-list"></i>
						<span class="text">
							Themes
						</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/countries*') ? 'active' : '' }}">
					<a href="{{action('Admin\Country\CountryController@index')}}">
						<i class="fa fa-th-list"></i>
						<span class="text">
							Pays
						</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/admins*') ? 'active' : '' }}">
				 <a href="{{action('Admin\Admins\AccountController@index')}}">
					 <i class="fa fa-user-secret"></i>
					 <span class="text">
						 Admins
					 </span>
				 </a>
			 </li>
				<li class="{{ Request::is('admin/users') ? 'active' : '' }}">
				 <a href="{{ action('Admin\User\AccountController@index') }}">
					 <i class="fa fa-user"></i>
					 <span class="text">
						 Tout les comptes
					 </span>
				 </a>
			 </li>
	        <li class="{{ Request::is('admin/parameters/emails*') ? 'active' : '' }}">
	        	<a href="{{ route('admin.parameters.emails.index') }}">
	        		<i class="fa fa-envelope"></i>
	        		<span class="text">
	        			E-mails
	        		</span>
	        	</a>
	        </li>

			<li class="{{ Request::is('admin/custompage*') ? 'active' : '' }}">
				<a href="{{route('admin.custompage.index')}}">
					 <i class="fa fa-asterisk"></i>
					 <span class="text">
							 Pages statiques
					 </span>
				</a>
			</li>

			<li class="{{ Request::is('admin/translate*') ? 'active' : '' }}">
				<a href="{{route('admin.translate.index')}}">
					<i class="fa fa-globe"></i>
					<span class="text">
							 Translate Tools
					 </span>
				</a>
			</li>
	      </ul>

	</li>
	<li>
		<a href="/">
			<i class="fa fa-home"></i>
			<span class="text">
				Retour au site
			</span>
		</a>
	</li>
</div>

@push('javascripts')
<script>
	$(function(){

		//close other tabs
		var $myGroup = $('#accordion');
		$myGroup.on('show.bs.collapse','.collapse', function() {
			$myGroup.find('.collapse.in').collapse('hide');
		});
	});
</script>


@endpush
