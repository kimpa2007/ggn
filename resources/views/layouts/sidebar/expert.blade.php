<li class="{{ Request::is('expert/dashboard*') ? 'active' : '' }}">
    <a href="{{ action('Expert\HomeController@index') }}">
        <i class="fa fa-home"></i>
        <span class="text">
            Home
        </span>
    </a>
</li>

<li class="{{ Request::is('expert/profile*') ? 'active' : '' }}">
    <a href="{{ action('Expert\ProfileController@index') }}">
        <i class="fa fa-address-card-o"></i>
        <span class="text">
            Ma presentation
        </span>
    </a>
</li>
<li class="{{ Request::is('expert/profile*') ? 'active' : '' }}">
    <a href="{{ action('Expert\AccountController@index') }}">
        <i class="fa fa-address-card-o"></i>
        <span class="text">
            Profil
        </span>
    </a>
</li>


<li class="{{ Request::is('expert/contents*') ? 'active' : '' }}">
    <a href="{{ action('Expert\ContentController@index') }}">
        <i class="fa fa-file"></i>
        <span class="text">
            Contents
        </span>
    </a>
</li>
<li>
  <a href="/">
    <i class="fa fa-home"></i>
    <span class="text">
      Retour au site
    </span>
  </a>
</li>
