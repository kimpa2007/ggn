<li class="{{ Request::is('partner/dashboard*') ? 'active' : '' }}">
    <a href="{{ action('Partner\HomeController@index') }}">
        <i class="fa fa-home"></i>
        <span class="text">
            Home
        </span>
    </a>
</li>

<li class="{{ Request::is('partner/users/pending*') ? 'active' : '' }}">
    <a href="{{ action('Partner\UserController@index') }}">
        <i class="fa fa-user"></i>
        <span class="text">
            Validate Users
        </span>
    </a>
</li>
