@extends('layouts.frontend',[
	"is_home" => 1,
	"banner_image" => asset('images/front/ggn-logo-2.png'),
	"og_title" => translate::key('meta.home.title'),
	"og_image" => asset('images/front/ggn-linkedin.png'),
	"og_description" => translate::key('meta.home.description'),
	"og_url" => Request::url()
])

@section('title', translate::key('meta.home.title') )
@section('metaDescription', translate::key('meta.home.description'))

@php
	$zonesIds = Auth::user()
		? Auth::user()->getAllowedZones()
		: App\Models\Zone::where('status',App\Models\Zone::STATUS_PUBLISHED)->get()->pluck('id');
@endphp

@section('content')
	<div style="clear:both"></div>
		<div class="home-map">

		    <div class="spinner">
		      <div class="sk-fading-circle">
		        <div class="sk-circle1 sk-circle"></div>
		        <div class="sk-circle2 sk-circle"></div>
		        <div class="sk-circle3 sk-circle"></div>
		        <div class="sk-circle4 sk-circle"></div>
		        <div class="sk-circle5 sk-circle"></div>
		        <div class="sk-circle6 sk-circle"></div>
		        <div class="sk-circle7 sk-circle"></div>
		        <div class="sk-circle8 sk-circle"></div>
		        <div class="sk-circle9 sk-circle"></div>
		        <div class="sk-circle10 sk-circle"></div>
		        <div class="sk-circle11 sk-circle"></div>
		        <div class="sk-circle12 sk-circle"></div>
		      </div>
		    </div>

		    <div id="map">
		    </div>

		    <div id="map-legends">
		    </div> 


		</div>

		 

		@push('stylesheets')
		    <link rel="stylesheet" media="all" href="{{ asset('css/leaflet.css')}}" />
		    <link href="https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.1/mapbox-gl.css" rel='stylesheet' />
		@endpush

		@push('javascripts-libs')
		    {{Html::script('//code.createjs.com/createjs-2015.11.26.min.js')}}
		    <script src="{{asset('js/libs/leaflet.js')}}"></script>
		    <script src="https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.1/mapbox-gl.js"></script>
		    <script src="{{asset('js/libs/leaflet-mapbox-gl.js')}}"></script>
		    <script src="https://cdn.klokantech.com/openmaptiles-language/v1.0/openmaptiles-language.js"></script>
		    <script src="https://unpkg.com/topojson@3"></script>
		    <script src="{{asset('js/home/app.js')}}"></script>
		    <script src="{{asset('js/home/app.map.js')}}"></script>
		    <script src="{{asset('js/home/app.markerMap.js')}}"></script>
		@endpush

		@push('javascripts')
			<script>
			  var WEBROOT = "{{asset('')}}";

			  $(function(){
			    $.ajax({
			        type: 'GET',
			        url: '{{ route('map.data') }}',
			        data: {
			            _token: "@csrf",
			        },
			        success: function(data){
			            app.map.init(data, '{{ env('MAP_TILER_KEY') }}', '{{ LaravelLocalization::getCurrentLocale() }}');
			        }
			    });
			  });
			</script>
		@endpush

@endsection

@push('stylesheets')
	{{Html::style('plugins/slick/slick.css')}}
	{{Html::style('plugins/slick/slick-zone.css')}}
	{{Html::style('/vendor/owlcarousel/assets/owl.carousel.min.css')}}
	{{Html::style('/vendor/owlcarousel/assets/owl.theme.default.min.css')}}
@endpush

@push('javascripts-libs')
	{{Html::script('plugins/slick/slick.js')}}
	{{Html::script('plugins/jquery-rotate/jQueryRotate.js')}}
	{{Html::script('/vendor/owlcarousel/owl.carousel.min.js')}}
@endpush


