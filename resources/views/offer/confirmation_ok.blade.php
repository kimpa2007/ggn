@extends('layouts.frontend')
@section('title', 'Abonnements')
@section('metaDescription', 'Abonnements')
@section('content')



<div class="horizontal-container  block-2 container-content">
<div id="generic_price_table">   
	<section>
	        <div class="container">
	            <div class="row">
	                <div class="col-md-12">
	                    <div class="price-heading clearfix">
	                        <h1>Confirmation de commande</h1>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="container">				           
	        	<div class="row">
					<div class="jumbotron jumbotron-fluid">
					  <div class="container">
					    <h2 class="display-2">Votre commande a été payé.</h2s>
					    <p class="lead">Nous vous confirmons le paiement et l'activation de votre abonnement. Vous pouvez dès maintenant accéder à tout notre contenu.</p>
					  </div>
					</div>             
	            </div>				            
	        </div>
	    </section>             
	</div>
</div>
@endsection



