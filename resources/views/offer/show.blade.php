@extends('layouts.frontend')
@section('title', 'Abonnements')
@section('metaDescription', 'Abonnements')
@section('content')




<div class="horizontal-container  block-2 container-content">
<div id="generic_price_table">   
	<section>
	        <div class="container">
	            <div class="row">
	                <div class="col-md-12">
	                    <div class="price-heading clearfix">
	                        <h1>Abonnez-vous</h1>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="container">				           
	        	<div class="row">
	            	@foreach($abonnements as $abonnement)
		                <div class="col-md-3" style="min-height: 350px;">		                
	            		@php
	            			$cic = new \App\Models\CicPayment();
	            			$cic->initialize($abonnement);
	            		@endphp


		                    <div class="generic_content clearfix">
		                        
		                        <div class="generic_head_price clearfix">		                        
		                            <div class="generic_head_content clearfix">		                            
		                                <div class="head_bg"></div>
		                                <div class="head">
		                                    <span style="font-size: 16px !important">{{ $abonnement->name}}</span>
		                                </div>		                                
		                            </div>
		                            
		                            <div class="generic_price_tag clearfix">	
		                                <span class="price">
		                                    <span class="currency">{{ $abonnement->price }}</span>
		                                    <span class="sign">€</span>
		                                </span>
		                            </div>		                            
		                        </div>                            
		                        
		                        <div class="generic_feature_list">
		                        	{!! $abonnement->description !!}
		                        </div>



		                        @if ($abonnement->is_etudiant == 1)

		                        	<div class="generic_price_btn clearfix">
					                	<button class="abonnement-etudiant" data-toggle="modal" data-target="#myModal"data-id="{{ $abonnement->id }}" type="button">S'abonner</a>
					                </div> 

		                        @elseif ($abonnement->has_promo_field == 1)
		                        	<div class="generic_price_btn clearfix">
					                	<button class="abonnement-promo" data-toggle="modal" data-target="#myModal"data-id="{{ $abonnement->id }}" type="button">S'abonner</a>
					                </div> 

		                        @else
			                        <div class="generic_price_btn clearfix">
					                	<button class="order" data-id="{{ $abonnement->id }}" type="button">S'abonner</a>
					                </div> 
		                        @endif
		                   		                       
		                    </div>				                        
		                </div>
	                @endforeach			              
	            </div>				            
	        </div>
	    </section>             
	</div>
</div>


<div id="dialog-form" title="Informations étudiant">
  <p class="validateTips"></p>
  <form>
    <fieldset>
        <div class="modal-body">
                        <p>Veuillez remplir les informations suivantes pour créer votre abonnement étudiant: </p>
                        <div class="form-group">
                                <label>Nom de l’école ou de l’université</label>
                                <input class="form-control" id="ecole" />
                        </div>

                        <div class="form-group">
                                <label>Cursus suivi</label>
                                <input class="form-control" id="cursus" />
                        </div>
                        <input type="hidden" id="abonnement" />
                </div>
    </fieldset>
  </form>
</div>

<div id="dialog-form-promo" title="Code promo">
  <p class="validateTipsPromo"></p>
  <form>
    <fieldset>
      	<div class="modal-body">
			<div class="form-group">
				<label>Entrez votre code promo</label>
				<input class="form-control" id="promo" />
			</div>
			<input type="hidden" id="abonnement-promo-id" />
		</div>
    </fieldset>
  </form>
</div>

@endsection


@push('javascripts')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

        $(function() {


        var csrf_token = "{{ csrf_token() }}";

		var dialog, form,
	     	ecole = $( "#ecole" ),
	     	cursus = $( "#cursus" ),
	     	allFields = $( [] ).add( ecole ).add( cursus ),
	     	tips = $( ".validateTips" );
	 
	    function updateTips( t ) {
	      tips.text( t ).addClass( "ui-state-highlight" );
	      setTimeout(function() {
	        tips.removeClass( "ui-state-highlight", 1500 );
	      }, 500 );
	    }
	 
	    function checkLength( o, n, min, max ) {
	      if ( o.val().length > max || o.val().length < min ) {
	        o.addClass( "ui-state-error" );
	        updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );
	        return false;
	      } else {
	        return true;
	      }
	    }
	 
	    function addOrder() {
	      	var valid = true;
	      	allFields.removeClass( "ui-state-error" );
	      	valid = valid && checkLength( ecole, "ecole", 3, 200 );
	      	valid = valid && checkLength( cursus, "cursus", 3, 200 );
	 
		    if ( valid ) {
		        $.ajax({
	  				type: 'POST',
	  				url: '{{ action("Offers\OfferController@createCommand") }}',
	  				data: {
	  					_token: csrf_token,
	  					plan_id : $("#abonnement").val(),
	  					ecole: ecole.val(),
	  					cursus: cursus.val()
	  				},
	  				dataType: 'json',
	  				success: function(data){
	  					if(!data.error){
		  					var form = (data.form);
		  					$("body").append(form);
		  					$("#form-"+data.id).submit();
		  				}
	  				}
	  			});
		        dialog.dialog( "close" );
		      }
		      return valid;
	    }

	    $(".order").on('click', function(e) {

			e.preventDefault(e);
			e.stopPropagation();
			var plan_id = $(this).attr('data-id');

       		$.ajax({
  				type: 'POST',
  				url: '{{ action("Offers\OfferController@createCommand") }}',
  				data: {
  					_token: csrf_token,
  					plan_id : plan_id
  				},
  				dataType: 'json',
  				success: function(data){
  					if(!data.error){
	  					var form = (data.form);
	  					$("body").append(form);
	  					$("#form-"+data.id).submit();
	  				}
  				}
  			});
  		});
	 
	    dialog = $( "#dialog-form" ).dialog({
	    	autoOpen: false,
	    	height: 400,
	    	width: 350,
	    	modal: true,
	    	buttons: {
	        	"S'abonner": addOrder,
	        	Cancel: function() {
			        dialog.dialog( "close" );
	        	}
	      	},
	      	close: function() {
	        	form[ 0 ].reset();
	        	allFields.removeClass( "ui-state-error" );
	      	}
	    });
	 
	    form = dialog.find( "form" ).on( "submit", function( event ) {
	    	event.preventDefault();
	    	addOrder();
	    });
	 
	    $( ".abonnement-etudiant" ).button().on( "click", function() {
	      dialog.dialog( "open" );
	      $("#abonnement").val($(this).attr('data-id'));
	    });




	    /// form promo

	    function addOrderPromo() {
	      	var valid = true;
	      	allFieldsPromo.removeClass( "ui-state-error" );
	      	valid = valid && checkLength( promo, "promo", 6, 200 );
	 
		    if ( valid ) {
		        $.ajax({
	  				type: 'POST',
	  				url: '{{ action("Offers\OfferController@createCommand") }}',
	  				data: {
	  					_token: csrf_token,
	  					plan_id : $("#abonnement-promo-id").val(),
	  					promo: promo.val(),
	  				},
	  				dataType: 'json',
	  				success: function(data){
	  					if(!data.error){
		  					var form = (data.form);
		  					$("body").append(form);
		  					$("#form-"+data.id).submit();
		  				}
	  				}
	  			});
		        dialogPromo.dialog( "close" );
		      }
		      return valid;
	    }

	    var dialogPromo, formPromo,
	     	promo = $( "#promo" ),
	     	allFieldsPromo = $( [] ).add( promo ),
	     	tipsPromo = $( ".validateTipsPromo" );

	     dialogPromo = $( "#dialog-form-promo" ).dialog({
	    	autoOpen: false,
	    	height: 400,
	    	width: 350,
	    	modal: true,
	    	buttons: {
	        	"S'abonner": addOrderPromo,
	        	Cancel: function() {
			        dialogPromo.dialog( "close" );
	        	}
	      	},
	      	close: function() {
	        	form[ 0 ].reset();
	        	dialogPromo.removeClass( "ui-state-error" );
	      	}
	    });

	    formPromo = dialogPromo.find( "form" ).on( "submit", function( event ) {
	    	event.preventDefault();
	    	addOrderPromo();
	    });
	 
	   	$( ".abonnement-promo" ).button().on( "click", function() {
	    	dialogPromo.dialog( "open" );
	    	$("#abonnement-promo-id").val($(this).attr('data-id'));
	    });


 	});
  </script>
@endpush
