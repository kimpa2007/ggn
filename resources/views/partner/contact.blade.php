@extends('layouts.frontend')
@section('title','Hello, partner')

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
			@role('user')
			<li class="breadcrumb-item"><a href="{{route('user.home')}}">{{ translate::key('front.general.account') }}t</a></li>
			@endrole
			<li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
		  <li class="breadcrumb-item">Contact</li>
		</ol>
	</div>
</div>
<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-info">
			<div class="section-title">
				<h2>Contact</h2>
				<hr class="decoration" />
			</div>

			@if($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if(Session::get('alert-type') == "success")
				<div class="alert alert-success">
					<ul>
						<li>{{ Session::get('message') }}</li>
					</ul>
				</div>
			@endif


			<div class="row">
				<div class="col-md-4">
					<div class="left-frame">
						<div class="inner-data">
							<i class="icon icon-lineart-separe5-94" style="font-size:85px;"></i>


						</div>

					</div>

				</div>
				<div class="col-md-8">
					<div class="form-frame">
						<form action="{{route('partner.contact.submit')}}" method="POST" >
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('name')?'has-error':''}}">
										<label for="name" class="control-label">Name</label>
										<input type="text" name="name" class="form-control" value="{{$user->firstname . ' ' . $user->lastname}}" readonly="readonly" />

									</div>

								</div>

								<div class="col-xs-6">
									<div class="form-group {{$errors->has('email')?'has-error':''}}">
										<label for="email" class="control-label">Email</label>
										<input type="text" name="email" class="form-control" value="{{$user->email}}" readonly />

									</div>

								</div>
							</div>
							<div class="row">
									<div class="col-xs-9">
									<div class="form-group {{$errors->has('subject')?'has-error':''}}">
										<label for="subject" class="control-label">Subject</label>
										<input type="text" name="subject" class="form-control" />

									</div>

								</div>
								<div class="col-xs-12">
									<div class="form-group {{$errors->has('comment')?'has-error':''}}">
										<label for="comment" class="control-label" >Message</label>

										<textarea name="comment" class="form-control" rows="5"></textarea>
									</div>
								</div>
							</div>


							<div class="row">
								<button type="submit">Send</button>
							</div>
						</form>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>


@endsection

@push('javascripts')

@endpush
