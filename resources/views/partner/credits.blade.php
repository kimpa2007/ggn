@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

@include('partner.partials.credits-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item">Credits</li>
        </ol>
    </div>
</div>



<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-credits">
			<div class="section-title">
				<h2>Repartition de crédits</h2>
				<hr class="decoration" />
			</div>

			<div class="body">
			    <div class="row">
			        <div class="col-md-12 bg-black form-container">
			            <table id="table" class="table front-table">
			                <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>Prénom / Nom</th>
			                        <th>Nom du produit</th>
			                        <th>Prix du produit</th>
			                        <th>Fait le</th>
			                        <th></th>
			                    </tr>
			                </thead>
			            </table>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection
