@extends('layouts.frontend')
@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
			@role('user')
			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
			@endrole
			<li class="breadcrumb-item">Customer account</li>
		</ol>
	</div>
</div>

<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-index">
			@if(Session::has('flash_message'))
				<div class="row">
				  <div class="col-md-12">
				   <ul class="flash-msg-list">
						<li class="flash-msg common">{!! session('flash_message') !!}</li>
					</ul>
				  </div>
				</div>
	    @endif
			<div class="row">
				<div class="col col-md-6">
					<div class="frame frame-partner-name">
							<a href="@role(['partner','admin']){{route('partner.account.index')}}@endrole">
							<i class="icon icon-lineart-separe3-66"></i>
							<h2 class="lastname">{{$user->partner->name or ''}}</h2>
							<p class="address">{{$user->address}} {{$user->postal_code}} {{$user->city}}</p>
							<p class="phone">{{ $user->email != "" ? 'E-mail. '.$user->email : '' }} {{ $user->telephone != "" ? 'Tél. '.$user->telephone : '' }}</p>

							@role(['partner','admin'])
							<span class="pane-button">My informations</span>
							@endrole
						</a>

					</div>
				</div>
				<div class="col col-md-3">
					<div class="frame frame-utilisateurs">
						<a href="@permission(['manage-users']){{route('partner.users.index')}}@endpermission" class="toparea">
							<i class="icon icon-lineart-separe-83"></i>
							<h3 class="docs-title">Users</h3>
						</a>

						<ul class="users-actions">
							<li><a class="list-button" href="{{route('partner.users.index')}}">User Managements</a></li>
						</ul>
					</div>
				</div>
					</div>
		{{--	<div class="row">
				<div class="col col-md-5">
					<div class="frame frame-contact-partner">
						<a href="{{route('partner.contact')}}">
							<i class="icon icon-lineart-separe5-94"></i>
							<span class="pane-button">Contact us</span>
						</a>
					</div>

				</div>
				<div class="col col-md-4">
						<div class="col col-xs-12">
							<div class="frame frame-contact-partner">
								@permission(['buy-content'])
								<a href="{{route('partner.plans')}}">
									<i class="icon icon-lineart-separe6-11"></i>
									<span class="pane-button">Subscription</span>
								</a>
								@endpermission
							</div>
						</div>
				</div>
			</div>--}}

		</div>



	</div>
</div>


@endsection

@push('javascripts')

<script>

  var csrf_token = "{{ csrf_token() }}";

  $(function(){
      $(".flip-container.disabled").click(function(e){
          var item = $(e.target).closest('.flip-container');

          var id = item.data('zone-id');

          console.log("id : "+id);

          bootbox.confirm({
            message: "I want to access this theme",
            buttons: {
                confirm: {
                    label: 'Send',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-secdonary'
                }
            },
            callback: function (result) {

                if(result){

                      $.ajax({
                        url : '{{route('partner.zones.petition')}}',
                        type: 'POST',
                        dataType: 'json',
                        data : {
                          zone_id : id,
                          _token : csrf_token
                        },
                        success : function(response){

                          if (response.code == 200) {

                            bootbox.alert("Request made successfully");


                          } else if (response.code == 400) {
                              //error
                              toastr.error(response.message, 'Error !', {
                                  timeOut: 5000
                              });
                          } else {
                              //nothing to change
                          }

                        },
                        error:function(msg){

                          var errors = response.responseJSON;

                          if(errors.hasOwnProperty('zone_id')){

                            toastr.error(errors.zone_id[0], 'Error !', {
                                timeOut: 3000
                            });

                          }
                          else if (errors.code == 400) {
                              //error
                              toastr.error(errors.message, 'Error !', {
                                  timeOut: 3000
                              });
                          }
                          else {

                            toastr.error('An error occurred while saving', 'Error !', {
                                timeOut: 3000
                            });

                          }
                        }
                      });

                }
            }
          });

      });
  });
</script>

@endpush
