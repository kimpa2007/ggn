@extends('layouts.frontend')
@section('title','Hello, partner')

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
			@role('user')
			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
			@endrole
			<li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
		  <li class="breadcrumb-item">Informations</li>
		</ol>
	</div>
</div>
<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-info">
			<div class="section-title">
				<h2>Informations</h2>
				<hr class="decoration" />
			</div>

			@if(Session::has('flash_message'))
				<div class="row">
				  <div class="col-md-12">
				   <ul class="flash-msg-list">
						<li class="flash-msg success">{!! session('flash_message') !!}</li>
					</ul>
				  </div>
				</div>
	      	@endif

	      	@if($errors->any())
				<div class="row">
				    <div class="col-md-12">
			             <ul class="flash-msg-list">
			                {!! implode('', $errors->all('<li class="flash-msg">:message</li>')) !!}
			            </ul>
				    </div>
				</div>
			@endif


			<div class="row">
				<div class="col-md-4">
					<div class="left-frame">
						<div class="inner-data">
							<h1 class="lastname">{{$partner->name}}</h2>
							<p class="address">{{$partner->address}}</p>
							<p class="phone">Manager <span><b>{{$partner->manager->firstname}}</b></span></p>
							<p class="phone">Ref. <span>{{$partner->referent_data}}</span></p>
							<p class="email">E-mail <span>{{$partner->manager->email}}</span></p>

						</div>

					</div>

				</div>
				<div class="col-md-8">
					<div class="form-frame">
						<form action="{{route('partner.account.update')}}" method="POST" >
							{{ csrf_field() }}
							<input name="_method" type="hidden" value="PUT">
							<div class="row">
								<div class="col-xs-12">
									<legend>Modify informations</legend>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('name')?'has-error':''}}">
										<label for="name" class="control-label">Name</label>
										<input type="text" name="name" class="form-control" value="{{$partner->name}}" />

									</div>

								</div>
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('lastname')?'has-error':''}}">
										<label for="lastname" class="control-label" >Type</label>

										<select name="type" class="form-control">
											<option value="CE" @if(old('type',$partner->type) == 'CE') selected="selected" @endif>Commitee</option>
											<option value="Autre" @if(old('type',$partner->type) == 'Autre') selected="selected" @endif>Autre</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group {{$errors->has('address')?'has-error':''}}">
										<label for="address" class="control-label">Address</label>
										<input type="text" name="address" class="form-control" value="{{$partner->address}}" />
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('telephone')?'has-error':''}}">
										<label for="referent_data" class="control-label">Referent data</label>
										<input type="text" name="referent_data" class="form-control" value="{{$partner->referent_data}}"/>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('email')?'has-error':''}}">
										<label for="email" class="control-label">Manager</label>
										<input type="text" name="email" class="form-control"  value="{{$user->firstname}} {{$user->lastname}}" readonly />
									</div>
								</div>
							</div>

							<div class="row">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>


@endsection

@push('javascripts')

@endpush
