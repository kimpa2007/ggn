@extends('layouts.frontend')

@section('title', 'Commandes')

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
          <li class="breadcrumb-item">{{ __('messages.front.user.orders.list') }}</li>
		</ol>
	</div>
</div>

<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-credits">
			<div class="section-title">
				<h2>{{ __('messages.front.user.orders.list') }}</h2>
				<hr class="decoration" />
			</div>

			<div class="body">
			    <div class="row">
			        <div class="col-md-12 bg-witch form-container">
			            <table id="table" class="table front-table">
			                <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>{{ __('messages.front.user.orders.field.date') }}</th>
			                        <th>{{ __('messages.front.user.orders.field.type') }}</th>
			                        <th>{{ __('messages.front.user.orders.field.plan') }}</th>
			                        <th>{{ __('messages.front.user.orders.field.start') }}</th>
			                        <th>{{ __('messages.front.user.orders.field.end') }}</th>
			                        <th>{{ __('messages.front.user.orders.field.amount') }}</th>
                              		<th>{{ __('messages.front.user.orders.field.status') }}</th>
			                      <!--  <th>Actions</th> -->
			                    </tr>
			                </thead>
			            </table>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection


@push('stylesheets')
   {!! Html::style('/plugins/datatables/datatables.min.css') !!}
@endpush

@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : '{{ route("partner.orders.data") }}'
        };
    </script>

	<script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>
	{!! Html::script('/js/partners/orders.js') !!}
@endpush
