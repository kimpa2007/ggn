@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item">Subscription</li>
        </ol>
    </div>
</div>


<div class="horizontal-inner-container">
    <div class="header">
        <h1>
          Subscriptions
        </h1>
    </div>

</div>
@endsection
