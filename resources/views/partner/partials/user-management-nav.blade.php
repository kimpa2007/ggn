<div class="user-sub-nav">
	<div class="horizontal-inner-container">
		<ul>
			<li @if(Route::is('partner.users.index')) class="active" @endif>
				<a href="{{route('partner.users.index')}}">Management</a>
			</li>
		</ul>
	</div>

</div>
