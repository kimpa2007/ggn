<div class="user-nav">
	<div class="horizontal-inner-container">
		<ul>
			<li @if(Route::is('partner.home')) class="active" @endif>
				<a href="{{route('partner.home')}}" >{{ __('messages.front.user.menu.customer_account') }}</a>
			</li>
			@role(['partner'])
			<li @if(Route::is('partner.account.index')) class="active" @endif>
				<a href="{{route('partner.account.index')}}" >{{ __('messages.front.user.menu.informations') }}</a>
			</li>

			<li @if(Route::is('partner.account.index')) class="active" @endif>
				<a href="{{route('partner.orders')}}">{{ __('messages.front.user.menu.orders') }}</a>
			</li>
			@endrole
			<li @if(Route::is('partner.users.index')) class="active" @endif>
				<a href="{{route('partner.users.index')}}">{{ __('messages.front.user.menu.users') }}</a>
			</li>
			<li @if(Route::is('partner.contact')) class="active" @endif>
				<a href="{{route('partner.contact')}}">{{ __('messages.front.user.menu.contact') }}</a>
			</li>
		</ul>
	</div>

</div>
