@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            @role('user')
            <li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
            @endrole
            <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
            <li class="breadcrumb-item">Subscription</li>
        </ol>
    </div>
</div>

<div class="user-account">
  <div class="horizontal-inner-container">
    <div class="user-account-info">
      <div class="section-title">
        <h2>Packages</h2>
        <hr class="decoration" />
      </div>

      @if(Session::has('flash_message'))
        <div class="row">
          <div class="col-md-12">
            <ul class="flash-msg-list">
              <li class="flash-msg success">{!! session('flash_message') !!}</li>
            </ul>
          </div>
        </div>
      @endif

      @if($errors->any())
        <div class="row">
            <div class="col-md-12">
                   <ul class="flash-msg-list">
                      {!! implode('', $errors->all('<li class="flash-msg">:message</li>')) !!}
                  </ul>
            </div>
        </div>
      @endif
      <div class="body">
        <table id="table" class="table front-table" style="background:#FFF;margin-top: 20px;">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Areas</th>
                    <th>Duration</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
             <tfoot>
               <tr>
                    <th>Name</th>
                    <th>Areas</th>
                    <th>Duration</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection


@push('stylesheets')
   {!! Html::style('/plugins/datatables/datatables.min.css') !!}
@endpush

@push('javascripts')
<script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>
<script>
    var usersTable = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: '{{ route("partner.plansdata") }}',
        columns: [
            {data: 'name', name: 'name'},
            {data: 'zones', name: 'zones'},
            {data: 'duration', name: 'duration'},
            {data: 'status', name: 'status'},
            {data: 'actions', searchable: false, orderable: false}
        ],
        initComplete: function () {
            this.api().columns([3]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val)
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );

            $('.toggle-delete').on('click', function(e){
                e.preventDefault();
                var el = $(this);
				bootbox.confirm({
		            message: "Êtes-vous sur de vouloir faire cette action ?",
		            buttons: {
		                confirm: {
		                    label: 'Oui',
		                    className: 'btn-primary'
		                },
		                cancel: {
		                    label: 'Non',
		                    className: 'btn-default'
		                }
		            },
		            callback: function (result) {
		            	if(result){
		                	window.location.href = el.attr('href');
		                }
		            }
		        });
			});
        }
    })
</script>
@endpush
