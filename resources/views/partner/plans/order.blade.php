@extends('layouts.frontend')
@section('title','Hello, ' . Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            @role('user')
            <li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
            @endrole
            <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
            <li class="breadcrumb-item">Subscription</li>
        </ol>
    </div>
</div>

<div class="user-account">
	<div class="horizontal-inner-container">

        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Purchase of subscription</h2>
                <table class="table">
                    <tr>
                        <td>Name : </td>
                        <td>{{ $order->plan->name }}</td>
                    </tr>

                    <tr>
                        <td>Price :</td>
                        <td>{{ $order->plan->price }}&euro;</td>
                    </tr>

                    <tr>
                        <td>Duration :</td>
                        <td>{{ $order->plan->time }} month</td>
                    </tr>

                    <tr>
                        <td>Areas :</td>
                        <td>
                            @foreach($order->plan->zones as $t)
                                <p>{{ $t->name }}</p>
                            @endforeach
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="payment_type" class="control-label">Choice of payment method: </label>
                        </td>
                        <td>
                            {{
                                Form::open([
                                    'url' => route('partner.plans.order.confirm.save', $order->id),
                                    'method' => 'POST'
                                ])
                            }}
                            <select name="payment_type" class="form-control">
                                <option value="{{ App\Models\PartnerOrder::PAYMENT_CHEQUE }}">Check</option>
                                <option value="{{ App\Models\PartnerOrder::PAYMENT_TRANSFERT }}">Bank transfer</option>
                                <option value="{{ App\Models\PartnerOrder::PAYMENT_CB }}" disabled>Credit card</option>
                            </select>

                            <input type="submit" value="Envoyer" class="btn btn-default btn-sm"/>

                            {{ Form::close() }}
                        </td>
                    </tr>

                </table>
            </div>
        </div>


	</div>
</div>
@endsection

@push('javascripts')
@endpush
