@extends('layouts.frontend')
@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            @role('user')
            <li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
            @endrole
            <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
            <li class="breadcrumb-item">Subscription</li>
        </ol>
    </div>
</div>

<div class="user-account">
	<div class="horizontal-inner-container">

        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Thank you,<br />We have taken into account your purchase.</h1>

                @if($order->type_payment == App\Models\PartnerOrder::PAYMENT_TRANSFERT)
                    <p>
                      To complete your purchase, please make the bank transfer to the account
                    </p>

                    <p>
                        <strong>XXXX XXXX XXXX XXXX XXX</strong>
                    </p>

                    <p>
                      Please specify the <strong> order number {{$order->id}} </strong> in the transfer name.
                    </p>
                @endif

                @if($order->type_payment == App\Models\PartnerOrder::PAYMENT_CHEQUE)
                    <p>
                      To finalize your purchase, send us your check to the following address:
                    </p>

                    <p>
                        <strong>

                        </strong>
                    </p>

                    <p>
                        Please specify the <strong> order number {{$order->id}} </strong> on the back of the check.
                    </p>
                @endif
            </div>
        </div>


	</div>
</div>
@endsection

@push('javascripts')

@endpush
