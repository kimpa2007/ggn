@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

@include('partner.partials.user-management-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item"><a href="{{route('partner.users.index')}}">Users</a></li>
          <li class="breadcrumb-item">Add user</li>
        </ol>
    </div>
</div>


<div class="horizontal-inner-container">
    <div class="header">
        <h1>@if(isset($user->id)) {{'Edition'}} @else {{ 'Add  user' }}@endif</h1>
    </div>
    <div class="body">

              	{!!
                      Form::open([
              					'url' => isset($user)
              						? route('partner.users.update', $user)
              						: route('partner.users.save'),
                        'files' => true,
              					'method' => 'POST'
                      ])
                  !!}


      	            <div class="row">
      	                <div class="col-md-6">
      	                    <div class="form-group label-floating">
      	                        <label class="control-label">Last name</label>
      	                        <input type="text" name="lastname" value="{{ $user->lastname or old('lastname') }}" class="form-control">
      	                    </div>
      	                </div>

      	                <div class="col-md-6">
      	                    <div class="form-group label-floating">
      	                        <label class="control-label">First name</label>
      	                        <input type="text" name="firstname" value="{{ $user->firstname or old('firstname') }}" class="form-control"/>
      	                    </div>
      	                </div>
      	            </div>


                      <div class="row">
      	                <div class="col-md-6">
              				<div class="form-group label-floating">
              	                <label class="control-label">E-mail</label>
              	                <input type="text" name="email" value="{{ $user->email or old('email') }}" class="form-control"/>
              	            </div>
                          </div>

      	                <div class="col-md-6">
              	            <div class="form-group label-floating">
              	                <label class="control-label">Phone</label>
              	                <input type="text" name="telephone" value="{{ $user->telephone or old('telephone') }}" class="form-control"/>
              	            </div>
                          </div>
      	            </div>



                    <div class="row">
                      <div class="col-md-6">
      	            <div class="form-group label-floating">
      	                <label class="control-label">Mot de passe</label>
      	                <input type="password" name="password" value="" class="form-control"/>
      	            </div>
                  </div>

                <div class="col-md-6">
      	            <div class="form-group label-floating">
      	                <label class="control-label">Confirmation du mot passe</label>
      	                <input type="password" name="password_confirmation" value="" class="form-control"/>
      	            </div>


                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                            <div class="form-group label-floating">
                              <label class="control-label">Status</label>
                      {{
                        Form::select(
                          'status',
                          App\Models\User::getStatus(),
                          isset($user) ? $user->status : null,
                          [
                            'class' => 'form-control'
                          ]
                        )
                      }}
                          </div>
                  </div>
                  <div class="form-group label-floating text-center">
                     <input type="submit" value="Enregistrer" class="btn btn-primary submit-form"/>
                 </div>
            </div>
              <input type="hidden" value="1" name="role_id"/>
              <input type="hidden" value="ACTIVE" name="status"/>
              <input type="hidden" value="1" name="partner_id"/>
              {{ Form::hidden('id', isset($user) ? $user->id : null) }}
              {!! Form::close() !!}

              </div>
          </div>
      </div>



    </div>

</div>
@endsection
