@extends('layouts.frontend')

@section('title','Users')

@section('content')

@include('partner.partials.user-nav')


<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item">Users</li>
        </ol>
    </div>
</div>


<div class="user-account">
  <div class="horizontal-inner-container">
      <div class="section-title">
        <h2>Users</h2>
        <hr class="decoration" />
      </div>
      <a href="{{route('partner.users.add')}}">Add an user</a>
      <div class="body">
          <table id="table" class="table" style="background:#FFF;margin-top: 20px;">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>E-mail</th>
                      <th>Status</th>
                      <th>joined</th>
                      <th></th>
                  </tr>
              </thead>
               <tfoot>
                  <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>E-mail</th>
                      <th>Status</th>
                      <th></th>
                      <th></th>
                  </tr>
              </tfoot>
          </table>

      </div>
  </div>
</div>
@endsection

@push('stylesheets')
   {!! Html::style('/plugins/datatables/datatables.min.css') !!}
@endpush

@push('javascripts')

<script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: '{{ route("partner.users.getdata") }}',
        order: [[ 4, "desc" ]],
        columns: [
            {data: 'id', name: 'id', width: "40"},
            {data: 'full_name', name: 'full_name'},
			      {data: 'email', name: 'email'},
            {data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function () {
            this.api().columns([3]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val)
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    })
</script>
@endpush
