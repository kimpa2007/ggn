@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

@include('partner.partials.user-management-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Partner account</a></li>
          <li class="breadcrumb-item"><a href="{{route('partner.users.index')}}">Users</a></li>
           <li class="breadcrumb-item"><a href="{{route('partner.users.notify.index')}}">Notifications</a></li>
          <li class="breadcrumb-item">Edit Notification</li>
        </ol>
    </div>
</div>




<div class="user-account">
  <div class="horizontal-inner-container">
    <div class="user-account-info">
      <div class="section-title">
        <h2>Editing <em>'{{$notification->title}}'</em></h2>
        <hr class="decoration" />
      </div>

      @if($errors->any())
        <div class="row">
            <div class="col-md-12">
                   <ul class="flash-msg-list">
                      {!! implode('', $errors->all('<li class="flash-msg">:message</li>')) !!}
                  </ul>
            </div>
        </div>
      @endif
       @if(Session::has('flash_message'))
        <div class="row">
          <div class="col-md-12">
           <ul class="flash-msg-list">
                <li class="flash-msg common">{!! session('flash_message') !!}</li>
            </ul>
          </div>
        </div>
        <ul>
          <li><a href="#"></a></li>
        </ul>
    @endif
    <div class="body">
      {!!
              Form::open([
                  'url' => route('partner.users.notify.update', $notification->id),
                  'method' => 'PUT'
              ])
          !!}
          <input class="form-control" name="partner_id" type="hidden" value="{{\Auth::user()->partner_id}}"  >

            <div class="row">
              <div class="col-md-10">
                <div class="form-group">
                  <label for="title" class="control-label">Title</label>
                  <input class="form-control" name="title" value="{{old('title',$notification->title)}}"  >
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                   <label for="message" class="control-label">Message</label>
                   <textarea name="message" class="form-control" rows="5">{{old('message',$notification->message)}}</textarea>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-3 pull-right">

                <button type="submit" name="status" value="SENT"  class="btn btn-lg btn-default">Send</button>
              </div>
              <div class="col-md-3 pull-right">
                <button type="submit" name="status" value="DRAFT" class="btn btn-lg btn-default"  >Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection
