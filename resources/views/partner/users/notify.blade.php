@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

@include('partner.partials.user-management-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item"><a href="{{route('partner.users.index')}}">Users</a></li>
          <li class="breadcrumb-item">Notifications</li>
        </ol>
    </div>
</div>




<div class="user-account">
  <div class="horizontal-inner-container">
    <div class="user-account-info">
      <div class="section-title">
        <h2>Notifications</h2>
        <hr class="decoration" />
      </div>
      <div class="text-center">
        <a onclick="openNewNotificationModal()" class="btn btn-default ">Send a new notification</a>
      </div>
      @if($errors->any())
        <div class="row">
            <div class="col-md-12">
                   <ul class="flash-msg-list">
                      {!! implode('', $errors->all('<li class="flash-msg">:message</li>')) !!}
                  </ul>
            </div>
        </div>
      @endif
       @if(Session::has('flash_message'))
        <div class="row">
          <div class="col-md-12">
           <ul class="flash-msg-list">
                <li class="flash-msg common">{!! session('flash_message') !!}</li>
            </ul>
          </div>
        </div>

    @endif
    <div class="body">
      <table id="table" class="table" style="background:#FFF;margin-top: 20px;">
          <thead>
              <tr>
                  <th>Title</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Send to</th>
                  <th>Read</th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>Title</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Send to</th>
                  <th>Read</th>
              </tr>
          </tfoot>
        </table>

    </div>
</div>


@endsection

@push('stylesheets')
   {!! Html::style('/plugins/datatables/datatables.min.css') !!}
   {!! Html::style('/plugins/select2/css/select2.min.css') !!}
@endpush

@push('javascripts')
<script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>
<script>
    function openNewNotificationModal(){
      console.log('openNewNotificationModal');
      $('#newNotificationModal').modal('show');
    }
    var luesTable =null;
    function readUnreadModalOpen(id){
      console.log('readUnreadModalOpen: ' + id + ', {{ route("partner.users.notify.readunreaddata") }}/'+id);
      if(luesTable){
        luesTable.destroy();
      }
      $('#readUnreadModal').modal('show');


       luesTable = $('#luestable').DataTable({
          language: {
              "url": "/plugins/datatables/locales/french.json"
          },
          processing: true,
          serverSide: true,
          pageLength: 10,
          ajax: '{{ route("partner.users.notify.readunreaddata") }}/'+id,
          columns: [
                {data: 'full_name', name: 'full_name'},

                {data: 'read', name: 'read'},
                {data: 'status', name: 'status'}

            ]
            /* FIXME
            ,
            initComplete: function () {
                this.api().columns([2]).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val)
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }*/
      });


    }
    var usersTable = $('#table').DataTable({
        language: {
            "url": "/plugins/datatables/locales/french.json"
        },
        processing: true,
        serverSide: true,
        pageLength: 50,
        ajax: '{{ route("partner.users.notify.data") }}',
        columns: [
            {data: 'title', name: 'title'},
            {data: 'created_at', name: 'created_at'},
            {data: 'status', name: 'status'},
            {data: 'users_count', name: 'users_count'},
            {data: null, searchable: false, orderable: false, render:
                      function ( data, type, row, meta ) {
                          var htmlData = '';
                          if(data.status == '{{App\Models\Notification::getStatus()['SENT']}}'){
                            htmlData = '<a class="btn btn-secondary-3 btn-sm" onclick="readUnreadModalOpen('+data.id+')">Lues</a>'
                          }else{
                            htmlData = '<a class="btn btn-secondary-2 btn-sm" href="{{ route("partner.users.notify.draft") }}/'+data.id+'">Editer</a>';
                          }

                          return htmlData;
                      }
             }
           ],
           initComplete: function () {
               this.api().columns([2]).every( function () {
                   var column = this;
                   var select = $('<select><option value=""></option></select>')
                       .appendTo( $(column.footer()).empty() )
                       .on( 'change', function () {
                           var val = $.fn.dataTable.util.escapeRegex(
                               $(this).val()
                           );

                           column
                               .search( val)
                               .draw();
                       } );

                   column.data().unique().sort().each( function ( d, j ) {
                       select.append( '<option value="'+d+'">'+d+'</option>' )
                   } );
               } );
           }

    });

</script>
<div class="modal fade cms-modal draggable" id="newNotificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">New notification</h4>
      </div>

        <form action="{{ route('partner.users.notify.send') }}" method="POST">
        <input class="form-control" name="partner_id" type="hidden" value="{{\Auth::user()->partner_id}}"  >
        <div class="modal-body">

          {!! csrf_field() !!}
          <div class="row">
            <div class="col-md-10">
              <div class="form-group">
                <label for="title" class="control-label">Title</label>
                <input class="form-control" name="title"  >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                 <label for="message" class="control-label">Message</label>
                 <textarea name="message" class="form-control" rows="5"></textarea>
              </div>
            </div>
          </div>


          <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
          <a class="btn btn-default pull-left" data-dismiss="modal">Cancel</a>
          <div class="pull-right">
            <button type="submit" name="status" value="DRAFT" class="btn btn-default">Save as draft</button>
            <button type="submit" name="status" value="SENT"  class="btn btn-lg btn-secondary-3">Send</button>
          </div>

        </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
</div>

<div class="modal fade cms-modal draggable" id="readUnreadModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
     <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Read</h4>
      </div>
       <div class="modal-body">


          <div class="row">
            <div class="col-md-12">
              <table id="luestable" class="table" style="background:#FFF;margin-top: 20px;">
                  <thead>
                      <tr>
                          <th>User</th>
                          <th>Date</th>
                          <th>Read / Unread</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>User</th>
                          <th>Date</th>
                          <th>Read / Unread</th>
                      </tr>
                  </tfoot>

              </table>
            </div>
          </div>
       </div>
    </div>
    <!-- /.modal-content -->
  </div>
</div>



@endpush
