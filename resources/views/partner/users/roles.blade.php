@extends('layouts.frontend')

@section('title',"Users' powers")

@section('content')

@include('partner.partials.user-nav')

@include('partner.partials.user-management-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
			@role('user')
			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item"><a href="{{route('partner.users.index')}}">Users</a></li>
          <li class="breadcrumb-item">Powers for '{{$user->firstname}}'</li>
		</ol>
	</div>
</div>

<div class="user-account">
  <div class="horizontal-inner-container">
    <div class="user-account-info">
      <div class="section-title">
        <h2>Powers for {{$user->firstname}} {{$user->lastname}}</h2>
        <hr class="decoration" />
      </div>
      <div class="partner-roles-area">
      	@if(Session::has('flash_message'))
			<div class="row">
			  <div class="col-md-12">
			   <ul class="flash-msg-list">
					<li class="flash-msg common">{!! session('flash_message') !!}</li>
				</ul>
			  </div>
			</div>
      	@endif
      	<div class="row">
      		{!! Form::open(['url' => route('partner.users.roles.update',$user->id), 'files'=>true, 'method' => 'PUT']) !!}
	      		<div class="col-xs-4">
	      			<div class="perm permission-users @if($user->can('manage-users')) enabled @endif">
	      				<input type="hidden" name="partner_id" value="{{Auth::user()->partner_id}}">
	      				<h2>Manage Users</h2>
	      				<p>The User can manage other Users</p>
						<button name="permission" value="manage-users" type="submit" class="btn btn-default" >
	      				@if($user->can('manage-users'))
	      					Delete
	      				@else
	      					Add
						@endif
						</button>
	      			</div>

	      		</div>
	      		<div class="col-xs-4">
	      			<div class="perm permission-credits @if($user->can('manage-credits')) enabled @endif">
	      				<h2>Manage Credits</h2>
	      				<p>L'User peut organiser et distribuer les credits entre les Users</p>
	      				<button name="permission" value="manage-credits" type="submit" class="btn btn-default" >
	      				@if($user->can('manage-credits'))
	      					Supprimer
	      				@else
	      					Ajouter
						@endif
						</button>
	      			</div>

	      		</div>
	      		<div class="col-xs-4">
	      			<div class="perm permission-forfaits @if($user->can('buy-content')) enabled @endif">
	      				<h2>Manage subscription</h2>
	      				<p>The User can purchase packages</p>
	      				<button name="permission" value="buy-content" type="submit" class="btn btn-default" >
	      				@if($user->can('buy-content'))
	      					Delete
	      				@else
	      					Add
						@endif
						</button>
	      			</div>

	      		</div>
      		</form>
      	</div>

      </div>


    </div>
  </div>
</div>



@endsection

@push('javascripts')

<script>

</script>

@endpush
