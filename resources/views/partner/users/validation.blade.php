@extends('layouts.frontend')

@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('partner.partials.user-nav')

@include('partner.partials.user-management-nav')

<div class="breadcrumbs-container">
    <div class="horizontal-inner-container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
          @role('user')
    			<li class="breadcrumb-item"><a href="{{route('user.home')}}">My account</a></li>
    			@endrole
          <li class="breadcrumb-item"><a href="{{route('partner.home')}}">Customer account</a></li>
          <li class="breadcrumb-item"><a href="{{route('partner.users.index')}}">Users</a></li>
          <li class="breadcrumb-item">Validation</li>
        </ol>
    </div>
</div>



<div class="user-account">
  <div class="horizontal-inner-container user-account-credits">

      @if($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif

      @if(Session::get('alert-type') == "success")
        <div class="alert alert-success">
          <ul>
            <li>{{ Session::get('message') }}</li>
          </ul>
        </div>
      @endif

      <div class="section-title">
  			<h2>Users validation</h2>
  			<hr class="decoration" />
  		</div>

      @if(Session::has('flash_message'))
          <div class="row">
            <div class="col-md-12">
             <ul class="flash-msg-list">
                  <li class="flash-msg common">{!! session('flash_message') !!}</li>
              </ul>
            </div>
          </div>
      @endif

      <div class="text-center">
        <a class="btn btn-secondary-2 select-all" href="#">Select all</a>&nbsp;
      </div>

      <div class="body form-container">
      	<table id="table" class="table front-table" style="background:#FFF;margin-top: 20px;">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>E-mail</th>
                      <th>Status</th>
      				            <th>Joined</th>
                      <th></th>
                  </tr>
              </thead>
               <tfoot>
                  <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>E-mail</th>
                      <th>Status</th>
                      <th>Joined</th>
                      <th></th>
                  </tr>
              </tfoot>
          </table>
          <br>

          <div class="buttons-container text-center">
              <a class="btn btn-secondary-2 multiple-cancel" data-loading-text="Chargement..."  href="#">Decline</a>&nbsp;
              <a class="btn btn-primary multiple-accept" data-loading-text="Chargement..." href="#">Validate</a>

          </div>

      </div>
  </div>
</div>
@endsection

@push('stylesheets')
   {!! Html::style('/plugins/datatables/datatables.min.css') !!}
@endpush

@push('javascripts')
<script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>
<script>
    var csrf_token = "{{ csrf_token() }}";
    var routes = {
    	data : "{{route("partner.users.validationdata")}}",
    	active : "{{route("partner.users.accept")}}",
      refused : "{{route("partner.users.refuse")}}",
      activeMultiple : "{{route("partner.users.accept.multiple")}}",
      refuseMultiple : "{{route("partner.users.refuse.multiple")}}",

    };
</script>
{!! Html::script('/js/partners/users/pending.js') !!}
@endpush
