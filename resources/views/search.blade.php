@extends('layouts.frontend',[
	"is_home" => 1
])
@section('title', translate::key('meta.search.title') )
@section('metaDescription', translate::key('meta.search.description'))

@section('content')
	{{--@include ('home.partials.map')--}}

<div class="horizontal-container bg-white block-2">
    <div class="horizontal-inner-container">
        <h2>{{ translate::key('front.general.result_for') }} : {{ Request::get('q') }}</h2>
        <div class="seprator" style="height:20px;"></div>
        <div class="row content-container search">

            @if(sizeof($contents))
                @foreach($contents as $content)
                    <div class="item">
						<div class="image">

							<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
			                  <div class="flipper">
			                    <div class="front" style="width: 95%;height: 210px;">
			                      <!-- front content -->
			                      @if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
			                      <div class="background-image" style="background-image:url('{{ Storage::url($content->image) }}')"></div>
			                      <div class="background-hover"></div>
			                        <div class="grid-item-content">
			                        <h3>{{ str_limit(strip_tags($content->title), $limit = 30,'...')}}</h3>
			                      </div>
			                    </div>
			                    <div class="back" style="width: 95%;height: 210px;">
			                      <!-- back content -->
			                      <div class="background-image" style="background-image:url('{{ Storage::url($content->image) }}')"></div>
			                      <div class="background-hover-turn"></div>
			                      <div class="grid-item-content-turn">
			                        <h4>{{$content->title}}</h4>
			                        <p style="text-align:justify;">
																{{ str_limit(strip_tags($content->description), $limit = 150,'...')}}
			                            @if(Auth::user())
			                              <div class="button-container">
			                                <a href="{{ route('user.content.show',['id'=>$content->id]) }}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
			                              </div>
			                            @else
			                            {{--<div class="button-container">
			                               <a href="{{ route('user.content.show', $content->id) }}" class="btn btn-primary">{{ translate::key('front.general.contact_us') }}</a>
			                            </div>--}}
			                           		<div class="button-container">
                                 				<a href="{{ route('offer.show') }}" class="btn btn-primary">{{ __('messages.abonnement.abonnezvous') }}</a>
											</div>
			                          @endif
			                        </p>
			                      </div>
			                    </div>
			                  </div>
			                </div>


						</div>
						<div class="content">
							<div class="title"><a href="{{route('user.content.show', $content->id)}}">{{ $content->title }}</a></div>
							<p style="text-align:justify;">
								@if($content->type == App\Models\Content::TYPE_MOVIE)
									{{ str_limit(strip_tags($content->description), 400) }}
								@else
									{{ str_limit(strip_tags($content->content), 400) }}
								@endif
								@if(Auth::user())
									<a href="{{ route('user.content.show',['id'=>$content->id]) }}" class="link">{{ translate::key('front.general.see_content') }}</a>
							  	@endif
							</p>


						</div>
                    </div>
                @endforeach()
			@else
				<p align="center">{{ translate::key('front.general.no_results') }}</p>
			@endif



			<div class="paginator">
            {{ isset($contents) ? $contents->appends(['q' => Request::get('q')])->links() : null }}
			</div>
        </div>
    </div>
</div>
@endsection

@push('stylesheets')
@endpush

@push('javascripts-libs')
@endpush

@push('javascripts')
@endpush
