@extends('layouts.frontend')
@php
switch ($theme["name"]) {
	case 'Geopolitics':
			$metaTitle = "GlobalGeoNews Geopolitics | Geopolitical information";
			$metaDescription = "GlobalGeoNews, daily geopolitical analysis and news by reporters and researchers. Retrouvez quotidiennement les analyses et informations géopolitiques des reporters et chercheurs qui collaborent avec GlobalGeoNews.";
	break;

	case 'Analysis':
			$metaTitle = "GlobalGeoNews Analysis | Maghreb, Middle East and Asia";
			$metaDescription = "GlobalGeoNews, analysis focused on Maghreb, Middle East and Asia on a broad perspective. Retrouvez les analyses GlobalGeoNews centrées sur le Maghreb, le Moyen-Orient et l'Asie dans un contexte international.";
	break;

	case 'Economy':
			$metaTitle = "GlobalGeoNews Economy | Financial news for risk analysis";
			$metaDescription = "GlobalGeoNews, economical and financial news intended for risk analysis and investment companies. Retrouvez les actualités économiques et financières GlobalGeoNews destinées aux entreprises dans leur politique d'investissements.";
	break;

	case 'Society':
			$metaTitle = "GlobalGeoNews Society | Reportages, analysis and written forecasting";
			$metaDescription = "GlobalGeoNews Society, reportages, analysis and written forecasting. Reportages, analyses et prospectives sur des sujets d'actualités de société sur notre plateforme Web GlobalGeoNews.";
	break;
}
@endphp
@section('title',$metaTitle)
@section('metaDescription',$metaDescription)
@section('content')

<div class="horizontal-container bg-black block-2 container-content">

	<div class="horizontal-inner-container">
		<h2>{{ $theme["name"] or '' }}</h2>
		<div class="seprator" style="height:20px;"></div>
		<div class="row content-wrapper">

			@foreach($contents as $content)

			<div class="grid-item col-md-3">

				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">

						<div class="front">
							<!-- front content -->
							@if($content->type == \App\Models\Content::TYPE_MOVIE)<div class="badge-warning-position"><span class="badge badge-warning">Video</span></div>@endif
							<div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
							<div class="background-hover"></div>
								<div class="grid-item-content">
								@php
									//remove all html tags from description
									$name = strip_tags($content->title);
								@endphp
								<h3>
									{{ str_limit($name, $limit = 50,'...')}}
								</h3>
							</div>
						</div>

						<div class="back">
							<!-- back content -->
							<div class="background-image" style="background-image:url('{{ Storage::url($content->image)}}')"></div>
							<div class="background-hover-turn"></div>
							<div class="grid-item-content-turn">
								<h4>{{ $content->name }}</h4>
								<p>
									{{ str_limit(strip_tags($content->description), $limit = 150, '...')}}

									@if(Auth::user())
										<div class="button-container">
											<a href="{{route('user.content.show',['id'=>$content->id])}}" class="btn btn-primary">{{ translate::key('front.general.discover') }}</a>
										</div>
									@else
										<div class="button-container">
											<a href="#" class="btn btn-primary">{{ translate::key('front.general.contact') }}</a>
										</div>
									@endif
								</p>

							</div>
						</div>
					</div>
				</div>

			</div>
			@endforeach
		</div>

		@if($count > $itemsPerPage)
			<div class="see-more">
				<a href="#" class="btn btn-secondary" id="toogle-more">{{ translate::key('front.general.see_more') }}</a>
			</div>
		@endif

		<div class="seprator" style="height:20px;"></div>
	</div>
</div>

@endsection

@push('javascripts')
<script>
	var itemsPerPage = {{ $itemsPerPage }};
	var count = {{ $count }};
	var page = 1;
	var availablePages = Math.ceil(count / itemsPerPage);

	$(function(){
		$("#toogle-more").click(function(e){

			e.preventDefault();

			if(page >= availablePages) {
				return;
			}

			$.ajax({
				url: "{{ route('theme', $theme["slug"]) }}",
				type: "GET",
				data: {
					page : page
				},
				success: function(response){
					page++;

					$(".content-wrapper").append($(response).find('.content-wrapper').html());

					if(parseInt(page) >= parseInt(availablePages)){
						$("#toogle-more").parent().fadeOut();
					}
				},
				error:function(msg){}
			});
		});
	});
</script>
@endpush
