@extends('layouts.frontend')
@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('user.partials.user-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
			<li class="breadcrumb-item"><a href="{{route('user.index')}}">{{ translate::key('front.general.account') }}</a></li>
		  <li class="breadcrumb-item">{{ translate::key('front.general.contact_us') }}</li>
		</ol>
	</div>
</div>


<div class="horizontal-container">

	<div class="horizontal-inner-container">

		<div class="contact">

			@if($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if(Session::get('alert-type') == "success")
				<div class="alert alert-success">
					<ul>
						<li>{{ Session::get('message') }}</li>
					</ul>
				</div>
			@endif


			{{Form::open([
					'url' => route('user.contact.send'),
					'method' => 'POST',
					'id' => 'contact-form'
				])}}

				<input type="hidden" name="name" value="{{ $user->getFullNameAttribute() }}">
				<input type="hidden" name="email" value="{{ $user->email }}">

				<div class="form-group {{$errors->has('subject')?'has-error':''}}">
					<label for="subject" class="control-label">{{ translate::key('front.contact.subject') }} *</label>
					<input type="text" name="subject" class="form-control" value="{{old('subject')}}" />
				</div>

				<div class="form-group {{$errors->has('message')?'has-error':''}}">
					<label for="comment" class="control-label">{{ translate::key('front.contact.message') }} *</label>
					<textarea name="comment" rows="5" class="form-control">{{old('comment')}}</textarea>
				</div>

				<div class="submit-container text-center">
					<input type="submit" class="btn btn-primary" value="{{ translate::key('front.contact.send') }}" data-sending="{{ translate::key('front.contact.sending') }}">
				</div>

			{{Form::close()}}

		</div>

	</div>
</div>


@endsection

@push('javascripts')

<script>
	$(function(){
		$('#contact-form').submit(function(){
			var input = $('#contact-form').find('input[type="submit"]');
			input.css({pointerEvents:'none', opacity:0.3}).val(input.data('sending'));

		});
	});
</script>

@endpush
