@extends('layouts.frontend')
@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('user.partials.user-nav')


<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item"><a href="{{route('user.index')}}">{{ translate::key('front.general.account') }}</a></li>
		  <li class="breadcrumb-item">{{ translate::key('front.general.history') }}</li>
		</ol>
	</div>
</div>
<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-credits">
			<div class="section-title">
				<h2>{{ translate::key('front.general.history') }}</h2>
				<hr class="decoration" />
			</div>

			<div class="body">
			    <div class="row">
			        <div class="col-md-12 bg-white form-container">
			            <table id="table-orders" class="table front-table">
			                <thead>
			                    <tr>
			                        <th>Date</th>
			                        <th>{{ translate::key('front.general.title') }}</th>
			                        <th>Description</th>
			                        <th>{{ translate::key('front.general.countries') }}</th>
			                        <th></th>
			                    </tr>
			                </thead>
			            </table>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>


@endsection


@push('stylesheets')
	{{ Html::style('/css/video-js.css')}}
@endpush

@push('javascripts')
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var routes = {
        	data : '{{ route("user.docs.data") }}',
        	deleteItem : ''
        };
    </script>
	<script src="{{ asset('/plugins/datatables/datatables.min.js') }}"></script>

	{!! Html::script('/js/users/contents/index.js') !!}
	{!! Html::script('/js/libs/videojs/videojs-ie8.min.js') !!}
	{!! Html::script('/js/libs/videojs/video.js') !!}


@endpush
