@extends('layouts.frontend')
@section('title','Hello, '.$user->firstname)

@section('content')

@include('user.partials.user-nav')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item">{{ translate::key('front.general.account') }}</li>
		</ol>
	</div>
</div>



<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-index">
			@if(Session::has('flash_message'))
				<div class="row">
				  <div class="col-md-12">
				   <ul class="flash-msg-list">
						<li class="flash-msg common">{!! session('flash_message') !!}</li>
					</ul>
				  </div>
				</div>
	      	@endif

	      	@if(count($notifications))

				<div class="row">
				  <div class="col-md-12">
				   <ul class="notifications">
				   		@foreach($notifications as $notify)
						<li class="notification">
							<div class="row">
								<div class="col-xs-1 mt-1">
									<i class="icon icon-lineart-separe7-29"></i>
								</div>
								<div class="col-xs-10">
									<h2>{{$notify->title}}</h2>
									{{$notify->message}}
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12" style="margin-top: 10px;">
									{!! Form::open(['url' => route('user.notification.read', $notify->id),'method' => 'PUT']) !!}
										<button class="read-notification" type="submit" value="">{{ translate::key('front.user.ok_read_understood') }}</button>
									{!!Form::close()!!}
								</div>
							</div>

						</li>
						@endforeach
					</ul>
				  </div>
				</div>

	      	@endif
			<div class="row">
				<div class="col col-md-6">
					<div class="frame frame-name">
						<a href="{{route('user.info')}}">
							<i class="icon icon-lineart-separe-83"></i>
							<h1 class="firstname">{{$user->firstname}}</h1>
							<h2 class="lastname">{{$user->lastname}}</h2>
							<p class="address">{{$user->address}} {{$user->postal_code}} {{$user->city}}</p>
							<p class="phone">{{ $user->email != "" ? 'E-mail. '.$user->email : '' }} {{ $user->telephone != "" ? 'Tél. '.$user->telephone : '' }}</p>
							<span class="pane-button">{{ translate::key('front.user.manage_informations') }}</span>
						</a>
					</div>
				</div>
				<div class="col col-md-3">
					<div class="frame frame-documents">

							<i class="icon icon-lineart-separe5-93"></i>
							<h3 class="docs-title">{{ translate::key('front.user.recent_documents') }}</h3>
							<ul>
								@foreach($contents as $contentView)
									<li><a href="{{route('user.content.show',['id' => $contentView->content->id])}}">{{ $contentView->content->title }}</a></li>
								@endforeach
							</ul>
							<a href="{{route('user.docs')}}" class="pane-button">{{ translate::key('front.general.see_all') }}</a>

					</div>
				</div>

			</div>
			<div class="row">

				<div class="col col-md-3">
					<div class="frame frame-contacter">
						<a href="{{route('user.contact')}}">
							<i class="icon icon-lineart-separe5-94"></i>
							<span class="pane-button">{{ translate::key('front.general.contact_us') }}</span>
						</a>
					</div>
				</div>

				@if($user->partner)
				<div class="col col-md-3">

					<div class="frame frame-referent">
						<div class="inner-referent">
							<h4>{{ translate::key('front.general.referent') }}</h4>
							<p>{{--<a href="{{ route('partner.home')}}">--}}{{$user->partner->name}}{{--</a>--}}</p>
						</div>

					</div>


				</div>
				@endif

			</div>

		</div>



	</div>
</div>
@endsection

@push('javascripts')

<script>

  var csrf_token = "{{ csrf_token() }}";

  $(function(){
      $(".flip-container.disabled").click(function(e){
          var item = $(e.target).closest('.flip-container');

          var id = item.data('zone-id');

          console.log("id : "+id);

          bootbox.confirm({
            message: "I want to acess this theme",
            buttons: {
                confirm: {
                    label: 'Send',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-secdonary'
                }
            },
            callback: function (result) {

                if(result){

                      $.ajax({
                        url : '{{route('partner.zones.petition')}}',
                        type: 'POST',
                        dataType: 'json',
                        data : {
                          zone_id : id,
                          _token : csrf_token
                        },
                        success : function(response){

                          if (response.code == 200) {

                            bootbox.alert("Request made successfully");


                          } else if (response.code == 400) {
                              //error
                              toastr.error(response.message, 'Error !', {
                                  timeOut: 5000
                              });
                          } else {
                              //nothing to change
                          }

                        },
                        error:function(msg){

                          var errors = response.responseJSON;

                          if(errors.hasOwnProperty('zone_id')){

                            toastr.error(errors.zone_id[0], 'Error !', {
                                timeOut: 3000
                            });

                          }
                          else if (errors.code == 400) {
                              //error
                              toastr.error(errors.message, 'Error !', {
                                  timeOut: 3000
                              });
                          }
                          else {

                            toastr.error('An error occurred while saving', 'Error !', {
                                timeOut: 3000
                            });

                          }
                        }
                      });

                }
            }
          });

      });
  });
</script>
@endpush
