@extends('layouts.frontend')
@section('title','Hello, '.Auth::user()->firstname)

@section('content')

@include('user.partials.user-nav')



<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item"><a href="{{route('user.index')}}">{{ translate::key('front.general.account') }}</a></li>
		  <li class="breadcrumb-item">{{ translate::key('front.user.info') }}</li>
		</ol>
	</div>
</div>
<div class="user-account">
	<div class="horizontal-inner-container">
		<div class="user-account-info">
			<div class="section-title">
				<h2>{{ translate::key('front.user.info') }}</h2>
				<hr class="decoration" />
			</div>

			@if(Session::has('flash_message'))
				<div class="row">
				  <div class="col-md-12">
				   <ul class="flash-msg-list">
						<li class="flash-msg success">{!! session('flash_message') !!}</li>
					</ul>
				  </div>
				</div>
	      	@endif

	      	@if($errors->any())
				<div class="row">
				    <div class="col-md-12">
			             <ul class="flash-msg-list">
			                {!! implode('', $errors->all('<li class="flash-msg">:message</li>')) !!}
			            </ul>
				    </div>
				</div>
			@endif


			<div class="row">
				<div class="col-md-4">
					<div class="left-frame">
						<div class="inner-data">
							<h2 class="firstname">{{$user->firstname}}</h1>
							<h1 class="lastname">{{$user->lastname}}</h2>
							<p class="address">{{$user->address}}</p>
							<p class="pcode_city">{{$user->postal_code}} {{$user->city}}</p>
							<p class="phone">Phone <span>{{$user->telephone}}</span></p>
							<p class="email">E-mail <span>{{$user->email}}</span></p>

						</div>

					</div>

				</div>
				<div class="col-md-8">
					<div class="form-frame">
						<form action="{{route('user.info.update')}}" method="POST">
							{{ csrf_field() }}
							<input name="_method" type="hidden" value="PUT">
							<div class="row">
								<div class="col-xs-12">
									<legend>{{ translate::key('front.modify.info') }}</legend>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('firstname')?'has-error':''}}">
										<label for="firstname" class="control-label">{{ translate::key('front.user.firstname') }}</label>
										<input type="text" name="firstname" class="form-control" value="{{$user->firstname}}" />

									</div>

								</div>
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('lastname')?'has-error':''}}">
										<label for="lastname" class="control-label">{{ translate::key('front.user.lastname') }}</label>
										<input type="text" name="lastname" class="form-control" value="{{$user->lastname}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group {{$errors->has('address')?'has-error':''}}">
										<label for="address" class="control-label">{{ translate::key('front.user.addresse') }}</label>
										<input type="text" name="address" class="form-control" value="{{$user->address}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('postal_code')?'has-error':''}}">
										<label for="postal_code" class="control-label">{{ translate::key('front.user.zip_code') }}</label>
										<input type="text" name="postal_code" class="form-control"  value="{{$user->postal_code}}" />
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('city')?'has-error':''}}">
										<label for="city" class="control-label">{{ translate::key('front.user.city') }}</label>
										<input type="text" name="city" class="form-control" value="{{$user->city}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('telephone')?'has-error':''}}">
										<label for="telephone" class="control-label">{{ translate::key('front.user.phone') }}</label>
										<input type="text" name="telephone" class="form-control" value="{{$user->telephone}}" />
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group {{$errors->has('email')?'has-error':''}}">
										<label for="email" class="control-label">E-mail</label>
										<input type="text" name="email" class="form-control"  value="{{$user->email}}"  />
									</div>
								</div>
							</div>
							<div class="row">
								<button type="submit">{{ translate::key('front.general.save') }}</button>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


@endsection

@push('javascripts')

@endpush
