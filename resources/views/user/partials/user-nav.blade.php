<div class="user-nav">
	<div class="horizontal-inner-container">

		<div class="open-user-menu">
			<i class="fa fa-bars"></i>
		</div>

		<ul>

			<li @if(Route::is('user.info')) class="active" @endif>
				<a href="{{route('user.info')}}" >{{ translate::key('front.user.info') }}</a>
			</li>
			<li @if(Route::is('user.docs')) class="active" @endif>
				<a href="{{route('user.docs')}}">{{ translate::key('front.user.docs') }}</a>
			</li>
			<li @if(Route::is('user.contact')) class="active" @endif>
				<a href="{{route('user.contact')}}">{{ translate::key('front.user.contact') }}</a>
			</li>
		</ul>
	</div>

</div>
