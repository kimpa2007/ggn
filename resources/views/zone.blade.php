@extends('layouts.frontend',[
	'themeIcon' => $zone->icon,
	"banner_image" => Storage::url($zone->image)
])

@section('title',$zone->translate(LaravelLocalization::getCurrentLocale())->meta_title)
@section('metaDescription',$zone->translate(LaravelLocalization::getCurrentLocale())->meta_description)

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item"><a href="{{route('zones')}}">{{ translate::key('front.general.areas') }}</a></li>
		  <li class="breadcrumb-item">{{$zone->name}}</li>
		</ol>
	</div>
</div>


<div class="horizontal-container bg-white user-theme">
	<div class="categories">
		<div class="horizontal-inner-container">
			<ul>
				@foreach($countries as $countrie)
				<li>
					@php
						$children = \App\Models\Country::get()->toArray();
						$json = htmlspecialchars(json_encode($children), ENT_QUOTES, 'UTF-8')
					@endphp
					<a href="#" class="btn btn-sm select-category {{isset($countryId) && $countryId == $countrie->id ? 'active' : ''}}" data-cat-id="{{$countrie->id}}" data-cat-sub='{{$json}}'>{{$countrie->name}}</a>
				</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="horizontal-inner-container bg-black">
	<div class="filters">
		<div class="horizontal-inner-container ">
			<ul>
				<li>
					{{ translate::key('front.general.filter') }}
				</li>

				<li>
					<input type="checkbox" id="filter-2" class="filter-type" name="filter-2" value="{{App\Models\Content::TYPE_MOVIE}}">
					<label for="filter-2">Video</label>
				</li>
				<li>
					<input type="checkbox" id="filter-3" class="filter-type" name="filter-3" value="{{App\Models\Content::TYPE_RICHTEXT}}">
					<label for="filter-3">Article</label>
				</li>
			</ul>
		</div>
	</div>

	<div class="contents-container">
		<div class="horizontal-inner-container contents-grid-container">

			<div class="contents-message" style="{{ $contentsCount == 0 ? 'display:block;' : 'display:none;' }}">
				{{ translate::key('front.general.no_contents') }}
			</div>

			<div class="row" id="contents-grid">

				@foreach($contents as $content)

					<div class="col-md-6 col-xs-12">
						<div class="content-item">
							<div class="content-item-body"  style="background-image:url('{{ Storage::url($content->image)}}')">
											<div class="item-content">
												<h2>{{ $content->title }}</h2>
											@php
												$description = strip_tags($content->description);
											@endphp

											{!!str_limit($description, $limit = 150,'...')!!}

									</div>
								</div>
							<div class="content-item-footer">
								<p>
									{{ App\Models\Content::getTypes()[$content->type] }} | {{$content->countrie->name}}
								</p>
								@if(Auth::user())
									<a href="{{route('user.content.show',['id' => $content->id])}}" class="btn btn-primary btn-sm">{{ translate::key('front.general.see') }}</a>
								@endif
							</div>
						</div>
					</div>

				@endforeach

			</div>

			<div id="count" data-count="{{$contentsCount}}"></div>

			<div class="see-more-container" style="{{ $contents->count() < $contentsCount ? 'display:block;' : 'display:none;' }}">
				<a href="#" class="btn btn-secondary-2 load-more-content">{{  translate::key('front.general.load_more_content') }}</a>
			</div>

		</div>
	</div>



</div>


@endsection

@push('javascripts')

<script>

	var itemsPerPage = {{$itemsPerPage}};
	var count = {{$contentsCount}};
	var initCountryId = {{isset($countryId) ? $countryId : 'null'}};

	var routes = {
		loadContent : "{{route('zones.show',['id'=>$zone->id])}}"
	};

</script>

{{Html::script('js/users/zones/index.js')}}

@endpush
