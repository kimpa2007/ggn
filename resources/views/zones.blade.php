@extends('layouts.frontend')

@section('title', translate::key('meta.zones.title') )
@section('metaDescription', translate::key('meta.zones.description'))

@section('content')

<div class="breadcrumbs-container">
	<div class="horizontal-inner-container">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{route('home')}}">{{ translate::key('front.general.home') }}</a></li>
		  <li class="breadcrumb-item">{{ translate::key('front.general.areas') }}</li>
		</ol>
	</div>
</div>


<div class="horizontal-container bg-white block-2">

	<div class="horizontal-inner-container">

		<div class="row theme-container">

			@php
				$counterInit = ($page-1)*$itemsPerPage;
			@endphp

			@foreach($zones as $index => $zone)

			<div class="grid-item col-md-6" >

				<div id="item-{{$index + $counterInit}}" class="flip-container" ontouchstart="this.classList.toggle('hover');" data-theme-id="{{$zone->id}}" data-countries="@foreach($zone->countries as $countrie) @if($countrie->parent_id == null) {{$countrie->name}}:{{$countrie->id}}, @endif @endforeach">
					<div class="flipper">
						<div class="front">
							<!-- front content -->
							<div class="background-image" style="background-image:url('{{ Storage::url($zone->image)}}')"></div>
							<div class="background-hover"></div>
							<div class="grid-item-content">
								@php
									//remove all html tags from description
									$name = strip_tags($zone->name);
								@endphp
								<h3>
									<i class="fa {{$zone->icon}}"></i>
									{{ str_limit($name, $limit = 33,'...')}}
								</h3>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="background-image" style="background-image:url('{{ Storage::url($zone->image)}}')"></div>
							<div class="background-hover-turn"></div>
							<div class="grid-item-content-turn">
								<h4>{{ $zone->name}}</h4>
								<p>

									@php
										//remove all html tags from description
										$description = strip_tags($zone->description);
									@endphp
									{{ str_limit($description, $limit = 150,'...')}}
								</p>
								@if(Auth::user())
									<div class="button-container">
										<a href="{{route('zones.show',['id'=>$zone->id])}}" class="btn btn-primary">See area</a>
									</div>
								@endif

							</div>
						</div>
					</div>
				</div>

			</div>

			@endforeach

		</div>

		@if($zones_count > $itemsPerPage)
		<div class="see-more-container">
			<a href="#" class="btn btn-secondary  see-more-themes">{{ translate::key('front.general.see_more_areas') }}</a>
		</div>
		@endif

		<div class="seprator" style="height:20px;"></div>


	</div>
</div>


@endsection

@push('javascripts')

<script>

	var itemsPerPage = {{$itemsPerPage}};
	var count = {{$zones_count}};
	var page = 1;
	var availablePages = Math.ceil(count / itemsPerPage);

	//this will change with responsive, to know how many items on every line, and at the block at to the end
	var itemsPerLine = 4;
	var lastRowIndex = Math.ceil(count / itemsPerLine);
	var lastItemId = count - 1;

	var isTablet = false;

	var routes = {
		loadContent : "{{route('zones')}}"
	};

	var loggedIn = {{Auth::user() ? 'true' : 'false'}};

	$(function(){

		$(window).resize(function(e){
			if($(window).width() < 1090){
				//is tablet
				isTablet = true;
				$(".grid-item-categories").remove();
			}
			else {
				isTablet = false;
			}
		});

		$(".see-more-themes").click(function(e){
			e.preventDefault();

			if(page >= availablePages)
	      return;

			page ++;

			$.ajax({
					url: routes.loadContent,
					type: "GET",
					data: {
						page : page
					},
						success: function(response){

							$(".theme-container").append($(response).find('.theme-container').html());

							if(parseInt(page) >= parseInt(availablePages)){
								$(".see-more-container").fadeOut();
							}
						},
						error:function(msg){}
				});

		});

		$(document).on('click',".flip-container",function(e){
			//e.preventDefault();

			if(!loggedIn)
				return;

			var gridItem = $(e.target).closest('.flip-container');
			console.log(gridItem);

			var itemId = gridItem.attr('id').split("-");
			itemId = itemId[1];
			var rowIndex = Math.ceil((parseInt(itemId)+1) / itemsPerLine);

			var itemIdToAppend = (rowIndex*itemsPerLine) - 1;

			if(isTablet){
				//same element
				itemIdToAppend = itemId;
			}
			else if(rowIndex == lastRowIndex){
				//put to the last element of the row
				itemIdToAppend = lastItemId;
			}

			console.log("item to append :"+itemIdToAppend);

			//the div must the added to itemIdToAppend id

			if(gridItem.hasClass("disabled")){
				return;
			}

			$(".flip-container").removeClass("selected");
			gridItem.addClass("selected");

			var timeToWait = 0;
			if($(".grid-item-categories").hasClass('open')){
				timeToWait = 650;
				$(".grid-item-categories").removeClass('open');

				//$('.grid-item-categories').slideUp();
			}

			setTimeout(function() {

				$('.grid-item-categories').remove();

				var container = gridItem.data('cat-container');
				var themeId = gridItem.data('theme-id');
				var categoriesList = gridItem.data('countries');

				var countries = categoriesList.split(',');

			//	var route = "{{route('zones.show',['id'=>':id','countryId' => ':cat-id'])}}";
			//	route = route.replace(':id',themeId);
				var country = "{{route('country',['countryId' => ':cat-id'])}}";

				$('<div class="grid-item-categories container-'+itemIdToAppend+'"></div>').insertAfter($('#item-'+itemIdToAppend).parent());

				for(var i=0;i<countries.length-1;i++){
					var countrie = countries[i].split(':');
					$('.container-'+itemIdToAppend).append('<a href="'+country.replace(':cat-id',countrie[1])+'">'+countrie[0]+'</a>');
				}

				setTimeout(function() {
					$('.grid-item-categories').addClass('open');
					//$('.grid-item-categories').slideDown();
				},100);

			}, timeToWait);

		});
	});
</script>

@endpush
