<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| AUTH ROUTES
|--------------------------------------------------------------------------
*/
Auth::routes();
Route::get('logout', ['as' => 'logout', 'uses' => '\App\Http\Controllers\Auth\LoginController@logout']);
Route::get('/autologin', ['as' => 'autologin', 'uses' => 'Front\FrontController@autologin']);

Route::group(['prefix' => LaravelLocalization::setLocale() ], function () {
    Route::get('language', function ()    {
        dd("The language is: " . LaravelLocalization::getCurrentLocale());
    });
});

/*
|--------------------------------------------------------------------------
| FRONT END ROUTES
|--------------------------------------------------------------------------
*/


Route::get('/cmd-validation', ['as' => 'offer.check', 'uses' => 'Offers\OfferController@check']); 
Route::post('/cmd-validation', ['as' => 'offer.check', 'uses' => 'Offers\OfferController@check']); 
Route::get('/cmd-ok', ['as' => 'offer.check', 'uses' => 'Offers\OfferController@ok']); 
Route::get('/cmd-ko', ['as' => 'offer.check', 'uses' => 'Offers\OfferController@ko']); 
Route::get('/rss.xml', ['as' => 'content.rss', 'uses' => 'Front\ContentController@rss']); 

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    //'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    'middleware' => [ 'localeViewPath' ],
],
function () {
    // HomeController
    Route::get('/maintenance', ['as' => 'maintenance', 'uses' => 'Front\FrontController@maintenance']);

    Route::get('/', ['as' => 'home', 'uses' => 'Front\FrontController@index']);
    Route::get('/login', ['as' => 'user.login', 'uses' => 'Front\FrontController@login']);

    // PageController (custom page)
    Route::get('/faq', ['as' => 'faq', 'uses' => 'Front\FrontController@faq']);

    // ContactController
    Route::get('/contact', ['as' => 'contact', 'uses' => 'Front\FrontController@contact']);
    Route::post('/contact/send', ['as' => 'contact.send', 'uses' => 'Front\FrontController@sendContact']);

    // ZoneController
    Route::get('/zones', ['as' => 'zones', 'uses' => 'Front\ZoneController@index']);
    Route::get('/zones/{id}/{countryId?}', ['as' => 'zones.show', 'uses' => 'Front\ZoneController@show']);

    // CountryController
    Route::get('/country/{id}', ['as' => 'country', 'uses' => 'Front\CountryController@index']);
    Route::get('/country/{id}/contributors', ['as' => 'country.contributors', 'uses' => 'Front\CountryController@contributors']);
    Route::get('/country/{id}/{theme_slug}', ['as' => 'country.theme', 'uses' => 'Front\CountryController@index']);

    // ContributorController
    Route::get('/contributors/zone/{id}', ['as' => 'contributors.zone', 'uses' => 'Front\ContributorController@zone']);
    Route::get('/contributor/{id}', ['as' => 'contributor', 'uses' => 'Front\ContributorController@show']);
    Route::get('/contributors', ['as' => 'contributors', 'uses' => 'Front\ContributorController@index']);

    // ThemeController
    Route::get('/theme/{slug}', ['as' => 'theme', 'uses' => 'Front\ThemeController@show']);

    // ContentController
    Route::get('/content-free/{id}', ['as' => 'content-free', 'uses' => 'Front\ContentController@showFree']);
    Route::get('/content-preview/{id}', ['as' => 'content-preview', 'uses' => 'Front\ContentController@showPreview'])->middleware('role:admin|super-admin');
    Route::get('/for-more-content', ['as' => 'for-more-content', 'uses' => 'Front\ContentController@forMoreContent']);
  //  Route::get('/content/{id}', ['as' => 'user.content.show', 'uses' => 'Front\ContentController@show'])->middleware('role:expert|user|partner|admin|super-admin');
     Route::get('/content/{id}', ['as' => 'user.content.show', 'uses' => 'Front\ContentController@show']);

    // SearchController
    Route::get('/search', ['as' => 'search', 'uses' => 'Front\SearchController@index']);

    //CustomPageController
    Route::get('{slug}.html', ['as' => 'custompage', 'uses' => 'Front\CustomPageController@show']);

    // MapController
    Route::get('/map', ['as' => 'map.show', 'uses' => 'Front\MapController@show']); //a modifier
    Route::get('/map/data', ['as' => 'map.data', 'uses' => 'Front\MapController@getData']);
    Route::get('/create', 'User\UserController@create');
    Route::post('/create', ['uses' => 'User\UserController@create']);

    Route::get('/abonnements', ['as' => 'offer.show', 'uses' => 'Offers\OfferController@show', 'middleware' => ['role:user|partner|super-admin'] ]); 
    Route::get('/upd-theme', ['uses' => 'ImportController@importTheme']); 


    Route::post('/offer-create', ['as' => 'offer.createCommande', 'uses' => 'Offers\OfferController@createCommand']); 

});


/*
|--------------------------------------------------------------------------
| FILE UPLOAD
|--------------------------------------------------------------------------
*/
Route::post('/file/upload', ['as' => 'upload-post', 'uses' =>'FileUploadController@postUpload']);


/*
|--------------------------------------------------------------------------
| USER
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'user', 'as' => 'user', 'middleware' => ['role:user|partner']], function () {
    Route::group(['middleware' => ['role:user']], function () {
        Route::get('/dashboard', ['as' => '.home' , 'uses' => 'User\AccountController@index']);
    });

    //User account routes
    Route::group(['prefix'=>'account', 'middleware' => ['role:user']], function () {


        Route::get('/', ['as' => '.index', 'uses' => 'User\AccountController@index']);
        Route::get('/info', ['as' => '.info', 'uses' => 'User\AccountController@info']);
        Route::put('/info/update', ['as' => '.info.update', 'uses' => 'User\AccountController@updateSelfInfo']);

        Route::get('/credits', ['as' => '.credits', 'uses' => 'User\CreditsController@show']);
        Route::get('/credits/operations', ['as' => '.credits.operations', 'uses' => 'User\CreditsController@getCreditOperationData']);

        Route::get('/orders', [ 'as' => '.orders', 'uses' =>  'User\OrderController@index']);
        Route::get('/orders/data', [ 'as' => '.orders.data', 'uses' => 'User\OrderController@getData']);
        Route::get('/orders/{order?}/delete', ['as' => '.orders.delete', 'uses' => 'User\OrderController@delete']);

        Route::get('/basket/remove', ['as' => '.panier.remove', 'uses' => 'User\BasketController@removeOrderFromBasket']);
        Route::get('/basket/payment', ['as' => '.panier.payment', 'uses' => 'User\BasketController@payment']);
        Route::post('/basket/success', ['as' => '.panier.success', 'uses' => 'User\BasketController@successCredits']);
        Route::get('/basket/{order?}', ['as' => '.panier', 'uses' => 'User\BasketController@index']);

        Route::get('/docs', ['as' => '.docs', 'uses' => 'User\ContentViewedController@index']);
        Route::get('/docs/data', ['as' => '.docs.data', 'uses' => 'User\ContentViewedController@getData']);

        Route::get('/contact', ['as' => '.contact', 'uses' => 'User\ContactController@index']);
        Route::post('/contact/send', ['as' => '.contact.send', 'uses' => 'User\ContactController@sendContact']);
    });

    Route::put('/readnotification/{id}', ['as' => '.notification.read', 'uses' => 'Partner\UserNotificationsController@readNotification']);
});

/*
|--------------------------------------------------------------------------
| PARTNER
|--------------------------------------------------------------------------
*/
Route::group(
[
    'prefix' => LaravelLocalization::setLocale()."/partner",
    //'prefix'=>'partner',
    'as' => 'partner',
    'middleware' => ['role:user|partner'],
    'namespace'=>'Partner'
],
function () {

    //Dashboard
    Route::get('/dashboard', [  'as' => '.home' ,'uses' => 'HomeController@index']);

    //ZONES
    Route::get('zones', ['as' => '.zones', 'uses' => 'ZonesController@index']);
    Route::post('zones', ['as' => '.zones.petition', 'uses' => 'ZonesController@sendThemePetition']);

    //CONTACT
    Route::get('contact', ['as' => '.contact', 'uses' => 'ContactController@index']);
    Route::post('contact', ['as' => '.contact.submit', 'uses' => 'ContactController@submit']);

    //Users/Employees management
    Route::group(['prefix'=>'users','as' => '.users'], function () {
        Route::get('/', ['as' => '.index', 'uses' => 'UserController@index']);
        Route::get('/getdata', ['as' => '.getdata', 'uses' => 'UserController@getData']);

        Route::get('/create', ['as' => '.add', 'uses' => 'UserController@create','middleware' => ['permission:manage-users']]);
        Route::post('/save', ['as' => '.save', 'uses' => 'UserController@save','middleware' => ['permission:manage-users']]);
        Route::get('/{id}/show', ['as' => '.show', 'uses' => 'UserController@show','middleware' => ['permission:manage-users']]);


        Route::post('/{user?}/update', ['as' => '.update', 'uses' => 'UserController@update','middleware' => ['permission:manage-users']]);


        Route::get('/suivi', ['as' => '.suivi', 'uses' => 'UserController@suivi','middleware' => ['permission:manage-users']]);

        //Users notifications
        Route::group(['prefix'=>'notify','as' => '.notify','middleware' => ['permission:send-notes']], function () {
            Route::get('/', ['as' => '.index', 'uses' => 'UserNotificationsController@index']);
            Route::get('data', ['as' => '.data', 'uses' => 'UserNotificationsController@getData']);
            Route::get('readunreaddata/{id?}', ['as' => '.readunreaddata', 'uses' => 'UserNotificationsController@getNotificationReadsData']);
            Route::get('draft/{id?}', ['as' => '.draft', 'uses' => 'UserNotificationsController@getDraft']);
            Route::put('draft/{id?}', ['as' => '.update', 'uses' => 'UserNotificationsController@updateDraft']);
            Route::post('send', ['as' => '.send', 'uses' => 'UserNotificationsController@saveNotification']);
        });
    });

    Route::group(['prefix'=>'orders','as' => '.orders'], function () {
        Route::get('/', ['as' => '', 'uses' => 'OrdersController@index']);
        Route::get('/{id}/cancel', ['as' => '.cancel', 'uses' => 'OrdersController@cancel']);
        Route::get('/{id}/pay', ['as' => '.pay', 'uses' => 'OrdersController@pay']);
        Route::get('/data', ['as' => '.data', 'uses' => 'OrdersController@data']);
    });

/*
    Route::group(['prefix'=>'credits','as' => '.credits'], function () {

        Route::get('/', ['as' => '', 'uses' => 'CreditsController@index','middleware' => ['permission:manage-credits']]);
        Route::get('/history/data', ['as' => '.historydata', 'uses' => 'CreditsController@getSubventionsHistoryData','middleware' => ['permission:manage-credits']]);
        Route::get('/distribution', ['as' => '.distribution', 'uses' => 'CreditsController@distribution','middleware' => ['permission:manage-credits']]);
        Route::get('/distribution/data', ['as' => '.distribution.data', 'uses' => 'CreditsController@getDistributionData','middleware' => ['permission:manage-credits']]);
        Route::post('/distribution/save', ['as' => '.distribution.save', 'uses' => 'CreditsController@saveDistribution','middleware' => ['permission:manage-credits']]);
        Route::post('/distribution/same', ['as' => '.distribution.same', 'uses' => 'CreditsController@saveSameCredits','middleware' => ['permission:manage-credits']]);


        Route::get('/order', ['as' => '.order', 'uses' => 'CreditsController@order','middleware' => ['permission:manage-credits']]);
        Route::post('/order/save', ['as' => '.order.save', 'uses' => 'CreditsController@processOrder','middleware' => ['permission:manage-credits']]);
        Route::get('/order/success', ['as' => '.order.success', 'uses' => 'CreditsController@orderSuccess','middleware' => ['permission:manage-credits']]);

        Route::get('/voucher', ['as' => '.voucher', 'uses' => 'Credit\VoucherController@index','middleware' => ['permission:manage-credits']]);
        Route::post('/voucher/save', ['as' => '.voucher.save', 'uses' => 'Credit\VoucherController@process','middleware' => ['permission:manage-credits']]);
        Route::get('/voucher/success', ['as' => '.voucher.success', 'uses' => 'Credit\VoucherController@success','middleware' => ['permission:manage-credits']]);

    });*/

    Route::group(['middleware' => ['permission:buy-content']], function () {
        //PLANS
        Route::get('plans', ['as' => '.plans', 'uses' => 'PlansController@index']);
        Route::get('plansdata', ['as' => '.plansdata', 'uses' => 'PlansController@getData']);

        Route::get('order/{id}/confirm', ['as' => '.plans.order.confirm', 'uses' => 'PlansController@orderConfirm']);
        Route::post('order/{id}/confirm/save', ['as' => '.plans.order.confirm.save', 'uses' => 'PlansController@orderSave']);
        Route::get('order/{id}/cancel', ['as' => '.plans.order.cancel', 'uses' => 'PlansController@orderCancel']);

        Route::get('purpose/{id}/confirm', ['as' => '.plans.purpose.confirm', 'uses' => 'PlansController@purposeConfirm']);
        Route::get('purpose/{id}/refuse', ['as' => '.plans.purpose.refuse', 'uses' => 'PlansController@purposeRefuse']);
        Route::post('purpose/{id}/confirm/save', ['as' => '.plans.purpose.confirm.save', 'uses' => 'PlansController@purposeSave']);
    });

    Route::group(['middleware' => ['role:partner']], function () {


      //Partner account
      Route::group(['prefix'=>'account','as' => '.account','middleware' => ['permission:manage-users']], function () {
          Route::get('/', ['as' => '.index', 'uses' => 'AccountController@index']);
          Route::put('/update', ['as' => '.update', 'uses' => 'AccountController@update']);
      });
    });
});

/*
|--------------------------------------------------------------------------
| EXPERT
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'expert', 'as' => 'expert',  'middleware' => ['role:expert']], function () {
    Route::get('/', 'Expert\HomeController@index');
    Route::get('/dashboard', 'Expert\HomeController@index');

    // Account
    Route::get('/account', ['as' => '.account', 'uses' => 'Expert\AccountController@index']);
    //Route::post('/account/upload', ['as' => 'upload-post', 'uses' =>'Expert\AccountController@postUpload']);
    Route::post('/account/upload/delete', ['as' => 'upload-remove', 'uses' =>'Expert\AccountController@deleteUpload']);
    Route::post('/account/save', 'Expert\AccountController@save');

    // Profile
    Route::get('/profile', 'Expert\ProfileController@index');
    Route::post('/profile/save', 'Expert\ProfileController@save');
    //Route::post('/profile/upload', ['as' => 'upload-post', 'uses' =>'Expert\ProfileController@postUpload']);


    // Content
    Route::get('/contents', 'Expert\ContentController@index');
    Route::get('/contents/data', 'Expert\ContentController@getData');
    Route::post('/contents/save', 'Expert\ContentController@save');
    Route::get('/contents/create', 'Expert\ContentController@create');
    Route::post('/contents/change-status', 'Expert\ContentController@changeStatus');
    Route::post('/contents/update-order', 'Expert\ContentController@updateOrder');
    Route::get('/contents/{id}', 'Expert\ContentController@show')->name('.contents.show');
    //Route::post('/contents/upload', ['as' => 'upload-post', 'uses' =>'Expert\ContentController@postUpload']);
});


/*
|--------------------------------------------------------------------------
| ADMIN
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin', 'as' => 'admin', 'middleware' => ['role:admin|super-admin']], function () {
    Route::get('/', 'Admin\HomeController@index');
    Route::get('/dashboard', 'Admin\HomeController@index');
    // Account
    Route::get('/account', ['as' => '.account', 'uses' => 'Admin\AccountController@index' ]);
    Route::post('/theme/position/update', ['as' => 'theme', 'uses' => 'Admin\Theme\ThemeController@updatePosition']);

    // Users
    Route::get('/users', ['as' => '.users.index', 'uses' => 'Admin\User\AccountController@index']);
    Route::get('/users/data', ['as' => '.users.data', 'uses' => 'Admin\User\AccountController@getData']);

   //Route::get('/users/admins', ['as' => '.users.admins', 'uses' => 'Admin\User\AccountController@admins']);
    Route::get('/users/create', ['as' => '.users.create', 'uses' => 'Admin\User\AccountController@create']);
    Route::post('/users/create/save', ['as' => '.users.save', 'uses' => 'Admin\User\AccountController@save']);

    Route::post('/users/{user?}/update', ['as' => '.users.update', 'uses' => 'Admin\User\AccountController@update']);
    Route::delete('/users/{user?}/delete', ['as' => '.users.delete', 'uses' => 'Admin\User\AccountController@delete']);

    Route::get('/users/{user?}/account', ['as' => '.users.account', 'uses' => 'Admin\User\AccountController@show']);
    //Route::get('/users/{user?}/credits', ['as' => '.users.credits', 'uses' => 'Admin\User\CreditController@show']);
    //Route::get('/users/{user?}/credits/operations', ['as' => '.users.credits.operations', 'uses' => 'Admin\User\CreditController@getCreditOperationData']);
    //Route::post('/users/{user?}/credit/save', ['as' => '.users.credits.save', 'uses' => 'Admin\User\CreditController@save']);
   /* Route::get('/users/{user?}/orders', ['as' => '.users.orders', 'uses' => 'Admin\User\OrderController@show']);
    Route::get('/users/{user?}/orders/data', ['as' => '.users.orders.data', 'uses' => 'Admin\User\OrderController@getData']);
    Route::get('/users/{user?}/orders/{order?}/delete', ['as' => '.users.orders.delete', 'uses' => 'Admin\User\OrderController@delete']);*/
  /*  Route::get('/users/{user?}/threads', ['as' => '.users.threads', 'uses' => 'Admin\User\ThreadController@index']);
    Route::get('/users/{user?}/threads/data', ['as' => '.users.threads.data', 'uses' => 'Admin\User\ThreadController@getData']);
    Route::get('/users/{user?}/threads/{thread?}/delete', ['as' => '.users.threads.delete', 'uses' => 'Admin\User\ThreadController@delete']);*/

    // Users pending
  //  Route::get('/users/pending', 'Admin\User\PendingAccountController@index');
  //  Route::get('/users/pending/data', 'Admin\User\PendingAccountController@getData');
  //  Route::post('/users/pending/status/active', 'Admin\User\PendingAccountController@setActive');
  //  Route::post('/users/pending/status/refused', 'Admin\User\PendingAccountController@setRefused');
  //  Route::post('/users/pending/partner/update', 'Admin\User\PendingAccountController@setPartner');


     // Admins
    Route::get('/admins', 'Admin\Admins\AccountController@index');
    Route::get('/admins/create', 'Admin\Admins\AccountController@create');
    Route::get('/admins/data', 'Admin\Admins\AccountController@getData');
    Route::get('/admins/{id}/account', 'Admin\Admins\AccountController@show');
    Route::post('/admins/delete', 'Admin\Admins\AccountController@delete');
    Route::post('/admins/account/save', 'Admin\Admins\AccountController@save');

    // Experts
    Route::get('/experts', 'Admin\Expert\AccountController@index');
    Route::get('/experts/create', 'Admin\Expert\AccountController@create');
    Route::get('/experts/data', 'Admin\Expert\AccountController@getData');
    Route::get('/experts/{id}/account', 'Admin\Expert\AccountController@show');
    Route::post('/experts/delete', 'Admin\Expert\AccountController@delete');
    Route::post('/experts/account/save', 'Admin\Expert\AccountController@save');
    Route::post('/experts/update-order', 'Admin\Expert\AccountController@updateOrder');

    Route::get('/experts/{id}/profile', 'Admin\Expert\ProfileController@index');
    Route::post('/experts/profile/save', 'Admin\Expert\ProfileController@save');



    // Partners
    Route::get('/partners', 'Admin\Partner\PartnerController@index');
    Route::get('/partners/data', 'Admin\Partner\PartnerController@getData');
    Route::get('/partners/create', 'Admin\Partner\PartnerController@newPartner');
    Route::post('/partners/create/save', 'Admin\Partner\PartnerController@create');
    Route::post('/partners/{id}/delete', 'Admin\Partner\PartnerController@delete');

    // Partners users
    Route::get('/partners/{id}/users', 'Admin\Partner\UserController@index');
    Route::get('/partners/{id}/users/data', 'Admin\Partner\UserController@getData');

    // Partners account
    Route::get('/partners/{id}/account', 'Admin\Partner\PartnerController@show');
    Route::post('/partners/{id}/account/save', 'Admin\Partner\PartnerController@update');

    // Partners options
    Route::get('/partners/{id}/options', 'Admin\Partner\OptionController@show');
    Route::post('/partners/{id}/options/save', 'Admin\Partner\OptionController@save');
    Route::post('/partners/{id}/options/upload/delete', 'Admin\Partner\OptionController@deleteUpload');
    Route::post('/partners/{id}/options/upload', 'Admin\Partner\OptionController@postUpload');

    // Partners credits
    Route::get('/partners/{id}/credits', 'Admin\Partner\CreditController@show');
    Route::post('/partners/{id}/credits/save', 'Admin\Partner\CreditController@save');
    Route::get('/partners/{id}/credits/operations', 'Admin\Partner\CreditController@getCreditOperationData');

    // Partners plans
    Route::get('/partners/{id}/plans', 'Admin\Partner\PlanController@show');
    Route::get('/partners/{id}/plans/data', 'Admin\Partner\PlanController@getData');
    Route::post('/partners/{id}/plans/create', 'Admin\Partner\PlanController@create')->name('.partner.plan.create');

    // Partners rules
    Route::get('/partners/{id}/rules', ['as' => '.partner.rules', 'uses' => 'Admin\Partner\RuleController@show']);
    Route::post('/partners/{id}/rules/save', ['as' => '.partner.rules.save', 'uses' => 'Admin\Partner\RuleController@save']);

    // Partners orders
    Route::get('/partners/{id}/orders', ['as' => '.partner.orders', 'uses' => 'Admin\Partner\OrderController@show']);
    Route::get('/partners/{id}/orders/data/plans', ['as' => '.partner.orders.data.plans', 'uses' => 'Admin\Partner\OrderController@getPlansData']);
    Route::get('/partners/{id}/orders/data/credits', ['as' => '.partner.orders.data.credits', 'uses' => 'Admin\Partner\OrderController@getCreditsData']);

    // Partners notifications
    Route::get('/partners/{id}/notifications', ['as' => '.partner.notifications', 'uses' => 'Admin\Partner\NotificationController@index']);
    Route::get('/partners/{id}/notifications/create', ['as' => '.partner.notifications.create', 'uses' => 'Admin\Partner\NotificationController@create']);
    Route::post('/partners/{id}/notifications/save', ['as' => '.partner.notifications.save', 'uses' => 'Admin\Partner\NotificationController@save']);
    Route::get('/partners/{id}/notifications/{notification?}/show', ['as' => '.partner.notifications.show', 'uses' => 'Admin\Partner\NotificationController@show']);
    Route::post('/partners/{id}/notifications/{notification?}/save', ['as' => '.partner.notifications.update', 'uses' => 'Admin\Partner\NotificationController@update']);
    Route::get('/partners/{id}/notifications/data', ['as' => '.partner.notifications.data', 'uses' => 'Admin\Partner\NotificationController@getData']);

    // Zones
    Route::get('/zones', 'Admin\Zone\ZoneController@index');
    Route::get('/zones/data', 'Admin\Zone\ZoneController@getData');
    Route::get('/zones/create', ['as' => '.zone.create', 'uses' => 'Admin\Zone\ZoneController@create']);
    Route::post('/zones/save', 'Admin\Zone\ZoneController@save');
    Route::post('/zones/delete', 'Admin\Zone\ZoneController@delete');
    Route::post('/zones/update-order', 'Admin\Zone\ZoneController@updateOrder');
    Route::get('/zones/{id}/show', ['as' => '.zone.show', 'uses' => 'Admin\Zone\ZoneController@show']);
    Route::get('/zones/{id}/experts', 'Admin\Zone\ExpertController@index');
    Route::get('/zones/{id}/stats', 'Admin\Zone\StatsController@index');
    Route::get('/zones/{id}/experts/data', 'Admin\Zone\ExpertController@getData');



    // THEMES
    Route::get('/themes', 'Admin\Theme\ThemeController@index');
    Route::get('/themes/data', 'Admin\Theme\ThemeController@getData');
    Route::get('/themes/create', ['as' => '.theme.create', 'uses' => 'Admin\Theme\ThemeController@create']);
    Route::post('/themes/delete', 'Admin\Theme\ThemeController@delete');

    Route::post('/themes/save', 'Admin\Theme\ThemeController@save');
    Route::get('/themes/{id}/show', ['as' => '.theme.show', 'uses' => 'Admin\Theme\ThemeController@show']);


    //Categories
    Route::get('/countries', 'Admin\Country\CountryController@index');
    Route::get('/countries/data', 'Admin\Country\CountryController@getData');
    Route::get('/countries/create', 'Admin\Country\CountryController@create');
    Route::post('/countries/save', 'Admin\Country\CountryController@save');
    Route::post('/countries/delete', 'Admin\Country\CountryController@delete');
    Route::post('/countries/update-order', 'Admin\Country\CountryController@updateOrder');
    Route::get('/countries/{id}/show', 'Admin\Country\CountryController@show');
    Route::get('/countries/{id}/contents', 'Admin\Country\CountryController@contents');
    Route::get('/countries/{id}/contents/data', 'Admin\Country\CountryController@getContentsData');
    Route::post('/countries/zone/', 'Admin\Country\CountryController@getCategoriesByTheme');

    // Content
    Route::get('/contents', 'Admin\Content\ContentController@index');
    Route::get('/contents/data', 'Admin\Content\ContentController@getData');
    Route::post('/contents/save', 'Admin\Content\ContentController@save');
    Route::post('/contents/delete', 'Admin\Content\ContentController@delete');
    Route::get('/contents/create', 'Admin\Content\ContentController@create')->name('.content.create');
    Route::post('/contents/change-status', 'Admin\Content\ContentController@changeStatus');
    Route::get('/contents/latestnews', 'Admin\Content\ContentController@indexLatestnews');
    Route::get('/contents/latestnews/data', 'Admin\Content\ContentController@getDateLatestnews');
    Route::post('/contents/latestnews/update-order', 'Admin\Content\ContentController@updateOrderLatestnews');
    Route::get('/contents/update-order', 'Admin\Content\ContentController@updateOrder');
    Route::get('/contents/{id}', 'Admin\Content\ContentController@show')->name('.content.show');

    // Map Legend
    Route::get('/map-legend', 'Admin\MapLegend\MapLegendController@index');
    Route::get('/map-legend/data', 'Admin\MapLegend\MapLegendController@getData');
    Route::post('/map-legend/store', 'Admin\MapLegend\MapLegendController@store');
    Route::get('/map-legend/create', 'Admin\MapLegend\MapLegendController@create');
    Route::delete('/map-legend/{mapLegend?}/delete', 'Admin\MapLegend\MapLegendController@delete');
    Route::get('/map-legend/{mapLegend?}', 'Admin\MapLegend\MapLegendController@show');
    Route::put('/map-legend/{mapLegend?}/update', 'Admin\MapLegend\MapLegendController@update');

    //Partners
    Route::post('/partners/{id}/options/upload/delete', 'Admin\Partner\OptionController@deleteUpload');
    Route::post('/partners/{id}/options/upload', 'Admin\Partner\OptionController@postUpload');
    Route::get('/partners/{id}/credits', 'Admin\Partner\CreditController@show');
    Route::post('/partners/{id}/credits/save', 'Admin\Partner\CreditController@save');
    Route::get('/partners/{id}/credits/operations', 'Admin\Partner\CreditController@getCreditOperationData');
    Route::get('/partners/{id}/plans', 'Admin\Partner\PlanController@show');
    Route::get('/partners/{id}/plans/data', 'Admin\Partner\PlanController@getData');

    // Plans
    Route::get('/plans', 'Admin\Plan\PartnerPlanController@index');
    Route::get('/plans/data', ['as' => '.plan.data', 'uses' => 'Admin\Plan\PartnerPlanController@getData']);
    Route::get('/plans/create', ['as' => '.plan.form', 'uses' => 'Admin\Plan\PartnerPlanController@create']);
    Route::post('/plans/save', ['as' => '.plan.save', 'uses' => 'Admin\Plan\PartnerPlanController@save']);
    Route::post('/plans/delete', ['as' => '.plan.delete', 'uses' => 'Admin\Plan\PartnerPlanController@delete']);
    Route::get('/plans/{plan?}', ['as' => '.plan', 'uses' => 'Admin\Plan\PartnerPlanController@show']);
    Route::post('/plans/{plan?}/update', ['as' => '.plan.update', 'uses' => 'Admin\Plan\PartnerPlanController@update']);


    // Order plans
    Route::get('/orders/plans', ['as' => '.orders.plan.index', 'uses' => 'Admin\Order\OrderPlanController@index']);
    Route::get('/orders/plans/data', ['as' => '.orders.plan.data', 'uses' => 'Admin\Order\OrderPlanController@getData']);
    Route::get('/orders/plans/{order?}', ['as' => '.orders.plan.show', 'uses' => 'Admin\Order\OrderPlanController@show']);
    Route::post('/orders/plans/{order?}/save', ['as' => '.orders.plan.save', 'uses' => 'Admin\Order\OrderPlanController@save']);
    Route::get('/orders/plans/{order?}/delete', ['as' => '.orders.plan.delete', 'uses' => 'Admin\Order\OrderPlanController@delete']);

    // Order credits
    Route::get('/orders/credits', ['as' => '.orders.credit.index', 'uses' => 'Admin\Order\OrderCreditController@index']);
    Route::get('/orders/credits/data', ['as' => '.orders.credit.data', 'uses' => 'Admin\Order\OrderCreditController@getData']);
    Route::get('/orders/credits/{order?}', ['as' => '.orders.credit.show', 'uses' => 'Admin\Order\OrderCreditController@show']);
    Route::post('/orders/credits/{order?}/save', ['as' => '.orders.credit.save', 'uses' => 'Admin\Order\OrderCreditController@save']);
    Route::get('/orders/credits/{order?}/delete', ['as' => '.orders.credit.delete', 'uses' => 'Admin\Order\OrderCreditController@delete']);

    // Order zones
    Route::get('/orders/zones', ['as' => '.orders.zones', 'uses' => 'Admin\Order\OrderThemeController@index']);
    Route::get('/orders/zones/data', ['as' => '.orders.zones.data', 'uses' => 'Admin\Order\OrderThemeController@getData']);
    Route::get('/orders/zones/{order}/accept', ['as' => '.orders.zones.accept', 'uses' => 'Admin\Order\OrderThemeController@accept']);
    Route::get('/orders/zones/{order}/delete', ['as' => '.orders.zones.delete', 'uses' => 'Admin\Order\OrderThemeController@delete']);

    // Config emails
    Route::get('/parameters/emails', ['as' => '.parameters.emails.index', 'uses' => 'Admin\Parameter\EmailTemplateController@index']);
    Route::get('/parameters/emails/new', ['as' => '.parameters.emails.new', 'uses' => 'Admin\Parameter\EmailTemplateController@newEmailTemplate']);
    Route::post('/parameters/emails/save', ['as' => '.parameters.emails.create', 'uses' => 'Admin\Parameter\EmailTemplateController@create']);
    Route::get('/parameters/emails/data', ['as' => '.parameters.emails.data', 'uses' => 'Admin\Parameter\EmailTemplateController@getData']);
    Route::get('/parameters/emails/{template?}', ['as' => '.parameters.emails.show', 'uses' => 'Admin\Parameter\EmailTemplateController@show']);
    Route::put('/parameters/emails/{template?}/save', ['as' => '.parameters.emails.update', 'uses' => 'Admin\Parameter\EmailTemplateController@update']);
    Route::get('/parameters/emails/{template?}/delete', ['as' => '.parameters.emails.delete', 'uses' => 'Admin\Parameter\EmailTemplateController@delete']);

    // Custom pages
    //Route::resource('custom', 'Admin\CustomPage\CustomPagesController', ['except' => ['show']]);
    Route::get('/custompage/data', 'Admin\CustomPage\CustomPagesController@getData')->name('.custompage.data');
    Route::get('/custompage', 'Admin\CustomPage\CustomPagesController@index')->name('.custompage.index');
    Route::get('/custompage/create', 'Admin\CustomPage\CustomPagesController@create')->name('.custompage.create');
    Route::post('/custompage/store', 'Admin\CustomPage\CustomPagesController@store')->name('.custompage.store');
    Route::get('/custompage/{id}', 'Admin\CustomPage\CustomPagesController@show')->name('.custompage.show');
    Route::put('/custompage/{id}/update', 'Admin\CustomPage\CustomPagesController@update')->name('.custompage.update');
    Route::delete('/custompage/{id}/delete', 'Admin\CustomPage\CustomPagesController@delete')->name('.custompage.delete');


    //Translate Tools
    Route::get('/translate', 'Admin\Translate\TranslateToolsController@index')->name('.translate.index');
    Route::get('/translate/create', 'Admin\Translate\TranslateToolsController@create')->name('.translate.create');
    Route::post('/translate/save', 'Admin\Translate\TranslateToolsController@save')->name('.translate.save');
    Route::put('/translate/save/{id}', 'Admin\Translate\TranslateToolsController@save')->name('.translate.save.edit');
    Route::get('/translate/edit/{id}', 'Admin\Translate\TranslateToolsController@edit')->name('.translate.edit');
    Route::put('/translate/edit/{id}', 'Admin\Translate\TranslateToolsController@edit');
    Route::delete('/translate/delete/{id?}', 'Admin\Translate\TranslateToolsController@delete')->name('.translate.delete');
    Route::get('/translatedata', 'Admin\Translate\TranslateToolsController@getData')->name('.translate.getData');


    //invoices
    Route::get('/invoices', ['as' => '.invoices', 'uses' => 'Admin\Invoices\InvoicesController@index']);
    Route::get('/invoices/data', ['as' => '.invoices.data', 'uses' => 'Admin\Invoices\InvoicesController@getData']);

    // Tags
    Route::get('/tags', ['as' => '.tags.index', 'uses' => 'Admin\Tag\TagController@index']);
    Route::post('/tags/delete', ['as' => '.tags.delete', 'uses' => 'Admin\Tag\TagController@delete']);
    Route::post('/tags/create', ['as' => '.tags.create', 'uses' => 'Admin\Tag\TagController@create']);
    Route::post('/tags/update', ['as' => '.tags.update', 'uses' => 'Admin\Tag\TagController@update']);
});

/*
|--------------------------------------------------------------------------
| SUPER ADMIN
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'admin', 'as' => 'admin', 'middleware' => ['role:super-admin']], function () {
    Route::get('/users/{user?}/login', ['as' => '.users.login', 'uses' => 'Admin\User\AccountController@loginAs']);
});
