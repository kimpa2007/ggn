<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;
use Hash;

class LoginTest extends DuskTestCase
{
	
	//use DatabaseMigrations;
	
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_login_foobar()
    {
        dump('Test login with foo@bar.com');

        $this->browse(function (Browser $browser) {

            $user = User::where('email', 'foo@bar.com')->first();

            if(!$user) {
                $user = User::create([
                    'name' => 'Foo Bar',
                    'email' => 'foo@bar.com',
                    'password' => Hash::make('secret')
                ]);
            }
			
			/*
			$browser->loginAs($user)
          		->visit('/home');
			*/

            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->click('button[type="submit"]')
                ->waitForText('Home')
                ->pause(1000)
                ->assertPathIs('/admin/user/dashboard');
        });
    }
	
	public function test_logut_expert()
    {
        dump('Test logout');

        $this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();
			
			$browser->visit('/logout')
                ->assertPathIs('/');
			
        });
    }
}
