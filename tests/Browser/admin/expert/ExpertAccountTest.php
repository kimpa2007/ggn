<?php

namespace Tests\Browser\admin\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertAccountTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateExpert()
    {
    	dump('Admin :: Experts :: testCreateExpert');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'admin@bar.com')->first();

			$randNumber = rand(1,1000);

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->assertPathIs('/admin/dashboard')
				
          		->visit('/admin/experts')
			    ->click('.header a.btn-primary')
				->type('firstname',"Name ".$randNumber)
				->type('lastname',"Last name ".$randNumber)
				->select('zone_id')
				->type('email','test'.$randNumber.'@gmail.com')
				->type('password','1234')
				->type('password_confirmation','1234')
				->driver->executeScript('window.scrollTo(0, 500);');
				
			$browser->click('input[type="submit"]')
				->pause(1000)
				->assertVisible('.alert-success');
        });
		
    }
	
	public function testCreateExpertSameEmail()
    {
    	dump('Admin :: Experts :: testCreateSameEmailFailed');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'admin@bar.com')->first();

			$randNumber = rand(1,1000);

			$browser->visit('/admin/experts')
			    ->click('.header a.btn-primary')
				->type('firstname',"Name ".$randNumber)
				->type('lastname',"Last name ".$randNumber)
				->select('zone_id')
				->type('email','expert@bar.com')
				->type('password','1234')
				->type('password_confirmation','1234')
				->driver->executeScript('window.scrollTo(0, 500);');
				
			$browser->click('input[type="submit"]')
				->pause(1000)
				->assertVisible('.alert-danger')
				->type('firstname',"Name ".$randNumber)
				->type('lastname',"Last name ".$randNumber)
				->select('zone_id')
				->type('email','test'.$randNumber.'@gmail.com')
				->type('password','1234')
				->type('password_confirmation','1234')
				->driver->executeScript('window.scrollTo(0, 500);');
				
			$browser->click('input[type="submit"]')
				->pause(1000)
				->assertVisible('.alert-success');
        });
    }
	
	
	public function testUploadImage()
	{
		dump('Admin :: Experts :: testUploadImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->driver->executeScript("var fakeFileInput = window.$('<input/>').attr(".
					"{id: 'fakeFileInput', type:'file', name: 'fakeFileInput'}".
				").appendTo('body');");
						
			$browser->attach('fakeFileInput',base_path('tests/Browser/data/300.png'));
					
			$browser->driver->executeScript('var fileList = $("#fakeFileInput").prop("files");'.
					'var e = jQuery.Event(\'drop\', { dataTransfer : { files : fileList } });'.
    				'$(\'.dropzone\')[0].dropzone.listeners[0].events.drop(e);'.
					'window.scrollTo(0, 500);');
					
			$browser->pause(1000)
				->click('input[type="submit"]')
				->pause(1000)
                ->assertVisible('.alert-success')
				->assertInputValueIsNot('image','');
		});
	}
	
	public function testRemoveImage()
	{
		dump('Admin :: Experts :: testRemoveImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->click('#remove-picture');
			
			$browser->driver->executeScript('window.scrollTo(0, 500);');
					
			$browser->click('input[type="submit"]')
                ->pause(1000)
                ->assertVisible('.alert-success')
				->assertInputValue('image','');
			
		});
	}
	
	public function testRemoveExpert()
	{
		dump('Content :: testRemoveExpert');
		
		$this->browse(function (Browser $browser) {
			
			$browser->visit('/admin/experts')
				->waitFor('.remove-item')
				->click('.remove-item')
				->pause(1500)
				->whenAvailable('.bootbox',function($modal) use($browser){
					$modal->click('.btn-success');
					
					$browser->whenAvailable('.toast',function($modal) use($browser){
							$modal->assertSee('Succès');
						});
				});
		});
	}
	
}
