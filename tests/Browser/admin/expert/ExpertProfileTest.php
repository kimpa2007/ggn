<?php

namespace Tests\Browser\admin\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertProfileTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testEditBasicInformation()
    {
    	dump('Admin :: Experts :: Profile :: testEditBasicInformation');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'admin@bar.com')->first();

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->assertPathIs('/admin/dashboard')
				
          		->visit('/admin/experts')
				->waitFor('.view-item')
				->click('.view-item')
				->pause(1000)
				->click('.tabs-inline li:last-child a')
				->pause(1000)
				
			    ->keys('.cke_textarea_inline', 'Test text')
				//->type('email', 'expert2@bar.com')
				->pause(1000)
				
				->driver->executeScript('window.scrollTo(0, 500);');
				
            $browser->click('input[type="submit"]')
				->assertVisible('.alert-success')
                ->assertSeeIn('.cke_textarea_inline', 'Test text');
				//->assertInputValue('email', 'expert2@bar.com');
        });
		
    }
	
	
	public function testUploadImage()
	{
		dump('Admin :: Experts :: Profile :: testUploadImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->driver->executeScript("var fakeFileInput = window.$('<input/>').attr(".
					"{id: 'fakeFileInput', type:'file', name: 'fakeFileInput'}".
				").appendTo('body');");
						
			$browser->attach('fakeFileInput',base_path('tests/Browser/data/300.png'));
					
			$browser->driver->executeScript('var fileList = $("#fakeFileInput").prop("files");'.
					'var e = jQuery.Event(\'drop\', { dataTransfer : { files : fileList } });'.
    				'$(\'.dropzone\')[0].dropzone.listeners[0].events.drop(e);'.
					'window.scrollTo(0, 500);');
					
			$browser->pause(1000)
				->click('input[type="submit"]')
				->pause(1000)
                ->assertVisible('.alert-success')
				->assertInputValueIsNot('image','');
		});
	}
	
	public function testRemoveImage()
	{
		dump('Admin :: Experts :: Profile :: testRemoveImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->click('#remove-picture');
			
			$browser->driver->executeScript('window.scrollTo(0, 500);');
					
			$browser->click('input[type="submit"]')
                ->pause(1000)
                ->assertVisible('.alert-success')
				->assertInputValue('image','');
			
		});
	}
	
	
}
