<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertAccountTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testEditBasicInformation()
    {
    	dump('Account :: testEditBasicInformation');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->waitForText('Home')
				
          		->visit('/admin/expert/account')
			    ->type('firstname', 'Test Name')
				->type('lastname', 'Test LastName')
				//->type('email', 'expert2@bar.com')
				->pause(1000)
				
				->driver->executeScript('window.scrollTo(0, 500);');
				
            $browser->click('input[type="submit"]')
                ->assertPathIs('/admin/expert/account')
                ->assertInputValue('firstname', 'Test Name')
				->assertInputValue('lastname', 'Test LastName');
				//->assertInputValue('email', 'expert2@bar.com');
        });
		
    }
	
	public function testEditPassword()
    {
    	dump('Account :: editPassword');
		
		$this->browse(function (Browser $browser) {

			$browser->type('password', 'secret 2')
				->type('confirm_password', 'secret')
				->click('input[type="submit"]')
				->assertSee("Votre mot de passe n'est pas correctement confirmé");
				
			$browser->type('password', 'secret')
				->type('confirm_password', 'secret')
				->click('input[type="submit"]')
				->assertSee("Votre compte vient d'être mis à jours");
				
				//->assertInputValue('email', 'expert2@bar.com');
        });
		
    }
	
	/*
	 * TODO
	public function testAddPhoto()
    {
    	dump('addPhoto');
		
    }
	
	public function testRemovePhoto()
    {
    	dump('removePhoto');
		
    }
	*/
}
