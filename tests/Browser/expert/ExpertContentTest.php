<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertContentTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateContent()
    {
    	dump('Content :: testCreateContent');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();
			
			$rand = rand(0,1000);

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->waitForText('Home')
				
          		->visit('/admin/expert/contents')
				->click('a.btn-primary')
				->type('title','Content '.$rand)
				->waitFor('.hierarchy-select .dropdown-toggle')
				->click('.hierarchy-select .dropdown-toggle')
				->pause(1000)
				->waitFor('a.select-option')
				->click('a.select-option')
				->pause(2000)
				->keys('.cke_textarea_inline','Description content '.$rand)
				->select('type','LINK')
				->type('content[LINK]','http://www.google.com')
				->driver->executeScript('window.scrollTo(0, 1500);');
				
			$browser->click('input[type="submit"]')
				->assertVisible('.alert-success');
				
        });
		
    }


	public function testUploadImage()
	{
		dump('Content :: testUploadImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->driver->executeScript("var fakeFileInput = window.$('<input/>').attr(".
					"{id: 'fakeFileInput', type:'file', name: 'fakeFileInput'}".
				").appendTo('body');");
						
			$browser->attach('fakeFileInput',__DIR__.'/data/300.png');
					
			$browser->driver->executeScript('var fileList = $("#fakeFileInput").prop("files");'.
					'var e = jQuery.Event(\'drop\', { dataTransfer : { files : fileList } });'.
    				'$(\'.dropzone\')[0].dropzone.listeners[0].events.drop(e);'.
					'window.scrollTo(0, 500);');
					
			$browser->pause(1000)
				->click('input[type="submit"]')
                ->assertSee('Enregistrement effectué avec succès')
				->assertInputValueIsNot('image','');
			
		});
	}
	
	public function testRemoveImage()
	{
		dump('Content :: testRemoveImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->click('#remove-picture');
			
			$browser->driver->executeScript('window.scrollTo(0, 500);');
					
			$browser->click('input[type="submit"]')
                ->assertSee('Enregistrement effectué avec succès')
				->assertInputValue('image','');
			
		});
	}
	
	public function testPublish()
	{
		dump('Content :: testPublish');
		
		$this->browse(function (Browser $browser) {
			
			$browser->click('.btn-status')
				->pause(1000)
				->click('ul.dropdown-menu a.status-PUBLISHED')
				->whenAvailable('.toast',function($modal) use($browser){
					$modal->assertSee('Sauvegardé');
					
					
					$browser->click('.btn-status')
					->pause(1000)
					->click('ul.dropdown-menu a.status-DRAFT')
					->whenAvailable('.toast',function($modal) use($browser){
						$modal->assertSee('Sauvegardé');
						
					});
					
				});
		});
	}
}
