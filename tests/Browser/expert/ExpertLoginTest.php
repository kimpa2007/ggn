<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertLoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLoginExpert()
    {
    	
		dump('Expert login');
		
        $this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();

			$browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->click('button[type="submit"]')
                ->waitForText('Home')
                ->assertPathIs('/admin/expert/dashboard');
        });
    }
}
