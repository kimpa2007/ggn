<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertOrderTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testEditOrder()
    {
    	
		dump('Orders :: testEditOrder');
		
        $this->browse(function (Browser $browser) {

			$user = User::where('email', 'expert@bar.com')->first();

			$rand = rand(0,1000);

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->waitForText('Home')
				
          		->visit('/admin/expert/orders/list/pending')
				->waitFor('.open-modal')
				->click('.open-modal')
                ->whenAvailable('.modal',function($modal) use ($browser){
                	$modal
                		->select('status','ENGAGED')
                		->type('comment','Test text')
						->click('button[type="submit"]');
						
						$browser->whenAvailable('.toast',function($modal) {
							$modal->assertSee('Sauvegardé');
						});
                });
        });
    }
	
	public function testEngagedOrder()
    {
    	
		dump('Orders :: testEngagedOrder');
		
        $this->browse(function (Browser $browser) {


			$browser->visit('/admin/expert/orders/list/engaged')
				->waitFor('.open-modal')
				->click('.open-modal')
                ->whenAvailable('.modal',function($modal) use ($browser){
                	$modal
                		->select('status','DONE')
                		->type('comment','Test text 2')
						->click('button[type="submit"]');
						
						$browser->whenAvailable('.toast',function($modal) {
							$modal->assertSee('Sauvegardé');
						});
                });
        });
    }
	
	public function testDoneOrder()
    {
    	
		dump('Orders :: testEngagedOrder');
		
        $this->browse(function (Browser $browser) {


			$browser->visit('/admin/expert/orders/list/history')
				->waitFor('.open-modal')
				->click('.open-modal')
                ->whenAvailable('.modal',function($modal) use ($browser){
                	$modal
                		->select('status','ABANDONED')
                		->type('comment','Test text 3')
						->click('button[type="submit"]');
						
						$browser->whenAvailable('.toast',function($modal) use ($browser) {
							$modal->assertSee('Sauvegardé');
							
							$browser->waitFor('.status-ABANDONED')
								->assertVisible('.status-ABANDONED');
						});
                });
        });
    }
}
