<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertProductsTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateProduct()
    {
    	dump('Product :: testCreateProduct');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();

			$rand = rand(0,1000);

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->waitForText('Home')
				
          		->visit('/admin/expert/products/create')
			    ->type('name', 'Product Name '.$rand)
				->select('zone_id')
				->type('time',100)
				->type('price',20)
				->keys('.description .cke_textarea_inline', 'Test text 1')
				->keys('.content .cke_textarea_inline', 'Test text 2')
				->driver->executeScript('window.scrollTo(0, 500);');
				
            $browser->click('input[type="submit"]')
                ->assertSee('Enregistrement effectué avec succès')
				->assertInputValue('name', 'Product Name '.$rand)
				->assertInputValue('time',100)
				->assertInputValue('price',20)
                ->assertSeeIn('.description .cke_textarea_inline', 'Test text 1')
				->assertSeeIn('.content .cke_textarea_inline', 'Test text 2');
        });
		
    }
	
	public function testUploadImage()
	{
		dump('Product :: testUploadImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->driver->executeScript("var fakeFileInput = window.$('<input/>').attr(".
					"{id: 'fakeFileInput', type:'file', name: 'fakeFileInput'}".
				").appendTo('body');");
						
			$browser->attach('fakeFileInput',__DIR__.'/data/300.png');
					
			$browser->driver->executeScript('var fileList = $("#fakeFileInput").prop("files");'.
					'var e = jQuery.Event(\'drop\', { dataTransfer : { files : fileList } });'.
    				'$(\'.dropzone\')[0].dropzone.listeners[0].events.drop(e);'.
					'window.scrollTo(0, 500);');
					
			$browser->pause(1000)
				->click('input[type="submit"]')
                ->assertSee('Enregistrement effectué avec succès')
				->assertInputValueIsNot('image','');
			
		});
	}
	
	public function testRemoveImage()
	{
		dump('Product :: testRemoveImage');
		
		$this->browse(function (Browser $browser) {
			
			$browser->click('#remove-picture');
			
			$browser->driver->executeScript('window.scrollTo(0, 500);');
					
			$browser->click('input[type="submit"]')
                ->assertSee('Enregistrement effectué avec succès')
				->assertInputValue('image','');
			
		});
	}
	
	public function testPublish()
	{
		dump('Product :: testPublish');
		
		$this->browse(function (Browser $browser) {
			
			$browser->click('.btn-status')
				->pause(1000)
				->click('ul.dropdown-menu a.status-PUBLISHED')
				->whenAvailable('.toast',function($modal) use($browser){
					$modal->assertSee('Sauvegardé');
					
					
					$browser->click('.btn-status')
					->pause(1000)
					->click('ul.dropdown-menu a.status-DRAFT')
					->whenAvailable('.toast',function($modal) use($browser){
						$modal->assertSee('Sauvegardé');
						
					});
					
				});
		});
	}
	
}
