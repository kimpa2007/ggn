<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertProfileTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testEditBasicInformation()
    {
    	dump('Profile :: testEditBasicInformation');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->waitForText('Home')
				
          		->visit('/admin/expert/profile')
			    ->keys('.cke_textarea_inline', 'Test text')
				//->type('email', 'expert2@bar.com')
				->pause(1000)
				
				->driver->executeScript('window.scrollTo(0, 500);');
				
            $browser->click('input[type="submit"]')
                ->assertPathIs('/admin/expert/profile')
                ->assertSeeIn('.cke_textarea_inline', 'Test text');
				//->assertInputValue('email', 'expert2@bar.com');
        });
		
    }
	
	/*
	 * TODO
	public function testAddPhoto()
    {
    	dump('addPhoto');
		
    }
	
	public function testRemovePhoto()
    {
    	dump('removePhoto');
		
    }
	*/
}
