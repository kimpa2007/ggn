<?php

namespace Tests\Browser\expert;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class ExpertThreadTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSelectSender()
    {
    	dump('Thread :: testSelectSender');
		
		$this->browse(function (Browser $browser) {

            $user = User::where('email', 'expert@bar.com')->first();

			$browser->visit('/login')
				->type('email', $user->email)
                ->type('password', 'secret')
				->click('button[type="submit"]')
                ->waitForText('Home')
				
          		->visit('/admin/expert/threads')
				->pause(1000)
				->driver->executeScript(
					"$('select[name=sender_id]').val(2);"
					."$('.selectpicker').selectpicker('refresh');"
					."$('#filter-sender').submit()");
					
			$browser->assertQueryStringHas('sender_id', 2);
				
				/*
				->click('.bootstrap-select .dropdown-toggle')
				->pause(1000)
			    ->click('.bootstrap-select .dropdown-menu li:nth-child(2) a')
				->pause(3000)
				->assertQueryStringHas('sender_id', 3);
				 */
                
        });
		
    }
	
	public function testAddThread()
    {
    	dump('Thread :: testAddThread');
		
		$this->browse(function (Browser $browser) {

		   $randValue = rand(0,1000);

           $browser->visit('/admin/expert/threads/1')
				->type('message','Test message '.$randValue)
				->driver->executeScript('window.scrollTo(0, 1500);');
				
		   $browser->click('input[type="submit"]')
				->assertSee('Test message '.$randValue);
                
        });
		
    }
	
	public function testAddFile()
    {
    	dump('Thread :: testAddFile');
		
		$this->browse(function (Browser $browser) {

		   $randValue = rand(0,1000);

           $browser->visit('/admin/expert/threads/1')
				->type('message','Test message '.$randValue.' '.__DIR__.'/data/300.png')
				->driver->executeScript('window.scrollTo(0, 1500);'.
					'$( "input[name=\'file\']" ).css({display:"block"})');
				
				
		   $browser
		   		->attach('file',__DIR__.'/data/300.png')
		   		->click('input[type="submit"]')
				->assertSee("Votre message vient d'être envoyé");
        });
		
    }
	
	public function testRemoveThread()
    {
    	dump('Thread :: testRemoveThread');
		
		$this->browse(function (Browser $browser) {

		   $browser->click('.remove-thread-toogle')
		   		->whenAvailable('.bootbox.modal', function($modal) use ($browser){
		   			$modal->click('.btn-default')
						->pause(1000);
					
					$browser->assertDontSee("Votre message vient d'être supprimé.");
						
					$browser->click('.remove-thread-toogle')
				   		->whenAvailable('.bootbox.modal', function($modal) use ($browser){
				   			$modal->click('.btn-primary')
								->pause(1000);
							
							$browser->assertSee("Votre message vient d'être supprimé.");	
				   		});	
							
		   		});
				
				
        });
		
    }
	
	/*
	 * TODO
	public function testEditThread()
    {
    	dump('addPhoto');
		
    }
	
	*/
}
