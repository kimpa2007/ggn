<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Thread;
use App\Models\User;

class ThreadTest extends TestCase
{

    public function test_send_message_to_user()
    {
        dump("Test send message to another user");

        factory(User::class, 2)->create();

        $sender = User::find(1);
        $recipient = User::find(2);

        $t = Thread::open(
            "Hello from sender :)",
            $recipient,
            $sender
        );

        $this->assertEquals($t->sender_id, $sender->id);
        $this->assertEquals($t->recipient_id, $recipient->id);
    }

    public function test_reply_message_to_user()
    {
        dump("Test reply message");

        factory(User::class, 2)->create();

        $sender = User::find(1);
        $recipient = User::find(2);

        $t = Thread::open(
            "Hello from sender :)",
            $recipient,
            $sender
        );

        $t2 = $t->reply(
            "Hello from recipient :)",
            $recipient
        );

        $this->assertEquals($t->sender_id, $sender->id);
        $this->assertEquals($t->recipient_id, $recipient->id);
        $this->assertEquals($t2->message, "Hello from recipient :)");
    }
}
